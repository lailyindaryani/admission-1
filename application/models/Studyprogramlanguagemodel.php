<?php 
class StudyProgramLanguageModel extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');

    }

    function GetProgramDegreeByIdLanguage($languagedeliveryId = 0)
    {
      $sql = "SELECT a.*, b.* FROM studyprogramlanguage a JOIN studyprogram b ON (a.STUDYPROGRAMID = b.STUDYPROGRAMID) WHERE a.LANGUAGEDELIVERYMAPPINGID = ? ";
        $result = $this->db->query($sql, array($languagedeliveryId));
        return $result->row_array();  
    }
	
	 function GetLanguageDeliveryMappingByStudyPogramId($studyprogramId = 0)
    {
      $sql = "SELECT * FROM studyprogramlanguage WHERE STUDYPROGRAMID = ? ";
        $result = $this->db->query($sql, array($studyprogramId));
        return $result->row_array();  
    }

}
?>
