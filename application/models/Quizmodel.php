<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 12/02/2018
 * Time: 13:39
 */

class Quizmodel extends CI_Model {

    private $table_name = "questionanswer";
    private $table_participant_quiz = "quizparticipant";

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function getQuestionList(){
        $sql = "SELECT * FROM `questionaire` ";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function getAnswerList($questionid = null){
        $where = !empty($questionid) ? " where A.QUESTIONID = '$questionid'" : '';
        $sql = "SELECT
                OPTIONID, OPTIONVALUE
            FROM
                `questionoption` AS A
            JOIN `questionaire` AS B ON B.`QUESTIONID` = A.`QUESTIONID` $where ";
       // print $sql;die();
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function insert($data)
    {
        return $this->db->insert($this->table_name, $data);
    }

    function insertQuizParticipant($data)
    {
        return $this->db->insert($this->table_participant_quiz, $data);
    }

    function getParticipantId($userid){
        $where = !empty($userid) ? " WHERE USERID = '$userid'" : '';
        $sql = "SELECT PARTICIPANTID, PASSPORTNO  FROM `participants` $where ";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
	 function getAnswer($questionid,$enrollmentId){
        $sql = "SELECT 
				A.QUESTIONID,
				A.QUESTION,
				A.OPTIONID,
				A.OPTIONVALUE,
				COUNT(B.PARTICIPANTID) AS JUMLAH
			FROM 
				(SELECT
				A.QUESTIONID,
				A.QUESTION,
				B.OPTIONID,
				B.OPTIONVALUE
			FROM questionaire A
				RIGHT JOIN questionoption B ON (A.QUESTIONID = B.QUESTIONID)) A
				LEFT JOIN (SELECT A.OPTIONID, A.QUESTIONID, A.PARTICIPANTID , C.ENROLLMENTID FROM questionanswer A
				JOIN participants C ON (A.PARTICIPANTID = C.PARTICIPANTID) WHERE ENROLLMENTID =$enrollmentId
			)B ON (A.QUESTIONID = B.QUESTIONID AND A.OPTIONID = B.OPTIONID)
			WHERE A.QUESTIONID = $questionid
			GROUP BY A.QUESTIONID,
				A.QUESTION,
				A.OPTIONID,
				A.OPTIONVALUE";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function getMinQuiestionId(){
		$sql ="SELECT QUESTIONID
		FROM questionaire";
		$result = $this->db->query($sql);
        return $result->result_array();
	}
	

}