<?php 
class Loginmodel extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function Login($username,$password){
        if($username=="" || $password=="")
            return null;

        $username=xss_clean($username);
        $password=md5($password);

        $sql = "SELECT * FROM users WHERE EMAIL = ? AND PASSWORD = ? AND ACTIVESTATUS = 'Y'";
        $result = $this->db->query($sql, array($username, $password));
        //echo $this->db->last_query();
        return $result->result_array();
    }
	
	function CekAktivasiUser($username='')
    {
		$username=xss_clean($username);
        $sql = "SELECT * FROM users WHERE EMAIL = ?";
        $result = $this->db->query($sql, array($username));
        return $result->result_array();
    }
	
	function LoginasStaff($userid){
        if($userid=="")
            return null;
        $sql = "SELECT * FROM users WHERE USERID = ? AND ACTIVESTATUS = 'Y'";
        $result = $this->db->query($sql, array($userid));
        //echo $this->db->last_query();
        return $result->result_array();
    }
	
	
	
	
    

}
?>