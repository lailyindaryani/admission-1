<?php 
class FeeModel extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetFeeByDegree($degree = '', $program)
    {   
        if($program!=''){
            $sql = "SELECT * FROM fee WHERE DEGREE = ? AND PROGRAM = ?";
            $result = $this->db->query($sql, array($degree, $program));
        }else{
            $sql = "SELECT * FROM fee WHERE DEGREE = ?";
            $result = $this->db->query($sql, array($degree));
        }       
        
        return $result->row_array();
    }

}
?>