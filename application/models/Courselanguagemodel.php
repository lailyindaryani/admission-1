<?php 
class CourseLanguageModel extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetProgramDegreeByIdLanguage($languagedeliveryId = 0)
    {
      $sql = "SELECT a.*, b.* FROM COURSELANGUAGE a JOIN COURSE b ON (a.COURSEID = b.COURSEID) WHERE a.LANGUAGEDELIVERYMAPPINGID = ? ";
        $result = $this->db->query($sql, array($languagedeliveryId));
        return $result->row_array();  
    }
	
	 function GetLanguageDeliveryMappingByCourseId($courseId = 0)
    {
      $sql = "SELECT * FROM courselanguage WHERE COURSEID = ? ";
        $result = $this->db->query($sql, array($courseId));
        return $result->row_array();  
    }

}
?>