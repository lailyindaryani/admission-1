<?php 
class Usermodel extends CI_Model {

    private $table_name = "users";
    private $primary_key = "USERID";

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function Register($email, $fullname, $password, $confirmationKey)
    {
        $create_data = array(
                'EMAIL' => $email,
                'FULLNAME'  => $fullname,
                'PASSWORD'  => md5($password),
                'CONFIRMATIONKEY' => $confirmationKey,
                'ACTIVESTATUS' => 'N',
                'INPUTDATE' => date('Y-m-d H:i:s'),
                'USERGROUPID' => 2
            );

        return $this->db->insert($this->table_name, $create_data);
    }

    function Login($username,$password){
        if($username=="" || $password=="")
            return null;

        $username=xss_clean($username);
        $password=md5($password);

        $sql = "SELECT * FROM users WHERE EMAIL = ? AND PASSWORD = ? AND ACTIVESTATUS = 'Y'";
        $result = $this->db->query($sql, array($username, $password));
        //echo $this->db->last_query();
        return $result->result_array();
    }

    function insert($data)
    {
        return $this->db->insert($this->table_name, $data);
    }

    function update($user_id = 0, $data = array())
    {
        $this->db->where($this->primary_key, $user_id);
        return $this->db->update($this->table_name, $data);
    }

    function GetUserByConfirmationKey($id, $key)
    {
        $sql = "SELECT * FROM users WHERE USERID = ? AND CONFIRMATIONKEY = ?";
        $result = $this->db->query($sql, array($id, $key));
        return $result->result_array();
    }
    function getAllData($userid = null){
        $where = !empty($userid) ? " where userid = '$userid'" : '';
        $sql = "SELECT * FROM users $where ";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function cek_user($username)
    {
        $query = $this->db->query("select * from users where EMAIL = '$username'");       
        if($query->num_rows()==1){
            return "ADA";
        }else{
			return "TIDAK ADA";
        }
    }
}
?>