<?php 
class CountryModel extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function GetCountryById($countryId = 0)
    {
        $sql = "SELECT * FROM country WHERE COUNTRYID = ?";
        $result = $this->db->query($sql, array($countryId));
        return $result->row_array();
    }

    function GetAllCountry()
    {
        $sql = "SELECT * FROM country";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function GetCountryByName($countryName = '')
    {
        $sql = "SELECT * FROM country WHERE COUNTRYNAMEENG = ?";
        $result = $this->db->query($sql, array($countryName));
        return $result->row_array();
    }
	
	function getCountry(){
        $sql = "SELECT
  B.NATIONALITY,
  COUNT(B.NATIONALITY) AS JUMLAH
FROM
participants B
GROUP BY B.NATIONALITY";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
	
	function getCountryByStatus($acceptanceStatus, $academicyear, $period){
		 $sql = "SELECT
  B.NATIONALITY,
  COUNT(B.NATIONALITY) AS JUMLAH
FROM
participants B
LEFT JOIN enrollment E ON (B.ENROLLMENTID=E.ENROLLMENTID)
        ";
		if($acceptanceStatus!='' && $academicyear!='' && $academicyear!='UNDEFINED YEAR' && $period==''){
			$sql .= " 
				WHERE B.ACCEPTANCESTATUS = '".$acceptanceStatus."' AND E.ACADEMICYEAR = ".$academicyear."
				";
		}else if($acceptanceStatus!='' && $academicyear=='' && $period==''){
			$sql .= " 
				WHERE B.ACCEPTANCESTATUS = '".$acceptanceStatus."'
				";
		}else if($acceptanceStatus=='' && $academicyear!='' && $academicyear!='UNDEFINED YEAR' && $period==''){
			$sql .= " 
				WHERE E.ACADEMICYEAR = ".$academicyear."
				";
		}else if($acceptanceStatus!='' && $academicyear=='UNDEFINED YEAR' && $period==''){
			$sql .= " 
				WHERE B.ACCEPTANCESTATUS = '".$acceptanceStatus."' AND E.ACADEMICYEAR IS NULL
				";
		}else if($acceptanceStatus=='' && $academicyear=='UNDEFINED YEAR' && $period==''){
			$sql .= " 
				WHERE E.ACADEMICYEAR IS NULL";
		}else 
			if($acceptanceStatus!='' && $academicyear!='' && $academicyear!='UNDEFINED YEAR' && $period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " 
				WHERE B.ACCEPTANCESTATUS = '".$acceptanceStatus."' AND E.ACADEMICYEAR = ".$academicyear."
				AND (".$period.")
				";
		}else if($acceptanceStatus!='' && $academicyear=='' && $period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " 
				WHERE B.ACCEPTANCESTATUS = '".$acceptanceStatus."' AND (".$period.")
				";
		}else if($acceptanceStatus=='' && $academicyear!='' && $academicyear!='UNDEFINED YEAR' && $period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " 
				WHERE E.ACADEMICYEAR = ".$academicyear." AND (".$period.")
				";
		}else if($acceptanceStatus!='' && $academicyear=='UNDEFINED YEAR' && $period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " 
				WHERE B.ACCEPTANCESTATUS = '".$acceptanceStatus."' AND E.ACADEMICYEAR IS NULL AND (".$period.")
				";
		}else if($acceptanceStatus=='' && $academicyear=='UNDEFINED YEAR' && $period!='' && $period!="UNDEFINED YEAR"){
			$sql .= " 
				WHERE E.ACADEMICYEAR IS NULL AND (".$period.")";
		}else 
			if($acceptanceStatus!='' && $academicyear!='' && $academicyear!='UNDEFINED YEAR' && $period=="UNDEFINED YEAR"){
			$sql .= " 
				WHERE B.ACCEPTANCESTATUS = '".$acceptanceStatus."' AND E.ACADEMICYEAR = ".$academicyear."
				AND (E.PERIOD IS NULL)
				";
		}else if($acceptanceStatus!='' && $academicyear=='' && $period=="UNDEFINED YEAR"){
			$sql .= " 
				WHERE B.ACCEPTANCESTATUS = '".$acceptanceStatus."' AND (E.PERIOD IS NULL))
				";
		}else if($acceptanceStatus=='' && $academicyear!='' && $academicyear!='UNDEFINED YEAR' && $period=="UNDEFINED YEAR"){
			$sql .= " 
				WHERE E.ACADEMICYEAR = ".$academicyear." AND (E.PERIOD IS NULL)
				";
		}else if($acceptanceStatus!='' && $academicyear=='UNDEFINED YEAR' && $period=="UNDEFINED YEAR"){
			$sql .= " 
				WHERE B.ACCEPTANCESTATUS = '".$acceptanceStatus."' AND E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL)
				";
		}else if($acceptanceStatus=='' && $academicyear=='UNDEFINED YEAR' && $period=="UNDEFINED YEAR"){
			$sql .= " 
				WHERE E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL)";
		}else if($acceptanceStatus=='' && $academicyear=='' && $period=="UNDEFINED YEAR"){
			$sql .= " 
				WHERE (E.PERIOD IS NULL)";
		}else if($acceptanceStatus=='' && $academicyear=='' && $period!="UNDEFINED YEAR" && $period!=''){
			$sql .= " 
				WHERE (".$period.")";
		}
		
		$sql .= " GROUP BY B.NATIONALITY";
		//echo "<pre>"; print($sql);
        $result = $this->db->query($sql);
        return $result->result_array();
	}
	
	function getNumberCountry($academicyear,$acceptanceStatus, $period){
		if($academicyear!='' && $acceptanceStatus!='' && $academicyear!='UNDEFINED YEAR' && $period==''){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) WHERE E.ACADEMICYEAR =".$academicyear." AND 
  P.ACCEPTANCESTATUS='".$acceptanceStatus."' ) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($academicyear!='' && $acceptanceStatus=='' && $academicyear!='UNDEFINED YEAR' && $period=='' ){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) WHERE E.ACADEMICYEAR =".$academicyear.") AS A WHERE NATIONALITY IS NOT NULL GROUP BY NATIONALITY) AS B";
		}else if($academicyear=='' && $acceptanceStatus!='' && $period==''){
            $sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE P.ACCEPTANCESTATUS='".$acceptanceStatus."' ) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($acceptanceStatus!='' && $academicyear=='UNDEFINED YEAR' && $period==''){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE E.ACADEMICYEAR IS NULL AND P.ACCEPTANCESTATUS='".$acceptanceStatus."') AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($acceptanceStatus=='' && $academicyear=='UNDEFINED YEAR' && $period==''){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE E.ACADEMICYEAR IS NULL ) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}
		else if($academicyear!='' && $acceptanceStatus!='' && $academicyear!='UNDEFINED YEAR' && $period!='' && $period!="UNDEFINED YEAR"){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) WHERE E.ACADEMICYEAR =".$academicyear." AND 
  P.ACCEPTANCESTATUS='".$acceptanceStatus."' AND (".$period.")) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($academicyear!='' && $acceptanceStatus=='' && $academicyear!='UNDEFINED YEAR' && $period!='' && $period!="UNDEFINED YEAR"){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) WHERE E.ACADEMICYEAR =".$academicyear."
  AND (".$period.")) AS A WHERE NATIONALITY IS NOT NULL GROUP BY NATIONALITY) AS B";
		}else if($academicyear=='' && $acceptanceStatus!='' && $period!='' && $period!="UNDEFINED YEAR"){
            $sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE P.ACCEPTANCESTATUS='".$acceptanceStatus."' AND (".$period.")) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($acceptanceStatus!='' && $academicyear=='UNDEFINED YEAR' && $period!='' && $period!="UNDEFINED YEAR"){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE E.ACADEMICYEAR IS NULL AND P.ACCEPTANCESTATUS='".$acceptanceStatus."' AND (".$period.")) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($acceptanceStatus=='' && $academicyear=='UNDEFINED YEAR' && $period!='' && $period!="UNDEFINED YEAR"){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE E.ACADEMICYEAR IS NULL AND (".$period.")) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else 
		if($academicyear!='' && $acceptanceStatus!='' && $academicyear!='UNDEFINED YEAR' && $period=="UNDEFINED YEAR"){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) WHERE E.ACADEMICYEAR =".$academicyear." AND 
  P.ACCEPTANCESTATUS='".$acceptanceStatus."' AND (E.PERIOD IS NULL)) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($academicyear!='' && $acceptanceStatus=='' && $academicyear!='UNDEFINED YEAR' && $period=="UNDEFINED YEAR"){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) WHERE E.ACADEMICYEAR =".$academicyear."
  AND (E.PERIOD IS NULL)) AS A WHERE NATIONALITY IS NOT NULL GROUP BY NATIONALITY) AS B";
		}else if($academicyear=='' && $acceptanceStatus!='' && $period=="UNDEFINED YEAR"){
             $sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE P.ACCEPTANCESTATUS='".$acceptanceStatus."' AND (E.PERIOD IS NULL)) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($acceptanceStatus!='' && $academicyear=='UNDEFINED YEAR' && $period=="UNDEFINED YEAR"){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE E.ACADEMICYEAR IS NULL AND P.ACCEPTANCESTATUS='".$acceptanceStatus."' AND (E.PERIOD IS NULL)) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($acceptanceStatus=='' && $academicyear=='UNDEFINED YEAR' && $period=="UNDEFINED YEAR"){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL)) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($acceptanceStatus=='' && $academicyear=='' && $period=="UNDEFINED YEAR"){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE (E.PERIOD IS NULL)) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else if($acceptanceStatus=='' && $academicyear=='' && $period!="UNDEFINED YEAR" && $period!=''){
			$sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT P.NATIONALITY FROM participants P JOIN enrollment E ON (P.ENROLLMENTID = E.ENROLLMENTID) 
  WHERE (".$period.")) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}else{
            $sql= "SELECT COUNT(JML) AS JUMLAH FROM (SELECT DISTINCT NATIONALITY, SUM(1) AS JML
  FROM (SELECT DISTINCT NATIONALITY FROM participants) AS A WHERE NATIONALITY IS NOT NULL
GROUP BY NATIONALITY) AS B";
		}		
		//echo "<pre>"; print($sql);
		$result = $this->db->query($sql);
        return $result->result_array();	
	}
	
	function getCountryExcept($countryid){
		$sql = "SELECT * FROM country WHERE COUNTRYID !=".$countryid;
        $result = $this->db->query($sql);
        return $result->result_array();
	}

}
?>