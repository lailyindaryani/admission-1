<?php 
class FamilyModel extends CI_Model {

    private $table_name = "family";
    private $primary_key = "FAMILYID";
    private $foreign_key = array(
            'USERS'     => 'USERID'
        );

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }

    function getFamilyOfUser($id = 0)
    {
        $this->db->select('*');
        $this->db->where('PARTICIPANTID',$id);
        return $this->db->get('family')->result_array();
    }

    function GetAllFamily()
    {
        $sql = "SELECT * FROM degree";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function insert($data)
    {
        return $this->db->insert($this->table_name, $data);
    }

    function update($participant_id = 0, $data = array())
    {
        $this->db->where($this->primary_key, $participant_id);
        return $this->db->update($this->table_name, $data);
    }

    function exists($participant_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE USERID = ?";
        $result = $this->db->query($sql, array($participant_id));

        return $result->result_array();
    }

    function fatherData($participant_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name JOIN country USING (COUNTRYID) WHERE PARTICIPANTID = ? AND RELATIONSHIP = 'FATHER'";
        $result = $this->db->query($sql, array($participant_id));

        return $result->row_array();
    }

    function motherData($participant_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name JOIN country USING (COUNTRYID) WHERE PARTICIPANTID = ?  AND RELATIONSHIP = 'MOTHER'";
        $result = $this->db->query($sql, array($participant_id));

        return $result->row_array();
    }

    function getFamilyDetail($family_id = 0)
    {
        $sql = "SELECT * FROM $this->table_name WHERE FAMILYID = ?";
        $result = $this->db->query($sql, array($family_id));

        return $result->row_array();
    }
}
?>
