<?php


	function curPageURL() {
		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on"){
			$pageURL .= "s";
		}
		$pageURL .= "://";
		$serverName = $_SERVER["HTTP_HOST"];
		if($serverName=="10.14.203.36"){
			$serverName = "academic.ittelkom.ac.id";
		}		
		if ($_SERVER["SERVER_PORT"] != "80"){
			$pageURL .= $serverName.":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		}else{
			$pageURL .= $serverName.$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
	
	function MonthName($i) {
		if($i==1) return "january";
		else if($i==2) return "february";
		else if($i==3) return "march";
		else if($i==4) return "april";
		else if($i==5) return "may";
		else if($i==6) return "june";
		else if($i==7) return "july";
		else if($i==8) return "august";
		else if($i==9) return "september";
		else if($i==10) return "october";
		else if($i==11) return "november";
		else return "december";
	}
	
	function NamaHari($i) {
		if($i==1) return "senin";
		else if($i==2) return "selasa";
		else if($i==3) return "rabu";
		else if($i==4) return "kamis";
		else if($i==5) return "jumat";
		else if($i==6) return "sabtu";
		else return "minggu";
	}
	
	function NewNamaHari($i) {
		if($i==2) return "senin";
		else if($i==3) return "selasa";
		else if($i==4) return "rabu";
		else if($i==5) return "kamis";
		else if($i==6) return "jumat";
		else if($i==7) return "sabtu";
		else return "minggu";
	}
	
	
	function GetIPAddressClient(){
			if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
			{
			  $ip=$_SERVER['HTTP_CLIENT_IP'];
			}
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
			{
			  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			else
			{
			  $ip=$_SERVER['REMOTE_ADDR'];
			}
			return $ip;
	}
	
	function StrToDb($words, $style="") {
		$words = @str_replace('\'','\'\'',$words);
        $words= @str_replace('=',' ',$words);
        $words= @str_replace('%',' ',$words);
        $words= @str_replace('sleep',' ',$words);
		if($style=='ucwords') $words = @ucwords($words);
		else if($style=='ucfirst') $words = @ucfirst($words);
		else if($style=='upper') $words = @strtoupper($words);
		else if($style=='lower') $words = @strtolower($words);


		return @trim($words);
	}

	function DbToStr($words, $style='', $str = '') {
		$words = @htmlentities($words);
		if($style=='ucwords') $words = @ucwords(@strtolower($words));
		else if($style=='ucfirst') $words = @ucfirst(@strtolower($words));
		else if($style=='upper') $words = @strtoupper($words);
		else if($style=='lower') $words = @strtolower($words);
		return (@trim($words)!=null?@trim($words):($str!=null?$str:""));
	}
	
	function GeneratePassword($length=6,$level=2) {
		list($usec, $sec) = explode(' ', microtime());
		@srand((float) $sec + ((float) $usec * 100000));
		$validchars[1] = "0123456789abcdfghjkmnpqrstvwxyz";
		$validchars[2] = "0123456789abcdfghjkmnpqrstvwxyz";
		$password = "";
		$counter = 0;
		while ($counter < $length) {
			$actChar = substr($validchars[$level], rand(0, strlen($validchars[$level])-1), 1);
			// All character must be different
			if (!strstr($password, $actChar)) {
				$password .= $actChar;
				$counter++;
			}
		}		
		return $password;		
	}
	
	function GetIndexConversion($index) {
		switch(@strtoupper($index)) {
			case 'A' : return 4; break;
			case 'B' : return 3; break;
			case 'C' : return 2; break;
			case 'D' : return 1; break;
			case 'E' : return 0; break;
			default : return 0;
		}
	}
	
	function OptionTarget($selected='') {
		if(@strtolower($selected)=="_blank") {
			$selected1 = "selected";
			$selected2 = "";
			$selected3 = "";
			$selected4 = "";
		}
		else if(@strtolower($selected)=="_self") {
			$selected1 = "";
			$selected2 = "selected";
			$selected3 = "";
			$selected4 = "";
		}
		else if(@strtolower($selected)=="_parent") {
			$selected1 = "";
			$selected2 = "";
			$selected3 = "selected";
			$selected4 = "";
		}
		else if(@strtolower($selected)=="_top") {
			$selected1 = "";
			$selected2 = "";
			$selected3 = "";
			$selected4 = "selected";
		}
		echo "<option $selected1 value='_blank'>_blank</option>";
		echo "<option $selected2 value='_self'>_self</option>";
		echo "<option $selected3 value='_parent'>_parent</option>";
		echo "<option $selected4 value='_top'>_top</option>";
	}
	
	function OptionGrade($selected='') {
		if(@strtoupper($selected)=="A") {
			$selectedA = "selected";
			$selectedB = "";
			$selectedC = "";
			$selectedD = "";
			$selectedE = "";
		}
		else if(@strtoupper($selected)=="B") {
			$selectedA = "";
			$selectedB = "selected";
			$selectedC = "";
			$selectedD = "";
			$selectedE = "";
		}
		else if(@strtoupper($selected)=="C") {
			$selectedA = "";
			$selectedB = "";
			$selectedC = "selected";
			$selectedD = "";
			$selectedE = "";
		}
		else if(@strtoupper($selected)=="D") {
			$selectedA = "";
			$selectedB = "";
			$selectedC = "";
			$selectedD = "selected";
			$selectedE = "";
		}
		else if(@strtoupper($selected)=="E") {
			$selectedA = "";
			$selectedB = "";
			$selectedC = "";
			$selectedD = "";
			$selectedE = "selected";
		}
		echo "<option $selectedA value='A'>A</option>";
		echo "<option $selectedB value='B'>B</option>";
		echo "<option $selectedC value='C'>C</option>";
		echo "<option $selectedD value='D'>D</option>";
		echo "<option $selectedE value='E'>E</option>";
	}
	
	function OptionSwitchSemester($startSemester,$endSemester,$selectedSemester) {
		$semester = $startSemester;
		
		echo "<div id='semester_list' style='display:none;margin-left:-10px;'>";
		while($semester!=$endSemester&&$startSemester!="") {			
			$temp = @explode("/",$semester);			
			$thn = @intval(@substr($temp[0],0,2));
			$thn = @GetFullYear($thn);
			$strSemester=$thn."/".(@intval($thn)+1);
			
			if($temp[1]=='1')
				($_SESSION['language']=='eng')?$strSemester .= ' - ODD':$strSemester .= ' - GANJIL';
			else 
				($_SESSION['language']=='eng')?$strSemester .= ' - EVEN':$strSemester .= ' - GENAP';
			
			if($semester==$selectedSemester)
				echo "<div class='change_semester' onClick=\"currentSemesterChange='".$semester."'\"><div id='dashboard_current_semester'><a href='#'>$strSemester</a></div></div>";
			else
				echo "<div class='change_semester' onClick=\"currentSemesterChange='".$semester."'\"><a href='#' style='font-weight:bold;'>$strSemester</a></div>";
			
			$temp = @explode("/",$semester);
			if($temp[1]=="1") {			
				$semester=$temp[0]."/2";
			}
			else {
				$nextThn1=@substr($temp[0],2,2);
				$nextThn2=@intval($nextThn1)+1;
				($nextThn2<10)?$nextThn2="0$nextThn2":$nextThn2=$nextThn2;
				$semester=$nextThn1.$nextThn2."/1";
				$semesterValue=$nextThn1.$nextThn2."1";
			}
		}
		
		echo "</div>";
		
		$temp = @explode("/",$selectedSemester);			
		$thn = @intval(@substr($temp[0],0,2));
		$thn = @GetFullYear($thn);
		$strSemester=$thn."/".(@intval($thn)+1);
		
		if($temp[1]=='1') {
			($_SESSION['language']=='eng')?$strSemester .= ' - ODD':$strSemester .= ' - GANJIL';
		}
		else {
			($_SESSION['language']=='eng')?$strSemester .= ' - EVEN':$strSemester .= ' - GENAP';
		}
		echo "<div id='current_semester'>";
		echo "<span class='other_semester' style='margin-left:-10px;'>".@GetLang('current_semester')." :</span>";
		echo "<div class='change_semester' onClick=\"currentSemesterChange='".$semester."'\"><div id='dashboard_current_semester' style='margin-left:-15px;'><a href='#'>$strSemester</a></div></div>";		
		echo "<span class='other_semester' style='margin-left:-10px;'><a href='#'>".@GetLang('other_semester')."</a></span>";
		echo "</div>";
	}	
	
	function GetSwitchSemester($startSemester,$endSemester,$selectedSemester="") {
		$semester = $startSemester;
		$listSemester = array();
		while($semester!=$endSemester) {			
			($semester==$selectedSemester)?$strSelected="selected":$strSelected="";
			$temp = @explode("/",$semester);			
			$thn = @intval(@substr($temp[0],0,2));
			$thn = @GetFullYear($thn);
			$strSemester=$thn."/".(@intval($thn)+1);
			if($temp[1]=='1') {
				($_SESSION['language']=='eng')?$strSemester .= ' - ODD':$strSemester .= ' - GANJIL';
			}
			else {
				($_SESSION['language']=='eng')?$strSemester .= ' - EVEN':$strSemester .= ' - GENAP';
			}			
			$listSemester[] = array("value"=>$semester,"string"=>$strSemester);
			$temp = @explode("/",$semester);
			if($temp[1]=="1") {			
				$semester=$temp[0]."/2";
			}
			else {
				$nextThn1=@substr($temp[0],2,2);
				$nextThn2=@intval($nextThn1)+1;
				($nextThn2<10)?$nextThn2="0$nextThn2":$nextThn2=$nextThn2;
				$semester=$nextThn1.$nextThn2."/1";
			}
		}
		($semester==$selectedSemester)?$strSelected="selected":$strSelected="";
		$temp = @explode("/",$semester);			
		$thn = @intval(@substr($temp[0],0,2));
		$thn = @GetFullYear($thn);
		$strSemester=$thn."/".(@intval($thn)+1);
		if($temp[1]=='1') {
			($_SESSION['language']=='eng')?$strSemester .= ' - ODD':$strSemester .= ' - GANJIL';
		}
		else {
			($_SESSION['language']=='eng')?$strSemester .= ' - EVEN':$strSemester .= ' - GENAP';
		}
		$listSemester[] = array("value"=>$semester,"string"=>$strSemester);
		return $listSemester;
	}
	
	function OptionSemester($currentSemester,$before,$after,$selectedSemester) {
		$listSemester = GetListSemester($currentSemester,$before,$after);
		if($listSemester):foreach($listSemester as $key=>$val) {
			($val['value']==$selectedSemester)?$str="selected":$str="";
			echo "<option value='".$val['value']."' $str>".$val['string']."</option>";
		} endif;
	}
	
	function GetListSemester($currentSemester,$before,$after) {		
		$arrBefore = array();
		$listSemester = array();
		//before current semester
		$semester = $currentSemester;
		for($i=0;$i<$before;$i++) {
			$temp = @explode("/",$semester);
			if($temp[1]=="2") {
				$semester=$temp[0]."/1";
			}
			else {
				$prevThn2=@substr($temp[0],0,2);
				$prevThn1=@intval($prevThn2)-1;
				($prevThn1<10)?$prevThn1="0$prevThn1":$prevThn1=$prevThn1;
				$semester=$prevThn1.$prevThn2."/2";
			}
			
			$temp = @explode("/",$semester);			
			$thn = @intval(@substr($temp[0],2,2));
			$thn = @GetFullYear($thn);
			$strSemester=(@intval($thn)-1)."/".$thn;
			if($temp[1]=='1') {
				($_SESSION['language']=='eng')?$strSemester .= ' - ODD':$strSemester .= ' - GANJIL';
			}
			else {
				($_SESSION['language']=='eng')?$strSemester .= ' - EVEN':$strSemester .= ' - GENAP';
			}
			$arrBefore[] = array("value"=>$semester,"string"=>$strSemester);
		}		
		for($i=@count($arrBefore)-1;$i>=0;$i--) {
			$listSemester[] = $arrBefore[$i];
		}
		$temp = @explode("/",$currentSemester);			
		$thn = @intval(@substr($temp[0],0,2));
		$thn = @GetFullYear($thn);
		$strSemester=$thn."/".(@intval($thn)+1);
		if($temp[1]=='1') {
			($_SESSION['language']=='eng')?$strSemester .= ' - ODD':$strSemester .= ' - GANJIL';
		}
		else {
			($_SESSION['language']=='eng')?$strSemester .= ' - EVEN':$strSemester .= ' - GENAP';
		}
		$listSemester[] = array("value"=>$currentSemester,"string"=>$strSemester);
		//after current semester
		$semester = $currentSemester;
		for($i=0;$i<$after;$i++) {
			$temp = @explode("/",$semester);
			if($temp[1]=="1") {			
				$semester=$temp[0]."/2";
			}
			else {
				$nextThn1=@substr($temp[0],2,2);
				$nextThn2=@intval($nextThn1)+1;
				($nextThn2<10)?$nextThn2="0$nextThn2":$nextThn2=$nextThn2;
				$semester=$nextThn1.$nextThn2."/1";
			}
			
			$temp = @explode("/",$semester);			
			$thn = @intval(@substr($temp[0],0,2));
			$thn = @GetFullYear($thn);
			$strSemester=$thn."/".(@intval($thn)+1);
			if($temp[1]=='1') {
				($_SESSION['language']=='eng')?$strSemester .= ' - ODD':$strSemester .= ' - GANJIL';
			}
			else {
				($_SESSION['language']=='eng')?$strSemester .= ' - EVEN':$strSemester .= ' - GENAP';
			}		
			$listSemester[] = array("value"=>$semester,"string"=>$strSemester);
		}
		return $listSemester;
	}
	
	function OptionSchoolYear($currentSchoolYear,$before,$after,$selectedSchoolYear) {		
		$listSchoolYear = GetListSchoolYear($currentSchoolYear,$before,$after);
		if($listSchoolYear):foreach($listSchoolYear as $key=>$val) {
			($val['value']==$selectedSchoolYear)?$str="selected":$str="";
			echo "<option value='".$val['value']."' $str>".$val['string']."</option>";
		} endif;
	}
		function OptionSchoolYeares($currentSchoolYear,$before,$after,$selectedSchoolYear) {		
		$listSchoolYear = GetListSchoolYear($currentSchoolYear,$before,$after);
		if($listSchoolYear):foreach($listSchoolYear as $key=>$val) {
			echo "<option value='".$val['value']."' $str>".$val['string']."</option>";
		} endif;
	}
	
	function SpanSchoolYear($schoolYear, $semester = "") {
            $thn1 = @intval(@substr($schoolYear, 0, 2));
            $thn2 = @intval(@substr($schoolYear, 2, 2));
            $result = @GetFullYear($thn1) . "/" . @GetFullYear($thn2);

            if ($semester == '1') {
                ($_SESSION['language'] == 'eng') ? $result .= ' - ODD' : $result .= ' - GANJIL';
            } elseif ($semester == '2') {
                ($_SESSION['language'] == 'eng') ? $result .= ' - EVEN' : $result .= ' - GENAP';
            }

            return $result;
        }

        function GetListSchoolYear($currentSchoolYear,$before,$after) {		
		$arrBefore = array();
		$listSchoolYear = array();
		//before current school year
		$schoolYear = $currentSchoolYear;
		for($i=0;$i<$before;$i++) {
			$prevThn2=@substr($schoolYear,0,2);
			$prevThn1=@intval($prevThn2)-1;
			($prevThn1<10)?$prevThn1="0$prevThn1":$prevThn1=$prevThn1;
			$schoolYear=$prevThn1.$prevThn2;			
			$thn = @intval(@substr($schoolYear,2,2));
			$thn = @GetFullYear($thn);
			$strSchoolYear=(@intval($thn)-1)."/".$thn;
			$arrBefore[] = array("value"=>$schoolYear,"string"=>$strSchoolYear);
		}
		//echo options
		for($i=@count($arrBefore)-1;$i>=0;$i--) {
			$listSchoolYear[] = $arrBefore[$i];
		}
		//current school year
		$thn = @intval(@substr($currentSchoolYear,0,2));
		$thn = @GetFullYear($thn);
		$strSchoolYear=$thn."/".(@intval($thn)+1);
		$listSchoolYear[] = array("value"=>$currentSchoolYear,"string"=>$strSchoolYear);
		//after current school year
		$schoolYear = $currentSchoolYear;
		for($i=0;$i<$after;$i++) {
			$nextThn1=@substr($schoolYear,2,2);
			$nextThn2=@intval($nextThn1)+1;
			($nextThn2<10)?$nextThn2="0$nextThn2":$nextThn2=$nextThn2;
			$schoolYear=$nextThn1.$nextThn2;			
			$thn = @intval(@substr($schoolYear,0,2));
			$thn = @GetFullYear($thn);
			$strSchoolYear=$thn."/".(@intval($thn)+1);
			$listSchoolYear[] = array("value"=>$schoolYear,"string"=>$strSchoolYear);
		}
		return $listSchoolYear;
	}
	
	function GetFullYear($year) {
		$thn =intval($year);
		($thn<10)?$thn="0$thn":$thn=$thn;
		(@intval($thn)<80)?$thn="20$thn":$thn="19$thn";
		return $thn;
	}
	
	function OptionImageFolder($dirname,$selected) {		
		if($d = @dir($dirname)) {
			while(false !== ($entry = $d->read())) {
				if(@substr($entry,0,1) != '.') {
					if(@is_dir($dirname . '/' . $entry)) {			
						echo '<option value="' . $entry . '/"' .
							(($selected == $dirname . $entry . '/') ? ' selected="selected"' : '') . ' />' .
							@substr($dirname, 3) . $entry . '</option>' . "\n";
					}
				}
			}
			$d->close();
		}
	}

	function CreateThumbnail($pathToImages,$pathToThumbs,$thumbWidth) {
		// parse path for the extension
		$info = @pathinfo($pathToImages);
		// continue only if this is a JPEG image
		if ( @strtolower($info['extension']) == 'jpg' || @strtolower($info['extension']) == 'jpeg') {
			// load image and get image size
			$img = @imagecreatefromjpeg($pathToImages);
			$width = @imagesx( $img );
			$height = @imagesy( $img );

			// calculate thumbnail size
			$new_width = $thumbWidth;
			$new_height = @floor( $height * ( $thumbWidth / $width ) );

			// create a new tempopary image
			$tmp_img = @imagecreatetruecolor( $new_width, $new_height );

			// copy and resize old image into new image 
			@imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

			// save thumbnail into a file
			@imagejpeg($tmp_img,$pathToThumbs);
		}
		else if ( @strtolower($info['extension']) == 'png' ) {
			// load image and get image size
			$img = @imagecreatefrompng($pathToImages);
			$width = @imagesx( $img );
			$height = @imagesy( $img );

			// calculate thumbnail size
			$new_width = $thumbWidth;
			$new_height = @floor( $height * ( $thumbWidth / $width ) );

			// create a new tempopary image
			$tmp_img = @imagecreatetruecolor( $new_width, $new_height );

			// copy and resize old image into new image 
			@imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

			// save thumbnail into a file
			@imagepng($tmp_img,$pathToThumbs);
		}
		else if ( @strtolower($info['extension']) == 'gif' ) {
			// load image and get image size
			$img = @imagecreatefromgif($pathToImages);
			$width = @imagesx( $img );
			$height = @imagesy( $img );

			// calculate thumbnail size
			$new_width = $thumbWidth;
			$new_height = @floor( $height * ( $thumbWidth / $width ) );

			// create a new tempopary image
			$tmp_img = @imagecreatetruecolor( $new_width, $new_height );
			
			// copy and resize old image into new image 
			@imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

			// save thumbnail into a file
			@imagegif($tmp_img,$pathToThumbs);
		}
		else if ( @strtolower($info['extension']) == 'bmp' || @strtolower($info['extension']) == 'wbmp') {
			// load image and get image size
			$img = @imagecreatefromwbmp($pathToImages);
			$width = @imagesx( $img );
			$height = @imagesy( $img );

			// calculate thumbnail size
			$new_width = $thumbWidth;
			$new_height = @floor( $height * ( $thumbWidth / $width ) );

			// create a new tempopary image
			$tmp_img = @imagecreatetruecolor( $new_width, $new_height );

			// copy and resize old image into new image 
			@imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

			// save thumbnail into a file
			@imagewbmp($tmp_img,$pathToThumbs);
		}
	}
	/*
	function RecreateThumbs($pathToImages,$pathToThumbs,$thumbWidth) {
		// open the directory
		$dir = @opendir( $pathToImages );
		
		// loop through it, looking for any/all JPG files:
		while (false !== ($fname = @readdir( $dir ))) {
			// parse path for the extension
			$info = @pathinfo($pathToImages . $fname);
			// continue only if this is a JPEG image
			if ( @strtolower($info['extension']) == 'jpg' ) {
				// load image and get image size
				$img = @imagecreatefromjpeg( "{$pathToImages}{$fname}" );
				$width = @imagesx( $img );
				$height = @imagesy( $img );

				// calculate thumbnail size
				$new_width = $thumbWidth;
				$new_height = @floor( $height * ( $thumbWidth / $width ) );

				// create a new tempopary image
				$tmp_img = @imagecreatetruecolor( $new_width, $new_height );

				// copy and resize old image into new image 
				@imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

				// save thumbnail into a file
				@imagejpeg( $tmp_img, "{$pathToThumbs}{$fname}" );
			}
		}
		// close the directory
		@closedir( $dir );
	}
	*/
	function Terbilang($x) {
		$x = abs($x);
		$angka = array("", "satu", "dua", "tiga", "empat", "lima",
			"enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
	    
		if ($x <12)
	        $temp = " ". $angka[$x];
	    else if ($x <20)
	        $temp = Terbilang($x - 10). " belas";
	    else if ($x <100)
	        $temp = Terbilang($x/10)." puluh". Terbilang($x % 10);
	    else if ($x <200)
	        $temp = " seratus" . Terbilang($x - 100);
	    else if ($x <1000)
	        $temp = Terbilang($x/100) . " ratus" . Terbilang($x % 100);
	    else if ($x <2000)
	        $temp = " seribu" . Terbilang($x - 1000);
	    else if ($x <1000000)
	        $temp = Terbilang($x/1000) . " ribu" . Terbilang($x % 1000);
	    else if ($x <1000000000)
	        $temp = Terbilang($x/1000000) . " juta" . Terbilang($x % 1000000);
	    else if ($x <1000000000000)
	        $temp = Terbilang($x/1000000000) . " milyar" . Terbilang(@fmod($x,1000000000));
	    else if ($x <1000000000000000)
	        $temp = Terbilang($x/1000000000000) . " trilyun" . Terbilang(@fmod($x,1000000000000));
		
        return $temp;
	}
	
	function OptionMonth($sortType='asc',$bulanDipilih='') {
		if($sortType=='desc') {
			for($i=12;$i>=1;$i--) {
				$selected = "";
				if($bulanDipilih==$i) $selected = "selected";
					echo "<option $selected value='$i'>$i</option>";
			}
		}
		else {
			for($i=1;$i<=12;$i++) {
				$selected = "";
				if($bulanDipilih==$i) $selected = "selected";
					echo "<option $selected value='$i'>$i</option>";
			}
		}
	}
	
	function OptionYear($tahunMulai,$tahunAkhir,$inc=1,$sortType='asc',$tahunDipilih='') {		
		if($sortType=='desc') {
			for($i=$tahunAkhir;$i>=$tahunMulai;$i-=$inc) {
				$selected = "";
				if($tahunDipilih==$i) $selected = "selected";
					echo "<option $selected value='$i'>$i</option>";
			}
		}
		else {
			for($i=$tahunMulai;$i<=$tahunAkhir;$i+=$inc) {
				$selected = "";
				if($tahunDipilih==$i) $selected = "selected";
					echo "<option $selected value='$i'>$i</option>";
			}
		}
	}
	
	function OptionTime($max,$timeSelected='',$addZero=true) {
		for($i=0;$i<$max;$i++) {			
			($timeSelected==$i)?$selected="selected":$selected="";
			($addZero && $i<10)?$j="0$i":$j=$i;
			echo "<option $selected value='$j'>$j</option>";
		}
	}
	
	function FetchInsert($arrData=array()){
		$data = array(
				"fields" => "",
				"values" => ""
			);
		$fields = null;
		$values = null;
		foreach($arrData as $f => $v){
			$fields[] = $f;
			$values[] = $v;
		}
		$data["fields"] = implode(", ", $fields);
		$data["values"] = implode(", ", $values);
		return $data;
	}
	
	function FetchUpdate($arrData=array()){		
		$data = array(
				"assignData" => "",
			);
		$assignData = null;
		foreach($arrData as $f => $v){
			$assignData[] = "$f = $v";
		}
		$data["assignData"] = implode(", ", $assignData);
		return $data;
	}
	
	function GetActiveYear(){
		$diff = (date('Y')-1996)%4;
		return date('Y')-$diff;
	}	
	
	function GetCurrentDate(){
		$today = date("Y m d");  
		return @ToInaDateFormat($today);
	}
	
	function GetCurrentTime(){
		$today = date("H:i:s");  
		return $today;
	}
	
	function GetMessage($msg, $msgType='WARNING',$hide=TRUE){
		if(isset($msg)){
			$className = "";
			$icon = "";
			$state = "";
			switch(strtoupper($msgType)){
				case "WARNING" :
							$className = "alert-warning";
							$icon = "ui-icon-info";
							$state = "Warning";
							break;
				case "ERROR" : 
							$className = "alert-danger";
							$icon = "ui-icon-alert";
							$state = "Error";
							break;
				case "FAILED" :
							$className = "alert-danger";
							$icon = "ui-icon-alert";
							$state = "Failed";
							break;
				case "SUCCESS" :
							$className = "alert-success";
							$icon = "ui-icon-check";
							$state = "Success";
							break;
			}
			if($hide){$h="message";}else{$h="";}
			/* $stringMessage = '
				<div class="'.$h.' ui-widget" style="padding:5px;" id="'.$h.'">
					<div style="padding: 0 .7em;" class="'.$state.' ui-corner-all"> 
						<p style="margin-bottom:6px;"><span style="float: left; margin-right: .3em;" class="ui-icon '.$icon.'"></span> 
						<strong>Note:</strong><br/></p><div style="margin:6px;">'.$msg.'</div>
					</div>
				</div>'; */
			$stringMessage =	'<div class="alert '.$className.' alert-dismissable">
						<a href="" class="close">&times;</a>
					<strong>'.$state.'!</strong> '.$msg.'
				</div>';
			//$stringMessage = "<div class='$className generatedMessage' id='message'> <span id='iconNotification' style='float: left; margin-right: .3em;' class='ui-icon $icon '></span> <span class='messageContent' style='' > Note :  $msg </span> </div>";
			return 	$stringMessage;
		}else{
			return null;
		}
	}
	
	function GetCopInfo(){
		$sHtml = '<div class="cop_print_info" width="100%" align="left" style="float:left;">
					<div style="width:auto;float:left;margin:5px;"> <img src="../images/logo_cop_report1.jpg" style=""/> </div>
					<div align="left" style="float:left;margin:10px 10px 0px 10px;">
						<address><b>Telkom University</b> <br/>
						Jl.Telekomunikasi No.1, Terusan Buah Batu <br/>
						Bandung 40257 <br/>
						Indonesia <br/>
						Tel.62-22-756 4108,<br/>
						Fax.62-22 756 5200<br/>
						</address>
					</div>
				</div>
				<div id="" style="display:block;width:100%;clear:both;" >&nbsp;</div>
				<hr style="border : 1px solid;"/>';
		return $sHtml;
	}

	function GetCopInfo2(){
		$sHtml = '<div class="cop_print_info" width="100%" align="left" style="float:left;">
						<div style="width:auto;float:left;margin:5px;"> <img src="../images/lpdp_logo_login.png" style=""/> </div>
						<div align="left" style="float:left;margin:10px 10px 0px 10px;">
							<address><b>Lembaga Pengelola Dana Pendidikan</b> <br/>
							Gedung Ali Wardhana Lantai 2, Jalan Lapangan Banteng Timur No.1 <br/>
							Jakarta 10710, Kotak Pos 1139 <br/>
							Indonesia <br/>
							Tel.(021)384 - 6474,<br/>
							</address>
						</div>
					</div>
					<div id="" style="display:block;width:100%;clear:both;" >&nbsp;</div>
					<hr style="border : 1px solid;"/>';
		return $sHtml;
	}

    function uploadFile($files,$fileToUpload,$target_dir,$maxSize,$arrAllowedExt){
        //upload file
        $arrVal=array(
            "status"    =>"failed",
            "filename"  =>"",
            "msg"       =>"Undefined Error"
        );
        $fileName=uniqid('', true)."_".str_replace(" ","_",basename($files[$fileToUpload]["name"]));
        $target_file = $target_dir . $fileName;
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($files[$fileToUpload]["tmp_name"]);
            if($check !== false) {
                $arrVal=array(
                        "status"    =>"success",
                        "filename"  =>"",
                        "msg"       =>"File is an image - " . $check["mime"] . "."
                );
                //return $arrVal;
                $uploadOk = 1;
            } else {

                $arrVal=array(
                    "status"    =>"failed",
                    "filename"  =>"",
                    "msg"       =>"File is not an image."
                );
                return $arrVal;
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $arrVal=array(
                "status"    =>"failed",
                "filename"  =>"",
                "msg"       =>"Sorry, file already exists."
            );
            return $arrVal;

            $uploadOk = 0;
        }
        // Check file size
        if ($files[$fileToUpload]["size"] > $maxSize) {
            $arrVal=array(
                "status"    =>"failed",
                "filename"  =>"",
                "msg"       =>"Sorry, your file is too large."
            );
            return $arrVal;

            $uploadOk = 0;
        }
        // Allow certain file formats
        if(!in_array(strtolower($imageFileType), $arrAllowedExt) ) {

            $arrVal=array(
                "status"    =>"failed",
                "filename"  =>"",
                "msg"       =>"Sorry, file type is not allowed."
            );
            return $arrVal;
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {

            $arrVal=array(
                "status"    =>"failed",
                "filename"  =>"",
                "msg"       =>"Sorry, your file was not uploaded."
            );
            return $arrVal;
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($files[$fileToUpload]["tmp_name"], $target_file)) {

                $arrVal=array(
                    "status"    =>"success",
                    "filename"  =>$fileName,
                    "msg"       =>"success"
                );
                return $arrVal;
            } else {

                $arrVal=array(
                    "status"    =>"failed",
                    "filename"  =>"",
                    "msg"       =>"Sorry, there was an error uploading your file."
                );
                return $arrVal;
            }
        }

    }
        
        function GetCopInfoforPDF(){
                $sHtml = '<table>
                            <tr>
                                <td>
                                    <div style="margin:5px"> <img src="'.IMAGESPATH.'logo_cop_report1.jpg"/> </div>
                                </td>
                                <td>&nbsp;&nbsp;</td>
                                <td>
                                    <div style="margin:10px 10px 10px 20px;">
                                        <address><b>Telkom University</b> <br/>
                                        Jl.Telekomunikasi No.1, Terusan Buah Batu <br/>
                                        Bandung 40257 <br/>
                                        Indonesia <br/>
                                        Tel.62-22-756 4108,<br/>
                                        Fax.62-22 756 5200<br/>
                                        </address>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <hr style="border : 1px solid;"/>';
		return $sHtml;
	}
	
	
	function GetCopSisfo(){
		/*$sHtml = '<div class="cop_print_info" width="100%" align="left" style="float:left;">
					<div style="width:auto;float:left;margin:5px;"> <img src="../images/logo_cop_report1.jpg" style=""/> </div>
					<div align="left" style="width:60%;float:left;margin:10px 10px 0px 10px;">
						<address>
							<h3><b>DIREKTORAT SISTEM INFORMASI</b></h3>
							<h3><b>Universitas Telkom</b></h3>
							Gedung D Lantai 2 - Email: sisfo@telkomuniversity.ac.id - Telp Ext 2216, 2070, 2066, 2068
						</address>
					</div>
				</div>
				<div id="" style="display:block;width:100%;clear:both;" >&nbsp;</div>
				<div id="" style="display:block;width:100%;clear:both;" ><hr style="border : 1px solid;width:100%;"/></div>';*/
		$sHtml = '<div class="cop_print_info" width="100%" align="left" style="float:left;">
					<div style="width:auto;float:left;margin:5px;"> <img src="../images/logo_cop_report1.jpg" style=""/> </div>
					<div align="left" style="width:60%;float:left;margin:10px 10px 0px 10px;">
						<address>
							<h3><b>DIREKTORAT SISTEM INFORMASI</b></h3>
							<h3><b>Universitas Telkom</b></h3>
							Gedung D Lantai 2 <br/>Email: is@telkomuniversity.ac.id
						</address>
					</div>
				</div>
				<div id="" style="display:block;width:100%;clear:both;" >&nbsp;</div>
				<div id="" style="display:block;width:100%;clear:both;" ><hr style="border : 1px solid;width:100%;"/></div>';
		return $sHtml;
	}
        
        function GetFooterInfo($formInfo = "-", $applicationInfo = "-", $username="", $page=""){		
		include_once "db.class.useraccount.php";	
		include "db.config.php";
		$dbFWork= new DBUserAccount($frameworkConfig);
		$oleh = $username;
		if($username=="") {
			$userAccountData = $dbFWork->GetDataUserById($_SESSION[userid]);
			$oleh = $userAccountData[NAME];
		}
                if ($page) {
                    $pageInfo = $page." {PAGENO} / {nb}";
                }
		$sHtml = '<div style="clear:both;width:100%;">&nbsp;</div><div class="footer_print_info" width="100%" align="left">
					<div style="width:auto;float:left;margin:10px;font-style:italic;font-size: 8pt;">  Pencetakan '.$formInfo.' pada tanggal <b>'.GetCurrentDate().'</b> pukul <b>'.GetCurrentTime().'</b> oleh <b>'.$oleh.'</b> <br/>
					'.$applicationInfo.' '.$pageInfo.'
					</div>
				</div>';
		return $sHtml;
	}
	
	function truncate($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
		if ($considerHtml) {
			// if the plain text is shorter than the maximum length, return the whole text
			if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
				return $text;
			}
			// splits all html-tags to scanable lines
			preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
			$total_length = strlen($ending);
			$open_tags = array();
			$truncate = '';
			foreach ($lines as $line_matchings) {
				// if there is any html-tag in this line, handle it and add it (uncounted) to the output
				if (!empty($line_matchings[1])) {
					// if it's an "empty element" with or without xhtml-conform closing slash
					if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
						// do nothing
					// if tag is a closing tag
					} else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
						// delete tag from $open_tags list
						$pos = array_search($tag_matchings[1], $open_tags);
						if ($pos !== false) {
						unset($open_tags[$pos]);
						}
					// if tag is an opening tag
					} else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
						// add tag to the beginning of $open_tags list
						array_unshift($open_tags, strtolower($tag_matchings[1]));
					}
					// add html-tag to $truncate'd text
					$truncate .= $line_matchings[1];
				}
				// calculate the length of the plain text part of the line; handle entities as one character
				$content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
				if ($total_length+$content_length> $length) {
					// the number of characters which are left
					$left = $length - $total_length;
					$entities_length = 0;
					// search for html entities
					if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
						// calculate the real length of all entities in the legal range
						foreach ($entities[0] as $entity) {
							if ($entity[1]+1-$entities_length <= $left) {
								$left--;
								$entities_length += strlen($entity[0]);
							} else {
								// no more characters left
								break;
							}
						}
					}
					$truncate .= substr($line_matchings[2], 0, $left+$entities_length);
					// maximum lenght is reached, so get off the loop
					break;
				} else {
					$truncate .= $line_matchings[2];
					$total_length += $content_length;
				}
				// if the maximum length is reached, get off the loop
				if($total_length>= $length) {
					break;
				}
			}
		} else {
			if (strlen($text) <= $length) {
				return $text;
			} else {
				$truncate = substr($text, 0, $length - strlen($ending));
			}
		}
		// if the words shouldn't be cut in the middle...
		if (!$exact) {
			// ...search the last occurance of a space...
			$spacepos = strrpos($truncate, ' ');
			if (isset($spacepos)) {
				// ...and cut the text in this position
				$truncate = substr($truncate, 0, $spacepos);
			}
		}
		// add the defined ending to the text
		$truncate .= $ending;
		if($considerHtml) {
			// close all unclosed html-tags
			foreach ($open_tags as $tag) {
				$truncate .= '</' . $tag . '>';
			}
		}
		return $truncate;
	}
	
	function SendSms($applicationName,$destinationNumber,$sms_message,$showStatus = false) {
		$destinationNumber = @str_replace("(","",$destinationNumber);
		$destinationNumber = @str_replace(")","",$destinationNumber);
		$destinationNumber = @str_replace("-","",$destinationNumber);
		$destinationNumber = @str_replace("/","",$destinationNumber);
		$destinationNumber = @str_replace(" ","",$destinationNumber);
		$destinationNumber = @str_replace("+62","0",$destinationNumber);
		if(substr($destinationNumber,0,1)) {
			$destinationNumber = "0".$destinationNumber;
		}
		$sms_message = @urlencode($sms_message);
		$sms_message = @substr($sms_message,0,140);
		//$url= "http://www.ittelkom.ac.id/sisfo/sms/sms_overserver.php?applicationname=".$applicationName."&destination=".$destinationNumber."&message=".$sms_message."";
		//$url= "http://10.14.203.111/sisfo/sms/sms_overserver.php?applicationname=".$applicationName."&destination=".$destinationNumber."&message=".$sms_message."";
		/* XL for GENERAL */
                $url= "http://10.14.211.233:9333/ozeki?action=sendMessage&ozmsUserInfo=admin%3Aabc123&recepient=".$destinationNumber."&messageData=".$sms_message." [NO-REPLY] SISFO TEL-U";
                $ch = @curl_init();
		@curl_setopt($ch, CURLOPT_URL, $url);
                if (!$showStatus) {
                    @curl_setopt($ch, CURLOPT_NOBODY, true);
                }
		@curl_exec($ch);
		@curl_close($ch);
	}
        
        function SendSmsError($applicationName,$destinationNumber,$sms_message,$showStatus = false) {
		$destinationNumber = @str_replace("(","",$destinationNumber);
		$destinationNumber = @str_replace(")","",$destinationNumber);
		$destinationNumber = @str_replace("-","",$destinationNumber);
		$destinationNumber = @str_replace("/","",$destinationNumber);
		$destinationNumber = @str_replace(" ","",$destinationNumber);
		$destinationNumber = @str_replace("+62","0",$destinationNumber);
		if(substr($destinationNumber,0,1)) {
			$destinationNumber = "0".$destinationNumber;
		}
		$sms_message = @urlencode($sms_message);
		$sms_message = @substr($sms_message,0,140);
                /* SIMPATI for TOKEN */
		$url= "http://10.14.203.4:9333/ozeki?action=sendMessage&ozmsUserInfo=admin%3Aabc123&recepient=".$destinationNumber."&messageData=".$sms_message." [NO-REPLY] SISFO TEL-U";
		$ch = @curl_init();
		@curl_setopt($ch, CURLOPT_URL, $url);
                if (!$showStatus) {
                    @curl_setopt($ch, CURLOPT_NOBODY, true);
                }
		@curl_exec($ch);
		@curl_close($ch);
	}
        
        function SendSmsTrial($applicationName, $destinationNumber, $sms_message, $showStatus = false) {
            $destinationNumber = str_replace("(", "", $destinationNumber);
            $destinationNumber = str_replace(")", "", $destinationNumber);
            $destinationNumber = str_replace("-", "", $destinationNumber);
            $destinationNumber = str_replace("/", "", $destinationNumber);
            $destinationNumber = str_replace(" ", "", $destinationNumber);
            $destinationNumber = str_replace("+62", "0", $destinationNumber);
            $destinationNumber = substr_replace($destinationNumber, "62", 0, 1);

            $sms_message = urlencode($sms_message);
            //$sms_message = substr($sms_message,0,140);

            /* Telkomsel Official */
            $user = "telkomuniv";
            $password = "Telkomsel@123";
            $url = "http://202.3.208.142:9002/submit.jsp?cp_name=" . $user . "&pwd=" . $password . "&msisdn=" . $destinationNumber . "&sms=" . $sms_message;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            if (!$showStatus) {
                curl_setopt($ch, CURLOPT_NOBODY, true); 
            }
            curl_exec($ch);
            curl_close($ch);
        }

        function SendEmail($sendTo,$name,$subject,$textBody,$arrAttachment=null) {
		include_once SERVERPATH."/email1243/class.phpmailer.php";
		
		
		$mail = new PHPMailer();
		$body = "<body style='margin: 10px;'><div style='width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;'>$textBody</div></body>";
		$body = eregi_replace("[\]",'',$body);
		
		$mail->IsSMTP(); // telling the class to use SMTP
		//$mail->SMTPDebug = 2; // enables SMTP debug information (for testing)
								// 1 = errors and messages
								// 2 = messages only
		$mail->SMTPAuth   = true; // enable SMTP authentication
		/*-------------setting for Tel-U webmail-------------*/
		$mail->SMTPSecure = 'TLS'; // secure transfer enabled
		$mail->Host = "10.100.90.25"; // SMTP server Tel-U mail
		// $mail->Host = "mx1.telkomuniversity.ac.id"; // SMTP server Tel-U mail
		$mail->Port = 25; // set the SMTP port for the Tel-U mail server
		$mail->Username = "lpdp.noreply@depkeu.go.id"; // SMTP account username system@telkomuniversity.ac.id
		$mail->Password = "Lpdp1#";        // SMTP account password system@telkomuniversity.ac.id
		$mail->SetFrom('lpdp.noreply@depkeu.go.id', 'LPDP');
		/*-----------------setting for Gmail-----------------*/
		// $mail->Host = 'ssl://smtp.gmail.com'; //SMTP server gmail
		// $mail->SMTPSecure = 'SSL'; // secure transfer enabled REQUIRED for GMail		
		// $mail->Port = 465; // set the SMTP port for the GMAIL server		
		// $mail->Username = "infoittelkom"; // SMTP account username infoittelkom@gmail.com
		// $mail->Password = "bukaajah"; // SMTP account password uptsisfo@gmail.com
		// $mail->SetFrom('infoittelkom@gmail.com', 'INFO IT Telkom');		
		
		$mail->Subject = $subject;
		// $mail->AltBody = "Testing this is just testing okey.. ;)"; // optional, comment out and test
		$mail->MsgHTML($body);
		$mail->AddAddress($sendTo, $name);
		if($arrAttachment!=null) {
			foreach($arrAttachment as $attachment) {
				$mail->AddAttachment($attachment);
			}
		}
		
		return $mail->Send();
	}
	
	function NumToLetter($num, $uppercase = FALSE)
	{
		$num -= 1;

		$letter = chr(($num % 26) + 97);
		$letter .= (floor($num/26) > 0) ? str_repeat($letter, floor($num/26)) : '';
		return ($uppercase ? strtoupper($letter) : $letter);
	}


	function NumberToRoman($num,$type){

		if($type == 1){ //upper character number
			// Make sure that we only use the integer portion of the value

			$n = intval($num);
			$result = '';

			// Declare a lookup array that we will use to traverse the number:
			$lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
			'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
			'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

			foreach ($lookup as $roman => $value){
				// Determine the number of matches
				$matches = intval($n / $value);

				// Store that many characters
				$result .= str_repeat($roman, $matches);

				// Substract that from the number
				$n = $n % $value;
			}
			// The Roman numeral should be built, return it
			return $result;
		}
		elseif($type == 2){ //low character number

			// Make sure that we only use the integer portion of the value
			$n = intval($num);
			$result = '';

			// Declare a lookup array that we will use to traverse the number:

			$lookup = array('m' => 1000, 'cm' => 900, 'd' => 500, 'cd' => 400,
			'c' => 100, 'xc' => 90, 'l' => 50, 'xl' => 40,
			'x' => 10, 'ix' => 9, 'v' => 5, 'iv' => 4, 'i' => 1);

			foreach ($lookup as $roman => $value){
				// Determine the number of matches
				$matches = intval($n / $value);

				// Store that many characters
				$result .= str_repeat($roman, $matches);

				// Substract that from the number
				$n = $n % $value;
			}
			// The Roman numeral should be built, return it
			return $result;
		}
	}
	
	function GetINADate($format="Y-m-d") {
		return ToInaDateFormat(date($format));
	}
	
	function ToInaDateFormat($date,$format="Y-m-d") {
		//if default, ex : 2011-02-21
		$tahun = substr($date, 0, 4);
		$tanggal = substr($date, 8, 2);
		$bulan = substr($date, 5, 2);
		if($format=="d-m-Y") { //if d-m-Y, ex : 21-02-2011
			$tahun = substr($date, 6, 4);
			$tanggal = substr($date, 0, 2);
			$bulan = substr($date, 3, 2);
		}
		$arrayOfMonth = array(
							1 => "Januari",
							2 => "Februari",
							3 => "Maret",
							4 => "April",
							5 => "Mei",
							6 => "Juni",
							7 => "Juli",
							8 => "Agustus",
							9 => "September",
							10 => "Oktober",
							11 => "November",
							12 => "Desember"
						);
		$t = substr($date, 11, 8);
		if(($t)!=""){
			$date_converted = "$tanggal ".$arrayOfMonth[intval($bulan)]." $tahun ".substr($date, 11, 8);
		}
		else
			$date_converted = "$tanggal ".$arrayOfMonth[intval($bulan)]." $tahun";
		return $date_converted;
	}
	function createHstat(){
	   $str="
	   <!-- Histats.com  START  (aync)-->
<script type='text/javascript'>var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,1588312,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('http://s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>

<!-- Histats.com  END  -->
	   
	   ";
	   
	   echo $str;
	
	}
	
	function createSettingsWrapperOnMetroUi($accountSetting=true,$notifSetting=true,$notif=true,$toTop=true,$feedBack=true) {
		include "db.config.php";
		include "db.variable.framework.php";
		include_once "db.class.framework.php";
		include_once "db.class.semester.php";
		include_once "db.class.student.php";
		include_once "db.class.notification.php";
		//init	
		$framework = new Framework($frameworkConfig);
		$semesterObj = new DBSemester($academicConfig);
		$studentObj = new DBStudent($academicConfig);
		$notificationObj = new NotificationDb($frameworkConfig);
		
		$sWhere1 = " where ".$users['userId']."='".$_SESSION['userid']."'";
		$q1 = $framework->GetListUser($sWhere1);
		$r1 = $framework->FetchArray($q1);
		$name = @DbToStr($_SESSION['fullname'],'upper');
		$sWhere2 = " where ".$userGroups['userGroupId']."='".$_SESSION['groupid']."'";
		$q2 = $framework->GetListUserGroup($sWhere2);
		$r2 = $framework->FetchArray($q2);
		$userGroupName = @DbToStr($r2[$userGroups['userGroupName']],'ucwords');

		$listUserGroups = array();

		//get user groups this user. 1 user may have > 1 user groups
		$sWhere3 = " where USERMAPPINGVIEW.USERID='".$_SESSION['userid']."' ".
			" and USERMAPPINGVIEW.USERGROUPID in ".
			"(select ".$applicationUserGroup['userGroupId']." from ".$table['applicationUserGroup'].
			" where ".$applicationUserGroup['applicationId']."='".APPLICATION_ID."')";
		$q3 = $framework->GetListUserMapping($sWhere3,"order by UPPER(USERGROUPNAME) asc");
		while($r3 = $framework->FetchArray($q3)) {
			$listUserGroups[] = array(
						"id" => $r3[$userMapping['userGroupId']],
						"name" => @DbToStr($r3[$userGroups['userGroupName']],'x')
						);
		}
		?>
		<div class="user_account_wrapper">
			<div id="user_account_wrapper_content">
				<?php 
					echo "<h3>".@GetLang('usergroup')." </h3>";
					echo "<div class='right_content'>";
										
					foreach($listUserGroups as $key=>$val) {
					
						if($val[id]==$_SESSION['groupid']){
							echo "<div id='usergroup_list_current'>";
							echo "<tr>";
								echo "<td width='5px'>&nbsp;</td>";
								//echo "<td><a href='index.php?action=switch_user_group&id=".$val['id']."'> ".@DbToStr($val['name'],'upper')."</a></td>";
								echo "<td><span class='change_usergroup'><a  href='#' onClick='currentUserGroup=".$val['id']."'> ".@DbToStr($val['name'],'upper')."</a></span></td>";
							echo "</tr>";
							echo "</div>";
						} else {
							echo "<div id='usergroup_list'>";
							echo "<tr>";
								echo "<td width='5px'>&nbsp;</td>";
								echo "<td><span class='change_usergroup'><a  href='#' onClick='currentUserGroup=".$val['id']."'> ".@DbToStr($val['name'],'upper')."</a></span></td>";
							echo "</tr>";
							echo "</div>";
						}
						
					}
					echo "</div>";
					echo "<hr />";
					/*if($_SESSION[groupid]==CONSELORGROUPID || $_SESSION[groupid]==9 || $_SESSION[groupid]==17 ){
						
						$studentSchoolYear="0203";
						$startSemester = "$studentSchoolYear/1";
						$endSemester = $semesterObj->GetCurrentSemester();
						$nextSemester = $semesterObj->GetNextSemester();
						
						if(!$_SESSION[schoolyear]){
						   $_SESSION['schoolyear']=$endSemester['SCHOOLYEAR'];
						   $_SESSION['semester']=$endSemester['SEMESTER'];
						}
						
						//print_r($_SESSION);
						echo "<h3>".@GetLang('school_year')." </h3>";	
						echo "<ul>";				
							@OptionSwitchSemester($startSemester,$nextSemester['SCHOOLYEAR']."/".$nextSemester['SEMESTER'],$_SESSION['schoolyear']."/".$_SESSION['semester']);				
						echo "</ul>";
						echo "<hr/>";
					
					} else*/ 
					
					if(isset($_SESSION['studentid'])) {
				
							
						//get current student semester & current semester
						$studentSchoolYear = $studentObj->GetStudentSchoolYear($_SESSION['studentid']);
						$startSemester = "$studentSchoolYear/1";
						$endSemester = $semesterObj->GetCurrentSemester();
						$nextSemester = $semesterObj->GetNextSemester();
						
						//jika sudah masuk masa registrasi, untuk tiap tipe registrasi: normal dan prs
						echo "<h3>".@GetLang('semester')." </h3>";
						echo "<ul>";
							@OptionSwitchSemester($startSemester,$nextSemester['SCHOOLYEAR']."/".$nextSemester['SEMESTER'],$_SESSION['schoolyear']."/".$_SESSION['semester']);
						echo "</ul>";
						echo "<hr/>";
						
					} else if($_SESSION['groupid']==31){
						//get current student semester & current semester
						$studentSchoolYear = $studentObj->GetStudentSchoolYear(substr($_SESSION['username'],0,10));
						$startSemester = "$studentSchoolYear/1";
						$endSemester = $semesterObj->GetCurrentSemester();
						$nextSemester = $semesterObj->GetNextSemester();
						
						//jika sudah masuk masa registrasi, untuk tiap tipe registrasi: normal dan prs
						echo "<h3>".@GetLang('semester')." </h3>";
						echo "<ul>";
							@OptionSwitchSemester($startSemester,$nextSemester['SCHOOLYEAR']."/".$nextSemester['SEMESTER'],$_SESSION['schoolyear']."/".$_SESSION['semester']);
						echo "</ul>";
						echo "<hr/>";
					}
				?>
				
				<ul>
				<?php 
					if($_SESSION['groupid']==12){
				?>
					<li><a class='logout_link' href='<?php echo FRAMEWORK_URL;?>account/index.php?pageid=3801'><b><?php echo @GetLang('edit_profile');?></b></a></li>
				<?php } ?>	
					<li><a class='logout_link' href='logout.php'><b><?php echo @GetLang('logout');?></b></a></li>
				 </ul>		 
			</div>
		</div>		
		<div class="settings_wrapper">
			<div id="settings_wrapper_content">
			<h3><?php echo @GetLang('settings');?></h3>
			<hr />
				<ul>
					<li><a href='#'><?php 
					echo "<span class='switchLanguageSpan'>";
					if(@strtoupper($_SESSION['language'])=="ENG"){
						echo @GetLang('language')." : <img src='".IMAGES_BASEURL."lang/en.jpg' /> <b>Eng</b> - 
						<a href='#' class='switch_language' rel='ina'><img src='".IMAGES_BASEURL."lang/in.jpg' /> Ina</a>";
					}else if(@strtoupper($_SESSION['language'])=="INA"){
						echo @GetLang('language')." : <a href='#' class='switch_language' rel='eng'><img src='".IMAGES_BASEURL."lang/en.jpg' /> Eng</a> - 
						<img src='".IMAGES_BASEURL."lang/in.jpg' /> <b>Ina</b> ";	
					}else{
						$_SESSION['language'] = 'eng';
						echo @GetLang('language')." : <img src='".IMAGES_BASEURL."lang/en.jpg' /> <b>Eng</b> - 
						<a href='#' class='switch_language' rel='ina'><img src='".IMAGES_BASEURL."lang/in.jpg' /> Ina</a>";
					}	
					echo "</span>";
					
					
					?></a></li>
					<?php if($accountSetting) { ?>
					<li><a href='<?php echo FRAMEWORK_URL;?>account/index.php?pageid=3782'><?php echo @GetLang('account_settings');?></a></li>
					<?php } if($notifSetting) { ?>
					<li><a href='<?php echo FRAMEWORK_URL;?>account/index.php?pageid=4461'><?php echo @GetLang('notification_settings');?></a></li>
					<?php } ?>
				 </ul>		 
			</div>
		</div>	
		<?php if($notif) { ?>
		<div class="notifications_wrapper">	
			<div id="notifications_wrapper_content">
			<h3><?php echo @GetLang('notifications');?></h3>	
			<hr />
				<ul id='notification_content'>			
				</ul>
			<hr />
			
			</div>
		</div>
		<?php } ?>
		<div class="feedbacks_wrapper">	
			<?php if($toTop) { ?>
			<div id="backtotop_wrapper_content" style="float:left;padding:5px;">			
				<a class="backtotop_link" href="#" ><?php echo @GetLang('back_to_top');?></a>		 			
			</div>
			<div id="separator" style="float:left;padding:5px;">			
				<!--|	-->	
			</div>
			<?php } if($feedBack) { ?>
			<div id="feedbacks_wrapper_content"  style="float:right;display:none;">			
				<a class="feedback_link" href="<?php echo FRAMEWORK_URL;?>feedback/index.php?pageid=3701"><?php echo @GetLang('send_feedback');?></a>					
			</div>
			<?php } ?>
		</div>
		<?php
	}
	
	
	function createNotifScript($notif=true,$setting=true,$account=true) {
		include "db.config.php";
		include_once "db.class.notification.php";
		$notificationObj = new NotificationDb($frameworkConfig);
		if($notif) {
		?>
			<li id='notification'>
				
			</li>
		<?php
		}
		
		?>		
		<script>
				
				$(document).ready(function(){
				$("#user_account").click(function(){	
					$("#user_account").css("background-color","#333");	
					$(".user_account_wrapper").toggle();
					$(".settings_wrapper").hide();	
					$(".notifications_wrapper").hide();
					return false;
				});
				
					
				 $(".user_account_wrapper").mouseleave(function(){
				
					$("#user_account").css("background-color","transparent");
					$("#settings_button").css("background-color","transparent");
					$(".user_account_wrapper").hide("slow");		
					$(".user_account_wrapper_content").show();
					return false;
				 });
				 
				 $(".other_semester").click(function(){	
					$("#user_account").css("background-color","#333");	
					$("#semester_list").toggle();
					$(".user_account_wrapper").show();
					$(".other_semester").hide();	
					$(".settings_wrapper").hide();	
					$(".notifications_wrapper").hide();
					$("#current_semester").hide();	
				});
				 
				 $("#settings_button").click(function(){	
					$("#settings_button").css("background-color","#333");	
					$(".settings_wrapper").toggle();
					$(".user_account_wrapper").hide();	
					$(".notifications_wrapper").hide();
				});
				
				 $(".settings_wrapper").mouseleave(function(){
					$("#settings_button").css("background-color","transparent");
					$("#user_account").css("background-color","transparent");
					$(".settings_wrapper").hide("slow");		
					$(".settings_wrapper_content").show();
				 });
				 
				  $("#notification").click(function(){	
					$("#notification").css("background-color","transparent");	
					$(".notifications_wrapper").toggle();
					$(".user_account_wrapper").hide();	
					$(".settings_wrapper").hide();
					document.getElementById("notification").innerHTML="<a href='#' class='fg-white'><i class=' icon-earth'> <b>"+0+"</b></a>";
					document.title=oriTitle;		
					
					$.ajax({
						type:"post",
						dataType:"html",
						url:libraryAjaxUrl+"ajax.util.php?act=last_open_notification",
						
					});
					
					 $.get("<?php echo LIB_AJAX  ?>ajax.dashboard.php?act=getlistnotification",
					   function(data){			
						 document.getElementById("notification_content").innerHTML=data;		
					   }
					 );
					
					return false;
				});
				
				 $(".notifications_wrapper").mouseleave(function(){
					$("#settings_button").css("background-color","transparent");
					$("#user_account").css("background-color","transparent");
					$(".notifications_wrapper").hide("slow");		
					$(".notifications_wrapper_content").show();
				 });  
				 
				  $(".change_username").live("click",function(evt){
					var userId = currentUsername;	
					
					$.ajax({
						type:"post",
						dataType:"html",
						url:libraryAjaxUrl+"ajax.util.php?act=switch_username",
						data:{ "id" : userId },
						success:function(data){
							self.location.href = currentUrl;
						},
						failed:function(){	
						}
					});	
					return false;	
				});
				 
				 $(".change_usergroup").live("click",function(evt){
					var userGroupId = currentUserGroup;	
					
					$.ajax({
						type:"post",
						dataType:"html",
						url:libraryAjaxUrl+"ajax.util.php?act=switch_user_group",
						data:{ "id" : userGroupId },
						success:function(data){
							self.location.href = currentUrl;
						},
						failed:function(){	
						}
					});	
					return false;	
				});
				 
				 
				$(".change_semester").live("click",function(evt){
					var semesterId = currentSemesterChange;	
					
					$.ajax({
						type:"post",
						dataType:"html",
						url:libraryAjaxUrl+"ajax.util.php?act=switch_semester",
						data:{ "id" : semesterId },
						success:function(data){
							self.location.href = currentUrl;
						},
						failed:function(){	
						}
					});
					return false;		
				});
				
				
				
					
					start_timer();
				  
				});
				
				
				var isDashboard=false;
				var oriTitle=document.title;
				function start_timer(){
				userSession=<?php echo $_SESSION['userid'];?>
				
					 $.get("<?php echo LIB_AJAX  ?>ajax.dashboard.php?act=notification",
					   function(data){
							if(data==-1){
							self.location.href='index.php';
							} else if(data==0){
							document.getElementById("notification").innerHTML="<a href='#' class='fg-white'><i class=' icon-earth'> </i> <b>"+data+"</b></a>";
							document.title=oriTitle;	
							} else {	
							 document.getElementById("notification").innerHTML="<a href='#' class='bg-red fg-white'><i class=' icon-earth'> </i> <b>"+data+"</b></a>";
							 
							 document.title="("+data+") "+oriTitle;	
							}
					   
					   
											 
					   }
					 ); 
					 
					 if(isDashboard==true){
						 $.get("<?php echo LIB_AJAX  ?>ajax.dashboard.php?act=messageCount",
						 
						   function(data){
							if(data){
							 document.getElementById("messageCount").innerHTML=data;
							}					 
						   }
						 );
					 }
					  setTimeout("start_timer()",60000);
				}
				
			</script>
		
		
		<?php
	}
	
	//new dashboard 2014
	function getNotifications(){
	?>
	
	<script>
	
	$(document).ready(function(){
	
			  $("#notification_btn").click(function(){						
					$("#notifications_wrapper").toggle();					
					document.getElementById("notification").innerHTML=0;
					document.title=oriTitle;		
					
					$.ajax({
						type:"post",
						dataType:"html",
						url:"<?php echo LIB_AJAX  ?>ajax.util.php?act=last_open_notification",
						
					});
					
					 $.get("<?php echo LIB_AJAX  ?>ajax.dashboard.php?act=listnotification",
					   function(data){			
						 document.getElementById("notifications_wrapper").innerHTML=data;		
					   }
					 );
					
					return false;
				});
				
				 $("#notifications_wrapper").mouseleave(function(){					
					$("#notifications_wrapper").hide("slow");
					
				 }); 
								
				start_timer();
				  
	});
	
				
				var oriTitle=document.title;
				function start_timer(){
				userSession=<?php echo $_SESSION['userid'];?>
				
					 $.get("<?php echo LIB_AJAX  ?>ajax.dashboard.php?act=notification",
					   function(data){
							if(data==-1){
							self.location.href='index.php';
							} else if(data==0){
							document.getElementById("notification").innerHTML=data;
							document.title=oriTitle;	
							} else {	
							 document.getElementById("notification").innerHTML=data;
							 
							 document.title="("+data+") "+oriTitle;	
							}
					   
					   
											 
					   }
					 ); 
					 
					 if(isDashboard==true){
                                                /*
						 $.get("<?php // echo LIB_AJAX  ?>ajax.dashboard.php?act=messageCount",
						 
						   function(data){
							if(data){
							 document.getElementById("messageCount").innerHTML=data;
							}					 
						   }
						 );
                                                */
					 }
					  setTimeout("start_timer()",60000);
				}
				
		</script>		
	<?php
	}
		
	function createSettingsScript($notif=true,$setting=true,$account=true) {
		include "db.config.php";
		include_once "db.class.notification.php";
		//init	
		$notificationObj = new NotificationDb($frameworkConfig);
		if($notif) {
		?>
		<LI class='page_item page-item-4' id="notification"><a href="#" title="Notifications"></a></LI>
		<?php } if($setting) { ?>
		<LI class='page_item page-item-4' id="settings_button"><img src="<?php echo IMAGES_BASEURL; ?>menu/settings-icon-small.png" title="Settings"/></LI>
		<?php } ?>
		<LI class='page_item page-item-4' id="logout_button"><a class='logout_link' href='logout.php'><?php echo @GetLang('logout');?></a></LI>
		<?php if($account) { ?>
		<LI class='page_item page-item-4' id="user_account"><!--<img src="<?php echo IMAGES_BASEURL; ?>igracias avatar.jpg" width="30px"/>--><a href="#" title="<?php echo $_SESSION['name'];?>"><?php echo $_SESSION['fullname'];?></a></LI>
		<?php } ?>		
		<script>
				
				$(document).ready(function(){
				$("#user_account").click(function(){	
					$("#user_account").css("background-color","#333");	
					$(".user_account_wrapper").toggle();
					$(".settings_wrapper").hide();	
					$(".notifications_wrapper").hide();
					return false;
				});
				
					
				 $(".user_account_wrapper").mouseleave(function(){
				
					$("#user_account").css("background-color","transparent");
					$("#settings_button").css("background-color","transparent");
					$(".user_account_wrapper").hide("slow");		
					$(".user_account_wrapper_content").show();
					return false;
				 });
				 
				 $(".other_semester").click(function(){	
					$("#user_account").css("background-color","#333");	
					$("#semester_list").toggle();
					$(".user_account_wrapper").show();
					$(".other_semester").hide();	
					$(".settings_wrapper").hide();	
					$(".notifications_wrapper").hide();
					$("#current_semester").hide();	
				});
				 
				 $("#settings_button").click(function(){	
					$("#settings_button").css("background-color","#333");	
					$(".settings_wrapper").toggle();
					$(".user_account_wrapper").hide();	
					$(".notifications_wrapper").hide();
				});
				
				 $(".settings_wrapper").mouseleave(function(){
					$("#settings_button").css("background-color","transparent");
					$("#user_account").css("background-color","transparent");
					$(".settings_wrapper").hide("slow");		
					$(".settings_wrapper_content").show();
				 });
				 
				  $("#notification").click(function(){	
					$("#notification").css("background-color","transparent");	
					$(".notifications_wrapper").toggle();
					$(".user_account_wrapper").hide();	
					$(".settings_wrapper").hide();
					document.getElementById("notification").innerHTML="<a href='#'>"+0+"</a>";
					document.title=oriTitle;		
					
					$.ajax({
						type:"post",
						dataType:"html",
						url:libraryAjaxUrl+"ajax.util.php?act=last_open_notification",
						
					});
					
					 $.get("<?php echo LIB_AJAX  ?>ajax.dashboard.php?act=getlistnotification",
					   function(data){			
						 document.getElementById("notification_content").innerHTML=data;		
					   }
					 );
					
					return false;
				});
				
				 $(".notifications_wrapper").mouseleave(function(){
					$("#settings_button").css("background-color","transparent");
					$("#user_account").css("background-color","transparent");
					$(".notifications_wrapper").hide("slow");		
					$(".notifications_wrapper_content").show();
				 });  
				 
				  $(".change_username").live("click",function(evt){
					var userId = currentUsername;	
					
					$.ajax({
						type:"post",
						dataType:"html",
						url:libraryAjaxUrl+"ajax.util.php?act=switch_username",
						data:{ "id" : userId },
						success:function(data){
							self.location.href = currentUrl;
						},
						failed:function(){	
						}
					});	
					return false;	
				});
				 
				 $(".change_usergroup").live("click",function(evt){
					var userGroupId = currentUserGroup;	
					
					$.ajax({
						type:"post",
						dataType:"html",
						url:libraryAjaxUrl+"ajax.util.php?act=switch_user_group",
						data:{ "id" : userGroupId },
						success:function(data){
							self.location.href = currentUrl;
						},
						failed:function(){	
						}
					});	
					return false;	
				});
				 
				 
				$(".change_semester").live("click",function(evt){
					var semesterId = currentSemesterChange;	
					
					$.ajax({
						type:"post",
						dataType:"html",
						url:libraryAjaxUrl+"ajax.util.php?act=switch_semester",
						data:{ "id" : semesterId },
						success:function(data){
							self.location.href = currentUrl;
						},
						failed:function(){	
						}
					});
					return false;		
				});
				
				
				
					
					start_timer();
				  
				});
				
				
				var isDashboard=false;
				var oriTitle=document.title;
				function start_timer(){
				userSession=<?php echo $_SESSION['userid'];?>
				
					 $.get("<?php echo LIB_AJAX  ?>ajax.dashboard.php?act=notification",
					   function(data){
							if(data==-1){
							self.location.href='index.php';
							} else if(data==0){
							document.getElementById("notification").innerHTML="<a href='#'>"+data+"</a>";
							document.title=oriTitle;	
							} else {	
							 document.getElementById("notification").innerHTML="<a href='#' style='background:#d00;'>"+data+"</a>";
							 
							 document.title="("+data+") "+oriTitle;	
							}
					   
					   
											 
					   }
					 ); 
					 
					 if(isDashboard==true){
						 $.get("<?php echo LIB_AJAX  ?>ajax.dashboard.php?act=messageCount",
						 
						   function(data){
							if(data){
							 document.getElementById("messageCount").innerHTML=data;
							}					 
						   }
						 );
					 }
					  setTimeout("start_timer()",60000);
				}
				
			</script>
		
		
		<?php
		
	}
	
	function createSettingsWrapper($accountSetting=true,$notifSetting=true,$notif=true,$toTop=true,$feedBack=true) {
		include "db.config.php";
		include "db.variable.framework.php";
		include_once "db.class.framework.php";
		include_once "db.class.semester.php";
		include_once "db.class.student.php";
		include_once "db.class.notification.php";
		//init	
		$framework = new Framework($frameworkConfig);
		$semesterObj = new DBSemester($academicConfig);
		$studentObj = new DBStudent($academicConfig);
		$notificationObj = new NotificationDb($frameworkConfig);
		
		$sWhere1 = " where ".$users['userId']."='".$_SESSION['userid']."'";
		$q1 = $framework->GetListUser($sWhere1);
		$r1 = $framework->FetchArray($q1);
		$name = @DbToStr($_SESSION['fullname'],'upper');
		$sWhere2 = " where ".$userGroups['userGroupId']."='".$_SESSION['groupid']."'";
		$q2 = $framework->GetListUserGroup($sWhere2);
		$r2 = $framework->FetchArray($q2);
		$userGroupName = @DbToStr($r2[$userGroups['userGroupName']],'ucwords');

		$listUserGroups = array();

		//get user groups this user. 1 user may have > 1 user groups
		$sWhere3 = " where USERMAPPINGVIEW.USERID='".$_SESSION['userid']."' ".
			" and USERMAPPINGVIEW.USERGROUPID in ".
			"(select ".$applicationUserGroup['userGroupId']." from ".$table['applicationUserGroup'].
			" where ".$applicationUserGroup['applicationId']."='".APPLICATION_ID."')";
		$q3 = $framework->GetListUserMapping($sWhere3,"order by UPPER(USERGROUPNAME) asc");
		while($r3 = $framework->FetchArray($q3)) {
			$listUserGroups[] = array(
						"id" => $r3[$userMapping['userGroupId']],
						"name" => @DbToStr($r3[$userGroups['userGroupName']],'x')
						);
		}
		?>
		<div class="user_account_wrapper">
			<div id="user_account_wrapper_content">
				<?php 
					echo "<h3>".@GetLang('usergroup')." </h3>";
					echo "<div class='right_content'>";
										
					foreach($listUserGroups as $key=>$val) {
					
						if($val[id]==$_SESSION['groupid']){
							echo "<div id='usergroup_list_current'>";
							echo "<tr>";
								echo "<td width='5px'>&nbsp;</td>";
								//echo "<td><a href='index.php?action=switch_user_group&id=".$val['id']."'> ".@DbToStr($val['name'],'upper')."</a></td>";
								echo "<td><span class='change_usergroup'><a  href='#' onClick='currentUserGroup=".$val['id']."'> ".@DbToStr($val['name'],'upper')."</a></span></td>";
							echo "</tr>";
							echo "</div>";
						} else {
							echo "<div id='usergroup_list'>";
							echo "<tr>";
								echo "<td width='5px'>&nbsp;</td>";
								echo "<td><span class='change_usergroup'><a  href='#' onClick='currentUserGroup=".$val['id']."'> ".@DbToStr($val['name'],'upper')."</a></span></td>";
							echo "</tr>";
							echo "</div>";
						}
						
					}
					echo "</div>";
					echo "<hr />";
					/*if($_SESSION[groupid]==CONSELORGROUPID || $_SESSION[groupid]==9 || $_SESSION[groupid]==17 ){
						
						$studentSchoolYear="0203";
						$startSemester = "$studentSchoolYear/1";
						$endSemester = $semesterObj->GetCurrentSemester();
						$nextSemester = $semesterObj->GetNextSemester();
						
						if(!$_SESSION[schoolyear]){
						   $_SESSION['schoolyear']=$endSemester['SCHOOLYEAR'];
						   $_SESSION['semester']=$endSemester['SEMESTER'];
						}
						
						//print_r($_SESSION);
						echo "<h3>".@GetLang('school_year')." </h3>";	
						echo "<ul>";				
							@OptionSwitchSemester($startSemester,$nextSemester['SCHOOLYEAR']."/".$nextSemester['SEMESTER'],$_SESSION['schoolyear']."/".$_SESSION['semester']);				
						echo "</ul>";
						echo "<hr/>";
					
					} else*/ 
					
					if(isset($_SESSION['studentid'])) {
				
							
						//get current student semester & current semester
						$studentSchoolYear = $studentObj->GetStudentSchoolYear($_SESSION['studentid']);
						$startSemester = "$studentSchoolYear/1";
						$endSemester = $semesterObj->GetCurrentSemester();
						$nextSemester = $semesterObj->GetNextSemester();
						
						//jika sudah masuk masa registrasi, untuk tiap tipe registrasi: normal dan prs
						echo "<h3>".@GetLang('semester')." </h3>";
						echo "<ul>";
							@OptionSwitchSemester($startSemester,$nextSemester['SCHOOLYEAR']."/".$nextSemester['SEMESTER'],$_SESSION['schoolyear']."/".$_SESSION['semester']);
						echo "</ul>";
						echo "<hr/>";
						
					} else if($_SESSION['groupid']==31){
						//get current student semester & current semester
						$studentSchoolYear = $studentObj->GetStudentSchoolYear(substr($_SESSION['username'],0,10));
						$startSemester = "$studentSchoolYear/1";
						$endSemester = $semesterObj->GetCurrentSemester();
						$nextSemester = $semesterObj->GetNextSemester();
						
						//jika sudah masuk masa registrasi, untuk tiap tipe registrasi: normal dan prs
						echo "<h3>".@GetLang('semester')." </h3>";
						echo "<ul>";
							@OptionSwitchSemester($startSemester,$nextSemester['SCHOOLYEAR']."/".$nextSemester['SEMESTER'],$_SESSION['schoolyear']."/".$_SESSION['semester']);
						echo "</ul>";
						echo "<hr/>";
					}
				?>
				
				<ul>
				<?php 
					if($_SESSION['groupid']==12){
				?>
					<li><a class='logout_link' href='<?php echo FRAMEWORK_URL;?>account/index.php?pageid=3801'><b><?php echo @GetLang('edit_profile');?></b></a></li>
				<?php } ?>	
					<li><a class='logout_link' href='logout.php'><b><?php echo @GetLang('logout');?></b></a></li>
				 </ul>		 
			</div>
		</div>		
		<div class="settings_wrapper">
			<div id="settings_wrapper_content">
			<h3><?php echo @GetLang('settings');?></h3>
			<hr />
				<ul>
					<li><a href='#'><?php 
					echo "<span class='switchLanguageSpan'>";
					if(@strtoupper($_SESSION['language'])=="ENG"){
						echo @GetLang('language')." : <img src='".IMAGES_BASEURL."lang/en.jpg' /> <b>Eng</b> - 
						<a href='#' class='switch_language' rel='ina'><img src='".IMAGES_BASEURL."lang/in.jpg' /> Ina</a>";
					}else if(@strtoupper($_SESSION['language'])=="INA"){
						echo @GetLang('language')." : <a href='#' class='switch_language' rel='eng'><img src='".IMAGES_BASEURL."lang/en.jpg' /> Eng</a> - 
						<img src='".IMAGES_BASEURL."lang/in.jpg' /> <b>Ina</b> ";	
					}else{
						$_SESSION['language'] = 'eng';
						echo @GetLang('language')." : <img src='".IMAGES_BASEURL."lang/en.jpg' /> <b>Eng</b> - 
						<a href='#' class='switch_language' rel='ina'><img src='".IMAGES_BASEURL."lang/in.jpg' /> Ina</a>";
					}	
					echo "</span>";
					
					
					?></a></li>
					<?php if($accountSetting) { ?>
					<li><a href='<?php echo FRAMEWORK_URL;?>account/index.php?pageid=3782'><?php echo @GetLang('account_settings');?></a></li>
					<?php } if($notifSetting) { ?>
					<li><a href='<?php echo FRAMEWORK_URL;?>account/index.php?pageid=4461'><?php echo @GetLang('notification_settings');?></a></li>
					<?php } ?>
				 </ul>		 
			</div>
		</div>	
		<?php if($notif) { ?>
		<div class="notifications_wrapper">	
			<div id="notifications_wrapper_content">
			<h3><?php echo @GetLang('notifications');?></h3>	
			<hr />
				<ul id='notification_content'>			
				</ul>
			<hr />
			
			</div>
		</div>
		<?php } ?>
		<div class="feedbacks_wrapper">	
			<?php if($toTop) { ?>
			<div id="backtotop_wrapper_content" style="float:left;padding:5px;">			
				<a class="backtotop_link" href="#" ><?php echo @GetLang('back_to_top');?></a>		 			
			</div>
			<div id="separator" style="float:left;padding:5px;">			
				<!--|	-->	
			</div>
			<?php } if($feedBack) { ?>
			<div id="feedbacks_wrapper_content"  style="float:right;display:none;">			
				<a class="feedback_link" href="<?php echo FRAMEWORK_URL;?>feedback/index.php?pageid=3701"><?php echo @GetLang('send_feedback');?></a>					
			</div>
			<?php } ?>
		</div>
            <?php
        }

        function GetAcademicYearAndSemester($studentId, $semester){
            include "db.config.php";
            include_once "db.class.student.php";

            $studentObj = new DBStudent($academicConfig);

            $studentSchoolYear = $studentObj->GetStudentSchoolYear($studentId);
            $schoolYear = @intval(@substr($studentSchoolYear, 0, 2));
            $cAdd = @intval(@floor((($semester-1)/2)));
            $cAcad = $schoolYear+$cAdd;
            $nAcad = $cAcad+1;
            if(@strlen($cAcad)==1){
                $cAcad = "0".$cAcad;
            }
            if(@strlen($nAcad)==1){
                $nAcad = "0".$nAcad;
            }
            $academicYear = $cAcad.$nAcad;
            $semester = ($semester%2==1)?"1":"2";
            return array(
                        'academicYear'=> $academicYear,
                        'semester' => $semester
                    );
        }

        function GetNickName($fullname) {
            $arrName = explode(' ',$fullname);
            (strlen($arrName[0])<10 && count($arrName)>1)?$nick=$arrName[0]." ".$arrName[1]:$nick=$arrName[0];
            return $nick;
        }

        //input using format YYYY ex:2010
        function GetAcademicYear($year){
            $cAcad = @substr($year, 2, 2);
            $nAcad = $cAcad+1;
            if(@strlen($cAcad)==1){
                $cAcad = "0".$cAcad;
            }
            if(@strlen($nAcad)==1){
                $nAcad = "0".$nAcad;
            }
            $academicYear = $cAcad.$nAcad;
            return $academicYear; //1011, 1213
        }

        function FormatToNormalYear($schoolYear){
            $tYear = substr(trim($schoolYear),0,2);
            if($tYear<90)
                return "20".$tYear;
            else
                return "19".$tYear;
        }

        function IsClientIpAllowed($arrIP = array()){
            $ipClient = @explode(",",@GetIPAddressClient());
            $isAllowed = false;
            for($i=0;$i<count($ipClient);$i++){
                if(@in_array($ipClient[$i], $arrIP)){
                    $isAllowed = true;
                    break;
                }
            }
            return $isAllowed;
        }

        function IsPindahan($studentId) {
            include "db.config.php";
            include_once "db.class.student.php";

            $studentObj = new DBStudent($academicConfig);
            return $studentObj->IsPindahan($studentId);
        }

        function IsD3($studentId) {
            include "db.config.php";
            include_once "db.class.student.php";

            $studentObj = new DBStudent($academicConfig);
            return $studentObj->IsD3($studentId);
        }

        function IsS1($studentId) {
            include "db.config.php";
            include_once "db.class.student.php";

            $studentObj = new DBStudent($academicConfig);
            return $studentObj->IsS1($studentId);
        }

        function IsS2($studentId) {
            include "db.config.php";
            include_once "db.class.student.php";

            $studentObj = new DBStudent($academicConfig);
            return $studentObj->IsS2($studentId);
        }

        function GenerateFilterStudentPPDU() {
            return "select STUDENTID from MASTERDATA.STUDENT
                left join MASTERDATA.STUDYPROGRAM using(STUDYPROGRAMID)
                where STUDENTTYPEID='1' and STUDYPROGRAMTYPE='S1'
                and STUDENTID not in (select STUDENTID from ACADEMICSTATUS where GRADE='1')";
        }

        function GenerateProdiUserFilter($level="studyprogram", $columnReferrer="STUDYPROGRAMID"){
            if(strtolower($level)=="faculty"){
                return " $columnReferrer IN (SELECT STUDYPROGRAMID FROM MASTERDATA.STUDYPROGRAM JOIN (SELECT FACULTYID FROM ACADEMIC.USEROFFACULTY WHERE USERID='".$_SESSION['userid']."') USING(FACULTYID)) ";
            }else if(strtolower($level)=="studyprogram"){
                return " $columnReferrer IN (SELECT STUDYPROGRAMID FROM ACADEMIC.USEROFSTUDYPROGRAM WHERE USERID='".$_SESSION['userid']."') ";
            }
        }

        function TimeRange($datedata,$datenow){
          $betweenseconds=@floor( ((@strtotime($datenow)-@strtotime($datedata))/1) );
          $betweenminutes=@floor( ((@strtotime($datenow)-@strtotime($datedata))/(60)) );
          $betweenhours=@floor( ((@strtotime($datenow)-@strtotime($datedata))/(60*60)) );
          $betweendays=@round((@strtotime($datenow)-@strtotime($datedata))/(24*60*60),0);
                     //echo "MIN:".$betweenminutes;

          if ( ($betweenminutes==0) )
              return @floor( ((@strtotime($datenow)-@strtotime($datedata))/1) )." ".@GetLang('seconds_ago');
          else

          if ( ($betweendays==0) and ($betweenhours<1) )
              return " " .@floor( ((@strtotime($datenow)-@strtotime($datedata))/(60)) )." ".@GetLang('minutes_ago');
          else

          if ( ($betweendays==0) )
              return @floor((@strtotime($datenow)-@strtotime($datedata))/(60*60))." ".@GetLang('hours_ago');
          else
          if ( ($betweendays>360) )
              return @floor((@strtotime($datenow)-@strtotime($datedata))/(12*30*24*60*60))." ".@GetLang('years_ago');
          else
          if ( ($betweendays>30) )
              return @floor((@strtotime($datenow)-@strtotime($datedata))/(30*24*60*60))." ".@GetLang('months_ago');
          else
              return @round((@strtotime($datenow)-@strtotime($datedata))/(24*60*60),0)." ".@GetLang('days_ago');


        }

        function Hyperlink($text){
            // match protocol://address/path/
            //$text = ereg_replace("[a-zA-Z]+://([.]?[a-zA-Z0-9_%/-])*", "<a target=\"_new\" href=\"\\0\" style=\"font-family: arial; color:#3B5998; text-decoration:none;\">\\0</a>", $text);
            $text = ereg_replace("[a-zA-Z]+://([.?=&;a-zA-Z0-9_%:/!#-])*", "<a target=\"_new\" href=\"\\0\">\\0</a>", $text);

            // match www.something
            $text = ereg_replace("(^| )(www([.]?[a-zA-Z0-9_%:/!#-])*)", "\\1<a target=\"_new\" href=\"http://\\2\">\\2</a>", $text);

            // return $text
            return $text;
        }

        function RupiahFormat($number) {
            if($number=="") $number = 0;
            return "Rp. ".number_format($number,2,',','.');
        }

        function UsdFormat($number) {
            return number_format($number,2,',','.')." $";
        }

        function IsExistAjaxRequest(){
            return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=="XMLHttpRequest";
        }

        function DebugVariable($varName){
            echo "<pre>";
            print_r($varName);
            echo "</pre>";
        }

        // $schoolYearA & $schoolYearB example : 0506, 0809
        // $semesterA & $semesterB only 1 or 2
        function CountDiffSemester($schoolYearA, $schoolYearB, $semesterA = null, $semesterB = null){
            $schoolYearA = substr($schoolYearA, 0, 2);
            if($schoolYearA>=90)
                $schoolYearA = "19".$schoolYearA;
            else
                $schoolYearA = "20".$schoolYearA;
            $schoolYearB = substr($schoolYearB, 0, 2);
            if($schoolYearB>=90)
                $schoolYearB = "19".$schoolYearB;
            else
                $schoolYearB = "20".$schoolYearB;
            $selisih = abs((2*$schoolYearA+$semesterA)-(2*$schoolYearB+$semesterB))+1;
            return $selisih;
        }

        function GetColumnExcelNameByIndex($index){ //index dimulai dari 1.
            $idxStart = 64;
            if($index<27){
                return chr($index+$idxStart);
            }else{
                return chr(floor($index/26)+$idxStart).chr($index%26+$idxStart);
            }
        }

        function getExtension($filename){
            $dot = substr (strrchr ($filename, "."), 1);
            return $dot;
        }

        function DisableTimeAndMemoryLimitation(){
            set_time_limit(0);
            ini_set("memory_limit",'-1');
        }



        if (function_exists('GetLang')) {

        } else{
            function GetLang($var,$style='x',$lang="") {
                session_start();
                global $langs;
                if($lang!="")
                    return @DbToStr($langs[$var][$lang],$style);
                else if(isset($langs[$var][$_SESSION['language']]))
                    return @DbToStr($langs[$var][$_SESSION['language']],$style);
                else return @DbToStr($var,$style);
            }
        }


        function getAgeDifference($start_date,$end_date,$month=false,$day=false){
            list($start_date,$start_month,$start_year) = split('-', $start_date);
            list($current_date,$current_month,$current_year) = split('-', $end_date);
             $result = '';

            /** days of each month **/

            for($x=1 ; $x<=12 ; $x++){

                $dim[$x] = date('t',mktime(0,0,0,$x,1,date('Y')));

            }

            /** calculate differences **/

            $m = $current_month - $start_month;
            $d = $current_date - $start_date;
            $y = $current_year - $start_year;

            /** if the start day is ahead of the end day **/

            if($d < 0) {

                $today_day = $current_date + $dim[$current_month];
                $today_month = $current_month - 1;
                $d = $today_day - $start_date;
                $m = $today_month - $start_month;
                if(($today_month - $start_month) < 0) {

                    $today_month += 12;
                    $today_year = $current_year - 1;
                    $m = $today_month - $start_month;
                    $y = $today_year - $start_year;

                }

            }

            /** if start month is ahead of the end month **/

                if($m < 0) {

                $today_month = $current_month + 12;
                $today_year = $current_year - 1;
                $m = $today_month - $start_month;
                $y = $today_year - $start_year;

                }

            /** Calculate dates **/

            if($y < 0) {

                die("Start Date Entered is a Future date than End Date.");

            } else {

                switch($y) {

                    case 0 : $result .= ''; break;
                    case 1 : $result .= $y.($m == 0 && $d == 0 ? ' Tahun' : ' Tahun'); break;
                    default : $result .= $y.($m == 0 && $d == 0 ? ' Tahun' : ' Tahun');

                }
                if($month)
                {
                    switch($m) {

                        case 0: $result .= ''; break;
                        case 1: $result .= ($y == 0 && $d == 0 ? $m.' Bulan' : ($y == 0 && $d != 0 ? $m.' Bulan' : ($y != 0 && $d == 0 ? ' dan '.$m.' Bulan' : ', '.$m.' Bulan'))); break;
                        default: $result .= ($y == 0 && $d == 0 ? $m.' Bulan' : ($y == 0 && $d != 0 ? $m.' Bulan' : ($y != 0 && $d == 0 ? ' dan '.$m.' Bulan' : ', '.$m.' Bulan'))); break;

                    }
                }
                if($day)
                {
                    switch($d) {
                        case 0: $result .= ($m == 0 && $y == 0 ? 'Hari ini' : ''); break;
                        case 1: $result .= ($m == 0 && $y == 0 ? $d.' hari' : ($y != 0 || $m != 0 ? ' dan '.$d.' hari' : '')); break;
                        default: $result .= ($m == 0 && $y == 0 ? $d.' hari' : ($y != 0 || $m != 0 ? ' dan '.$d.' hari' : ''));
                    }
                }


            }

            return $result;

        }

        function RpSatuan($angka,$debug)
        {
            $a_str['1']="satu";
            $a_str['2']="dua";
            $a_str['3']="tiga";
            $a_str['4']="empat";
            $a_str['5']="lima";
            $a_str['6']="enam";
            $a_str['7']="tujuh";
            $a_str['8']="delapan";
            $a_str['9']="sembilan";

            $panjang=strlen($angka);
            for ($b=0;$b<$panjang;$b++)
            {
                $a_bil[$b]=substr($angka,$panjang-$b-1,1);
            }

            if ($panjang>2)
            {
                if ($a_bil[2]=="1")
                {
                    $terbilang=" seratus";
                }
                else if ($a_bil[2]!="0")
                {
                    $terbilang= " ".$a_str[$a_bil[2]]. " ratus";
                }
            }

            if ($panjang>1)
            {
                if ($a_bil[1]=="1")
                {
                    if ($a_bil[0]=="0")
                    {
                        $terbilang .=" sepuluh";
                    }
                    else if ($a_bil[0]=="1")
                    {
                        $terbilang .=" sebelas";
                    }
                    else
                    {
                        $terbilang .=" ".$a_str[$a_bil[0]]." belas";
                    }
                    return $terbilang;
                }
                else if ($a_bil[1]!="0")
                {
                    $terbilang .=" ".$a_str[$a_bil[1]]." puluh";
                }
            }

            if ($a_bil[0]!="0")
            {
                $terbilang .=" ".$a_str[$a_bil[0]];
            }
            return $terbilang;

        }

        function RpTerbilang($angka,$debug)
        {

            $angka = str_replace(".",",",$angka);

            list ($angka, $desimal) = explode(",",$angka);
            $panjang=strlen($angka);
            for ($b=0;$b<$panjang;$b++)
            {
                $myindex=$panjang-$b-1;
                $a_bil[$b]=substr($angka,$myindex,1);
            }
            if ($panjang>9)
            {
                $bil=$a_bil[9];
                if ($panjang>10)
                {
                    $bil=$a_bil[10].$bil;
                }

                if ($panjang>11)
                {
                    $bil=$a_bil[11].$bil;
                }
                if ($bil!="" && $bil!="000")
                {
                    $terbilang .= RpSatuan($bil,$debug)." milyar";
                }

            }

            if ($panjang>6)
            {
                $bil=$a_bil[6];
                if ($panjang>7)
                {
                    $bil=$a_bil[7].$bil;
                }

                if ($panjang>8)
                {
                    $bil=$a_bil[8].$bil;
                }
                if ($bil!="" && $bil!="000")
                {
                    $terbilang .= RpSatuan($bil,$debug)." juta";
                }

            }

            if ($panjang>3)
            {
                $bil=$a_bil[3];
                if ($panjang>4)
                {
                    $bil=$a_bil[4].$bil;
                }

                if ($panjang>5)
                {
                    $bil=$a_bil[5].$bil;
                }
                if ($bil!="" && $bil!="000")
                {
                    $terbilang .= RpSatuan($bil,$debug)." ribu";
                }

            }

            $bil=$a_bil[0];
            if ($panjang>1)
            {
                $bil=$a_bil[1].$bil;
            }

            if ($panjang>2)
            {
                $bil=$a_bil[2].$bil;
            }
            //die($bil);
            if ($bil!="" && $bil!="000")
            {
                $terbilang .= RpSatuan($bil,$debug);
            }
            return trim($terbilang);
        }

        function FormatDate($date,$formatOutput='d, F Y', $formatInput='d-M-y'){
            $dateFormatted = $date;
            $dt = DateTime::createFromFormat($formatInput, $date);
            if($dt){
                $dateFormatted = $dt->format($formatOutput) ;
            }
            return $dateFormatted;
        }

        //input academicYear = 1011,1112, etc. academicSemester = 1/2
        function GetPeriodeDate($academicYear, $academicSemester, $formatDate="Y-m-d"){
            $year = FormatToNormalYear($academicYear);
            //$year=$year+1;
            if(intval($academicSemester)==1){
                $dt = DateTime::createFromFormat($formatDate, "$year-08-01");
            }else{
                $year=$year+1;
                $dt = DateTime::createFromFormat($formatDate, "$year-02-01");
            }
            return $dt->format($formatDate);
        }

        function GetDBDataType(){
            $DBDataType_static = array(
                                    "CHAR",
                                    "VARCHAR",
                                    "NUMBER",
                                    "NUMERIC",
                                    "DATE"
                                );
            return $DBDataType_static;
        }



        function sd_square($x, $mean) {
            return pow($x - $mean,2);
        }

        function std_dev($array) {
        // square root of sum of squares devided by N-1
            return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)) );
        }

            function nvl(&$var, $default = "") {
                return isset($var) ? $var : $default;
            }

            function indoTimestamp($data) {
                $partes = explode('/', $data);
                return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
            }

            function dateDifferent($startDate, $endDate) {
                $startTimeStamp = indoTimestamp($startDate);
                $endTimeStamp = indoTimestamp($endDate);

                $timeDiff = abs($endTimeStamp - $startTimeStamp);

                $numberDays = $timeDiff/86400;  // 86400 seconds in one day

                // and you might want to convert to integer
                return intval($numberDays);
            }

        function isEmpty($str){
            $emptyStr=array(""," ","0");
            if ( in_array($str,$emptyStr)){
                return true;
            }else
                return false;
        }

        function decryptStringArray ($stringArray, $key = "JHJmn778KJbke")
        {
            $s = unserialize(rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode(strtr($stringArray, '-_,', '+/=')), MCRYPT_MODE_CBC, md5(md5($key))), "\0"));
            return $s;
        }

        function encryptStringArray ($stringArray, $key = "JHJmn778KJbke")
        {
            $s = strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), serialize($stringArray), MCRYPT_MODE_CBC, md5(md5($key)))), '+/=', '-_,');
            return $s;
        }




    function isValidEscalationQuery($query){

        $query=strtolower($query);
        if (strpos($query,'insert') !== false) {
            return false;
        }

        if (strpos($query,'update') !== false) {
            return false;
        }

        if (strpos($query,'delete') !== false) {
            return false;
        }

        if (strpos($query,'create') !== false) {
            return false;
        }

        return true;
    }

    function get_public_ip_address()
    {
        // TODO: Add a fallback to http://httpbin.org/ip
        // TODO: Add a fallback to http://169.254.169.254/latest/meta-data/public-ipv4

        $url="simplesniff.com/ip";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }



    /**
     * Get the IP address of the client accessing the website
     *
     * @author     Dotan Cohen
     * @version    2013-07-02
     *
     * @param bool $force_string Force the return of a single address as a string, even if more than one address is found
    True: Always return a string with a single value
    False: Always return an array
    Null (empty): Return a string if a single value, array for multiple values
     *
     * @return bool|string|array
     */
    function get_user_ip_address($force_string=NULL)
    {
        // Consider: http://stackoverflow.com/questions/4581789/how-do-i-get-user-ip-address-in-django
        // Consider: http://networkengineering.stackexchange.com/questions/2283/how-to-to-determine-if-an-address-is-a-public-ip-address

        $ip_addresses = array();
        $ip_elements = array(
            'HTTP_X_FORWARDED_FOR', 'HTTP_FORWARDED_FOR',
            'HTTP_X_FORWARDED', 'HTTP_FORWARDED',
            'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_CLUSTER_CLIENT_IP',
            'HTTP_X_CLIENT_IP', 'HTTP_CLIENT_IP',
            'REMOTE_ADDR'
        );

        foreach ( $ip_elements as $element ) {
            if(isset($_SERVER[$element])) {
                if ( !is_string($_SERVER[$element]) ) {
                    // Log the value somehow, to improve the script!
                    continue;
                }

                $address_list = explode(',', $_SERVER[$element]);
                $address_list = array_map('trim', $address_list);

                // Not using array_merge in order to preserve order
                foreach ( $address_list as $x ) {
                    $ip_addresses[] = $x;
                }
            }
        }

        if ( count($ip_addresses)==0 ) {
            return FALSE;

        } elseif ( $force_string===TRUE || ( $force_string===NULL && count($ip_addresses)==1 ) ) {
            return $ip_addresses;

        } else {
            return $ip_addresses;
        }
    }

    function cleanTableTame($tableName){
        $replace = array("'", ",", " ", "/", "(", ")", ";", ".", "<", ">");
        return str_replace($replace, "_",$tableName);
    }
    
    function GetEmailLowongan($participantName, $vacantName, $tglPanggilan, $vacancyStep) {
        $sHtml = 'Yth ' . $participantName . ",<br/>"
                . "Sehubungan dengan lamaran anda untuk posisi " . $vacantName . ", maka kami bermaksud memanggil anda untuk melakukan
                            " . $vacancyStep . " yang akan diadakan pada : <br/>
                            Tanggal dan jam : " . $tglPanggilan . "<br/>
                            Tempat          : Kantor LPDP, Gd Ali Wardhana lt.2, Jl Lapangan Banteng Timur No.1, Jakarta Pusat <br/>
                            Tahap           : " . $vacancyStep . "<br/>";
        return $sHtml;
    }

    ?>

