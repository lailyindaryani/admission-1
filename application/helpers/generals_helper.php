<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
* @link http://stackoverflow.com/questions/1416697/converting-timestamp-to-time-ago-in-php-e-g-1-day-ago-2-days-ago
* @author Glavić, Sep 3 '13 at 22:24
* @example 
	echo time_elapsed_string('2013-05-01 00:22:35');
	echo time_elapsed_string('@1367367755'); # timestamp input
	echo time_elapsed_string('2013-05-01 00:22:35', true);
*
**/
if(!function_exists('time_elapsed')) 
{
	function time_elapsed($datetime, $full = false) {
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}

if(!function_exists('rupiah_format')) 
{
	function rupiah_format($money = 0) {
	    return 'Rp. ' . number_format($money, 2, ",", ".");
	}
}