<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct(){
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	  }
	public function index()
	{
        $this->load->model('countrymodel');
        $this->load->model('couponmodel');
        $this->load->model('couponmappingmodel');
        $data['program_type'] = strtolower($this->uri->segment(2));
		$coupon_id = !empty($_GET['ticket']) ? $_GET['ticket'] : null;
		$coupon_data = $this->couponmodel->GetCouponById($coupon_id);
        $data['coupon_id'] = '';
        if(!empty($coupon_data)) {
        	$data['coupon_id'] = $coupon_id;	
        	$data['listCountry'] = $this->couponmappingmodel->GetAllCountryByCoupon($coupon_id); 
        } else if($data['program_type']=='non-academic'){
			$data['listCountry'] = $this->countrymodel->getCountryExcept(65);//except indonesia
		}else{
        	$data['listCountry'] = $this->countrymodel->GetAllCountry();
        }
		//echo "<pre>"; print($data['program_type']);die;
		//echo "<pre>"; print_r($data['listCountry']);die;
		$this->load->view('registration', $data);
	}

    public function registrationProcess()
    {
		//$this->sendEmail($post_data['email'], 'Account Activation', $textBody, $post_data['fullname']);
		
        $this->load->model('usermodel');
        $this->load->model('participantmodel');
        $post_data = $this->input->post();
        //print_r($post_data);die();
        if($this->input->post('register')){
			$cek_email = $this->usermodel->cek_user($post_data['email']);
            $confirmationKey = md5($post_data['email'] . 'hus8e0ldj' . time());
			if($cek_email=='TIDAK ADA'){
				$response=$this->usermodel->Register($post_data['email'], $post_data['fullname'],$post_data['password'], $confirmationKey);
				$insert_id = $this->db->insert_id();
				//echo "<pre>"; print($insert_id);die;
				$participant_data = array(
                    'USERID'    => $insert_id,
                    'FULLNAME'  => $post_data['fullname'],
                    'NATIONALITY'  => $post_data['nationality'],
                    'PHONE'     => $post_data['phone_ext'] . "" . $post_data['phone'],
                    'CURRENTSTEP'   => 1,
                    'PROGRAMTYPE'   => strtoupper(str_replace('-', '_', $post_data['program_type'])),
                    'INPUTDATE' => date('Y-m-d H:i:s')
                );

				$this->participantmodel->insert($participant_data);
			}
            
            if($response || $cek_email=='TIDAK ADA') {
                $link     = base_url().'login/activation?key='.$confirmationKey.'&id='.$insert_id;
				if(substr($link,0,4)!='http'){
					$link = 'https://'.$link;
				}
                $textBody = '<html>
				<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
				<body><p style="text-align:center;">Telkom University</p>
				<p style="text-align:center;">Registration for Candidates Students</p>
				Dear Applicant,<br>
				Thank you for your application to Telkom University. This is your activation account email.<br>
				Here are your log in data:<br>
				Name		: '.$post_data['fullname'].'<br>
				Email		: '.$post_data['email'].'<br>
				Phone		: '.$post_data['phone_ext'] . '' . $post_data['phone'].'<br>
				Username	: '.$post_data['fullname'].'<br>
				Password	: '.$post_data['password'].'<br>
				
				Click the "Activation" button bellow to activate your registration account.<br>
				<a href="'.$link.'" target="_blank" style="text-align: center;">
                          <img src="http://cdndata.telkomuniversity.ac.id/button2.jpg" style="display: block; margin-left: auto; margin-right: auto; " width="200px" height="50px">      
                      </a><br>
					  
				<img src="http://cdndata.telkomuniversity.ac.id/activate.jpg" style="display: block; margin-left: auto; margin-right: auto; " width="500px" height="250px"></body></html>';
                $this->sendEmail($post_data['email'], 'Account Activation', $textBody, $post_data['fullname']);
                $this->session->set_flashdata('msg_success', 'CREATE DATA SUCCESSFULL, PLEASE CHECK YOUR INBOX or SPAM in EMAIL REGISTERED FOR ACTIVATION'); 
            }else if($cek_email=='ADA'){
				$this->session->set_flashdata('msg_error', 'SORRY, YOUR EMAIL ALREADY EXIST. PLEASE USE OTHER EMAIL'); 
			}
        redirect(base_url().'registration/'.$post_data['program_type']);
		}
    }

    public function httpPostBasicAuth($url, $fields, $username, $password) {
         if (is_array($fields)) {
            $postvars = http_build_query($fields, NULL, '&');
        }
        //open connection
        $ch = curl_init();	
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password); //BASIC AUTH
		//curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-type: text/html; charset=iso-8859-1' . "\r\n"));
		//curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
		//curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars); //HTTP POST
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //execute post
        $result = curl_exec($ch);
		
		if (curl_errno($ch)) {
			$result = curl_error($ch);
		}
		
        //close connection
        curl_close($ch);

        return $result;
    }

    public function sendEmail($sendTo, $subject, $textBody, $name)
    {
		$url ='https://api.telkomuniversity.ac.id/email/send';//'10.252.252.43/email/send';
		$type = 'gmail.com';
		if(strtolower(substr($sendTo, strlen($sendTo) - strlen($type), strlen($type))) == strtolower($type)){
            $fields = array(
                'to' => $sendTo,
                'subject' => $subject,
                'content' => nl2br($textBody)
            );
		}else{
			$url ='https://api.telkomuniversity.ac.id/email/send';//'10.252.252.43/email/send';
            $fields = array(
                'to' => $sendTo,
                'subject' => $subject,
                'content' => nl2br($textBody),
				'operatorid' => 4
            );
		}
		//for basic auth
		$result = $this->httpPostBasicAuth($url, $fields, "ioffice", "Cent4ur");
    }

}
