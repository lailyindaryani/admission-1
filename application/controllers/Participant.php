<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participant extends CI_Controller {
    private $data;
    private $session_user;

    public function __construct(){
        parent::__construct();
        $this->isLoginParticipant();
        date_default_timezone_set('Asia/Jakarta');

        $data=array(
            "pagetitle"=>"",
            "msg"=>"",
            "menu"=>"participant/_menu.php",

        );
        $this->data=$data;
        
        $users=$this->session->userdata('users');        
        $this->session_user = $users[0];

         $this->load->model('coursemodel');
          $this->load->model('courselanguagemodel');
        $this->load->model('programmodel');
        $this->load->model('degreemodel');
        $this->load->model('enrollmentmodel');
        $this->load->model('countrymodel');
        $this->load->model('languagedeliverymodel');
        $this->load->model('participantmodel');
        $this->load->model('languagedeliverymappingmodel');
        $this->load->model('familymodel');
        $this->load->model('quizmodel');
        $this->load->model('studyprogrammodel');
        $this->load->model('studyprogramlanguagemodel');

        $this->data['participant']  = $this->participantmodel->participantData($this->session_user['USERID']);
        //print_r($this->data['participant']);
        ini_set('memory_limit', '200M' );
        ini_set('upload_max_filesize', '200M');  
        ini_set('post_max_size', '200M');  
        ini_set('max_input_time', 3600);  
        ini_set('max_execution_time', 3600);

        $this->datatableConfig = array(
            "DB_DISPLAY_ERROR"=>false,
            "DB_BREAK_PROCESS"=>false,
            "DB_TYPE"=>"mysql",
            "DB_USERNAME"=>$this->db->username,
            "DB_PASSWORD"=>$this->db->password,
            "DB_HOST"=>$this->db->hostname,
            "DB_NAME_MYSQL"=>$this->db->database,
        );
    }

    public function index()
    {
        $participantId = $this->quizmodel->getParticipantId($this->session_user['USERID']);
        $partisipantQuiz = $this->participantmodel->getPartisipantQuiz($participantId[0]['PARTICIPANTID']);
        
        if(empty($partisipantQuiz)){
            $this->data['listQuestions'] = $this->quizmodel->getQuestionList();
            

            $html = "";
            $html .= "<ol type='1' >";
            for ($i=0; $i< sizeof($this->data['listQuestions']); $i++){
                $html .= "<li style='line-height: 25px;'>" . "<span style='font-size: 1.1em;'>".$this->data['listQuestions'][$i]['QUESTION']."</span>";
                $this->data['listAnswer'] = $this->quizmodel->getAnswerList($this->data['listQuestions'][$i]['QUESTIONID']);
                
                for ($x=0; $x< sizeof($this->data['listAnswer']); $x++) {
                    $html .="<label class='fancy-radio custom-color-green'>" .
                        "<input type='radio' required name='answer".$this->data['listQuestions'][$i]['QUESTIONID']."[]' value='".$this->data['listAnswer'][$x]['OPTIONVALUE']."'>" .
                        "<span><i></i>" . $this->data['listAnswer'][$x]['OPTIONVALUE'] . "</span>" .
                        "</label>";
                        
                }
                
                $html .=    "</li><br>";
            }
            $html .= "</ol>";
            $this->data['views'] = $html;
            $this->load->view('questionnaire', $this->data);

        } else{
            $participantId = $this->quizmodel->getParticipantId($this->session_user['USERID']);
            $currentStep =  $this->participantmodel->participantDatabyParticipantId($participantId[0]['PARTICIPANTID']);
            if($currentStep['CURRENTSTEP'] == 1) {
                redirect('/participant/selectProgram');
            } else if($currentStep['CURRENTSTEP'] == 2) {
                redirect('/participant/personalData');
            } else if($currentStep['CURRENTSTEP'] == 3) {
                redirect('/participant/familyData');
            } else if($currentStep['CURRENTSTEP'] == 4) {
                redirect('/participant/educationData');
            } else if($currentStep['CURRENTSTEP'] == 5) {
                redirect('/participant/supportingData');
            } else if($currentStep['CURRENTSTEP'] == 6) {
                redirect('/participant/summaryData');
            } else if($currentStep['CURRENTSTEP'] == 7) {
                redirect('/participant/AcceptanceStatus');
            } else {
                redirect('/participant/selectProgram');
            }
        }
    }

    public function selectProgram(){
    //      echo "<pre>"; print($this->session_user['USERID']);
    // echo "<pre>"; print_r($_SESSION);  
    // echo "<pre>"; print($this->session_user['USERID']);       die;
        $this->data['pagetitle'] = "IO Tel-U | Program Selection";
        $this->data['active'] = 1;
        if($this->data['participant']['ACCEPTANCESTATUS']!=''){
            $this->data['listEnrollment'] = $this->enrollmentmodel->getActiveEnrollment();
        }else{
            $this->data['listEnrollment'] = $this->enrollmentmodel->getAllData();
        }        
        $this->data['listProgram'] = $this->programmodel->GetAllData($this->data['participant']['PROGRAMTYPE']);
        $this->data['listLanguageDelivery'] = $this->languagedeliverymodel->GetAllData();
        $this->data['listDegree']           = $this->degreemodel->GetAllData();
        $this->data['listStudyProgram']       =$this->studyprogrammodel->GetAllData();
        $this->data['languageDeliveryId'] = $this->languagedeliverymappingmodel->GetLanguageDeliveryIdById($this->data['participant']['LANGUAGEDELIVERYMAPPINGID']);
        $this->data['listCourse'] = $this->coursemodel->GetAllData();
        //$this->data[]
        //echo "<pre>"; print_r($this->data['participant']);die;
        $this->load->view('_header', $this->data);
        $this->load->view('participant/programSelection', $this->data);
        $this->load->view('_footer');


    }

    public function savequestionnaire(){
        
        $post_data = $this->input->post();
        ksort($post_data);
        $x= $this->quizmodel->getMinQuiestionId();
        $participantId = $this->quizmodel->getParticipantId($this->session_user['USERID']);
        
        foreach($x as $key){
            $qId = substr('answer'.$key['QUESTIONID'],6,1);
            $questionanswer['QUESTIONID'] = $qId;
            $questionanswer['ANSWER'] = $post_data['answer'.$key['QUESTIONID']][0];
            $questionanswer['ANSWERADDVALUE'] = "";
            $questionanswer['PARTICIPANTID'] = $participantId[0]['PARTICIPANTID'];
           $this->quizmodel->insert($questionanswer);
        }
        $quizParticipant['PARTICIPANTID'] = $participantId[0]['PARTICIPANTID'];
        $quizParticipant['DATE'] = date("Y-m-d");
        $quizParticipant['STATUS'] = '1';
        $this->quizmodel->insertQuizParticipant($quizParticipant);

        redirect(base_url().'participant/selectProgram/');
    }

    public function personalData()
    {
        $this->createRegisNumber();
        $this->data['pagetitle']="IO Tel-U | Personal Data";
        $this->data['active'] = 2;
        $this->data['participant']            = $this->participantmodel->participantData($this->session_user['USERID']);
        $this->data['user']                   = $this->session_user;
        if($this->data['participant']['PROGRAMTYPE']=='NON_ACADEMIC'){
            $this->data['listCountry']            = $this->countrymodel->getCountryExcept(65);
        }else{
            $this->data['listCountry']            = $this->countrymodel->GetAllCountry();
        }        
        $this->data['listDegree']             = $this->degreemodel->GetAllDegree();
        $this->data['listLanguageDelivery']   = $this->languagedeliverymodel->GetAllData();
        $this->data['listStudyProgram']       = NULL;
        $this->data['participantDetail']=$this->participantmodel->GetParticipantDetail($this->data['participant']['PARTICIPANTID']);
        $this->load->view('_header',$this->data);
        $this->load->view('participant/personalData',$this->data);
        $this->load->view('_footer');
    }

    public function familyData()
    {
        $this->createRegisNumber();
        $this->data['pagetitle']="IO Tel-U | Family Data";
        $this->data['active'] = 3;

        //$this->_currentStep($this->data['participant']['CURRENTSTEP'], $this->data['active']);

        $this->data['listCountry']            = $this->countrymodel->GetAllCountry();
        //$this->data['participant']            = $this->participantmodel->participantData($this->session_user['USERID']);
        $this->data['user']                   = $this->session_user;
        if(empty($this->data['participant'])) redirect(base_url()."participant/selectProgram");
        
        $this->data['father']   = $this->familymodel->fatherData($this->data['participant']['PARTICIPANTID']);
        $this->data['mother']   = $this->familymodel->motherData($this->data['participant']['PARTICIPANTID']);
        
        $this->load->view('_header',$this->data);
        $this->load->view('participant/familyData',$this->data);
        $this->load->view('_footer');
    }

    public function educationData()
    {
        $this->createRegisNumber();
        $this->data['pagetitle']="IO Tel-U | Education Data";
        $this->data['active'] = 4;

        $this->load->model('educationmodel');
        if($this->data['participant']['PROGRAMTYPE']=='ACADEMIC'){
            $participantDetail = $this->participantmodel->GetParticipantDetail($this->data['participant']['PARTICIPANTID']);
        }else{
            $participantDetail = $this->participantmodel->GetParticipantDetailNonAcademic($this->data['participant']['PARTICIPANTID']);
        }

        if(empty($this->data['participant'])) redirect(base_url()."participant/selectProgram");
        $this->data['degree'] = $participantDetail;
        $this->data['highschool']   = $this->educationmodel->highschoolData($this->data['participant']['PARTICIPANTID']);
        $this->data['university']   = $this->educationmodel->universityData($this->data['participant']['PARTICIPANTID']);
        //echo "<pre>"; print_r($this->data);die;
        $this->load->view('_header',$this->data);
        $this->load->view('participant/educationData',$this->data);
        $this->load->view('_footer');
    }

    public function supportingData()
    {
        $this->createRegisNumber();
        $this->data['pagetitle']="IO Tel-U | Requirement Docs";
        $this->data['active'] = 5;

        //$this->_currentStep($this->data['participant']['CURRENTSTEP'], $this->data['active']);

        $this->load->model('requirementsmodel');
        $this->load->model('requirementmappingmodel');
        $this->load->model('requirementsvaluemodel');
        $this->load->model('enrollmentmodel');

        if(empty($this->data['participant'])) redirect(base_url()."participant/selectProgram");

        $enrollment_data = $this->enrollmentmodel->getEntrollmentById($this->data['participant']['ENROLLMENTID']);
        $this->data['requirements']    = $this->requirementmappingmodel->getAllRequirementParticipant($enrollment_data['PROGRAMDEGREEID']);

        $reqvaltemp      = $this->requirementsvaluemodel->GetAllData($this->data['participant']['PARTICIPANTID']);
        if(!empty($reqvaltemp)) {
            foreach ($reqvaltemp as $reqval) {
                $this->data['requirementvalue'][$reqval['REQUIREMENTID']] = $reqval;
            }
        } else {
            $this->data['requirementsvalue'] = array();    
        }
        //echo "<pre>"; print_r($this->data);die;
        $this->load->view('_header',$this->data);
        $this->load->view('participant/supportingData',$this->data);
        $this->load->view('_footer');
    }

    public function summaryData()
    {
        $this->createRegisNumber();
        $this->data['pagetitle']="IO Tel-U | Participant Card";
        $this->data['active'] = 6;
        $this->load->model('educationmodel');
        $this->load->model('requirementsvaluemodel');
        $participantId = $this->quizmodel->getParticipantId($this->session_user['USERID']);
        if($this->data['participant']['PROGRAMTYPE']=='ACADEMIC'){
            $this->data['participantDetail'] = $this->participantmodel->GetParticipantDetail($participantId[0]['PARTICIPANTID']);
        }else{
            $this->data['participantDetail'] = $this->participantmodel->GetParticipantDetailNonAcademic($participantId[0]['PARTICIPANTID']);
        }
        
        $this->data['personaldata'] = $this->participantmodel->getParticipantData($this->session_user['USERID']);
        $this->data['educationdata'] = $this->educationmodel->getEducation($participantId[0]['PARTICIPANTID']);
        $this->data['familydata'] = $this->familymodel->getFamilyOfUser($participantId[0]['PARTICIPANTID']);
        $this->data['requirementdata'] = $this->requirementsvaluemodel->GetDataByParticipantId($participantId[0]['PARTICIPANTID']);
        $this->data['program'] = $this->programmodel->getProgramById($this->data['participant']['PROGRAMID'])->row_array();
        $this->data['enrollment'] = $this->enrollmentmodel->getEntrollmentById($this->data['participant']['ENROLLMENTID']);
        $this->data['studyProgram'] = $this->studyprogramlanguagemodel->GetProgramDegreeByIdLanguage($this->data['participant']['LANGUAGEDELIVERYMAPPINGID']);
        $this->data['degree'] = substr($this->data['studyProgram']['STUDYPROGRAMNAME'],0,3);
        $this->data['languageDelivery'] = $this->languagedeliverymodel->GetLanguageDeliveryByIdLanguageDeliveryMapping($this->data['participant']['LANGUAGEDELIVERYMAPPINGID']);
       // 
        $this->load->view('_header',$this->data);
        $this->load->view('participant/summaryData',$this->data);
        $this->load->view('_footer');
    }
    public function interviewData()
    {
        $this->data['pagetitle']="IO Tel-U | Interview Report";
        //$this->data['active'] = 7;
        
        $this->load->view('_header',$this->data);
        $this->load->view('participant/interviewReport',$this->data);
        $this->load->view('_footer');
    }

    public function AcceptanceStatus()
    {
        $this->createRegisNumber();
        $this->data['participantDetail']=$this->participantmodel->GetParticipantDetail($this->data['participant']['PARTICIPANTID']);
        $participantid = $this->data['participant']['PARTICIPANTID'];
        $qrequirements = "SELECT COUNT(REQUIREMENTID) AS JUMLAH FROM requirements
                            LEFT JOIN (SELECT * FROM requirementvalue 
                              WHERE PARTICIPANTID=$participantid) AS requirementvalue
                              USING (REQUIREMENTID)
                            WHERE PRIVILEGE IN (1,3) AND REQUIREMENTID IN (
                                SELECT REQUIREMENTID
                                FROM requirementmapping
                                JOIN programdegree USING (PROGRAMDEGREEID)
                                JOIN enrollment USING (PROGRAMDEGREEID)
                                JOIN participants USING (ENROLLMENTID)
                                WHERE PARTICIPANTID=$participantid AND VERIFICATIONSTATUS='UNVERIFIED'
                            )";

        $requirements = $this->db->query($qrequirements)->row_array();
        if($this->data['participantDetail']['INTERVIEWSCORE']==0 && $this->data['participantDetail']['INTERVIEWSTATUS']=='' && $this->data['participantDetail']['PROGRAMTYPE']=='ACADEMIC'){
            $this->session->set_flashdata('msg_warning', 'YOU DO NOT HAVE TO ACCESS NEXT STEP. PLEASE CHECK YOUR INTERVIEW DATE IN THE DATA BELOW AND COME TO INTERVIEW TEST');
            redirect(base_url()."participant/summaryData");
        }else if($this->data['participantDetail']['PROGRAMTYPE']=='NON_ACADEMIC' && $requirements['JUMLAH']!=0){
            $this->session->set_flashdata('msg_warning', 'YOU DO NOT HAVE TO ACCESS NEXT STEP. YOUR APPLICATION IS STILL IN PROCESS NOW');
            redirect(base_url()."participant/summaryData");
        }else{
        $response = $this->participantmodel->update($this->session_user['USERID'], array('CURRENTSTEP' => 7));
        $this->data['pagetitle']="IO Tel-U | Acceptance Status";
        $this->data['active'] = 7;
        $this->data['todayDate'] = date('Y-m-d');
        $this->data['todayTime'] = date('H:i:s');
        if(empty($this->data['participant'])) redirect(base_url()."participant/selectProgram");
        //echo "<pre>"; print_r($this->data);die;
        $this->load->view('_header',$this->data);
        
        $this->load->view('participant/acceptanceStatus',$this->data);
        $this->load->view('_footer');
        }
    }

    public function enrollSubMapping(){
        $this->load->model('exchangemodel');
        //echo "<pre>"; print_r($_POST);
        $datax = $this->participantmodel->getSubjectMapping($this->data['participant']['PARTICIPANTID']);
        if(count($datax)>0 && $_POST['index']==0){
            $this->exchangemodel->deleteEnrollMapping($this->data['participant']['PARTICIPANTID']);
        }
        $data = array(
            'SUBJECTCODE' => $_POST['subjectcode'],
            'CURICULUMYEAR' => $_POST['curiculum'],
            'STUDYPROGRAMID' => $_POST['studyprogramid'],
            'ENROLLMENTID' => $_POST['enrollmentid'],
            'PARTICIPANTID' => $this->data['participant']['PARTICIPANTID']
        );
       // echo "<pre>"; print_r($data);
        $return = $this->exchangemodel->insert('enrollexchangemapping', $data);
        //echo "<pre>"; print_r($return);
        echo json_encode($_POST['index']);
        //echo json_encode("success");
    }

    public function selectionProgramProcess()
    {
        
        $post_data = $this->input->post();      
        $this->load->model('participantmodel');
        //echo "<pre>"; print_r($_POST);die;
        if(isset($_POST) && $_POST['acceptstatus']==''){
           //die;// echo "<pre>"; print_r($_POST);//die;
            $is_exists = $this->participantmodel->exists($this->session_user['USERID']);
            //echo "<pre>"; print_r($is_exists);
            if(!isset($post_data['enrollment_id'])){
                $enrollment_id = $post_data['after_enrollmentid'];
            }else{
                $enrollment_id = $post_data['enrollment_id'];
            }
                        
            $year= $this->enrollmentmodel->getEntrollmentById($enrollment_id);
            $today_year = $year['ACADEMICYEAR'];
            
            //create REGISTRATION NUMBER 
            $last_admissionid = $this->participantmodel->GetAdmissionId();
            $admissionid = $last_admissionid['ADMISSIONID']+1;
            if((ceil(log10($last_admissionid['ADMISSIONID']))==1 && $last_admissionid['ADMISSIONID']!=9 && $last_admissionid['ADMISSIONID']!=10) || $last_admissionid['ADMISSIONID']==1 || $last_admissionid['ADMISSIONID']==0){
                $admissionid = '000'.$admissionid;
            }else if(((ceil(log10($last_admissionid['ADMISSIONID'])))==2 && $last_admissionid['ADMISSIONID']!=99 && $last_admissionid['ADMISSIONID']!=100) || $last_admissionid['ADMISSIONID']==9 || $last_admissionid['ADMISSIONID']==10){
                $admissionid = '00'.$admissionid;
            }else if((ceil(log10($last_admissionid['ADMISSIONID']))==3 && $last_admissionid['ADMISSIONID']!=999 && $last_admissionid['ADMISSIONID']!=1000) || $last_admissionid['ADMISSIONID']==99 || $last_admissionid['ADMISSIONID']==100){
                $admissionid = '0'.$admissionid;
            }else if(ceil(log10($last_admissionid['ADMISSIONID']))==4 || $last_admissionid['ADMISSIONID']==999 || $last_admissionid['ADMISSIONID']==1000){  
                $admissionid = $admissionid;
            }
            if(!isset($post_data['program_id'])){
                $program_id = $post_data['after_programid'];                
            }else{
                $program_id = $post_data['program_id'];
            }
            $registration_number=$today_year.'0'.$program_id.$admissionid;
            $participan_data = array(
                'PROGRAMID' => $program_id,
                'ACADEMICYEAR' => $year['ACADEMICYEAR'],
                'ENROLLMENTID' => $enrollment_id,               
                'UPDATEDATE' => date('Y-m-d H:i:s')
            );
            if(!isset($post_data['study_program_id'])){
                $study_program_id = $post_data['after_studyprogramid'];             
            }else{
                $study_program_id = $post_data['study_program_id'];
            }
            if(!isset($post_data['study_course_id'])){
                $course_id = $post_data['after_courseid'];              
            }else{
                $course_id = $post_data['study_course_id'];
            }
            $statussubjectsetting = $post_data['statussubjectsetting'];
            if($this->data['participant']['PROGRAMTYPE']=='ACADEMIC'){
                if($statussubjectsetting!='Y'){
                    $language_delivery_mapping_id = $this->studyprogramlanguagemodel->GetLanguageDeliveryMappingByStudyPogramId($study_program_id);
                    $participan_data['LANGUAGEDELIVERYMAPPINGID'] = $language_delivery_mapping_id['LANGUAGEDELIVERYMAPPINGID'];
                    $participan_data['STUDYPROGRAMID'] = $study_program_id;
                }else{
                    $participan_data['LANGUAGEDELIVERYMAPPINGID'] =NULL;
                    $participan_data['STUDYPROGRAMID'] = 0;
                }                
                
            }else{
                $language_delivery_mapping_id = $this->courselanguagemodel->GetLanguageDeliveryMappingByCourseId($course_id);
                $participan_data['LANGUAGEDELIVERYMAPPINGID'] = $language_delivery_mapping_id['LANGUAGEDELIVERYMAPPINGID'];
                $participan_data['COURSEID'] = $course_id;
            }
            //echo "<pre>"; print_r($this->data['participant']['PROGRAMID']);
            
            if($this->data['participant']['PROGRAMID']!=$program_id || strlen($this->data['participant']['ADMISSIONID'])!=10){
                $participan_data['ADMISSIONID'] = $registration_number;
            }
            //echo "<pre>"; print_r($participan_data);die;
            if(!$is_exists) {

                $participan_data['USERID'] = $this->session_user['USERID'];
                $participan_data['CURRENTSTEP'] = 2;
                
                $response = $this->participantmodel->insert($participan_data);
                //echo "<pre>"; print($this->participantmodel->insert($participan_data));
                $message = "INSERT DATA SUCCESSFULL";
            } else {
                if($is_exists[0]['CURRENTSTEP'] < 2) $participan_data['CURRENTSTEP'] = 2;
                //echo "<pre>"; print_r($participan_data);die;
                $response = $this->participantmodel->update($this->session_user['USERID'], $participan_data);
                //echo "<pre>"; print($this->participantmodel->update($this->session_user['USERID'], $participan_data));
                $message = "UPDATE DATA SUCCESSFULL";
            }
            // echo "<pre>"; print_r($participan_data);//die;
            // echo "<pre>"; print($response);//die;
            // echo "<pre>"; print_r($response);die;
            if($response) $this->session->set_flashdata('msg_success', $message);
                else $this->session->set_flashdata('msg_failed', 'FAILED UPDATE DATA');
        
            redirect(base_url()."participant/personalData");
        } else if($_POST['acceptstatus']!=''){
            redirect(base_url()."participant/personalData");
        }  

    }

    public function personalDataProcess()
    {
        $this->createRegisNumber();
        $post_data = $this->input->post();
        $this->load->model('participantmodel');
        if($this->input->post()) {
            
            $new_name = $post_data['passport_number'];
            /*if (!is_dir('uploads/' . $new_name)) {
                mkdir('./uploads/' . $new_name, 0777, TRUE);
            }*/
            if (!is_dir('uploads/media/participant/requirement/' . $new_name)) {
                mkdir('./uploads/media/participant/requirement/' . $new_name, 0777, TRUE);
            }
            $foto_name_first=str_replace('.','_',$_FILES['userfile']['name']);
            $foto=str_replace(' ','_',$foto_name_first);
            $path = $_FILES['userfile']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $config = array(
                'upload_path' => "./uploads/media/participant/requirement/". $new_name."/",//"./uploads/media/participant/requirement/". $new_name."/",
                'allowed_types' => "gif|jpg|png",
                'overwrite' => TRUE,
                //'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                'file_name' => $foto.".".$ext,//"photo_".$new_name.".".$ext,
                'access_path' => "/uploads/media/participant/requirement/". $new_name."/"
                //'max_height' => "768",
                //'max_width' => "1024"
            );
            $this->load->helper(array('form', 'url'));
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('userfile')){
                $dataf = array('error' => $this->upload->display_errors());
            }else{
                $dataf = array('upload_data' => $this->upload->data());
            }   
            
            if(!empty($_FILES['userfile']['name'])){
                $participan_data = array(
                'FULLNAME' => $post_data['fullname'],
                'PASSPORTNO' => $post_data['passport_number'],
                'PASSPORTVALID' => $post_data['pass_valid_until'],
                'PASSPORTISSUED' => $post_data['pass_issued_by'],
                'BIRTHPLACE' => $post_data['place_of_birth'],
                'BIRTHDATE' => $post_data['date_of_birth'],
                'GENDER' => $post_data['sex'],
                'MARITALSTATUS' => $post_data['marital_status'],
                'RELIGION' => $post_data['religion'],
                'NATIONALITY' => $post_data['nationality'],
                'FIRSTLANGUAGE' => $post_data['first_language'],
                'PROFICIENCYEN' => $post_data['profiency_in_english'],
                'PROFICIENCYINA' => $post_data['profiency_in_indonesia'],
                'ADDRESS' => $post_data['mailing_address'],
                'CITY' => $post_data['city'],
                'COUNTRYID' => $post_data['country_id'],
                'ZIPCODE' => $post_data['zip_code'],
                'PHONE' => $post_data['mobile_phone'],
                'FAX' => $post_data['fax_number'],
                'PHOTOURL' => $config['upload_path'],
                'PHOTOFILENAME' => $config['file_name'],
                'UPDATEDATE' => date('Y-m-d H:i:s'),
                );
            }else{
                $participan_data = array(
                'FULLNAME' => $post_data['fullname'],
                'PASSPORTNO' => $post_data['passport_number'],
                'PASSPORTVALID' => $post_data['pass_valid_until'],
                'PASSPORTISSUED' => $post_data['pass_issued_by'],
                'BIRTHPLACE' => $post_data['place_of_birth'],
                'BIRTHDATE' => $post_data['date_of_birth'],
                'GENDER' => $post_data['sex'],
                'MARITALSTATUS' => $post_data['marital_status'],
                'RELIGION' => $post_data['religion'],
                'NATIONALITY' => $post_data['nationality'],
                'FIRSTLANGUAGE' => $post_data['first_language'],
                'PROFICIENCYEN' => $post_data['profiency_in_english'],
                'PROFICIENCYINA' => $post_data['profiency_in_indonesia'],
                'ADDRESS' => $post_data['mailing_address'],
                'CITY' => $post_data['city'],
                'COUNTRYID' => $post_data['country_id'],
                'ZIPCODE' => $post_data['zip_code'],
                'PHONE' => $post_data['mobile_phone'],
                'FAX' => $post_data['fax_number'],
                'PHOTOURL' => $config['upload_path'],
                'UPDATEDATE' => date('Y-m-d H:i:s'),
            );
            }
                        
            if ($this->data['participant']['CURRENTSTEP'] < 3){
                $participan_data['CURRENTSTEP'] = 3;
            }

            $response = $this->participantmodel->update($this->session_user['USERID'], $participan_data);

            if ($response) {
                $this->session->set_flashdata('msg_success', 'UPDATE DATA SUCCESSFULL');
                //echo "<script>window.location.href='".base_url() . "participant/familyData"."'</script>";
                redirect(base_url() . "participant/familyData");
            } else {
                $this->session->set_flashdata('msg_failed', 'FAILED UPDATE DATA');
                redirect(base_url() . "participant/personalData");
            }
        }else{
            redirect(base_url()."participant/programSelection");
        }        
    }

    public function familyDataProcess()
    {
        $this->createRegisNumber();
        $this->load->model('familymodel');
        $this->load->model('CountryModel');
        
        $post_data = $this->input->post();
        
        if($this->input->post()){
            
            $father_exists = $this->familymodel->fatherData($this->data['participant']['PARTICIPANTID']);
            $mother_exists = $this->familymodel->motherData($this->data['participant']['PARTICIPANTID']);
            
            //get father country id
            $father_country_id = $this->countrymodel->GetCountryByName($post_data['father_country']);
            $father_data    = array(
                    'FULLNAME'  =>            $post_data['father_name'],
                    'OCCUPATION'  =>            $post_data['father_occupation'],
                    'ADDRESS'  =>            $post_data['father_address'],
                    'CITY'  =>            $post_data['father_city'],
                    'COUNTRY'  =>            $post_data['father_country'],
                    'ZIPCODE'  =>            $post_data['father_zipcode'],
                    'PHONE'  =>            $post_data['father_phone'],
                    'EMAIL'  =>            $post_data['father_email'],
                    'COUNTRYID' => $father_country_id['COUNTRYID'],
                    'INPUTDATE' => date('Y-m-d H:i:s'),
                    'INPUTBY'   => $this->session_user['USERID']
                );  
                
            if(!empty($father_exists)){
                $father_data['UPDATEDATE']  = date('Y-m-d H:i:s');
                $father_data['UPDATEBY']  = $this->session_user['USERID'];
                $response_father = $this->familymodel->update($father_exists['FAMILYID'], $father_data);
            } else {
                $father_data['PARTICIPANTID']   = $this->data['participant']['PARTICIPANTID'];
                $father_data['RELATIONSHIP']    = 'FATHER';
                $response_father = $this->familymodel->insert($father_data);
            }
            
            //get mother country id
            $mother_country_id = $this->countrymodel->GetCountryByName($post_data['mother_country']);
            $mother_data    = array(
                    'FULLNAME'  =>            $post_data['mother_name'],
                    'OCCUPATION'  =>            $post_data['mother_occupation'],
                    'ADDRESS'  =>            $post_data['mother_address'],
                    'CITY'  =>            $post_data['mother_city'],
                    'COUNTRY'  =>            $post_data['mother_country'],
                    'ZIPCODE'  =>            $post_data['mother_zipcode'],
                    'PHONE'  =>            $post_data['mother_phone'],
                    'EMAIL'  =>            $post_data['mother_email'],
                    'COUNTRYID' => $mother_country_id['COUNTRYID'],
                    'INPUTDATE' => date('Y-m-d H:i:s'),
                    'INPUTBY'   => $this->session_user['USERID']
                );

            if(!empty($mother_exists)){
                $mother_data['UPDATEDATE']  = date('Y-m-d H:i:s');
                $mother_data['UPDATEBY']  = $this->session_user['USERID'];
                $response_mother = $this->familymodel->update($mother_exists['FAMILYID'], $mother_data);
            } else {
                $mother_data['RELATIONSHIP']    = 'MOTHER';
                $mother_data['PARTICIPANTID']   = $this->data['participant']['PARTICIPANTID'];
                $response_mother = $this->familymodel->insert($mother_data);
            }


            if($response_father && $response_mother){
                if($this->data['participant']['CURRENTSTEP'] < 4){
                    $this->participantmodel->update($this->session_user['USERID'], array('CURRENTSTEP' => 4));
                };
                $this->session->set_flashdata('msg_success', 'UPDATE DATA SUCCESSFULL');
                redirect(base_url()."participant/educationData");
            }  else {
                $this->session->set_flashdata('msg_failed', 'FAILED UPDATE DATA');
                redirect(base_url()."participant/familyData");
            }   
        }
        redirect(base_url()."participant/programSelection");
    }

    public function educationDataProcess($id="")
    {
        $this->createRegisNumber();
        $this->load->model('educationmodel');
        $post_data = $this->input->post();
        $degree=$post_data['degree'];
        $programName = $post_data['programName'];
        $programType = $this->data['participant']['PROGRAMTYPE'];
        //echo "<pre>"; print_r($post_data);die;
        // echo "<pre>"; print($programName);
        // echo "<pre>"; print(strpos($programName, 'STUDENT EXCHANGE'));
       // if( strpos( $programName, 'STUDENT EXCHANGE' ) !== false ) echo 'text';
        if($this->input->post()){
            if($degree=='S2' && strpos($programName, 'STUDENT EXCHANGE') === false){
                $universityData = $this->educationmodel->universityData($this->data['participant']['PARTICIPANTID']);
               // echo '1';
                $uData = array(
                    'EDUCATIONLEVEL'=> 'UNDERGRADUATE',
                    'SCHOOLNAME'    => $post_data['u_educationlevel'],
                    'START'         => $post_data['u_start'],
                    'END'           => $post_data['u_end'],
                    'DEGREES'       => $post_data['u_degrees'],
                );
        
                if(!empty($universityData)){
                    $uData['UPDATEDATE']  = date('Y-m-d H:i:s');
                    $uData['UPDATEBY']  = $this->session_user['USERID'];
                    $response_university = $this->educationmodel->update($universityData['EDUCATIONID'], $uData);
                } else {
                    $uData['INPUTDATE']        = date('Y-m-d H:i:s');
                    $uData['INPUTBY']          = $this->session_user['USERID'];
                    $uData['PARTICIPANTID']    = $this->data['participant']['PARTICIPANTID'];
                    $response_university = $this->educationmodel->insert($uData);
                }
                $response_highschool='';
            }else if($degree=='S1' && strpos($programName, 'STUDENT EXCHANGE') === false || $degree=='D3' && strpos($programName, 'STUDENT EXCHANGE') === false){
                $highschoolData = $this->educationmodel->highschoolData($this->data['participant']['PARTICIPANTID']);
                //echo '2';
                $data    = array(
                    'EDUCATIONLEVEL'=> 'HIGHSCHOOL',
                    'SCHOOLNAME'    =>  $post_data['h_educationlevel'],
                    'START'         =>  $post_data['h_start'],
                    'END'           =>  $post_data['h_end']
                );
                
                if(!empty($highschoolData)){
                    $data['UPDATEDATE']  = date('Y-m-d H:i:s');
                    $data['UPDATEBY']  = $this->session_user['USERID'];
                    $response_highschool = $this->educationmodel->update($highschoolData['EDUCATIONID'], $data);
                } else {
                    $data['INPUTDATE']        = date('Y-m-d H:i:s');
                    $data['INPUTBY']          = $this->session_user['USERID'];
                    $data['PARTICIPANTID']    = $this->data['participant']['PARTICIPANTID'];
                    $response_highschool      = $this->educationmodel->insert($data);               
                }
                $response_university='';
            }else if(strpos($programName, 'STUDENT EXCHANGE') !== false){
                $universityData = $this->educationmodel->universityData($this->data['participant']['PARTICIPANTID']);

                $uData = array(
                    'EDUCATIONLEVEL'=> 'UNDERGRADUATE',
                    'SCHOOLNAME'    => $post_data['s_educationlevel'],
                    'GRADE'       => $post_data['s_semester']
                );
                //echo "<pre>"; print_r($uData);
                if(!empty($universityData)){
                    $uData['UPDATEDATE']  = date('Y-m-d H:i:s');
                    $uData['UPDATEBY']  = $this->session_user['USERID'];
                    $response_university = $this->educationmodel->update($universityData['EDUCATIONID'], $uData);
                } else {
                    $uData['INPUTDATE']        = date('Y-m-d H:i:s');
                    $uData['INPUTBY']          = $this->session_user['USERID'];
                    $uData['PARTICIPANTID']    = $this->data['participant']['PARTICIPANTID'];
                    $response_university = $this->educationmodel->insert($uData);
                }
                //echo "<pre>"; print_r($response_university);die;
                $response_highschool='';
            }else if($programType=='NON_ACADEMIC'){
                $universityData = $this->educationmodel->universityData($this->data['participant']['PARTICIPANTID']);

                $uData = array(
                    'EDUCATIONLEVEL'=> 'UNDERGRADUATE',
                    'SCHOOLNAME'    => $post_data['s_educationlevel'],
                    'STUDYPROGRAM'       => $post_data['s_studyprogram']
                );
        
                if(!empty($universityData)){
                    $uData['UPDATEDATE']  = date('Y-m-d H:i:s');
                    $uData['UPDATEBY']  = $this->session_user['USERID'];
                    $response_university = $this->educationmodel->update($universityData['EDUCATIONID'], $uData);
                } else {
                    $uData['INPUTDATE']        = date('Y-m-d H:i:s');
                    $uData['INPUTBY']          = $this->session_user['USERID'];
                    $uData['PARTICIPANTID']    = $this->data['participant']['PARTICIPANTID'];
                    $response_university = $this->educationmodel->insert($uData);
                }
                $response_highschool='';
            }
                    //   die; 
            if($response_highschool || $response_university){
                if($this->data['participant']['CURRENTSTEP'] < 5){
                    $this->participantmodel->update($this->session_user['USERID'], array('CURRENTSTEP' => 5));
                };
                $this->session->set_flashdata('msg_success', 'UPDATE DATA SUCCESSFULL');
                redirect(base_url()."participant/supportingData");
            }  else {
                $this->session->set_flashdata('msg_failed', 'FAILED UPDATE DATA');
                redirect(base_url()."participant/educationData");
            }   
            
        }
        redirect(base_url()."participant/programSelection");
    }
    
    public function createRegisNumber(){
        if(strlen($this->data['participant']['ADMISSIONID'])!=10){      
            $enrollment_id = $this->data['participant']['ENROLLMENTID'];            
            $year= $this->enrollmentmodel->getEntrollmentById($enrollment_id);
            $today_year = $year['ACADEMICYEAR'];
            
            //create REGISTRATION NUMBER 
            $last_admissionid = $this->participantmodel->GetAdmissionId();
            $admissionid = $last_admissionid['ADMISSIONID']+1;
            if((ceil(log10($last_admissionid['ADMISSIONID']))==1 && $last_admissionid['ADMISSIONID']!=9 && $last_admissionid['ADMISSIONID']!=10) || $last_admissionid['ADMISSIONID']==1 || $last_admissionid['ADMISSIONID']==0){
                $admissionid = '000'.$admissionid;
            }else if(((ceil(log10($last_admissionid['ADMISSIONID'])))==2 && $last_admissionid['ADMISSIONID']!=99 && $last_admissionid['ADMISSIONID']!=100) || $last_admissionid['ADMISSIONID']==9 || $last_admissionid['ADMISSIONID']==10){
                $admissionid = '00'.$admissionid;
            }else if((ceil(log10($last_admissionid['ADMISSIONID']))==3 && $last_admissionid['ADMISSIONID']!=999 && $last_admissionid['ADMISSIONID']!=1000) || $last_admissionid['ADMISSIONID']==99 || $last_admissionid['ADMISSIONID']==100){
                $admissionid = '0'.$admissionid;
            }else if(ceil(log10($last_admissionid['ADMISSIONID']))==4 || $last_admissionid['ADMISSIONID']==999 || $last_admissionid['ADMISSIONID']==1000){  
                $admissionid = $admissionid;
            }
            $registration_number=$today_year.'0'.$this->data['participant']['PROGRAMID'].$admissionid;
            $participan_data = array('ADMISSIONID' => $registration_number);
            $this->participantmodel->update($this->session_user['USERID'], $participan_data);
        }
    }

    public function supportingDataProcess()
    {
        $this->createRegisNumber();
        $this->config->load('upload');
        $this->load->library('upload');
        $this->load->model('requirementsmodel');
        $this->load->model('requirementsvaluemodel');
        $post_data = $this->input->post();
        //$upload_config = $this->config->item('requirement');
        $success = $failed = 0;

        $participantId = $this->quizmodel->getParticipantId($this->session_user['USERID']);
        $new_name = $participantId[0]['PASSPORTNO'];
        if (!is_dir('uploads/media/participant/requirement/' . $new_name)) {
            mkdir('./uploads/media/participant/requirement/' . $new_name, 0777, TRUE);
        }
        $upload_config['upload_path']   = "./uploads/media/participant/requirement/". $new_name."/";
        $upload_config['access_path']   = "/uploads/media/participant/requirement/". $new_name."/";
        $upload_config['allowed_types'] = "jpg|jpeg|pdf";
        $upload_config['overwrite']     = TRUE;
        $upload_config['max_size']      = "1024000"; // Can be set to particular file size ; here it is 1 MB

        $files=$_FILES;
        if(!empty($post_data) || !empty($files)){
            
            $requirement = $this->requirementsmodel->GetModel();
            foreach ($requirement as $req) {
                if(!empty($post_data['requirement_'.$req['REQUIREMENTID']]) ||
                    !empty($_FILES['requirement_'.$req['REQUIREMENTID']]['name']))
                {
                    $requirementvalue = $this->requirementsvaluemodel->GetData($this->data['participant']['PARTICIPANTID'], $req['REQUIREMENTID']);

                    if(empty($requirementvalue)){
                        
                            $data    = array(
                                'REQUIREMENTID'            =>  $req['REQUIREMENTID'],
                                'PARTICIPANTID'            =>  $this->data['participant']['PARTICIPANTID'],
                                'INPUTDATE'                =>  date('Y-m-d H:i:s'),
                                'INPUTBY'                  =>  $this->session_user['USERID'],
                                'VERIFICATIONSTATUS' => 'UNVERIFIED'
                            );

                            if($req['TYPE'] == 'ATTACHMENT'){
                                $this->upload->initialize($upload_config);
                                $_FILES['requirement_'.$req['REQUIREMENTID']]['name'] = $this->data['participant']['PARTICIPANTID'] . '_' . $_FILES['requirement_'.$req['REQUIREMENTID']]['name'];
                                //echo json_encode($_FILES['requirement_'.$req['REQUIREMENTID']]);

                                $response_upload = $this->upload->do_upload('requirement_'.$req['REQUIREMENTID']);
                                if($response_upload)
                                {
                                    $upload_data = $this->upload->data();
                                    $data['FILENAME']   = $upload_data['file_name'];
                                    //$data['FILENAME']   = $upload_config['file_name'];
                                    //$data['FILEURL']    = $upload_data['file_path'];
                                    $data['FILEURL']    = $upload_config['access_path'];
                                }

                            } else if ($req['TYPE'] == 'TEXT') {
                                $data['VALUE'] = $post_data['requirement_'.$req['REQUIREMENTID']];
                            }

                            $response = $this->requirementsvaluemodel->insert($data);
                            if($response) $success++;
                            else $failed++;
                       // $this->participantmodel->update($this->session_user['USERID'], array('CURRENTSTEP' => 6));
                    } else {

                        $data   = array(
                                'UPDATEDATE'                => date('Y-m-d H:i:s'),
                                'UPDATEBY'                  => $this->session_user['USERID'],
                                'VERIFICATIONSTATUS' => 'UNVERIFIED'
                            );

                        if($req['TYPE'] == 'ATTACHMENT'){
                            $this->upload->initialize($upload_config);

                            $_FILES['requirement_'.$req['REQUIREMENTID']]['name'] = $this->data['participant']['PARTICIPANTID'] . '_' . $_FILES['requirement_'.$req['REQUIREMENTID']]['name'];

                            if($this->upload->do_upload('requirement_'.$req['REQUIREMENTID']))
                            {
                                $upload_data = $this->upload->data();
                                $data['FILENAME']   = $upload_data['file_name'];
                                //$data['FILENAME']   = $upload_config['file_name'];
                                //$data['FILEURL']    = $upload_data['file_path'];
                                $data['FILEURL']    = $upload_config['access_path'];

                                $img_path           = $upload_config['upload_path'] . $requirementvalue['FILENAME'];
                                if(is_file($img_path)){
                                    unlink($img_path);
                                }
                            }
                        } else if ($req['TYPE'] == 'TEXT') {
                            $data['VALUE'] = $post_data['requirement_'.$req['REQUIREMENTID']];
                        }

                        $response = $this->requirementsvaluemodel->update($requirementvalue['REQUIREMENTVALUEID'], $data);
                        if($response) $success++;
                        else $failed++;

                       // $this->participantmodel->update($this->session_user['USERID'], array('CURRENTSTEP' => 6));
                    }
                }
            }
            if($success > 0){
                if($this->data['participant']['CURRENTSTEP'] < 6){
                    $this->participantmodel->update($this->session_user['USERID'], array('CURRENTSTEP' => 6));
                };
                $this->session->set_flashdata('msg_success', 'UPDATE DATA SUCCESSFULL');
                redirect(base_url()."participant/summaryData");
            } else if($success == 0 && $failed == 0) {
                $this->session->set_flashdata('msg_success', 'NO DATA UPDATED');
                redirect(base_url()."participant/summaryData");
            } else {
                $this->session->set_flashdata('msg_failed', 'UPDATE DATA FAILED');
                redirect(base_url()."participant/supportingData");
            }
        } else {
            redirect(base_url()."participant/summaryData");
        }
    }

    public function summarySubmitProcess()
    {
        $this->createRegisNumber();
        $post=$this->input->post();
        if(!empty($post))
        {
            if($this->data['participant']['CURRENTSTEP'] == 6) {
                $response = $this->participantmodel->update($this->session_user['USERID'], array('CURRENTSTEP' => 7));
                
                if($response) {
                    redirect(base_url()."participant/AcceptanceStatus/");
                }
            } else if($this->data['participant']['CURRENTSTEP'] == 7){
                redirect(base_url()."participant/AcceptanceStatus/");
            }else{ 
                $this->session->set_flashdata('msg_failed', 'SUBMIT FAILED');
                redirect(base_url()."participant/summaryData");
            };
        }

        redirect(base_url()."participant/selectProgram");
    }

    public function isLoginParticipant(){
        $users=$this->session->userdata('users');
        $users=$users[0];
        if($this->session->userdata('login')&&$users['USERGROUPID']==PARTICIPANTUSERGROUPID){

        }else{
            redirect(base_url()."login");
        }
    }

    private function _currentStep($currentStep, $pageStep){
        if($currentStep == 7 && $pageStep != 6) redirect('/participant/summaryData');
        else if($currentStep < $pageStep) redirect('/participant/selectProgram');
    }

    function ConfirmStatusProcess(){
        $this->createRegisNumber();
        $post_data = $this->input->post();
        $new_name = $post_data['passportno'];
        $this->load->helper(array('form', 'url'));  

            $loa_name_first=str_replace('.','_',$_FILES['loa']['name']);
            $loa=str_replace(' ','_',$loa_name_first);
            $config_loa = array(
                'upload_path' => "./uploads/media/participant/requirement/". $new_name."/",
                'allowed_types' => "pdf",
                'overwrite' => TRUE,
                'file_name' => $loa.'.'.str_replace('application/','',$_FILES['loa']['type']),
                'access_path' => "/uploads/media/participant/requirement/". $new_name."/"
            );
            if (!is_dir('uploads/media/participant/requirement/' . $new_name)) {
                mkdir('./uploads/media/participant/requirement/' . $new_name, 0777, TRUE);
            }
            $this->upload->initialize($config_loa);
            $this->upload->do_upload('loa');
            $upload_data = $this->upload->data();
            
            //tiket
            $tiket_name_first=str_replace('.','_',$_FILES['userfile']['name']);
        $path = str_replace(' ','_',$tiket_name_first);
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $this->load->model('participantmodel');
        $new_id   = $post_data['participantid'];
        if(str_replace('image/','',$_FILES['userfile']['type'])=='jpeg'){
            $file_name=$path.'.jpg';
        }else{
            $file_name=$path.'.'.str_replace('image/','',$_FILES['userfile']['type']);
        }
        $config = array(
            'upload_path' => "./uploads/media/participant/requirement/". $new_name."/",
            'allowed_types' => "gif|jpg|png",
            'overwrite' => TRUE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'file_name' => $file_name,
            'access_path' => "/uploads/media/participant/requirement/". $new_name."/"
        );
        $this->upload->initialize($config);   
        $this->upload->do_upload('userfile');
        $upload_data = $this->upload->data();

        if(!empty($_FILES['userfile']['name']) && !empty($_FILES['loa']['name'])){
            $participan_data = array(
                'CONFIRMDATE' => $post_data['arrivaldate'],
                'LOAFILE' => $config_loa['file_name'],
                'ARRIVALTICKET' => $config['file_name'],
                'UPDATEDATE' => date('Y-m-d H:i:s'),
            );
        }else if(empty($_FILES['userfile']['name']) && !empty($_FILES['loa']['name'])){
            $participan_data = array(
                'CONFIRMDATE' => $post_data['arrivaldate'],
                'LOAFILE' => $config_loa['file_name'],
                'UPDATEDATE' => date('Y-m-d H:i:s'),
            );
        }else if(empty($_FILES['loa']['name']) && !empty($_FILES['userfile']['name'])){
            $participan_data = array(
                'CONFIRMDATE' => $post_data['arrivaldate'],
                'ARRIVALTICKET' => $config['file_name'],
                'UPDATEDATE' => date('Y-m-d H:i:s'),
            );
        }else if(empty($_FILES['userfile']['name']) && empty($_FILES['loa']['name'])){
            $participan_data = array(
                'CONFIRMDATE' => $post_data['arrivaldate'],
                'UPDATEDATE' => date('Y-m-d H:i:s'),
            );
        }
        $response = $this->participantmodel->update($this->session_user['USERID'], $participan_data);

        if ($response) {
            $this->session->set_flashdata('msg_success', 'UPDATE DATA SUCCESSFULL');
            echo "<script>window.location.href='".base_url() . "participant/acceptanceStatus"."'</script>";
           // redirect(base_url() . "participant/acceptanceStatus");
        } else {
            $this->session->set_flashdata('msg_failed', 'FAILED UPDATE DATA');
            redirect(base_url() . "participant/acceptanceStatus");
        }
    }

    public function dataTableSubjectExchange(){
        $data = $this->participantmodel->getSubjectExchange($_POST['filter']);
        $datax = $this->participantmodel->getSubjectMapping($this->data['participant']['PARTICIPANTID']);
        for($i=0; $i<count($data); $i++){
           // $data[$i]['NO'] = $i+1;
            $data[$i]['ACTION'] = '<input type="checkbox" class="right" name="" language="'.$data[$i]['LANGUAGE'].'" credit="'.$data[$i]['CREDIT'].'" subjectname="'.$data[$i]['SUBJECTNAME'].'" studyprogramname="'.$data[$i]['STUDYPROGRAMNAME'].'" enrollmentid="'.$data[$i]['ENROLLMENTID'].'" studyprogramid="'.$data[$i]['STUDYPROGRAMID'].'" curiculum="'.$data[$i]['CURICULUMYEAR'].'" id="'.$data[$i]['SUBJECTCODE'].'">';
            for($j=0; $j<count($datax); $j++){
                if($datax[$j]['SUBJECTCODE']==$data[$i]['SUBJECTCODE'] && $datax[$j]['STUDYPROGRAMID']==$data[$i]['STUDYPROGRAMID']){
                    $data[$i]['ACTION'] = '<input type="checkbox" class="right" name="" language="'.$data[$i]['LANGUAGE'].'" credit="'.$data[$i]['CREDIT'].'" subjectname="'.$data[$i]['SUBJECTNAME'].'" studyprogramname="'.$data[$i]['STUDYPROGRAMNAME'].'" enrollmentid="'.$data[$i]['ENROLLMENTID'].'" studyprogramid="'.$data[$i]['STUDYPROGRAMID'].'" curiculum="'.$data[$i]['CURICULUMYEAR'].'" id="'.$data[$i]['SUBJECTCODE'].'" checked>';
                    break;
                }
            }
        }
        $json_data = array(
            'data' => $data
        );
        echo json_encode($json_data);
    }

    public function dataTableSubjectMappingExchange(){
        $data = $this->participantmodel->getSubjectMapping($this->data['participant']['PARTICIPANTID']);
        for($i=0; $i<count($data); $i++){
           // $data[$i]['NO'] = $i+1;
            $data[$i]['ACTION'] = '<input type="checkbox" class="left" name="" language="'.$data[$i]['LANGUAGE'].'" credit="'.$data[$i]['CREDIT'].'" subjectname="'.$data[$i]['SUBJECTNAME'].'" studyprogramname="'.$data[$i]['STUDYPROGRAMNAME'].'" enrollmentid="'.$data[$i]['ENROLLMENTID'].'" studyprogramid="'.$data[$i]['STUDYPROGRAMID'].'" curiculum="'.$data[$i]['CURICULUMYEAR'].'" id="'.$data[$i]['SUBJECTCODE'].'">';
        }
        $json_data = array(
            'data' => $data
        );
        echo json_encode($json_data);
    }

    public function dataTableSubjectExAccepted(){
        $data = $this->participantmodel->getSubjectAccepted($this->data['participant']['PARTICIPANTID']);
        $json_data = array(
            'data' => $data
        );
        echo json_encode($json_data);
    }

    public function deleteEnrollMapping(){
        $this->load->model('exchangemodel');
        $datax = $this->participantmodel->getSubjectMapping($this->data['participant']['PARTICIPANTID']);
        if(count($datax)>0){
            $this->exchangemodel->deleteEnrollMapping($this->data['participant']['PARTICIPANTID']);
        }
        echo json_encode('SUCCESS');
    }

}
