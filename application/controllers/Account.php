<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller
{
    private $data;
    private $upload_config;
    private $datatableConfig;
    private $menu;

    public function __construct()
    {
        parent::__construct();

        $data = array(
                "pagetitle" => "",
                "msg" => "",
                "menu" => "",
                "page" => "",
                "subpage" => ""

            );
        $this->data = $data;

        $method = $this->router->fetch_method();
        if($method=='resetPassword' || $method=='doResetPassword'){
            if($this->session->userdata('login')){
                redirect();
            }
        }
        else{
            date_default_timezone_set('Asia/Jakarta');
            $this->isLogin();

            $this->data["menu"] = $this->menu;
        }

        $this->datatableConfig = array(
                "DB_DISPLAY_ERROR" => false,
                "DB_BREAK_PROCESS" => false,
                "DB_TYPE" => "mysql",
                "DB_USERNAME" => $this->db->username,
                "DB_PASSWORD" => $this->db->password,
                "DB_HOST" => $this->db->hostname,
                "DB_NAME_MYSQL" => $this->db->database,
        );

        $upload_config = array(
                'file_name' => "",
                'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/",
                'upload_url' => base_url() . "uploads/",
                'allowed_types' => "gif|jpg|png|jpeg|pdf|doc|xml|docx|xls|xlsx",
                'overwrite' => TRUE,
        );
        $this->upload_config = $upload_config;
		$this->load->model('participantmodel');
		$users=$this->session->userdata('users');        
        $this->session_user = $users[0];
        $this->data['participant']  = $this->participantmodel->participantData($this->session_user['USERID']);
    }

    public function index()
    {
        //$this->load->view('login');
    }

    public function isLogin(){
        $users=$this->session->userdata('users');
        $users=$users[0];
        if($this->session->userdata('login')){
            if($users['USERGROUPID']==ADMINUSERGROUPID)
                $this->menu = "admin/_menu.php";
            else if($users['USERGROUPID']==PARTICIPANTUSERGROUPID)
                $this->menu = "participant/_menu.php";
            else if($users['USERGROUPID']==STAFFUSERGROUPID)
                $this->menu = "staff/_menu.php";
            else if($users['USERGROUPID']==STRUCTURALUSERGROUPID)
                $this->menu = "structural/_menu.php";
        }else{
            redirect(base_url()."login");
        }
    }

    public function changePassword()
    {
        $this->data['pagetitle'] = "IO Tel-U | List Of userManagement Selection";
        $this->data['page'] = "user";

        $this->load->view('_header', $this->data);
        $this->load->view('account/changePassword', $this->data);
        $this->load->view('_footer');
    }

    public function doChangePassword(){
        $password   = $this->input->post("newpassword");
        $password   = md5($password);
        $updatedate = date('Y-m-d H:i:s');
        $users      = $this->session->userdata('users');
        $user      = $users[0];

        $this->db->set('PASSWORD',$password);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->where('USERID',$user['USERID']);
        $this->db->update('users');

        $user['PASSWORD']   = $password;
        $users[0]           = $user;

        $this->session->set_userdata('login', TRUE);
        $this->session->set_userdata('users', $users);
    }

    public function checkPassword(){
        $password = $this->input->post("oldpassword");
        $password = md5($password);

        $users=$this->session->userdata('users');
        $users=$users[0];

        $this->db->select('PASSWORD');
        $data = $this->db->get_where('users', array('EMAIL' => $users['EMAIL'],'USERID' => $users['USERID']))->row_array();
        echo $password == $data['PASSWORD']?"true":"false";
    }

    public function resetPassword(){
        $email  = urldecode($_GET['email']);
        $key    = urldecode($_GET['key']);
        $id     = urldecode($_GET['id']);
        
        $this->db->select('*');
        $data   = $this->db->get_where('users', array('USERID' => $id, 'EMAIL' => $email,'CONFIRMATIONKEY' => $key))->row_array();

        if($data!=null){
            $this->data['pagetitle']    = "IO Tel-U | Reset Password";
            $this->data['page']         = "resetpassword";
            $this->data['email']        = $email;
            $this->data['id']           = $id;
            
            $this->load->view('account/resetPassword', $this->data);
        }
        else{
            die('Link setting your password has expired');
        }
    }

    public function doResetPassword(){
        $email      = $this->input->post('email');
        $password   = md5($this->input->post('newpassword'));
        $id         = $this->input->post('id');
        $updatedate = date('Y-m-d H:i:s');
        $newNonfirmationKey = md5($email . 'hus8e0ldj' . time());

        $this->db->set('PASSWORD',$password);
        $this->db->set('CONFIRMATIONKEY',$newNonfirmationKey);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->where('USERID',$id);
        $this->db->where('EMAIL',$email);
        $this->db->update('users');

        echo ($this->db->affected_rows() != 1) ? 'false' : 'true';
    }
}