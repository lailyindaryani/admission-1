<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    private $data;
    private $upload_config;
    private $datatableConfig;

    public function __construct(){
        parent::__construct();

        $this->isLoginAdmin();

        date_default_timezone_set('Asia/Jakarta');

        $this->datatableConfig = array(
            "DB_DISPLAY_ERROR"=>false,
            "DB_BREAK_PROCESS"=>false,
            "DB_TYPE"=>"mysql",
            "DB_USERNAME"=>$this->db->username,
            "DB_PASSWORD"=>$this->db->password,
            "DB_HOST"=>$this->db->hostname,
            "DB_NAME_MYSQL"=>$this->db->database,
        );

        $data=array(
            "pagetitle" =>"",
            "msg"       =>"",
            "menu"      =>"admin/_menu.php",
            "page"      =>"",
            "subpage"   =>""

        );
        $this->data=$data;

        $upload_config = array(
            'file_name'         => "",
            'upload_path'       => "./uploads/requirements/",
            'upload_url'        => "./uploads/requirements/",
            'allowed_types'     => "gif|jpg|png|jpeg|pdf|doc|xml|docx|xls|xlsx",
            'overwrite'         => TRUE,
        );
        $this->upload_config = $upload_config;
    }
    public function index()
    {
        //$this->load->view('login');
    }

    //function Get Data From API
    private function curlGet($filter, $timeout=0){
        //initiate url, filter, user, pass
        $url = 'https://api.telkomuniversity.ac.id' . $filter;//'10.252.252.43' . $filter;////
        $username = 'igracias';
        $password = 'v01DSp!r1T';
        $process = curl_init($url);

        curl_setopt ($process, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt ($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($process, CURLOPT_AUTOREFERER, 1);
        curl_setopt ($process, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt ($process, CURLOPT_URL, $url);

        if($timeout>0){
            curl_setopt ($process, CURLOPT_CONNECTTIMEOUT, $timeout );
            curl_setopt ($process, CURLOPT_TIMEOUT, $timeout );
        }

        $output = json_decode(curl_exec($process));
        curl_close($process);

        $array = json_decode(json_encode($output), True);

        // for ($i = 0; $i < count($array); $i++) {
        //     $array[$i]['no'] = $i + 1;
        // }

        return $array;
    }

    //=======================BEGIN OF PROGRAM=======================//
    public function programList(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Program Selection";
        $this->data['page']         = "program";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/programList',$this->data);
        $this->load->view('_footer');
    }

    public function programAdd(){
        $this->data['pagetitle']    ="IO Tel-U | List Of Program Selection";
        $this->data['page']         = "program";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/programAdd',$this->data);
        $this->load->view('_footer');
    }

    public function programStore(){
        //echo "<pre>"; print_r($_POST);die;

        $programname    = $this->input->post('programname');
        $programtype    = $this->input->post('programtype');
        $statussubjectsetting    = $this->input->post('statussubjectsetting');
        $maxcredit    = $this->input->post('maxcredit');
        $maxstudy    = $this->input->post('maxstudy');
        $inputdate      = date('Y-m-d H:i:s');
        $users          = $this->session->userdata('users');
        $users          = $users[0];
        $inputby        = $users['USERID'];

        $data = array(
            'PROGRAMNAME'   => $programname,
            'PROGRAMTYPE'   => $programtype,
            'MAXCREDIT' => $maxcredit,
            'MAXSTUDYPROGRAM' => $maxstudy,
            'STATUSSUBJECTSETTING' => $statussubjectsetting,
            'INPUTDATE'     => $inputdate,
            'UPDATEDATE'    => $inputdate,
            'INPUTBY'       => $inputby,
            'UPDATEBY'      => $inputby
        );

        $this->db->insert('program',$data);
        redirect(base_url()."admin/programList");
    }

    public function programUpdate(){
        //echo "<pre>"; print_r($_POST);die;
        $id             = $this->input->post('programid');
        $programname    = $this->input->post('programname');
        $programtype    = $this->input->post('programtype');
        $updatedate     = date('Y-m-d H:i:s');
        $users          = $this->session->userdata('users');
        $users          = $users[0];
        $updateby       = $users['USERID'];
        $statussubjectsetting    = $this->input->post('statussubjectsetting');
        $maxcredit    = $this->input->post('maxcredit');
        $maxstudy    = $this->input->post('maxstudy');

        $this->db->set('PROGRAMNAME',$programname);
        $this->db->set('PROGRAMTYPE',$programtype);
        $this->db->set('STATUSSUBJECTSETTING',$statussubjectsetting);
        $this->db->set('MAXCREDIT',$maxcredit);
        $this->db->set('MAXSTUDYPROGRAM',$maxstudy);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->set('UPDATEBY',$updateby);
        $this->db->where('PROGRAMID',$id);
        $this->db->update('program');
    }

    public function programAjaxGetDetail(){
        $id = $this->input->post('programid');
        $program = $this->db->get_where('program', array('PROGRAMID' => $id));
        echo json_encode($program->row_array());
    }

    function dataTablePopulateProgram(){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('PROGRAMID');
        $this->datatables->SetColumns(array('PROGRAMNAME','PROGRAMTYPE','PROGRAMID','STATUSSUBJECTSETTING'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * from program ORDER BY UPDATEDATE DESC");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF PROGRAM=======================//

    //=======================BEGIN OF SUBJECT=======================//
    public function subjectList(){
        $users = $this->session->userdata('users');
        $users = $users[0];
        $this->load->model('exchangemodel');
        $this->load->model('enrollmentmodel');
        $subjectData = $this->exchangemodel->getAllData();
        
        // $prodis = $this->exchangemodel->getExistProdi();
        // $idprodi = $prodis[0]['STUDYPROGRAMID'];
        //echo "<pre>"; print_r($enrollment); die;
        $cek=false;
        if(count($subjectData)==0){
            $cek = $this->sinkronSubject();
        }else{
            $cek=true;
        }
        if($cek){
            $prodi = $this->exchangemodel->getFilter('STUDYPROGRAMNAME, STUDYPROGRAMID', 'STUDYPROGRAMID');
            $kurikulum = $this->exchangemodel->getFilter('MAX(CURICULUMYEAR) AS CURICULUMYEAR','CURICULUMYEAR');
            $degree = $this->exchangemodel->getFilter('DEGREE', 'DEGREE');
            $allkurikulum = $this->exchangemodel->getFilter('CURICULUMYEAR', 'CURICULUMYEAR');
            $enrollment = $this->exchangemodel->getEnrollment();
            //$this->data['idprodi'] = $idprodi;
            $this->data['pagetitle']    = "IO Tel-U | List Of Subject Selection";
            $this->data['page']         = "subject";
            $this->data['enrollment'] = $enrollment;
            $this->data['studyprogramname'] = $prodi;
            $this->data['curriculum'] = $kurikulum;
            $this->data['allcurriculum'] = $allkurikulum;
            $this->data['degree'] = $degree;
            //echo "<pre>"; print_r($this->data); die;
            $this->load->view('_header',$this->data);
            $this->load->view('admin/subjectList',$this->data);
            $this->load->view('_footer');
        }else{
            redirect(base_url().'admin/dashboardEnrollment');
        }        
    }

    function sinkronSubject(){
        $this->load->model('exchangemodel');
        $filter = '/igracias/getAllListSubject';
        $allSubject = $this->curlGet($filter);
        //echo "<pre>"; print_r($allSubject);die;
        $cek=false;
        if(isset($_POST['filter'])){
            $return = $this->exchangemodel->deleteAllSubject();
            //echo "<pre>"; print_r($return);die;
        }

        foreach ($allSubject as $key => $value) {
            $arr = array(
                'SUBJECTID' => $value['subjectid'],
                'SUBJECTCODE' => $value['subjectcode'],
                'CREDIT' => $value['credit'],
                'SUBJECTNAME' => $value['subjectname'],
                'SUBJECTDESCRIPTION' => $value['subjectdescription'],
                'SUBJECTCATEGORY' => $value['subjectcategory'],
                'STUDYPROGRAMID' => $value['studyprogramid'],
                'STUDYPROGRAMNAME' => $value['studyprogramname'],
                'CURICULUMYEAR' => $value['curiculumyear'],
                'ADMITSEMESTER' => $value['admitsemester'],
                'DEGREE' => $value['studyprogramtype']//,
                    // 'SUBJECTSTATUS' => 'Y',
                    // 'INPUTDATE' => date('Y-m-d H:i:s'),
                    // 'UPDATEDATE' => date('Y-m-d H:i:s'),
                    // 'INPUTBY' => $users['USERID'],
                    // 'UPDATEBY' => $users['USERID']
            );
            $result = $this->exchangemodel->insert('subjectforexchange', $arr);
            if($key == count($allSubject)-1){
                $cek=true;
            }
        }
        if(!isset($_POST['filter'])){
            return $cek;
        }else{
            echo json_encode("SUCCESS");
        }        
    }

    function dataTablePopulateSubject(){
        $this->load->model('exchangemodel');
        $this->load->model('enrollmentmodel');
        $studyprogramid = $_POST['filter1'];
        $enrollmentid = $_POST['filter2'];
        $degree = $_POST['filter3'];
        $curriculum = $_POST['filter4'];
        $semester = $_POST['filter5'];
        $data = $this->exchangemodel->getSubjectMapping($enrollmentid, $studyprogramid, $degree, $curriculum, $semester);
        $datax = $this->exchangemodel->getPopulateSubject($studyprogramid, $degree, $curriculum, $semester);
        for($i=0; $i<count($datax); $i++){
            $datax[$i]['NO'] = $i+1;
            $datax[$i]['ACTION'] = '<input type="checkbox" class="checkk" studyprogramid="'.$datax[$i]['STUDYPROGRAMID'].'" curiculum="'.$datax[$i]['CURICULUMYEAR'].'" id="'.$datax[$i]['SUBJECTCODE'].'">';
            if(count($data)>0){
                for($j=0; $j<count($data); $j++){
                    if($datax[$i]['SUBJECTCODE']==$data[$j]['SUBJECTCODE'] && $datax[$i]['STUDYPROGRAMID']==$data[$j]['STUDYPROGRAMID']){
                        $datax[$i]['ACTION'] = '<input studyprogramid="'.$datax[$i]['STUDYPROGRAMID'].'" curiculum="'.$datax[$i]['CURICULUMYEAR'].'" type="checkbox" class="checkk" checked id="'.$data[$j]['SUBJECTCODE'].'">';
                        break;
                    }
                }
            }
        }
        $json_data = array(
            'data' => $datax
        );
        echo json_encode($json_data);
    }

    function dataTableSubjectMapping(){
        $this->load->model('exchangemodel');
        $this->load->model('enrollmentmodel');
        $studyprogramid = $_POST['filter1'];
        $enrollmentid = $_POST['filter2'];
        $degree = $_POST['filter3'];
        $curriculum = $_POST['filter4'];
         $semester = $_POST['filter5'];
        $data = $this->exchangemodel->getSubjectMapping2($enrollmentid, $studyprogramid, $degree, $curriculum, $semester);
        for($i=0; $i<count($data); $i++){
            $data[$i]['NO'] = $i+1;           
        }
        //echo "<pre>"; print_r($data);die;
        $json_data = array(
            'data' => $data
        );
        echo json_encode($json_data);
    }

    function subjectStore(){
        //echo "<pre>"; print_r($_POST);//die;
        $this->load->model('exchangemodel');
        $data = array(
            'SUBJECTCODE' => $_POST['subjectcode'],
            'CURICULUMYEAR' => $_POST['curiculum'],
            'STUDYPROGRAMID' => $_POST['studyprogramid'],
            'ENROLLMENTID' => $_POST['enrollmentid']
        );
        if($_POST['selectstudyprogram']!=''){
            $enrollmentExist = $this->exchangemodel->getEnrollmentExist($_POST['enrollmentid'], $_POST['studyprogramid'], $_POST['curiculum']);

        }else{
            $enrollmentExist = $this->exchangemodel->getEnrollmentExist2($_POST['enrollmentid'], $_POST['curiculum']);
           // echo "<pre>"; print("yeah");
        }
        //die;
        if(count($enrollmentExist)>0 && $_POST['index']==0){
            if($_POST['selectstudyprogram']!=''){
               $result = $this->exchangemodel->deleteEnrollexchange($_POST['enrollmentid'], $_POST['studyprogramid'], $_POST['curiculum']);
                // echo "<pre>"; print($result);
                // echo "<pre>"; print("yeah");
            }else{
                $result = $this->exchangemodel->deleteEnrollexchange2($_POST['enrollmentid'], $_POST['curiculum']);
            }   
        }
        $return = $this->exchangemodel->insert('enrollexchange', $data);
        // echo "<pre>"; print_r($data);
        // echo "<pre>"; print_r($return);
        // echo "<pre>"; print($return);die;
        echo json_encode($_POST['index']);
        
    }
    //=======================END OF SUBJECT=======================//

    //=======================BEGIN OF FACULTY=======================//
    public function facultyList(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Faculty";
        $this->data['page']         = "master";
        $this->data['subpage']      = "faculty";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/facultyList',$this->data);
        $this->load->view('_footer');
    }

    public function facultyAdd(){
        $this->data['pagetitle']    ="IO Tel-U | Add new Faculty";
        $this->data['page']         = "master";
        $this->data['subpage']      = "faculty";
        $this->load->view('_header',$this->data);
        $this->load->view('admin/facultyAdd',$this->data);
        $this->load->view('_footer');
    }

    public function facultyStore(){
        $facultynameina = $this->input->post('facultynameina');
        $facultynameeng = $this->input->post('facultynameeng');

        $data = array(
            'FACULTYNAMEINA'   => $facultynameina,
            'FACULTYNAMEENG'   => $facultynameeng,
        );

        $response=$this->db->insert('faculty',$data);
        if($response) $this->session->set_flashdata('msg_success', 'INSERT DATA SUCCESSFULL');
                else $this->session->set_flashdata('msg_failed', 'FAILED INSERT DATA');
        redirect(base_url()."admin/facultyList");
    }

    public function facultyUpdate(){
        $id             = $this->input->post('facultyid');
        $facultynameina = $this->input->post('facultynameina');
        $facultynameeng = $this->input->post('facultynameeng');

        $this->db->set('FACULTYNAMEINA',$facultynameina);
        $this->db->set('FACULTYNAMEENG',$facultynameeng);
        $this->db->where('FACULTYID',$id);
        $response =$this->db->update('faculty');
        if($response) $this->session->set_flashdata('msg_success', 'UPDATE DATA SUCCESSFULL');
                else $this->session->set_flashdata('msg_failed', 'FAILED UPDATE DATA');
                 redirect(base_url()."admin/facultyList");
    }

    public function facultyDrop(){
        $id = $this->input->post('facultyid');
        $this->db->where('FACULTYID', $id);
        $this->db->delete('faculty');
    }

    public function facultyAjaxGetDetail(){
        $id = $this->input->post('facultyid');
        $faculty = $this->db->get_where('faculty', array('FACULTYID' => $id));
        echo json_encode($faculty->result());
    }

    function dataTablePopulateFaculty(){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('FACULTYID');
        $this->datatables->SetColumns(array('FACULTYNAMEINA','FACULTYNAMEENG','FACULTYID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * from faculty");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF FACULTY=======================//

    public  function dataTablePopulateStudyProgramByProgramDegree($programid,$degreeid){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('PROGRAMID');
        $this->datatables->SetColumns(array('PROGRAMNAME','PROGRAMNAME','INPUTDATE','PROGRAMID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * from program ");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }

    //=======================BEGIN OF COUPON=======================//
    public function couponList(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Coupon";
        $this->data['page']         = "coupon";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/couponList',$this->data);
        $this->load->view('_footer');
    }

    public function couponAdd(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Coupon Selection";
        $this->data['page']         = "coupon";
        $this->load->view('_header',$this->data);
        $this->load->view('admin/couponAdd',$this->data);
        $this->load->view('_footer');
    }

    public function couponStore(){
        $couponvalue = $this->input->post('couponvalue');
        $users          = $this->session->userdata('users');
        $updatedate     = date('Y-m-d H:i:s');
        $users          = $users[0];
        $updateby       = $users['USERID'];

        $data = array(
            'COUPONVALUE'   => $couponvalue,
            'INPUTBY'       => $updateby,
            'INPUTDATE'     => $updatedate,
        );

        $this->db->insert('coupon',$data);
        redirect(base_url()."admin/couponList");
    }

    public function couponUpdate(){
        $id             = $this->input->post('couponid');
        $couponvalue    = $this->input->post('couponvalue');
        $updatedate     = date('Y-m-d H:i:s');
        $users          = $this->session->userdata('users');
        $users          = $users[0];
        $updateby       = $users['USERID'];

        $this->db->set('COUPONVALUE',$couponvalue);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->set('UPDATEBY',$updateby);
        $this->db->where('COUPONID',$id);
        $this->db->update('coupon');
    }

    public function couponDrop(){
        $id = $this->input->post('couponid');
        $this->db->where('COUPONID', $id);
        $this->db->delete('coupon');
    }

    public function couponAjaxGetDetail(){
        $id = $this->input->post('couponid');
        $coupon = $this->db->get_where('coupon', array('COUPONID' => $id));
        echo json_encode($coupon->result());
    }

    function dataTablePopulateCoupon(){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('COUPONID');
        $this->datatables->SetColumns(array('COUPONVALUE','COUPONID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * from coupon ORDER BY UPDATEDATE DESC");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF COUPON=======================//

    //=======================BEGIN OF QUESTIONAIRE=======================//
    public function questionaireSetting(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Questionaire";
        $this->data['page']         = "questionaire";
//        $this->data['subpage']      = "setting";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/questionaireList',$this->data);
        $this->load->view('_footer');
    }

    public function questionaireAdd(){
        $this->data['pagetitle']    ="IO Tel-U | List Of Question";
        $this->data['page']         = "questionaire";
//        $this->data['subpage']      = "faculty";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/questionaireAdd',$this->data);
        $this->load->view('_footer');
    }

    public function questionaireStore(){
        $question   = $this->input->post('question');
        $istext     = $this->input->post('questionairetype');
        $option     = $this->input->post('field');

        $dataquestion['QUESTION'] = $question;
        $this->db->insert('questionaire',$dataquestion);

        $questionid = $this->db->insert_id();

        if($istext=='YES'){
            $dataoption['QUESTIONID']   = $questionid;
            $dataoption['ISTEXT']       = $istext;

            $this->db->insert('questionoption',$dataoption);
        }else{
            foreach ($option as $key){
                if($key=="")
                    continue;

                $dataoption['QUESTIONID']   = $questionid;
                $dataoption['ISTEXT']       = $istext;
                $dataoption['OPTIONVALUE']  = $key;

                $this->db->insert('questionoption',$dataoption);
            }
        }

        redirect(base_url()."admin/questionaireSetting");
    }

    public function manageQuestion($id){
        $this->data['pagetitle']    ="IO Tel-U | List Of Question";
        $this->data['page']         = "questionaire";
//        $this->data['subpage']      = "faculty";

        $question           = $this->db->get_where('questionaire', array('QUESTIONID' => $id))->row_array();
        $question_option    = $this->db->get_where('questionoption', array('QUESTIONID' => $id))->result_array();

        $this->data['question']         = $question;
        $this->data['question_option']  = $question_option;

        $this->load->view('_header',$this->data);
        $this->load->view('admin/questionaireManage',$this->data);
        $this->load->view('_footer');
    }

    public function questionaireUpdate(){
        $id                 = $this->input->post('questionid');
        $question           = $this->input->post('question');
        $questiontype       = $this->input->post('question-type');

        if($questiontype=='text'){
            $this->db->where('QUESTIONID', $id);
            $this->db->delete('questionoption');

            $dataquestion['QUESTIONID']     = $id;
            $dataquestion['ISTEXT']         = 'Y';
            $this->db->insert('questionoption',$dataquestion);
        }
        else{
            $this->db->where('QUESTIONID', $id);
            $this->db->where('ISTEXT', 'Y');
            $this->db->delete('questionoption');
        }

        $this->db->set('QUESTION',$question);
        $this->db->where('QUESTIONID',$id);
        $this->db->update('questionaire');

        redirect(base_url()."admin/questionaireSetting");
    }

    public function questionaireDrop(){
        $id = $this->input->post('questionaireid');
        $this->db->where('QUESTIONID', $id);
        $this->db->delete('questionaire');
    }

    public function questionaireAjaxGetDetail(){
        $id = $this->input->post('questionaireid');
        $questionaire = $this->db->get_where('questionaire', array('QUESTIONAIREID' => $id));
        echo json_encode($questionaire->result());
    }

    public function questionaireOptionAjaxGetDetail(){
        $id = $this->input->post('optionid');
        $options = $this->db->get_where('questionoption', array('OPTIONID' => $id));
        echo json_encode($options->row_array());
    }

    public function questionaireOptionStore(){
        $questionid     = $this->input->post('questionid');
        $optionvalue    = $this->input->post('optionvalue');

        $dataquestion['QUESTIONID']     = $questionid;
        $dataquestion['OPTIONVALUE']    = $optionvalue;
        $this->db->insert('questionoption',$dataquestion);
    }

    public function questionaireOptionUpdate(){
        $optionid       = $this->input->post('optionid');
        $optionvalue    = $this->input->post('optionvalue');

        $this->db->set('OPTIONVALUE',$optionvalue);
        $this->db->where('OPTIONID',$optionid);
        $this->db->update('questionoption');
    }

    public function questionaireOptionDrop(){
        $id = $this->input->post('optionid');
        $this->db->where('OPTIONID', $id);
        $this->db->delete('questionoption');
    }

    function dataTablePopulateQuestionaireOption($id=null){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('OPTIONID');
        $this->datatables->SetColumns(array('OPTIONVALUE','OPTIONID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * from questionoption WHERE QUESTIONID=$id AND ISTEXT IS NULL");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }

    function dataTablePopulateQuestionaire(){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('QUESTIONID');
        $this->datatables->SetColumns(array('QUESTION','QUESTIONID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * from questionaire");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF QUESTIONAIRE=======================//

    //=======================BEGIN OF QUESTIONAIRE RESULT=======================//
    public function questionaireEnrollment(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Enrollment Questionaire";
        $this->data['page']         = "questionaire";
        $this->data['subpage']      = "result";
        
        $this->load->view('_header',$this->data);
        $this->load->view('admin/questionaireEnrollmentList',$this->data);
        $this->load->view('_footer');
    }
    public function questionaireResult($enrollmentId){
        $this->data['pagetitle']    = "IO Tel-U | List Of Questionaire";
        $this->data['page']         = "questionaire";
        $this->data['subpage']      = "result";
        $this->data['enrollmentid']   = $enrollmentId;
        
        $this->load->view('_header',$this->data);
        $this->load->view('admin/questionaireResultList',$this->data);
        $this->load->view('_footer');
    }

    public function questionaireAnswer($id,$enrollmentId){
        $this->data['pagetitle']    = "IO Tel-U | List Of Questionaire Answers";
        $this->data['page']         = "questionaire";
        $this->data['subpage']      = "result";
        $this->data['questionid']   = $id;
        $this->data['enrollmentid']   = $enrollmentId;
        $this->load->model('quizmodel');
        $this->load->model('enrollmentmodel');
        $this->data['data_answer'] = $this->quizmodel->getAnswer($id,$enrollmentId);
        $dataEnrollment = $this->enrollmentmodel->getEntrollmentById($enrollmentId);
        $this->data['enrollmentName']=$dataEnrollment['DESCRIPTION'];
        $this->load->view('_header',$this->data);
        $this->load->view('admin/questionaireResultAnswers',$this->data);
        $this->load->view('_footer');
    }
    
    function dataTablePopulateQuestionaireResultAnswer($id,$enrollmentId){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('QUESTIONID');
        $this->datatables->SetColumns(array('OPTIONVALUE','JUMLAH'));

        $this->datatables->SetActiveCounter(true);

       $q = "SELECT 
                A.QUESTIONID,
                A.QUESTION,
                A.OPTIONID,
                A.OPTIONVALUE,
                COUNT(B.PARTICIPANTID) AS JUMLAH
            FROM 
                (SELECT
                A.QUESTIONID,
                A.QUESTION,
                B.OPTIONID,
                B.OPTIONVALUE
            FROM questionaire A
                RIGHT JOIN questionoption B ON (A.QUESTIONID = B.QUESTIONID)) A
                LEFT JOIN (SELECT A.OPTIONID, A.QUESTIONID, A.PARTICIPANTID , C.ENROLLMENTID FROM questionanswer A
                JOIN participants C ON (A.PARTICIPANTID = C.PARTICIPANTID) WHERE ENROLLMENTID =$enrollmentId
            )B ON (A.QUESTIONID = B.QUESTIONID AND A.OPTIONID = B.OPTIONID)
            WHERE A.QUESTIONID = $id
            GROUP BY A.QUESTIONID,
                A.QUESTION,
                A.OPTIONID,
                A.OPTIONVALUE
              ";
        $this->datatables->SetTable($q);
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    
    function dataTablePopulateEnrollmentQuestionaire(){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('ENROLLMENTID');
        $this->datatables->SetColumns(array('DESCRIPTION','ENROLLMENTID'));

        $this->datatables->SetActiveCounter(true);

        $q = "SELECT * FROM enrollment
              ORDER BY UPDATEDATE DESC
              ";

        $this->datatables->SetTable($q);
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }

    function dataTablePopulateQuestionaireResult(){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('QUESTIONANSWERID');
        $this->datatables->SetColumns(array('QUESTION','QUESTIONID'));

        $this->datatables->SetActiveCounter(true);

        $q = "SELECT * FROM questionaire
              ";

        $this->datatables->SetTable($q);
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF QUESTIONAIRE RESULT=======================//

    //=======================BEGIN OF DEGREE=======================//
    public function degreeList(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Degree Selection";
        $this->data['page']         = "master";
        $this->data['subpage']      = "degree";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/degreeList',$this->data);
        $this->load->view('_footer');
    }

    public function degreeAdd(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Degree Selection";
        $this->data['page']         = "master";
        $this->data['subpage']      = "degree";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/degreeAdd',$this->data);
        $this->load->view('_footer');
    }

    public function degreeStore(){
        $degreename     = $this->input->post('degreename');
        $degreetype     = $this->input->post('degreetype');
        $inputdate      = date('Y-m-d H:i:s');
        $users          = $this->session->userdata('users');
        $users          = $users[0];
        $inputby        = $users['USERID'];

        $data = array(
            'DEGREE'        => $degreename,
            'TYPE'          => $degreetype,
            'INPUTDATE'     => $inputdate,
            'UPDATEDATE'    => $inputdate,
            'INPUTBY'       => $inputby,
            'UPDATEBY'      => $inputby
        );

        $this->db->insert('degree',$data);
        redirect(base_url()."admin/degreeList");
    }

    public function degreeUpdate(){
        $id             = $this->input->post('degreeid');
        $degreename     = $this->input->post('degreename');
        $degreetype     = $this->input->post('degreetype');
        $updatedate     = date('Y-m-d H:i:s');
        $users          = $this->session->userdata('users');
        $users          = $users[0];
        $updateby       = $users['USERID'];

        $this->db->set('DEGREE',$degreename);
        $this->db->set('TYPE',$degreetype);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->set('UPDATEBY',$updateby);
        $this->db->where('DEGREEID',$id);
        $this->db->update('degree');
    }

    public function degreeDrop(){
        $id = $this->input->post('degreeid');
        $this->db->where('DEGREEID', $id);
        $this->db->delete('degree');
    }

    public function degreeAjaxGetDetail(){
        $id = $this->input->post('degreeid');
        $degree = $this->db->get_where('degree', array('DEGREEID' => $id));
        echo json_encode($degree->result());
    }

    function ajaxGetDegreeByProgram($programid){
        $q = "SELECT * FROM degree
          JOIN (SELECT PROGRAMID,DEGREEID FROM programdegree) AS programdegree USING (DEGREEID)
          WHERE PROGRAMID=$programid
        ";

        echo json_encode($this->db->query($q)->result_array());
    }

    function dataTablePopulateDegree(){
        $this->load->library('datatables',$this->datatableConfig);

        $this->datatables->SetIndexColumn('DEGREEID');
        $this->datatables->SetColumns(array('DEGREE','TYPE','DEGREEID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable("SELECT * from degree ORDER BY UPDATEDATE DESC");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF DEGREE=======================//

    //=======================BEGIN OF STUDYPROGRAM=======================//
    public function studyProgramList(){
        $this->data['pagetitle']    = "IO Tel-U | List Of StudyProgram Selection";
        $this->data['page']         = "master";
        $this->data['subpage']      = "studyprogram";

        $this->load->model('facultymodel');
        $this->data['faculties'] = $this->facultymodel->GetAllData();

        $this->load->view('_header',$this->data);
        $this->load->view('admin/studyProgramList',$this->data);
        $this->load->view('_footer');
    }

    public function studyProgramAdd(){
        $this->data['pagetitle']    = "IO Tel-U | List Of StudyProgram Selection";
        $this->data['page']         = "master";
        $this->data['subpage']      = "studyprogram";

        $this->load->model('facultymodel');
        $this->data['faculties'] = $this->facultymodel->GetAllData();

        $this->load->view('_header',$this->data);
        $this->load->view('admin/studyProgramAdd',$this->data);
        $this->load->view('_footer');
    }

    public function studyProgramStore(){
        $studyprogramname       = $this->input->post('studyprogramname');
        $igraciasstudyprogramid = $this->input->post('igraciasstudyprogramid');
        $facultyid              = $this->input->post('faculty');

        $data = array(
            'STUDYPROGRAMNAME'          => $studyprogramname,
            'IGRACIASSTUDYPROGRAMID'    => $igraciasstudyprogramid,
            'FACULTYID'                 => $facultyid,
        );

        $this->db->insert('studyprogram',$data);
        redirect(base_url()."admin/studyProgramList");
    }

    public function studyProgramUpdate(){
        $id                     = $this->input->post('studyprogramid');
        $studyprogramname       = $this->input->post('studyprogramname');
        $igraciasstudyprogramid = $this->input->post('igraciasstudyprogramid');
        $facultyid              = $this->input->post('faculty');

        $this->db->set('STUDYPROGRAMNAME',$studyprogramname);
        $this->db->set('IGRACIASSTUDYPROGRAMID',$igraciasstudyprogramid);
        $this->db->set('FACULTYID',$facultyid);
        $this->db->where('STUDYPROGRAMID',$id);
        $this->db->update('studyprogram');
    }

    public function studyProgramDrop(){
        $id = $this->input->post('studyprogramid');
        $this->db->where('STUDYPROGRAMID', $id);
        $this->db->delete('studyprogram');
    }

    public function studyProgramAjaxGetDetail(){
        $id = $this->input->post('studyprogramid');
        $studyProgram = $this->db->get_where('studyprogram', array('STUDYPROGRAMID' => $id));
        echo json_encode($studyProgram->result());
    }

    function ajaxGetStudyProgram(){
        $this->db->select('*');
        echo json_encode($this->db->get('studyprogram')->result_array());
    }

    function dataTablePopulateStudyProgram(){
        $this->load->library('datatables',$this->datatableConfig);

        $this->datatables->SetIndexColumn('STUDYPROGRAMID');
        $this->datatables->SetColumns(array('STUDYPROGRAMNAME','IGRACIASSTUDYPROGRAMID','FACULTYNAMEENG','STUDYPROGRAMID'));

        $this->datatables->SetActiveCounter(true);
        $q = "SELECT * FROM studyprogram
              LEFT JOIN (faculty) USING (FACULTYID)
              ";
        $this->datatables->SetTable($q);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }
    //=======================END OF STUDYPROGRAM=======================//

    //=======================BEGIN OF SCHOLARSHIP=======================//
    public function scholarshiptypeList(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Scholarship Selection";
        $this->data['page']         = "master";
        $this->data['subpage']      = "scholarshiptype";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/scholarshiptypeList',$this->data);
        $this->load->view('_footer');
    }

    public function scholarshiptypeAdd(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Scholarship Selection";
        $this->data['page']         = "master";
        $this->data['subpage']      = "scholarshiptype";
        $this->load->view('_header',$this->data);
        $this->load->view('admin/scholarshiptypeAdd',$this->data);
        $this->load->view('_footer');
    }

    public function scholarshiptypeStore(){
        $scholarshiptypename   = $this->input->post('scholarshiptypename');
        $activestatus   = $this->input->post('activestatus');

        $data = array(
            'SCHOLARSHIPTYPENAME'        => $scholarshiptypename,
            'ACTIVESTATUS' => $activestatus
        );

        $this->db->insert('scholarshiptype',$data);
        redirect(base_url()."admin/scholarshiptypeList");
    }

    public function scholarshiptypeUpdate(){
        $id             = $this->input->post('scholarshiptypeid');
        $scholarshiptypename   = $this->input->post('scholarshiptypename');
        $activestatus   = $this->input->post('activestatus');

        $this->db->set('SCHOLARSHIPTYPENAME',$scholarshiptypename);
        $this->db->set('ACTIVESTATUS',$activestatus);
        $this->db->where('SCHOLARSHIPTYPEID',$id);
        $this->db->update('scholarshiptype');
    }

    public function scholarshiptypeDrop(){
        $id = $this->input->post('scholarshiptypeid');
        $this->db->where('SCHOLARSHIPTYPEID', $id);
        $this->db->delete('scholarshiptype');
    }

    public function scholarshiptypeAjaxGetDetail(){
        $id = $this->input->post('scholarshiptypeid');
        $scholarshiptype = $this->db->get_where('scholarshiptype', array('SCHOLARSHIPTYPEID' => $id));
        echo json_encode($scholarshiptype->result());
    }

    function dataTablePopulateScholarship(){
        $this->load->library('datatables',$this->datatableConfig);

        $this->datatables->SetIndexColumn('SCHOLARSHIPTYPEID');
        $this->datatables->SetColumns(array('SCHOLARSHIPTYPENAME','ACTIVESTATUS','SCHOLARSHIPTYPEID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * FROM scholarshiptype");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }

    //=======================END OF SCHOLARSHIP=======================//

    //=======================BEGIN OF LANGUAGE DELIVERY=======================//
    public function languageList(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Language Selection";
        $this->data['page']         = "master";
        $this->data['subpage']      = "language";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/languageList',$this->data);
        $this->load->view('_footer');
    }

    public function languageAdd(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Language Selection";
        $this->data['page']         = "master";
        $this->data['subpage']      = "language";
        $this->load->view('_header',$this->data);
        $this->load->view('admin/languageAdd',$this->data);
        $this->load->view('_footer');
    }

    public function languageStore(){
        $languagename     = $this->input->post('languagename');

        $data = array(
            'LANGUAGE'        => $languagename,
        );

        $this->db->insert('languagedelivery',$data);
        redirect(base_url()."admin/languageList");
    }

    public function languageUpdate(){
        $id             = $this->input->post('languageid');
        $languagename   = $this->input->post('languagename');

        $this->db->set('LANGUAGE',$languagename);
        $this->db->where('LANGUAGEDELIVERYID',$id);
        $this->db->update('languagedelivery');
    }

    public function languageDrop(){
        $id = $this->input->post('languageid');
        $this->db->where('LANGUAGEDELIVERYID', $id);
        $this->db->delete('languagedelivery');
    }

    public function languageAjaxGetDetail(){
        $id = $this->input->post('languageid');
        $language = $this->db->get_where('languagedelivery', array('LANGUAGEDELIVERYID' => $id));
        echo json_encode($language->result());
    }

    function dataTablePopulateLanguage(){
        $this->load->library('datatables',$this->datatableConfig);

        $this->datatables->SetIndexColumn('LANGUAGEDELIVERYID');
        $this->datatables->SetColumns(array('LANGUAGE','LANGUAGEDELIVERYID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * FROM languagedelivery");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF LANGUAGE DELIVERY=======================//

    //=======================BEGIN OF COURSE=======================//
    public function courseList(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Course Selection";
        $this->data['page']         = "master";
        $this->data['subpage']      = "course";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/courseList',$this->data);
        $this->load->view('_footer');
    }

    public function courseAdd(){
        $this->data['pagetitle']    = "IO Tel-U | Add New Course";
        $this->data['page']         = "master";
        $this->data['subpage']      = "course";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/courseAdd',$this->data);
        $this->load->view('_footer');
    }

    public function courseStore(){
        $coursename   = $this->input->post('coursename');

        $data = array(
            'COURSENAME'  => $coursename
        );

        $this->db->insert('course',$data);
        redirect(base_url()."admin/courseList");
    }

    public function courseUpdate(){
        $id                 = $this->input->post('courseid');
        $coursename   = $this->input->post('coursename');

        $this->db->set('COURSENAME',$coursename);
        $this->db->where('COURSEID',$id);
        $this->db->update('course');
    }

    public function courseDrop(){
        $id = $this->input->post('courseid');
        $this->db->where('COURSEID', $id);
        $this->db->delete('course');
    }

    public function courseAjaxGetDetail(){
        $id = $this->input->post('courseid');
        $course = $this->db->get_where('course', array('COURSEID' => $id));
        echo json_encode($course->result());
    }

    function ajaxGetCourse(){
        $this->db->select('*');
        echo json_encode($this->db->get('course')->result_array());
    }

    function dataTablePopulateCourse(){
        $this->load->library('datatables',$this->datatableConfig);

        $this->datatables->SetIndexColumn('COURSEID');
        $this->datatables->SetColumns(array('COURSENAME','COURSEID'));

        $this->datatables->SetActiveCounter(true);
        $q = "SELECT * FROM course";
        $this->datatables->SetTable($q);
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF COURSE=======================//

    //=======================BEGIN OF COUNTRY=======================//
    public function countryList(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Country";
        $this->data['page']         = "master";
        $this->data['subpage']      = "country";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/countryList',$this->data);
        $this->load->view('_footer');
    }

    public function countryAdd(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Country Selection";
        $this->data['page']         = "master";
        $this->data['subpage']      = "country";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/countryAdd',$this->data);
        $this->load->view('_footer');
    }

    public function countryStore(){
        $countrynameina = $this->input->post('countrynameina');
        $countrynameeng = $this->input->post('countrynameeng');
        $phonecode      = $this->input->post('phonecode');

        $data = array(
            'COUNTRYNAMEINA'    => $countrynameina,
            'COUNTRYNAMEENG'    => $countrynameeng,
            'PHONECODE'         => $phonecode,
        );

        $this->db->insert('country',$data);
        redirect(base_url()."admin/countryList");
    }

    public function countryUpdate(){
        $id             = $this->input->post('countryid');
        $countrynameina = $this->input->post('countrynameina');
        $countrynameeng = $this->input->post('countrynameeng');
        $phonecode      = $this->input->post('phonecode');
        $blacklist      = $this->input->post('blacklist');

        $this->db->set('COUNTRYNAMEINA',$countrynameina);
        $this->db->set('COUNTRYNAMEENG',$countrynameeng);
        $this->db->set('PHONECODE',$phonecode);
        $this->db->set('BLACKLIST',$blacklist);
        $this->db->where('COUNTRYID',$id);
        $this->db->update('country');
    }

    public function countryDrop(){
        $id = $this->input->post('countryid');
        $this->db->where('COUNTRYID', $id);
        $this->db->delete('country');
    }

    public function countryAjaxGetDetail(){
        $id = $this->input->post('countryid');
        $country = $this->db->get_where('country', array('COUNTRYID' => $id));
        echo json_encode($country->row_array());
    }

    function dataTablePopulateCountry(){
        $this->load->library('datatables',$this->datatableConfig);

        //$dTables = new DataTables();

        $this->datatables->SetIndexColumn('COUNTRYID');
        $this->datatables->SetColumns(array('COUNTRYNAMEINA','COUNTRYNAMEENG','PHONECODE','BLACKLIST','COUNTRYID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * from country ");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF COUNTRY=======================//

    //=======================BEGIN OF REQUIREMENT=======================//
    public function requirementsList(){
        $this->data['pagetitle']    = "IO Tel-U | List of Requirements";
        $this->data['page']         = "master";
        $this->data['subpage']      = "requirement";

        $this->load->view('_header',$this->data);
        $this->load->view('admin/requirementsList',$this->data);
        $this->load->view('_footer');
    }

    public function requirementsAdd(){
        $this->data['pagetitle']    = "IO Tel-U | Add new Requirements";
        $this->data['page']         = "master";
        $this->data['subpage']      = "requirement";

        $listprivileges = array(
            array(2, 'ADMIN'),
            array(1, 'PARTICIPANT'),
            array(3, 'STAFF'),
            array(4, 'STRUCTURAL')
        );

        $this->data['listprivileges']     = $listprivileges;

        $this->load->view('_header',$this->data);
        $this->load->view('admin/requirementsAdd',$this->data);
        $this->load->view('_footer');
    }

    public function requirementsStore(){
        if($this->input->post('isrequired')){
            $isrequired  =$this->input->post('isrequired');
        }else{
            $isrequired  ='NO';
        }
        
        $requirementsname   = $this->input->post('requirementsname');
        $requirementstype   = $this->input->post('requirementstype');
        $privilege          = $this->input->post('privilege');
        $inputdate          = date('Y-m-d H:i:s');
        $users              = $this->session->userdata('users');
        $users              = $users[0];
        $inputby            = $users['USERID'];
        
        $data = array(
            'REQUIREMENTNAME'   => $requirementsname,
            'TYPE'              => $requirementstype,
            'PRIVILEGE'         => $privilege,
            'INPUTDATE'         => $inputdate,
            'UPDATEDATE'        => $inputdate,
            'ISREQUIRED'        => $isrequired,
            'INPUTBY'           => $inputby,
            'UPDATEBY'          => $inputby
        );

        if (!empty($_FILES['template-file']['name'])) {
            $this->config->load('upload');
            $this->load->library('upload');
          //  $this->upload_config = $this->config->item('documents');

            $this->upload_config['file_name'] = str_replace(' ', '-', trim(strtolower($requirementsname)));
            $this->upload->initialize($this->upload_config);

            if ( !$this->upload->do_upload('template-file')){
                $error = array('error' => $this->upload->display_errors());
                die(json_encode($error));
            }
            else{
                $uploadData = $this->upload->data();
                $data['TEMPLATEFILE'] = $uploadData['file_name'];
                $data['TEMPLATEFILEURL'] = "uploads/requirements/".$uploadData['file_name'];
            }
        }

        $response=$this->db->insert('requirements',$data);
        if($response) $this->session->set_flashdata('msg_success', 'INSERT DATA SUCCESSFULL');
                else $this->session->set_flashdata('msg_failed', 'FAILED INSERT DATA');
        redirect(base_url()."admin/requirementsList");
    }

    public function requirementEdit($id){
        $requirements       = $this->db->query("SELECT * FROM requirements WHERE REQUIREMENTID='$id'")->row_array();
        $assignedprivilege  = json_decode($requirements['PRIVILEGE']);

        $listprivileges = array(
            array(2, 'ADMIN'),
            array(1, 'PARTICIPANT'),
            array(3, 'STAFF'),
            array(4, 'STRUCTURAL')
        );

//        die(json_encode($listprivileges));

        $data['assignedprivilege']  = $assignedprivilege;
        $data['listprivileges']     = $listprivileges;
        $data['requirement']        = $requirements;
        $this->load->view('admin/requirement/_editRequirement',$data);
    }

    public function requirementsUpdate(){   
        if($this->input->post('isrequired')){
            $isrequired  =$this->input->post('isrequired');
        }else{
            $isrequired  ='NO';
        }   
        $isrequired  =$this->input->post('isrequired');
        $id                     = $this->input->post('requirementsid');
        $requirementsname       = $this->input->post('requirementsname');
        $requirementstype       = $this->input->post('requirementstype');
        $privilege              = $this->input->post('privilege');
        $updatedate             = date('Y-m-d H:i:s');
        $users                  = $this->session->userdata('users');
        $users                  = $users[0];
        $updateby               = $users['USERID'];
        
        $this->db->set('ISREQUIRED',$isrequired);
        $this->db->set('REQUIREMENTNAME',$requirementsname);
        $this->db->set('TYPE',$requirementstype);
        $this->db->set('PRIVILEGE',$privilege);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->set('UPDATEBY',$updateby);
        $this->db->where('REQUIREMENTID',$id);

        if (!empty($_FILES['template-file']['name'])) {
            $this->config->load('upload');
            $this->load->library('upload');
          //  $this->upload_config = $this->config->item('documents');

            $this->upload_config['file_name'] = str_replace(' ', '-', trim(strtolower($requirementsname)));
            $this->upload->initialize($this->upload_config);

            if ( ! $this->upload->do_upload('template-file')){
                $error = array('error' => $this->upload->display_errors());
                die(json_encode($error));
            }
            else{
                $uploadData = $this->upload->data();
                $this->db->set('TEMPLATEFILE',$uploadData['file_name']);
                $this->db->set('TEMPLATEFILEURL', "uploads/requirements/". $uploadData['file_name']);
            }
        }

        $response=$this->db->update('requirements');
        if($response) $this->session->set_flashdata('msg_success', 'UPDATE DATA SUCCESSFULL');
                else $this->session->set_flashdata('msg_failed', 'UPDATE INSERT DATA');
        redirect(base_url()."admin/requirementsList");
    }

    public function requirementsRemoveFile(){
        $id         = $this->input->post('requirementsid');
        $updatedate = date('Y-m-d H:i:s');
        $users      = $this->session->userdata('users');
        $users      = $users[0];
        $updateby   = $users['USERID'];

        $this->db->set('TEMPLATEFILE','');
        $this->db->set('TEMPLATEFILEURL','');
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->set('UPDATEBY',$updateby);
        $this->db->where('REQUIREMENTID',$id);
        $this->db->update('requirements');
    }

    public function requirementsDrop(){
        $id = $this->input->post('requirementsid');
        $this->db->where('REQUIREMENTID', $id);
        $this->db->delete('requirements');
    }

    public function requirementsAjaxGetDetail(){
        $id = $this->input->post('requirementsid');
        $requirements = $this->db->get_where('requirements', array('REQUIREMENTID' => $id));
        echo json_encode($requirements->result());
    }

    function dataTablePopulateRequirements(){
        $this->load->library('datatables',$this->datatableConfig);

        $this->datatables->SetIndexColumn('REQUIREMENTID');
        $this->datatables->SetColumns(array('REQUIREMENTNAME','TYPE','REQUIREMENTID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable(" SELECT * from requirements ORDER BY UPDATEDATE DESC");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF REQUIREMENT=======================//

    //=======================BEGIN OF MANAGE PROGRAM=======================//
    public function selectDegree($id){
        $this->data['pagetitle']    = "IO Tel-U | Manage Program";
        $this->data['page']         = "program";
        $this->data['programid']    = $id;

        $this->load->model("degreemodel");
        $this->load->model("programmodel");

        $program = $this->programmodel->getProgramById($id)->row_array();

        if($program==null)
            die('undefined id');

        if($program['PROGRAMTYPE']=='ACADEMIC'){
            $degtype = 'DEGREE';
        }
        else
            $degtype = 'SHORT COURSE';

        $q_degree = "SELECT * FROM degree WHERE TYPE='$degtype'";
        $q_assigned = "SELECT b.DEGREEID FROM programdegree a JOIN(degree b) USING (DEGREEID)  WHERE a.PROGRAMID=$id";

        $this->data['assigneddegree'] = $this->db->query($q_assigned)->result_array();
        $this->data['degrees'] = $this->db->query($q_degree)->result_array();

        $this->load->view('_header',$this->data);
        $this->load->view('admin/manageProgram/_selectDegree',$this->data);
        $this->load->view('_footer');
    }

    public function processSelectDegree($id){
        $this->load->model('programdegreemodel');
        $this->load->model('programmodel');
        $program = $this->programmodel->getProgramById($id)->row_array();

        if($program==null)
            die('undefined id');

        $degrees        = $this->input->post("degree");
        $inputdate      = date('Y-m-d H:i:s');
        $users          = $this->session->userdata('users');
        $users          = $users[0];
        $inputby        = $users['USERID'];

        $this->db->where('PROGRAMID', $id);
        $this->db->where_not_in('DEGREEID', $degrees);
        $this->db->delete('programdegree');

        foreach ($degrees as $dg){
            $this->db->where('PROGRAMID',$id);
            $this->db->where('DEGREEID',$dg);
            $available = $this->db->count_all_results('programdegree');

            if($available!=0)
                continue;

            $data = array(
                "DEGREEID"      => $dg,
                "PROGRAMID"     => $id,
                'UPDATEDATE'    => $inputdate,
                'INPUTBY'       => $inputby,
                'UPDATEBY'      => $inputby
            );

            $this->db->insert('programdegree',$data);
        }

        $program = $this->programmodel->getProgramById($id)->row_array();

        if($program['PROGRAMTYPE']=='ACADEMIC')
            redirect(base_url()."admin/manageProgram/$id/selectStudyProgram");
        else
            redirect(base_url()."admin/manageProgram/$id/selectCourse");
    }

    public function selectStudyProgram($id){
        $this->data['pagetitle']    = "IO Tel-U | Manage Program";
        $this->data['page']         = "program";
        $this->data['programid']    = $id;

        $this->load->model("programmodel");
        $this->load->model("studyprogrammodel");

        $program = $this->programmodel->getProgramById($id)->row_array();
        if($program==null)
            die('undefined id');


        $q = "SELECT * FROM programdegree JOIN (SELECT DEGREEID, DEGREE FROM degree) AS degree USING(DEGREEID) WHERE PROGRAMID=$id ORDER BY DEGREE";
        $this->data['programdegree'] = $this->db->query($q)->result_array();
        $this->data['studyprograms'] = $this->studyprogrammodel->GetAllData();


        $this->load->view('_header',$this->data);
        $this->load->view('admin/manageProgram/_selectStudyProgram',$this->data);
        $this->load->view('_footer');
    }

    public function selectCourse($id){
        $this->data['pagetitle']    = "IO Tel-U | Manage Program";
        $this->data['page']         = "program";
        $this->data['programid']    = $id;

        $this->load->model("coursemodel");
        $this->load->model("programmodel");

        $program = $this->programmodel->getProgramById($id)->row_array();
        if($program==null)
            die('undefined id');

        $this->data['courses'] = $this->coursemodel->GetAllData();

        $q = "SELECT * FROM programdegree JOIN (SELECT DEGREEID, DEGREE FROM degree) AS degree USING(DEGREEID) WHERE PROGRAMID=$id ORDER BY DEGREE";
        $this->data['programdegree'] = $this->db->query($q)->result_array();


        $this->load->view('_header',$this->data);
        $this->load->view('admin/manageProgram/_selectCourse',$this->data);
        $this->load->view('_footer');
    }

    public function setStudyProgram(){
        $programdegreeid = $this->input->post('programdegreeid');
        $studyprogramid = $this->input->post('studyprogramid');
        $languageid = $this->input->post('languageid');

        $data_langdelivmap = array(
          'LANGUAGEDELIVERYID'    => $languageid,
          'PROGRAMDEGREEID'       => $programdegreeid
        );

        $this->db->insert('languagedeliverymapping',$data_langdelivmap);
        $langdelivmapid = $this->db->insert_id();

        $this->db->select('*');
        $this->db->where('STUDYPROGRAMID',$studyprogramid);
        $this->db->where('LANGUAGEDELIVERYMAPPINGID',$langdelivmapid);
        $splang = $this->db->get('studyprogramlanguage')->row();

        if($splang==null){
            $data_splang = array(
                'STUDYPROGRAMID'    => $studyprogramid,
                'LANGUAGEDELIVERYMAPPINGID' => $langdelivmapid
            );
            $this->db->insert('studyprogramlanguage',$data_splang);
        }
    }

    public function unsetStudyProgram(){
        $langdelivmapid = $this->input->post('langdelivmapid');
        $studyprogramid = $this->input->post('studyprogramid');

        $this->db->where('LANGUAGEDELIVERYMAPPINGID',$langdelivmapid);
        $this->db->where('STUDYPROGRAMID',$studyprogramid);
        $this->db->delete('studyprogramlanguage');

        echo "success";
    }

    public function ajaxGetUnasignedStudyProgramLanguage(){
        $programdegreeid = $_GET['programdegreeid'];
        $studyprogramid = $_GET['studyprogramid'];
        $sub_q = "SELECT LANGUAGEDELIVERYID FROM languagedelivery
          JOIN languagedeliverymapping
          USING (LANGUAGEDELIVERYID)
          JOIN (studyprogramlanguage)
          USING (LANGUAGEDELIVERYMAPPINGID)
          JOIN (studyprogram)
          USING (STUDYPROGRAMID)
          WHERE PROGRAMDEGREEID=$programdegreeid
          AND STUDYPROGRAMID=$studyprogramid
        ";

        $q = "SELECT * FROM languagedelivery
            WHERE LANGUAGEDELIVERYID NOT IN ($sub_q)
         ";

        echo json_encode($this->db->query($q)->result_array());
    }

    public function ajaxGetUnasignedCourseLanguage(){
        $programdegreeid = $_GET['programdegreeid'];
        $courseid = $_GET['courseid'];
        $sub_q = "SELECT LANGUAGEDELIVERYID FROM languagedelivery
          JOIN languagedeliverymapping
          USING (LANGUAGEDELIVERYID)
          JOIN (courselanguage)
          USING (LANGUAGEDELIVERYMAPPINGID)
          JOIN (course)
          USING (COURSEID)
          WHERE PROGRAMDEGREEID=$programdegreeid
          AND COURSEID=$courseid
        ";

        $q = "SELECT * FROM languagedelivery
            WHERE LANGUAGEDELIVERYID NOT IN ($sub_q)
         ";

        echo json_encode($this->db->query($q)->result_array());
    }

    public function setCourse(){
        $programdegreeid = $this->input->post('programdegreeid');
        $courseid = $this->input->post('courseid');
        $languageid = $this->input->post('languageid');

        $data_langdelivmap = array(
            'LANGUAGEDELIVERYID'    => $languageid,
            'PROGRAMDEGREEID'       => $programdegreeid
        );

        $this->db->insert('languagedeliverymapping',$data_langdelivmap);
        $langdelivmapid = $this->db->insert_id();

        $this->db->select('*');
        $this->db->where('COURSEID',$courseid);
        $this->db->where('LANGUAGEDELIVERYMAPPINGID',$langdelivmapid);
        $splang = $this->db->get('courselanguage')->row();

        if($splang==null){
            $data_splang = array(
                'COURSEID'    => $courseid,
                'LANGUAGEDELIVERYMAPPINGID' => $langdelivmapid
            );
            $this->db->insert('courselanguage',$data_splang);
        }
    }

    public function unsetCourse(){
        $langdelivmapid = $this->input->post('langdelivmapid');
        $courseid = $this->input->post('courseid');

        $this->db->where('LANGUAGEDELIVERYMAPPINGID',$langdelivmapid);
        $this->db->where('COURSEID',$courseid);
        $this->db->delete('courselanguage');

        echo "success";
    }

    public function assignRequirement($id){
        $this->data['pagetitle']    = "IO Tel-U | Manage Program";
        $this->data['page']         = "program";
        $this->data['programid']    = $id;

        $this->load->model("requirementsmodel");
        $this->load->model('programmodel');

        $program = $this->programmodel->getProgramById($id)->row_array();
        if($program==null)
        die('undefined id');

        $q = "SELECT * FROM programdegree JOIN (SELECT DEGREEID, DEGREE FROM degree) AS degree USING(DEGREEID) WHERE PROGRAMID=$id";

        $this->data['requirements'] = $this->requirementsmodel->getAllData();
        $this->data['programdegree'] = $this->db->query($q)->result_array();
        $this->data['program'] = $program;

        $this->load->view('_header',$this->data);
        $this->load->view('admin/manageProgram/_assignRequirement',$this->data);
        $this->load->view('_footer');
    }

    public function setRequirement(){
        $programdegreeid = $this->input->post('programdegreeid');
        $requirementid = $this->input->post('requirementid');

        $data = array(
            'REQUIREMENTID'     => $requirementid,
            'PROGRAMDEGREEID'   => $programdegreeid
        );
        $this->db->insert('requirementmapping',$data);
    }

    public function unsetRequirement(){
        $requirementid = $this->input->post('requirementid');
        $programdegreeid = $this->input->post('programdegreeid');

        $this->db->where('REQUIREMENTID',$requirementid);
        $this->db->where('PROGRAMDEGREEID',$programdegreeid);
        $this->db->delete('requirementmapping');

        echo "success";
    }

    function ajaxGetRequirements(){
        $this->db->select('*');
        echo json_encode($this->db->get('requirements')->result_array());
    }

    public function dataTablePopulateAssignedCourse($programid,$degreeid){
        $this->load->library('datatables',$this->datatableConfig);

        $query = "SELECT * FROM course sp
          JOIN (courselanguage spl) USING (COURSEID)
          JOIN (languagedeliverymapping lpm) USING (LANGUAGEDELIVERYMAPPINGID)
          JOIN (languagedelivery ld) USING (LANGUAGEDELIVERYID)
          JOIN (SELECT PROGRAMDEGREEID, PROGRAMID, DEGREEID FROM programdegree) AS programdegree USING (PROGRAMDEGREEID)
          WHERE PROGRAMID = $programid
          AND DEGREEID = $degreeid
        ";

        $this->datatables->SetIndexColumn('LANGUAGEDELIVERYMAPPINGID');
        $this->datatables->SetColumns(array('COURSENAME','LANGUAGE','LANGUAGEDELIVERYMAPPINGID','COURSEID'));

        $this->datatables->SetActiveCounter(true);
        $this->datatables->SetTable($query);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }

    public function dataTablePopulateAssignedStudyProgram($programid,$degreeid){
        $this->load->library('datatables',$this->datatableConfig);

        $query = "SELECT * FROM studyprogram sp
          JOIN (studyprogramlanguage spl) USING (STUDYPROGRAMID)
          JOIN (languagedeliverymapping lpm) USING (LANGUAGEDELIVERYMAPPINGID)
          JOIN (languagedelivery ld) USING (LANGUAGEDELIVERYID)
          JOIN (SELECT PROGRAMDEGREEID, PROGRAMID, DEGREEID FROM programdegree) AS programdegree USING (PROGRAMDEGREEID)
          WHERE PROGRAMID = $programid
          AND DEGREEID = $degreeid
        ";

        $this->datatables->SetIndexColumn('LANGUAGEDELIVERYMAPPINGID');
        $this->datatables->SetColumns(array('STUDYPROGRAMNAME','LANGUAGE','LANGUAGEDELIVERYMAPPINGID','STUDYPROGRAMID'));

        $this->datatables->SetActiveCounter(true);
        $this->datatables->SetTable($query);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }

    public function dataTablePopulateAssignedRequirements($programdegreeid){
        $this->load->library('datatables',$this->datatableConfig);

        $query = "SELECT * FROM requirements
          JOIN (requirementmapping) USING (REQUIREMENTID)
          JOIN (SELECT PROGRAMDEGREEID, PROGRAMID, DEGREEID FROM programdegree) AS programdegree USING (PROGRAMDEGREEID)
          WHERE PROGRAMDEGREEID = $programdegreeid
        ";

        $this->datatables->SetIndexColumn('REQUIREMENTID');
        $this->datatables->SetColumns(array('REQUIREMENTNAME','TYPE','REQUIREMENTID','PROGRAMDEGREEID'));

        $this->datatables->SetActiveCounter(true);
        $this->datatables->SetTable($query);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }

    function dataTablePopulateAllStudyProgram(){
        $this->load->model("studyprogrammodel");

        echo json_encode($this->studyprogrammodel->getAllData());
    }

    function dataTablePopulateProgramDegree($id=0){
        $this->load->library('datatables',$this->datatableConfig);

        $query = "SELECT a.PROGRAMDEGREEID AS PROGRAMDEGREEID,
            a.PROGRAMID AS PROGRAMID,
            a.DEGREEID AS DEGREEID,
            c.DEGREE AS DEGREE
          FROM programdegree a, program b, degree c
          WHERE a.PROGRAMID = b.PROGRAMID
          AND a.DEGREEID = c.DEGREEID
          AND a.PROGRAMID = $id";

        $this->datatables->SetIndexColumn('PROGRAMDEGREEID');
        $this->datatables->SetColumns(array('DEGREE','PROGRAMDEGREEID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable($query);
//        $this->datatables->SetTable("SELECT * FROM program");
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF MANAGE PROGRAM=======================//

    //=======================BEGIN OF MANAGE COUPON=======================//
    public function selectEnrollment($id){
        $this->data['pagetitle']    = "IO Tel-U | Manage Coupon";
        $this->data['page']         = "coupon";
        $this->data['couponid'] = $id;

        $this->load->model("enrollmentmodel");
//        $this->load->model("coupondegreemodel");

        $q = "SELECT DISTINCT COUPONID,ENROLLMENTID
              FROM coupon
              LEFT JOIN (couponmapping) USING (COUPONID)
              LEFT JOIN(enrollment) USING (ENROLLMENTID)
              WHERE COUPONID='$id'";

        $assignedenrollment = $this->db->query($q)->row_array();

        if(!$assignedenrollment)
            die('undefined coupon id');

        $this->data['assignedenrollment'] = $assignedenrollment;
        $this->data['enrollments'] = $this->enrollmentmodel->GetAllData();

        $this->load->view('_header',$this->data);
        $this->load->view('admin/manageCoupon/_selectEnrollment',$this->data);
        $this->load->view('_footer');
    }

    public function processSelectEnrollment(){
        $enrollmentid=$this->input->post('enrollmentid');
        $couponid=$this->input->post('couponid');
        redirect(base_url()."admin/manageCoupon/selectCountry/$couponid/$enrollmentid");
    }

    public function selectCountry($couponid,$enrollid){
        $this->data['pagetitle']    = "IO Tel-U | Manage Coupon";
        $this->data['page']         = "coupon";
        $this->data['couponid']     = $couponid;
        $this->data['enrollmentid'] = $enrollid;

        $this->data['couponmapping'] = array(
            'couponid'      => $couponid,
            'enrollmentid'  => $enrollid
        );

        $this->load->model("enrollmentmodel");
        $this->data['enrollments'] = $this->enrollmentmodel->getEntrollmentById($enrollid);

        $this->load->model("countrymodel");
        $this->data['countrys'] = $this->countrymodel->GetAllCountry();

        $q = "SELECT * FROM country";
        $this->data['country'] = $this->db->query($q)->result_array();


        $this->load->view('_header',$this->data);
        $this->load->view('admin/manageCoupon/_selectCountry',$this->data);
        $this->load->view('_footer');
    }

    public function setCountry(){
        $couponid = $this->input->post('couponid');
        $enrollmentid = $this->input->post('enrollmentid');
        $countryid = $this->input->post('countryid');

        $data = array(
            'COUPONID'      => $couponid,
            'ENROLLMENTID'  => $enrollmentid,
            'COUNTRYID'     => $countryid
        );
        $this->db->insert('couponmapping',$data);
    }

    public function unsetCountry(){
        $couponmappingid = $this->input->post('couponmappingid');

        $this->db->where('COUPONMAPPINGID',$couponmappingid);
        $this->db->delete('couponmapping');
    }

    public function dataTablePopulateAssignedCountry($couponid,$enrollid){
        $this->load->library('datatables',$this->datatableConfig);

        $query = "SELECT * FROM country
          JOIN (couponmapping) USING (COUNTRYID)
          WHERE couponid = $couponid
          AND enrollmentid = $enrollid
        ";

        $this->datatables->SetIndexColumn('COUPONMAPPINGID');
        $this->datatables->SetColumns(array('COUNTRYNAMEENG','COUNTRYNAMEINA','COUPONMAPPINGID'));

        $this->datatables->SetActiveCounter(true);
        $this->datatables->SetTable($query);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }



    //=======================BEGIN OF ENROLLMENT=======================//
    public function enrollmentList(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Enrollment Selection";
        $this->data['page']         = "enrollment";

        $this->load->model('programmodel');
        $programs = $this->programmodel->getAllData();
        $this->data['programs']=$programs;
        //echo "<pre>"; print_r($this->data);die;
        $this->load->view('_header',$this->data);
        $this->load->view('admin/enrollmentList',$this->data);
        $this->load->view('_footer');
    }

    public function enrollmentAdd(){
        $this->data['pagetitle']    = "IO Tel-U | List Of Enrollment Selection";
        $this->data['page']         = "enrollment";

        $this->load->model('programmodel');
        $programs = $this->programmodel->getAllData();

        $this->data['programs']=$programs;
        $this->load->view('_header',$this->data);
        $this->load->view('admin/enrollmentAdd',$this->data);
        $this->load->view('_footer');
    }

    public function enrollmentStore(){
        $description            = $this->input->post('enrollmentname');
        $programid              = $this->input->post('program');
        $degreeid               = $this->input->post('degree');
        $year                   = $this->input->post('year');
        $start                  = $this->input->post('startdate');
        $end                    = $this->input->post('enddate');
        $period_year            = $this->input->post('period_year');
        $period_bulan           = $this->input->post('period_bulan');
        $period = $period_year.'-'.$period_bulan;
        $programdescription     = $this->input->post('programdescription');
        $status                 = $this->input->post('status');
        $updatedate             = date('Y-m-d H:i:s');
        $users                  = $this->session->userdata('users');
        $users                  = $users[0];
        $updateby               = $users['USERID'];

        $this->db->select("PROGRAMDEGREEID");
        $this->db->where('PROGRAMID',$programid);
        $this->db->where('DEGREEID',$degreeid);
        $programdegree = $this->db->get('programdegree')->row();

        $data = array(
            'DESCRIPTION'           => $description,
            'ACADEMICYEAR'          => $year,
            'PERIOD'                => $period,
            'STARTDATE'             => date('Y-m-d', strtotime($start)),
            'ENDDATE'               => date('Y-m-d', strtotime($end)),
            'PROGRAMDESCRIPTION'    => $programdescription,
            'ENROLLMENTSTATUS'      => $status,
            'INPUTBY'               => $updateby,
            'INPUTDATE'             => $updatedate,
            'PROGRAMDEGREEID'       => $programdegree->PROGRAMDEGREEID
        );

        $response = $this->db->insert('enrollment',$data);
        $this->load->model('enrollmentmodel');
        $filter1 = $this->enrollmentmodel->getAllFilterBy('ACADEMICYEAR');
        $filter2 = $this->enrollmentmodel->getAllFilterBy('PERIOD');
        $stat1 =0;
        $stat2 = 0;
        foreach($filter1 as $ker => $val){
            if($year==$val['FILTERNAME']){
                $stat1 = 1;
            }
        }
        foreach($filter2 as $ker => $val){
            if($period==$val['FILTERNAME']){
                $stat2 = 1;
            }
        }
        if($stat1==0){
            $data=array(
                'FILTERNAME' => $year,
                'COLUMN' => 'ACADEMICYEAR'
            );
            $this->db->insert('defaultfilter',$data);
        }
        if($stat2==0){
            $data=array(
                'FILTERNAME' => $period,
                'COLUMN' => 'PERIOD'
            );
            $this->db->insert('defaultfilter',$data);
        }
        if($response) $this->session->set_flashdata('msg_success', 'INSERT DATA SUCCESSFULL');
            else $this->session->set_flashdata('msg_failed', 'FAILED UPDATE DATA');
        redirect(base_url()."admin/enrollmentList");
    }

    public function enrollmentEdit($id){
        $this->db->join('programdegree', 'programdegree.PROGRAMDEGREEID = enrollment.PROGRAMDEGREEID');
        $this->db->join('program', 'programdegree.PROGRAMID = program.PROGRAMID');
        $this->db->join('degree', 'programdegree.DEGREEID = degree.DEGREEID');
        $this->db->select('*');
        $enrollment = $this->db->get_where('enrollment', array('ENROLLMENTID' => $id));
        $result = $enrollment->row_array();

        if($result==null)
            die("id not found");

        $result['STARTDATE'] = date("m/d/Y",strtotime($result['STARTDATE']));
        $result['ENDDATE'] = date("m/d/Y",strtotime($result['ENDDATE']));

        $data['enrollment']=$result;
        $this->load->model('programmodel');
        $programs = $this->programmodel->getAllData2();       
        $degrees = $this->programmodel->GetProgramDegreeById($result['PROGRAMID'])->result_array();
        $data['programs']=$programs;
        $data['degrees']=$degrees;
        $this->load->view('admin/enrollment/_editEnrollment',$data);
    }

    public function enrollmentUpdate(){
        $id                 = $this->input->post('enrollmentid');
        $this->load->model('enrollmentmodel');
        $this->load->model('programdegreemodel');
        $data = $this->enrollmentmodel->getEntrollmentById($id);
        $year = $data['ACADEMICYEAR'];
        $period = $data['PERIOD'];
        $filter11 = $this->enrollmentmodel->getAcademicYear();
        $filter12 = $this->enrollmentmodel->getPeriod();
        $stat11 =0;
        $stat12 = 0;
        foreach($filter11 as $ker => $val){
            if($year==$val['ACADEMICYEAR']){
                $stat11 = 1;
            }
        }
        foreach($filter12 as $ker => $val){
            if($period==$val['PERIOD']){
                $stat12 = 1;
            }
        }
        if($stat11==1){
            $this->db->where('FILTERNAME',$year);
            $this->db->where('COLUMN','ACADEMICYEAR');
            $this->db->delete('defaultfilter');
        }
        if($stat12==1){
            $this->db->where('FILTERNAME',$period);
            $this->db->where('COLUMN','PERIOD');
            $this->db->delete('defaultfilter');
        }
        
        
        //echo "<pre>"; print_r($_POST);die;
        $description        = $this->input->post('description');
        $programdescription = $this->input->post('programdescription');
        $status             = $this->input->post('status');
        $year               = $this->input->post('year');
        $start              = $this->input->post('startdate');
        $end                = $this->input->post('enddate');
        $period_lama            = $this->input->post('period_year');
        $period_year            = $this->input->post('period_year');
        $period_bulan           = $this->input->post('period_bulan');
        $period = $period_year.'-'.$period_bulan;
        $updatedate         = date('Y-m-d H:i:s');
        $users              = $this->session->userdata('users');
        $users              = $users[0];
        $updateby           = $users['USERID'];
        $tamps = explode('_',$this->input->post('degree')); 
        $degree = $tamps[0];
        $programdegree = $tamps[1];
        $program = $this->input->post('program');
        // $arr = array(
        //     'DEGREEID' => $degree,
        //     'PROGRAMID' => $program,
        //     'UPDATEDATE' => $updatedate,
        //     'UPDATEBY' => $updateby
        // );
        // $this->programdegreemodel->editProgramDegree($programdegree, $arr);

        $this->db->set('ACADEMICYEAR',$year);
        $this->db->set('ENROLLMENTSTATUS',$status);
        $this->db->set('STARTDATE',date('Y-m-d', strtotime($start)));
        $this->db->set('ENDDATE',date('Y-m-d', strtotime($end)));
        $this->db->set('PERIOD',$period);
        $this->db->set('PROGRAMDEGREEID',$programdegree);
        $this->db->set('DESCRIPTION',$description);
        $this->db->set('PROGRAMDESCRIPTION',$programdescription);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->set('UPDATEBY',$updateby);
        $this->db->where('ENROLLMENTID',$id);

        if (!empty($_FILES['imagefile']['name'])) {
            $this->config->load('upload');
            $this->load->library('upload');
            $this->upload_config['file_name'] = $id . '-' . str_replace(' ', '-', trim(strtolower($_FILES['imagefile']['name'])));
            $this->upload->initialize($this->upload_config);

            if ( ! $this->upload->do_upload('imagefile')){
                $error = array('error' => $this->upload->display_errors());
                die(json_encode($error));
            }
            else{
                $uploadData = $this->upload->data();
                $this->db->set('IMAGE', $this->upload_config['access_path'] . $uploadData['file_name']);
            }
        }
        $response=$this->db->update('enrollment');
        

       
        $filter1 = $this->enrollmentmodel->getAllFilterBy('ACADEMICYEAR');
        $filter2 = $this->enrollmentmodel->getAllFilterBy('PERIOD');
        $stat1 =0;
        $stat2 = 0;
        foreach($filter1 as $ker => $val){
            if($year==$val['FILTERNAME']){
                $stat1 = 1;
            }
        }
        foreach($filter2 as $ker => $val){
            if($period==$val['FILTERNAME']){
                $stat2 = 1;
            }
        }
        if($stat1==0){
            $data=array(
                'FILTERNAME' => $year,
                'COLUMN' => 'ACADEMICYEAR'
            );
            $this->db->insert('defaultfilter',$data);
        }
        if($stat2==0){
            $data=array(
                'FILTERNAME' => $period,
                'COLUMN' => 'PERIOD'
            );
            $this->db->insert('defaultfilter',$data);
        }
        if($response) $this->session->set_flashdata('msg_success', 'UPDATE DATA SUCCESSFULL');
            else $this->session->set_flashdata('msg_failed', 'FAILED UPDATE DATA');
        redirect(base_url()."admin/enrollmentList");
    }

    public function enrollmentDrop(){
        $id = $this->input->post('enrollmentid');       
        $this->load->model('enrollmentmodel');
        $data = $this->enrollmentmodel->getEntrollmentById($id);
        $year = $data['ACADEMICYEAR'];
        $period = $data['PERIOD'];
        $filter1 = $this->enrollmentmodel->getAcademicYear();
        $filter2 = $this->enrollmentmodel->getPeriod();
        $stat1 =0;
        $stat2 = 0;
        foreach($filter1 as $ker => $val){
            if($year==$val['ACADEMICYEAR']){
                $stat1 = 1;
            }
        }
        foreach($filter2 as $ker => $val){
            if($period==$val['PERIOD']){
                $stat2 = 1;
            }
        }
        if($stat1==1){
            $this->db->where('FILTERNAME',$year);
            $this->db->where('COLUMN','ACADEMICYEAR');
            $this->db->delete('defaultfilter');
        }
        if($stat2==1){
            $this->db->where('FILTERNAME',$period);
            $this->db->where('COLUMN','PERIOD');
            $this->db->delete('defaultfilter');
        }
        $this->db->where('ENROLLMENTID', $id);
        $this->db->delete('enrollment');
        //echo "<pre>"; print_r($data);die;
    }

    public function enrollmentRemoveFile(){
        $id         = $this->input->post('enrollmentid');
        $updatedate = date('Y-m-d H:i:s');
        $users      = $this->session->userdata('users');
        $users      = $users[0];
        $updateby   = $users['USERID'];

        $this->db->set('IMAGE','');
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->set('UPDATEBY',$updateby);
        $this->db->where('ENROLLMENTID',$id);
        $this->db->update('enrollment');
    }

    public function enrollmentAjaxGetDetail($id){
        $this->db->join('programdegree', 'programdegree.PROGRAMDEGREEID = enrollment.PROGRAMDEGREEID');
        $this->db->join('program', 'programdegree.PROGRAMID = program.PROGRAMID');
        $this->db->join('degree', 'programdegree.DEGREEID = degree.DEGREEID');
        $this->db->select('*');
        $enrollment = $this->db->get_where('enrollment', array('ENROLLMENTID' => $id));
        $result = $enrollment->result();
        $result[0]->STARTDATE = date("m/d/Y",strtotime($result[0]->STARTDATE));
        $result[0]->ENDDATE = date("m/d/Y",strtotime($result[0]->ENDDATE));
        echo json_encode($result);
    }

    function dataTablePopulateEnrollment(){
        
        $this->load->library('datatables',$this->datatableConfig);

        $this->datatables->SetIndexColumn('ENROLLMENTID');
        $this->datatables->SetColumns(array('DESCRIPTION','PROGRAMNAME','DEGREE','PERIOD','ACADEMICYEAR','ENROLLMENTSTATUS','ENROLLMENTID'));

        $this->datatables->SetActiveCounter(true);
        //$this->datatables->SetEncryptColumn(10);
        $qenrollment = "SELECT * FROM enrollment
                        LEFT JOIN (SELECT PROGRAMDEGREEID,DEGREEID,PROGRAMID FROM programdegree) AS programdegree USING(PROGRAMDEGREEID)
                        LEFT JOIN (SELECT DEGREEID, DEGREE FROM degree) AS degree USING (DEGREEID)
                        LEFT JOIN (SELECT PROGRAMNAME, PROGRAMID FROM program) AS program USING (PROGRAMID)
                        ";
        if($_GET['status']!=''){
            $qenrollment .= " WHERE enrollment.ENROLLMENTSTATUS = '".$_GET['status']."'";
        }
        $qenrollment .= "
        ORDER BY enrollment.UPDATEDATE DESC";
        //echo "<pre>"; print_r($qenrollment);die;
        $this->datatables->SetTable($qenrollment);
        $dt = $this->datatables->GetDataTables();
        // echo $this->datatables->GetActiveQuery();
        echo $dt;
    }
    //=======================END OF ENROLLMENT=======================//

    //=======================BEGIN OF USER=======================//
    public function userManagement(){
        $this->data['pagetitle']    = "IO Tel-U | List Of userManagement Selection";
        $this->data['page']         = "user";

        $this->load->model('programmodel');
        $programs = $this->programmodel->getAllData();
        $this->data['programs']=$programs;
        $this->db->select('*');
        $this->db->where('USERGROUPID !=','1');
        $groups = $this->db->get('usergroups')->result_array();

        $this->data['groups']=$groups;

        $this->load->view('_header',$this->data);
        $this->load->view('admin/userList',$this->data);
        $this->load->view('_footer');
    }

    public function userAdd(){
        $this->data['pagetitle']    = "IO Tel-U | List Of userManagement Selection";
        $this->data['page']         = "user";

        $this->db->select('*');
        $this->db->where('USERGROUPID !=','1');
        $groups = $this->db->get('usergroups')->result_array();

        $this->data['groups']=$groups;
        $this->load->view('_header',$this->data);
        $this->load->view('admin/userAdd',$this->data);
        $this->load->view('_footer');
    }

    public function userStore(){
        $fullname       = $this->input->post('fullname');
        $usergroup      = $this->input->post('usergroup');
        $email          = $this->input->post('email');
        $password       = md5($this->input->post('password'));
        $status         = $this->input->post('status');
        $updatedate     = date('Y-m-d H:i:s');


        $data = array(
            'EMAIL'         => $email,
            'PASSWORD'      => $password,
            'FULLNAME'      => $fullname,
            'INPUTDATE'     => $updatedate,
            'USERGROUPID'   => $usergroup,
            'ACTIVESTATUS'  => $status
        );

        $this->db->insert('users',$data);
        redirect(base_url()."admin/userManagement");
    }

    public function userUpdate(){
        $id             = $this->input->post('userid');
        $fullname       = $this->input->post('fullname');
        $email          = $this->input->post('email');
        $usergroupid    = $this->input->post('usergroup');
        $status         = $this->input->post('status');
        $updatedate     = date('Y-m-d H:i:s');

        $this->db->set('EMAIL',$email);
        $this->db->set('FULLNAME',$fullname);
        $this->db->set('ACTIVESTATUS',$status);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->set('USERGROUPID',$usergroupid);
        $this->db->where('USERID',$id);
        $this->db->update('users');
    }

    public function userChangePassword(){
        $id             = $this->input->post('userid');
        $password       = md5($this->input->post('password'));
        $updatedate     = date('Y-m-d H:i:s');

        $this->db->set('PASSWORD',$password);
        $this->db->set('UPDATEDATE',$updatedate);
        $this->db->where('USERID',$id);
        $this->db->update('users');
    }

    public function userDrop(){
        $id = $this->input->post('userid');
        $this->db->where('USERID', $id);
        $this->db->delete('users');
    }

    public function userManagementAjaxGetDetail(){
        $id = $this->input->post('userManagementid');
        // $id=7;
        $userManagement = $this->db->get_where('userManagement', array('USERID' => $id));
        $result = $userManagement->result();
        $result[0]->STARTDATE = date("m/d/Y",strtotime($result[0]->STARTDATE));
        $result[0]->ENDDATE = date("m/d/Y",strtotime($result[0]->ENDDATE));
        echo json_encode($result);
    }

    function userAjaxGetDetail($id){
        $this->db->select('USERID,EMAIL,FULLNAME,ACTIVESTATUS,users.USERGROUPID');
        $this->db->join('usergroups','users.USERGROUPID=usergroups.USERGROUPID');
        $this->db->where('USERID',$id);
        $data = $this->db->get('users')->row();

        echo json_encode($data);
    }

    function dataTablePopulateUsers(){
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('USERID');
        $this->datatables->SetColumns(array('FULLNAME','EMAIL','USERGROUPNAME','ACTIVESTATUS','USERID'));
        
        $this->datatables->SetActiveCounter(true);      
        $users          = $this->session->userdata('users');
        $users          = $users[0];
        $userid       = $users['USERID'];

        $quserManagement = "SELECT * FROM users
                            JOIN (usergroups) USING (USERGROUPID)
                            WHERE USERID != $userid";
       
        //$this->datatables->SetEncryptColumn(10);
        $this->datatables->SetTable($quserManagement);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }
    //=======================END OF USER=======================//
    public function isLoginAdmin(){
        $users=$this->session->userdata('users');
        $users=$users[0];
        if($this->session->userdata('login')&&$users['USERGROUPID']==ADMINUSERGROUPID){

        }else{
            redirect(base_url()."login");
        }
    }

    public function changePassword(){
        $this->data['pagetitle']    = "IO Tel-U | List Of userManagement Selection";
        $this->data['page']         = "user";

        $this->db->select('*');
        $this->db->where('USERGROUPID !=','1');
        $groups = $this->db->get('usergroups')->result_array();

        $this->data['groups']=$groups;
        $this->load->view('_header',$this->data);
        $this->load->view('admin/userAdd',$this->data);
        $this->load->view('_footer');
    }
    
    //DASHBOARD
    public function dashboardEnrollment(){
        $this->data['pagetitle']="IO Tel-U | Dashboard";
        $this->load->model('enrollmentmodel');
        $this->data['data_enrollment'] = $this->enrollmentmodel->getEnrollment('','');
        $this->load->model('countrymodel');
        $this->data['data_country'] = $this->countrymodel->getCountry();
        $this->load->model('studyprogrammodel');
        $this->load->model('participantmodel');
        $this->load->model('countrymodel');
        $this->data['data_studyprogram'] = $this->studyprogrammodel->getStudyProgram('',"");
        $number_acounts = $this->participantmodel->getNumberAcounts('');
        $number_participants = $this->participantmodel->getNumberParticipants('','','');
        $number_scholarship = $this->participantmodel->getNumberParticipants('scholarship','','');
        $number_not_scholarship = $this->participantmodel->getNumberParticipants('notscholarship','','');
        $number_country= $this->countrymodel->getNumberCountry('','','');
        $number_students = $this->participantmodel->getNumberParticipants('students','','');
        $step1 = $this->participantmodel->getStepParticipants(1,'','');
        $step2 = $this->participantmodel->getStepParticipants(2,'','');
        $step3 = $this->participantmodel->getStepParticipants(3,'','');
        $step4 = $this->participantmodel->getStepParticipants(4,'','');
        $step5 = $this->participantmodel->getStepParticipants(5,'','');
        //$step6 = $this->participantmodel->getStepParticipants(6,'');
        $step7 = $this->participantmodel->getStepSeven(7,'','');
        
        $number_accept = $this->participantmodel->getParticipantStatus('ACCEPTED','','');
        $number_unaccept = $this->participantmodel->getParticipantStatus('UNACCEPTED','','');
        $number_unscheduled_interview = $this->participantmodel->getInterview('','unscheduled_interview','');
        $number_not_interview = $this->participantmodel->getInterview('','not_interview','');
        $number_interview = $this->participantmodel->getInterview('','have_interview','');
        $this->data['number_acounts'] = $number_acounts[0]['JUMLAH'];
        $this->data['number_participants'] = $number_participants[0]['JUMLAH'];
        $this->data['number_scholarship'] = $number_scholarship[0]['JUMLAH'];
        $this->data['number_not_scholarship'] = $number_not_scholarship[0]['JUMLAH'];
        $this->data['number_country'] = $number_country[0]['JUMLAH'];
        $this->data['number_students'] = $number_students[0]['JUMLAH'];
        $this->data['step1'] = $step1[0]['JUMLAH'];
        $this->data['step2'] = $step2[0]['JUMLAH'];
        $this->data['step3'] = $step3[0]['JUMLAH'];
        $this->data['step4'] = $step4[0]['JUMLAH'];
        $this->data['step5'] = $step5[0]['JUMLAH'];
        
        $this->data['step7'] = $step7[0]['JUMLAH'];
        $this->data['number_interview'] = $number_interview[0]['JUMLAH'];
        $this->data['number_unscheduled_interview'] = $number_unscheduled_interview[0]['JUMLAH'];
        $this->data['number_not_interview'] = $number_not_interview[0]['JUMLAH'];
        $this->data['number_confirm'] = $this->participantmodel->getConfirmDate('accept','','');
        //$this->data['number_absent'] = $this->participantmodel->getConfirmDate('unaccept','');
        $this->data['number_accept'] = $number_accept[0]['JUMLAH'];
        $this->data['number_unaccept'] = $number_unaccept[0]['JUMLAH'];
        $this->data['academicyear'] = $this->enrollmentmodel->getAcademicYear();
        $this->data['period'] = $this->enrollmentmodel->getPeriod();
        $filter = $this->enrollmentmodel->getFilterStatus('Y','ACADEMICYEAR');
        if(count($filter)==0){
            $this->data['academicyear_filter'] = 0;
        }else{
            $this->data['academicyear_filter'] = $filter;
        }
        //echo "<pre>"; print_r($this->data['academicyear_filter']);die;
        $this->load->view('_header',$this->data);
        $this->load->view('admin/dashboardEnrollment',$this->data);
        $this->load->view('_footer');
    }
    
    public function datatableEnrollment(){
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('PROGRAMID');
        $this->datatables->SetColumns(array('PROGRAMNAME','JUMLAH','PROGRAMID','ACADEMICYEAR'));
        $this->datatables->SetActiveCounter(true);
        //echo "<pre>"; print_r($_GET['academicyear']);
        
        $periods ='';
        if($_GET['period']!=''){
            $period = explode(",",$_GET['period']);
            //echo "<pre>"; print_r($period);die;
            for($x=0;$x<count($period);$x++){
                    if($period[$x]=='UNDEFINED YEAR'){
                        $period[$x] = "";
                    }
                    if(($x+1)==count($period)){
                        $periods .= " E.PERIOD = '".$period[$x]."'";
                    }else{
                        
                        $periods .= " E.PERIOD = '".$period[$x]."' OR";
                    }
            }
        }
        //echo "<pre>"; print_r($periods);
        $sql = "SELECT
    A.PROGRAMNAME, 
    A.PROGRAMID,
    B.ACADEMICYEAR,
    COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
    LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
            JOIN participants D ON (C.PROGRAMID=D.PROGRAMID)
            LEFT JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
            WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' ";
        if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods==''){   
            $sql .= " AND E.ACADEMICYEAR=".$_GET['academicyear'];
        }else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){   
            $sql .= " AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (".$periods.")";
        }else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){    
            $sql .= " AND E.ACADEMICYEAR IS NULL AND (".$periods.")";
        }else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){   
            $sql .= " AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (E.PERIOD IS NULL)";
        }else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){    
            $sql .= " AND E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL)";
        }else if($_GET['academicyear']=='' && $periods=='UNDEFINED YEAR'){  
            $sql .= " AND (E.PERIOD IS NULL)";
        }else if($_GET['academicyear']=='' && $periods!='' && $periods!='UNDEFINED YEAR'){  
            $sql .= " AND (".$periods.")";
        }
        
        $sql .= ")B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
            GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
                SELECT
                    A.SCHOLARSHIPTYPENAME, 
                    A.SCHOLARSHIPTYPEID,
                    B.ACADEMICYEAR,
                    COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
                FROM scholarshiptype A
                LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
                    JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID) 
                    LEFT JOIN enrollment E ON (D.ENROLLMENTID=E.ENROLLMENTID)
                    WHERE D.ACCEPTANCESTATUS = 'ACCEPTED' ";
        
        if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods==''){   
            $sql .= "AND E.ACADEMICYEAR=".$_GET['academicyear'];
        }else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){   
            $sql .= "AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (".$periods.")";
        }else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){    
            $sql .= "AND E.ACADEMICYEAR IS NULL AND (".$periods.")";
        }else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){   
            $sql .= "AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (E.PERIOD IS NULL)";
        }else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){    
            $sql .= "AND E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL)";
        }else if($_GET['academicyear']=='' && $periods=='UNDEFINED YEAR'){  
            $sql .= "AND (E.PERIOD IS NULL)";
        }else if($_GET['academicyear']=='' && $periods!='' && $periods!='UNDEFINED YEAR'){  
            $sql .= " AND (".$periods.")";
        }
        $sql .= ")B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
                    GROUP BY A.SCHOLARSHIPTYPENAME, 
                    A.SCHOLARSHIPTYPEID
                ";
        //}else{
    /*  $sql = "SELECT
    A.PROGRAMNAME, 
    A.PROGRAMID,
    B.ACADEMICYEAR,
    COUNT(B.PROGRAMID) AS JUMLAH
FROM program A
    LEFT JOIN (SELECT C.PROGRAMNAME, C.PROGRAMID,D.ACADEMICYEAR FROM program C
            JOIN participants D ON (C.PROGRAMID=D.PROGRAMID) 
            LEFT JOIN enrollment E ON (B.ENROLLMENTID=E.ENROLLMENTID)
            WHERE D.ACCEPTANCESTATUS = 'ACCEPTED')B ON (A.PROGRAMID=B.PROGRAMID)
           WHERE A.PROGRAMNAME NOT IN ('SCHOLARSHIP')
            GROUP BY A.PROGRAMNAME, A.PROGRAMID
            UNION
                SELECT
                    A.SCHOLARSHIPTYPENAME, 
                    A.SCHOLARSHIPTYPEID,
                    B.ACADEMICYEAR,
                    COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
                FROM scholarshiptype A
                LEFT JOIN (SELECT C.SCHOLARSHIPTYPENAME, C.SCHOLARSHIPTYPEID,D.ACADEMICYEAR FROM scholarshiptype C
                    JOIN participants D ON (C.SCHOLARSHIPTYPEID=D.SCHOLARSHIPTYPEID) 
                    WHERE D.ACCEPTANCESTATUS = 'ACCEPTED')B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
                    GROUP BY A.SCHOLARSHIPTYPENAME, 
                    A.SCHOLARSHIPTYPEID
                ";
        }*/
        //echo "<pre>"; print_r($sql);die;
        $this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }
    
    public function dashboardCountry(){ 
        $this->data['pagetitle']="IO Tel-U | Dashboard";    
        $this->load->model('countrymodel');
        $this->data['data_country'] = $this->countrymodel->getCountry();
        $this->load->view('_header',$this->data);
        $this->load->view('admin/dashboardCountry',$this->data);
        $this->load->view('_footer');
    }
    
    public function datatableCountryParticipant(){
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('NATIONALITY');
        $this->datatables->SetColumns(array('NATIONALITY','JUMLAH'));
        $this->datatables->SetActiveCounter(true);
        $periods ='';
        if($_GET['period']!=''){
            $period = explode(",",$_GET['period']);
            //echo "<pre>"; print_r($period);die;
            for($x=0;$x<count($period);$x++){
                    if($period[$x]=='UNDEFINED YEAR'){
                        $period[$x] = "";
                    }
                    if(($x+1)==count($period)){
                        $periods .= " E.PERIOD = '".$period[$x]."'";
                    }else{
                        
                        $periods .= " E.PERIOD = '".$period[$x]."' OR";
                    }
            }
        }
            
        
        if($_GET['acceptance']==''){            
            $sql = "SELECT DISTINCT
  B.NATIONALITY,
  COUNT(B.NATIONALITY) AS JUMLAH
FROM
participants B
LEFT JOIN enrollment E ON (B.ENROLLMENTID=E.ENROLLMENTID)
";
            if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods==''){
                $sql .= "  WHERE E.ACADEMICYEAR=".$_GET['academicyear'];
            }else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){
                $sql .= "  WHERE E.ACADEMICYEAR=".$_GET['academicyear']." AND (".$periods.")";
            }else if($_GET['academicyear']=='' && $periods!='' && $periods!='UNDEFINED YEAR'){
                $sql .= "  WHERE (".$periods.")";
            }else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){
                $sql .= "  WHERE E.ACADEMICYEAR IS NULL AND (".$periods.")";
            }else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods==''){
                $sql .= "  WHERE E.ACADEMICYEAR IS NULL";
            }else if($_GET['academicyear']=='' && $periods=='UNDEFINED YEAR'){
                $sql .= "  WHERE (E.PERIOD IS NULL)";
            }else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){
                $sql .= "  WHERE E.ACADEMICYEAR=".$_GET['academicyear']." AND (E.PERIOD IS NULL)";
            }
            $sql .= " GROUP BY B.NATIONALITY";
        }else{
            $sql = "SELECT
  B.NATIONALITY,
  COUNT(B.NATIONALITY) AS JUMLAH
FROM
participants B
LEFT JOIN enrollment E ON (B.ENROLLMENTID=E.ENROLLMENTID) WHERE B.ACCEPTANCESTATUS = '".$_GET['acceptance']."' ";
            if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods==''){
                $sql .= "  AND E.ACADEMICYEAR=".$_GET['academicyear'];
            }else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){
                $sql .= "  AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (".$periods.")";
            }else if($_GET['academicyear']=='' && $periods!='' && $periods!='UNDEFINED YEAR'){
                $sql .= "  AND (".$periods.")";
            }else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods!='' && $periods!='UNDEFINED YEAR'){
                $sql .= "  AND E.ACADEMICYEAR IS NULL AND (".$periods.")";
            }else if($_GET['academicyear']=='UNDEFINED YEAR' && $periods==''){
                $sql .= "  AND E.ACADEMICYEAR IS NULL";
            }else if($_GET['academicyear']=='' && $periods=='UNDEFINED YEAR'){
                $sql .= "  AND (E.PERIOD IS NULL)";
            }else if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods=='UNDEFINED YEAR'){
                $sql .= "  AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (E.PERIOD IS NULL)";
            }
            $sql .= " GROUP BY B.NATIONALITY";
        }
        //echo "<pre>"; print_r($sql);//die;
        $this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }
    
    public function datatableCountryParticipant2(){
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('NATIONALITY');
        $this->datatables->SetColumns(array('NATIONALITY','JUMLAH'));
        
        $this->datatables->SetActiveCounter(true);
        if($_GET['acceptance']==''){            
            $sql = "SELECT
  B.NATIONALITY,
  COUNT(B.NATIONALITY) AS JUMLAH
FROM
participants B";
            $sql .= " GROUP BY B.NATIONALITY";
        }else{
            $sql = "SELECT
  B.NATIONALITY,
  COUNT(B.NATIONALITY) AS JUMLAH
FROM
participants B
                WHERE B.ACCEPTANCESTATUS = '".$_GET['acceptance']."'
                ";
            $sql .= " GROUP BY B.NATIONALITY";
        }
        $this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }
    public function dashboardStudyProgram(){
         $this->data['pagetitle']="IO Tel-U | Dashboard";
        $this->load->model('studyprogrammodel');
        $this->data['data_studyprogram'] = $this->studyprogrammodel->getStudyProgram('');
        $this->load->view('_header',$this->data);
        $this->load->view('admin/dashboardStudyProgram',$this->data);
        $this->load->view('_footer');
    }
    
    public function datatableStudyProgram(){
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('STUDYPROGRAMID');
        $this->datatables->SetColumns(array('STUDYPROGRAMNAME','JUMLAH'));
        $periods ='';
        if($_GET['period']!=''){
            $period = explode(",",$_GET['period']);
            //echo "<pre>"; print_r($period);die;
            for($x=0;$x<count($period);$x++){
                    if($period[$x]=='UNDEFINED YEAR'){
                        $period[$x] = "";
                    }
                    if(($x+1)==count($period)){
                        $periods .= " E.PERIOD = '".$period[$x]."'";
                    }else{
                        
                        $periods .= " E.PERIOD = '".$period[$x]."' OR";
                    }
            }
        }
        $this->datatables->SetActiveCounter(true);
        $sql = "SELECT
    C.STUDYPROGRAMID, 
    C.STUDYPROGRAMNAME,
    COUNT(B.STUDYPROGRAMID) AS JUMLAH
FROM studyprogram C
    LEFT JOIN (SELECT A.STUDYPROGRAMID, A.STUDYPROGRAMNAME AS JUMLAH
                FROM studyprogram A
                JOIN participants B ON (A.STUDYPROGRAMID=B.STUDYPROGRAMID)
                JOIN enrollment E ON (B.ENROLLMENTID=E.ENROLLMENTID)
                WHERE B.ACCEPTANCESTATUS = 'ACCEPTED'";
        if($_GET['academicyear']!='' && $_GET['academicyear']!='UNDEFINED YEAR' && $periods==''){
            $sql .= " AND E.ACADEMICYEAR=".$_GET['academicyear'].")B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
            GROUP BY C.STUDYPROGRAMID,  C.STUDYPROGRAMNAME";
        }else if($_GET['academicyear']=='' && $periods!='' && $periods!='UNDEFINED YEAR'){
            $sql .= " AND (".$periods."))B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
            GROUP BY C.STUDYPROGRAMID,  C.STUDYPROGRAMNAME";
        }else if($_GET['academicyear']=='' && $periods=='UNDEFINED YEAR'){
            $sql .= " AND (E.PERIOD IS NULL))B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
            GROUP BY C.STUDYPROGRAMID,  C.STUDYPROGRAMNAME";
        }else if($_GET['academicyear']=='UNDEFINED' && $periods=='UNDEFINED YEAR'){
            $sql .= " AND E.ACADEMICYEAR IS NULL AND (E.PERIOD IS NULL))B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
            GROUP BY C.STUDYPROGRAMID,  C.STUDYPROGRAMNAME";
        }else if($_GET['academicyear']=='UNDEFINED' && $periods==''){
            $sql .= " AND E.ACADEMICYEAR IS NULL)B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
            GROUP BY C.STUDYPROGRAMID,  C.STUDYPROGRAMNAME";
        }else if($_GET['academicyear']=='UNDEFINED' && $periods!='' && $periods!='UNDEFINED YEAR'){
            $sql .= " AND E.ACADEMICYEAR IS NULL AND (".$periods."))B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
            GROUP BY C.STUDYPROGRAMID,  C.STUDYPROGRAMNAME";
        }else if($_GET['academicyear']!='UNDEFINED' && $_GET['academicyear']!='' && $periods=='UNDEFINED YEAR'){
            $sql .= " AND E.ACADEMICYEAR=".$_GET['academicyear']." AND (E.PERIOD IS NULL))B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
            GROUP BY C.STUDYPROGRAMID,  C.STUDYPROGRAMNAME";
        }else{
                $sql .= ")B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
            GROUP BY C.STUDYPROGRAMID,  C.STUDYPROGRAMNAME";
        }
        //echo "<pre>"; print_r($sql);
        $this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }
    
    public function datatableStudyProgram2(){
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('STUDYPROGRAMID');
        $this->datatables->SetColumns(array('STUDYPROGRAMNAME','JUMLAH'));

        $this->datatables->SetActiveCounter(true);
        
                $sql = "SELECT
    C.STUDYPROGRAMID, 
    C.STUDYPROGRAMNAME,
    COUNT(B.STUDYPROGRAMID) AS JUMLAH
FROM studyprogram C
    LEFT JOIN (SELECT A.STUDYPROGRAMID, A.STUDYPROGRAMNAME AS JUMLAH
                FROM studyprogram A
                JOIN participants B ON (A.STUDYPROGRAMID=B.STUDYPROGRAMID)
                WHERE B.ACCEPTANCESTATUS = 'ACCEPTED')B ON (B.STUDYPROGRAMID=C.STUDYPROGRAMID)
            GROUP BY C.STUDYPROGRAMID,  C.STUDYPROGRAMNAME";
        
        $this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }
    
    public function dashboardStudyProgramByEnrollment($programid,$programname,$academicyear){
        //echo "<pre>"; print_r($academicyear);die;
        $this->data['pagetitle']="IO Tel-U | Dashboard";
        $this->load->model('studyprogrammodel');
        if(substr($programname,-4)=='ship'){
            $this->data['data_studyprogramby'] = $this->studyprogrammodel->getStudyProgramScholarship($programid,$academicyear);
        }else{
            $this->data['data_studyprogramby'] = $this->studyprogrammodel->getStudyProgramByEnrollment($programid,$academicyear);
        }               
        $this->data['programid'] = $programid;
        $this->data['academicyear'] = $academicyear;
        $this->data['programname'] = ucwords(strtolower(str_replace('%20',' ',$programname)));
        $this->load->view('_header',$this->data);
        $this->load->view('admin/dashboardStudyProgramByEnrollment',$this->data);
        $this->load->view('_footer');
    }
    
    public function datatableStudyProgramByEnrollment($programid,$programname,$academicyear){
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('STUDYPROGRAMID');
        $this->datatables->SetColumns(array('STUDYPROGRAMNAME','JUMLAH'));
        $this->datatables->SetActiveCounter(true);
        if(substr($programname,-4)=='ship'){            
            $sql = "SELECT A.SCHOLARSHIPTYPEID AS STUDYPROGRAMID, A.SCHOLARSHIPTYPENAME AS STUDYPROGRAMNAME, COUNT(B.SCHOLARSHIPTYPEID) AS JUMLAH
                FROM scholarshiptype A
                LEFT JOIN participants B ON (A.SCHOLARSHIPTYPEID=B.SCHOLARSHIPTYPEID)
                WHERE B.ACCEPTANCESTATUS = 'ACCEPTED' AND B.SCHOLARSHIPTYPEID='".$programid."'
                ";
            if($academicyear!='x'){
                $sql .= " AND B.ACADEMICYEAR=".$academicyear."
                       GROUP BY A.SCHOLARSHIPTYPEID, A.SCHOLARSHIPTYPENAME";
            }else{
                $sql .= " GROUP BY A.SCHOLARSHIPTYPEID, A.SCHOLARSHIPTYPENAME";
            }
        }else{
            $sql = "SELECT A.STUDYPROGRAMID, A.STUDYPROGRAMNAME, COUNT(B.STUDYPROGRAMID) AS JUMLAH
                FROM studyprogram A
                LEFT JOIN participants B ON (A.STUDYPROGRAMID=B.STUDYPROGRAMID)
                WHERE B.ACCEPTANCESTATUS = 'ACCEPTED' AND B.PROGRAMID='".$programid."'
                ";
            if($academicyear!='x'){
                $sql .= " AND B.ACADEMICYEAR=".$academicyear."
                       GROUP BY A.STUDYPROGRAMID, A.STUDYPROGRAMNAME";
            }else{
                $sql .= " GROUP BY A.STUDYPROGRAMID, A.STUDYPROGRAMNAME";
            }
        }   
        
        $this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }
    
    public function modalNumberParticipants($no,$nama,$academicyear,$period){
        $this->load->model('participantmodel'); 
        $this->data['status'] = $no;
        //echo "<pre>"; print_r($period);die;//2019-8_2019-1
        if($academicyear=='x' || $academicyear==''){
            $academicyear = '';
        }
        if($period=='x' || $period==''){
            $period = '';
        }
        $this->data['academicyear'] = $academicyear;
        $this->data['period'] = $period;
        $this->data['judul'] = str_replace('_',' ',$nama)." ".$academicyear." Period ".str_replace("_",",",$period);
        $this->load->view('admin/_numberParticipants',$this->data);
    }
    
    public function exportExcelModal(){
        $status=$this->uri->segment(3); 
        $academicyear=$this->uri->segment(4);
        $period=$this->uri->segment(5);
        $this->load->model('participantmodel');
        $data['data_participant'] = $this->participantmodel->modalParticipants($status,$academicyear,$period);
                
        foreach($data as $key){
            foreach($key as $key2){
                //if($key2['ENROLLMENTID']!=0 && $key2['PROGRAMID']!=0){
                    $response = $this->createRegisNumber($key2['ENROLLMENTID'], $key2['PROGRAMID'], $key2['USERID']);
                //}else{
                    //$response ='SUCCESS';
                //}
            }
        }
        $data['data_participant'] = $this->participantmodel->modalParticipants($status,$academicyear, $period);
        //echo "<pre>"; print_r($data);die;
        //if ($response) {
            $data['status'] = $status;
            $this->load->view('admin/exportExcelModal',$data);
        //}
        
   }
    
     function dataTableModal($status,$academicyear, $period){
         //echo "<pre>"; print_r($period);die;
        $periods = explode('_',$period);
        $periodss ='';
        if(($periods!='' || $periods!=NULL && $periods!='x') && $periods!='UNDEFINED YEAR' && $periods!='x'){
            for($x=0;$x<count($periods);$x++){
            if(($x+1)==count($periods) && $periods[$x]!='x'){
                $periodss .= " enrollment.PERIOD = '".$periods[$x]."'";
            }else if($periods[$x]!='x'){
                $periodss .= " enrollment.PERIOD = '".$periods[$x]."' OR";
            }
        }
        }else if($periods=='UNDEFINED YEAR'){
            $periodss = 'UNDEFINED YEAR';
        }else if($periods=='x'){
            $periodss ='';
        }else{
            $periodss = $periods;
        }
        //echo "<pre>"; print_r($periodss);die;
       $sql ="SELECT DISTINCT PARTICIPANTID,participants.FULLNAME,GENDER,PASSPORTNO,CASE 
WHEN NATIONALITY IS NULL OR NATIONALITY=COUNTRYID THEN COUNTRYNAMEENG
ELSE NATIONALITY
END AS NATIONALITY,COUNTRYNAMEENG,DESCRIPTION,PERIOD,ACCEPTANCESTATUS,ENROLLMENTID,STUDYPROGRAMNAME,FACULTYNAMEENG,PROGRAMTYPE from participants
                        LEFT JOIN country USING(COUNTRYID)
                         LEFT JOIN users USING (USERID)
                        LEFT JOIN quizparticipant USING (PARTICIPANTID)
                        LEFT JOIN (enrollment
                          LEFT JOIN (SELECT PROGRAMDEGREEID,PROGRAMID,DEGREEID FROM programdegree) AS programdegree
                          USING (PROGRAMDEGREEID)
                        )
                        USING (ENROLLMENTID)
                        LEFT JOIN (languagedeliverymapping
                          LEFT JOIN (studyprogramlanguage) USING (LANGUAGEDELIVERYMAPPINGID)
                          JOIN (SELECT STUDYPROGRAMID,STUDYPROGRAMNAME,FACULTYID FROM studyprogram) AS STUDYPROGRAM USING (STUDYPROGRAMID)
                          JOIN (SELECT FACULTYID,FACULTYNAMEENG FROM faculty) AS FACULTY USING (FACULTYID)
                        ) USING (LANGUAGEDELIVERYMAPPINGID)
                        LEFT JOIN (requirementvalue)
                          USING (PARTICIPANTID)
                          ";
        if($status=='scholarship'){
            $sql .=" WHERE participants.PROGRAMID=3";
        }else if($status=='notscholarship'){
            $sql .=" WHERE participants.PROGRAMID IN (1,2,4,5)";
        }else if($status==2||$status==3||$status==4||$status==5){
            $sql .=" WHERE participants.CURRENTSTEP=".$status;
        }else if($status==1){
            $sql .=" WHERE participants.CURRENTSTEP = 1
AND participants.PROGRAMID= 0
AND participants.ENROLLMENTID IS NULL
AND participants.STUDYPROGRAMID = 0
AND participants.LANGUAGEDELIVERYMAPPINGID IS NULL
AND users.ACTIVESTATUS ='Y'
AND quizparticipant.STATUS = 1";
        }else if($status=='have_interview'){
            $sql .=" where participants.INTERVIEWDATE != '0000-00-00 00:00:00'
and (participants.INTERVIEWSTATUS != '' or participants.INTERVIEWSCORE != 0)
and participants.CURRENTSTEP = 6
";
        }else if($status=='ACCEPTED' || $status=='UNACCEPTED'){
            $sql .=" WHERE (participants.PROGRAMTYPE='ACADEMIC' AND participants.ACCEPTANCESTATUS='".$status."' AND participants.INTERVIEWSTATUS!='' AND participants.INTERVIEWSCORE!=0 AND participants.INTERVIEWDATE!='0000-00-00 00:00:00') OR (participants.PROGRAMTYPE='NON_ACADEMIC' AND participants.ACCEPTANCESTATUS='".$status."') ";
        }else if($status=='attend'){
            $sql .=" WHERE participants.ACCEPTANCESTATUS = 'ACCEPTED'
AND participants.INTERVIEWSTATUS = 'Accept'
AND participants.INTERVIEWSCORE !=0
AND participants.INTERVIEWDATE != '0000-00-00 00:00:00' AND participants.LOAFILE IS NOT NULL";
        }else if($status=='absent'){
            $sql .=" WHERE participants.ACCEPTANCESTATUS = 'ACCEPTED'
AND participants.INTERVIEWSTATUS = 'Accept'
AND participants.INTERVIEWSCORE !=0
AND participants.INTERVIEWDATE != '0000-00-00 00:00:00' AND participants.LOAFILE IS NULL";
        }else if($status==7){
            $sql .= "WHERE (participants.PROGRAMTYPE='ACADEMIC' AND participants.CURRENTSTEP=7 AND (participants.ACCEPTANCESTATUS='' OR participants.ACCEPTANCESTATUS IS NULL)
AND participants.INTERVIEWSTATUS!='' AND participants.INTERVIEWSCORE!=0 
AND participants.INTERVIEWDATE!='0000-00-00 00:00:00') OR 
(participants.PROGRAMTYPE='NON_ACADEMIC' AND participants.CURRENTSTEP=7 AND (participants.ACCEPTANCESTATUS='' OR participants.ACCEPTANCESTATUS IS NULL)) ";
            /*" WHERE participants.CURRENTSTEP=7 AND (participants.ACCEPTANCESTATUS='' 
OR participants.ACCEPTANCESTATUS IS NULL)
AND participants.INTERVIEWSTATUS!='' AND participants.INTERVIEWSCORE!=0 
AND participants.INTERVIEWDATE!='0000-00-00 00:00:00' ";*/
        }else if($status=='not_interview'){
            $sql .= "where participants.INTERVIEWDATE != '0000-00-00 00:00:00'
and participants.CURRENTSTEP =6
and participants.INTERVIEWSCORE = 0 and participants.INTERVIEWSTATUS=''";
        }else if($status=='unscheduled_interview'){
            $sql .= " where participants.INTERVIEWDATE = '00-00-0000'
                    and participants.CURRENTSTEP =6 and participants.INTERVIEWSTATUS='' AND participants.INTERVIEWSCORE = 0";
        }else if($status=='acount'){
            $sql .= " WHERE participants.CURRENTSTEP = 1
AND participants.PROGRAMID= 0
AND participants.ENROLLMENTID IS NULL
AND participants.STUDYPROGRAMID = 0
AND participants.LANGUAGEDELIVERYMAPPINGID IS NULL";
        }else if($status=='all'){
            $sql .= " WHERE participants.PROGRAMID!= 0
AND participants.ENROLLMENTID IS NOT NULL
AND participants.STUDYPROGRAMID != 0
AND participants.LANGUAGEDELIVERYMAPPINGID IS NOT NULL";
        }
        
        /*if($academicyear!='x'){
            $sql .= " WHERE participants.ACADEMICYEAR = ".$academicyear;
        }else */if($academicyear!='x' && $academicyear!='UNDEFINED YEAR'){
            $sql .= " AND enrollment.ACADEMICYEAR = ".$academicyear;
        }else if($academicyear=='UNDEFINED YEAR'){
            $sql .= " AND enrollment.ACADEMICYEAR IS NULL";
        }
        if($periodss!='' && $periodss!="UNDEFINED YEAR" && $periodss!='x'){
            $sql .= " AND (".$periodss.")";
        }else if($period=="UNDEFINED YEAR"){
            $sql .= " AND (e.PERIOD IS NULL)";
        }
        $sql .= " ORDER BY participants.UPDATEDATE DESC";
        //echo "<pre>"; print($sql);die;
        //print_r($sql);die;
        $this->load->library('datatables',$this->datatableConfig);
        $this->datatables->SetIndexColumn('PARTICIPANTID');
        $this->datatables->SetColumns(array('FULLNAME','GENDER','NATIONALITY','DESCRIPTION','FACULTYNAMEENG','STUDYPROGRAMNAME','PARTICIPANTID','PROGRAMTYPE'));
        $this->datatables->SetActiveCounter(true);
        $this->datatables->SetTable($sql);
        $dt = $this->datatables->GetDataTables();
        echo $dt;
    }
    
    public function createRegisNumber($enrollment_id, $program_id, $user_id){
        $participant  = $this->participantmodel->participantData($user_id);
        if(strlen($participant['ADMISSIONID'])!=10 || $program_id!=$participant['PROGRAMID']){
            $year= $this->enrollmentmodel->getEntrollmentById($enrollment_id);
            $today_year = $year['ACADEMICYEAR'];
            
            //create REGISTRATION NUMBER 
            $last_admissionid = $this->participantmodel->GetAdmissionId();
            $admissionid = $last_admissionid['ADMISSIONID']+1;
            if((ceil(log10($last_admissionid['ADMISSIONID']))==1 && $last_admissionid['ADMISSIONID']!=9 && $last_admissionid['ADMISSIONID']!=10) || $last_admissionid['ADMISSIONID']==1 || $last_admissionid['ADMISSIONID']==0){
                $admissionid = '000'.$admissionid;
            }else if(((ceil(log10($last_admissionid['ADMISSIONID'])))==2 && $last_admissionid['ADMISSIONID']!=99 && $last_admissionid['ADMISSIONID']!=100) || $last_admissionid['ADMISSIONID']==9 || $last_admissionid['ADMISSIONID']==10){
                $admissionid = '00'.$admissionid;
            }else if((ceil(log10($last_admissionid['ADMISSIONID']))==3 && $last_admissionid['ADMISSIONID']!=999 && $last_admissionid['ADMISSIONID']!=1000) || $last_admissionid['ADMISSIONID']==99 || $last_admissionid['ADMISSIONID']==100){
                $admissionid = '0'.$admissionid;
            }else if(ceil(log10($last_admissionid['ADMISSIONID']))==4 || $last_admissionid['ADMISSIONID']==999 || $last_admissionid['ADMISSIONID']==1000){  
                $admissionid = $admissionid;
            }
            $registration_number=$today_year.'0'.$program_id.$admissionid;
            $participan_data = array('ADMISSIONID' => $registration_number);
            $response = $this->participantmodel->update($user_id, $participan_data);
            return $response;
        }
    }
}
