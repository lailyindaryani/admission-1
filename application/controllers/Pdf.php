<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Pdf extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }
    public function templateTypeA()
    {   
        $this->load->model('participantmodel');
        $participantId = $this->uri->segment(3);
        $participant   = $this->participantmodel->GetParticipantDetail($participantId);
		$academicYear = $this->participantmodel->GetAcademicYear($participantId);
    $scholarshiptype = $this->participantmodel->getScholarship('scholarshiptype',$participant['SCHOLARSHIPTYPEID']);
    //echo "<pre>"; print_r($scholarshiptype);die;
    if($participant['STUDYPROGRAMNAME']=='S2 Master of Management'){
          $program = 'Master of Management (2 years)';
        }else if($participant['DEGREE']=='S2' && $participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
          $program = 'Master of Engineering (2 years)';
        }else{
          $program = '';
        }
    $fee = $this->getFee($participant['DEGREE'], $program);
	//	$fee = $this->getFee($participant['DEGREE']);
		$loaNumberPdf=$this->loaNumber($participantId);
		if($participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
			$html = "<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                                    <p> No : ".$loaNumberPdf." <br>
                                    Subject : Letter of Acceptance <br>
                                    Attachment : <br>
                                    <br>
                                    To:    <br><b>"
                                    .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b> <br>
                                    Applicant Number <b>".$participant['ADMISSIONID']." </b><br>
                                    <br>
                                    Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>,
                                    <br>
                                    <p align='justify'> Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study
                                    program of <b>".$participant['FACULTYNAMEENG']."</b> for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile
                                    confirms your commitment to personal and educational growth. This acceptance letter means that you 
                                    have completed all necessary requirements to be our student. </p>
                                    <p align='justify'> On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are 
                                    pleased to inform you that you will be granted <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b> covering your admission fee, tuition fee, dormitory, 
                                    and living cost for your first year. The scholarship will be extended to further semesters as long as your 
                                    academic performance meets the requirements of <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b>. </p>
                                    <p align='justify'> Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us 
                                    before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." by using our online confirmation system or email at 
                                    info@io.telkomuniversity.ac.id. </p>
                                    <p align='justify'> Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site 
                                    Registration, and Orientation Program. Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).".</p>
                                    <br>

                                    Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))." <br>
                                    Your sincerely, <p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
                                    <u>Lia Yuldinawati, S.T., MM.</u><br>
                                    Director of Strategic Partnership and International Office Telkom University<br>
                                    <br>
				  <br>
				  <br>
				  <br><br>
                  <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee<br>
					  c. Health Insurance <br>
					  d. Bank Fee </td>
                  <td><br>
                      Scholarship <br>
                       Scholarship <br>
					   Self Payment <br>
					   Self Payment </td>
                  </tr>
                  </table>
                  <br>
                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                 <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white'>Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
				<tr>
                  <td>1</td>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td>
				  <td></td>
                  <td> Scholarship</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
				  
				  <td></td>
                  <td> Scholarship</td>
                  </tr>
				  <tr>
                  <td>3</td>
                  <td>Health Insurance</td>
                  <td>IDR 150.000,00</td>
				  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
                  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Bank Fee</td>
                  <td>IDR 2.500,00</td>
				  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
                  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR 152.500,00</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                                    <p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                                    <h2>Scholarship Information</h2>
                                    Below are the terms and condition of Telkom University Scholarship Scheme. <br>
                                    <h3 font color='blue'>".$scholarshiptype['SCHOLARSHIPTYPENAME']." (Admission Fee, Tuition Fee, Dormitory, and Living Cost Waived) </h3>
                                    The scholarship is dedicated to students who could show the best academic performance. A-Type
                                    scholarship holder will get: <br>
                                    1. Admission Fee (one time) <br>
                                    2. Tuition Fee, it will be reviewed every year. Student should maintain at least 3.0. If the GPA is less <br>
                                    than 3.00, the following scheme will be applied: <br>
                                    &nbsp; &nbsp; a. GPA ≥ 3.00. Tuition Fee waiver 100%. <br>
                                    &nbsp; &nbsp; b. 2.75 ≤ GPA &lt; 3.00. Tuition Fee waiver 75%. <br>
                                    &nbsp; &nbsp; c. 2.50 ≤ GPA &lt; 2.75. Tuition Fee waiver 50%. <br>
                                    &nbsp; &nbsp; d. 2.25 ≤ GPA &lt; 2.50. Tuition Fee waiver 25%. <br>
                                    &nbsp; &nbsp; e. GPA &lt; 2.25. Tuition Fee waiver 0%. <br>
                                    3. Dormitory will be reviewed per year. Student should maintain at least 3.0. If the GPA is less than
                                    3.00, the dormitory will be charged IDR 500.000,- (USD 35) monthly until the students could
                                    maintain to reach GPA ≥ 3.0 for the next year. <br>
                                    4. Living cost will be reviewed per year. Student should maintain at least 3.0. If the GPA is less than
                                    3.00, the living cost will be stopped until the students could maintain to reach GPA ≥ 3.0 for the
                                    next year. <br>
                                    <br>
                                    <h3 font color='blue'>Terms and Condition</h3>
                                    1. Please Note that all the Scholarship of Telkom University exclude flight ticket from and to your
                                    home country. <br>
                                    2. Indonesian Visa Application Fee in your home country is at your own cost. <br>
                                    3. The students cannot apply to another type of scholarship schemes once Telkom University
                                    decide the acceptance as mentioned in the Letter of Acceptance. <br>
                                    4. The students will receive the same scholarship condition if they could maintain the GPA to be at
                                    least 3.0. <br>
                                    5. The scholarship will not cover your health insurance and any other cost which is not mentioned
                                    in this terms and condition. <br>
                                    6. The recipient of the scholarship must obey all of the rules in Telkom University otherwise
                                    Telkom University has the right to discontinue the scholarship. <br>
                                    <br>
									<br>
									<br>
									<br>
                                    <h1>Telkom University Confirmation of<br>
                                    Admission</h1> <br>
                                    To: <br>
                                    Telkom University International Office, <br>
                                    Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
                                    <br>
                                    To whom it may concern, <br>
                                    Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective <br>
                                    student with the following identity: <br><br>
                                    <table border='1' align='center'>
                                    <tr bgcolor='#000000'>
                                      <th font color='white'>No</th>
                                      <th font color='white'>Student Information</th>
                                      <th font color='white' width='50%'>Details</th>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td>Full Name</td>
                                      <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td>Admission ID</td>
                                      <td>".$participant['ADMISSIONID']."</td>
                                    </tr>
                                    <tr>
                                      <td>3</td>
                                      <td>Birth Place and Birth Date</td>
                                      <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                                    </tr>
                                    <tr>
                                      <td>4</td>
                                      <td>Phone Number</td>
                                      <td>".$participant['PHONE']."</td>
                                    </tr>
                                    <tr>
                                      <td>5</td>
                                      <td>Email</td>
                                      <td>".$participant['EMAIL']."</td>
                                    </tr>
                                    </table>
                                    <br>
                                    <p align='justify'> I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                                    These confirmations and decisions I make without coercion and encouragement from any party. <br>
                                    <br>
                                    City of confirmation: ………………………………………………….. <br>
                                    Date Signed: ………………………………………………………………. <br>
                                    The Student, <br>
                                    <br>
                                    <br>
                                    <br>
									<br>
									<br>
                                    ………………………………………………………………………………….<br>
                                    ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."<br>
                                    *(Please sign above your name)<br>
									<br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  If you have any inquiries, please do not hesitate to contact us on: <br>
				  Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
				  Web: https://io.telkomuniversity.ac.id<br> 
				  Email: info@telkomuniversity.ac.id <br>
				  Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
					40257
				";
		}else{
			$html = "<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                                    <p> No : ".$loaNumberPdf." <br>
                                    Subject : Letter of Acceptance <br>
                                    Attachment : <br>
                                    <br>
                                    To:    <br><b>"
                                    .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b> <br>
                                    Applicant Number <b>".$participant['ADMISSIONID']." </b><br>
                                    <br>
                                    Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>,
                                    <br>
                                    <p align='justify'> Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study
                                    program of <b>".$participant['FACULTYNAMEENG']."</b> for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile
                                    confirms your commitment to personal and educational growth. This acceptance letter means that you 
                                    have completed all necessary requirements to be our student. </p>
                                    <p align='justify'> On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are 
                                    pleased to inform you that you will be granted <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b> covering your admission fee, tuition fee, dormitory, 
                                    and living cost for your first year. The scholarship will be extended to further semesters as long as your 
                                    academic performance meets the requirements of <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b>. </p>
                                    <p align='justify'> Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us 
                                    before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." by using our online confirmation system or email at 
                                    info@io.telkomuniversity.ac.id. </p>
                                    <p align='justify'> Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site 
                                    Registration, and Orientation Program. Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).".</p>
                                    <br>

                                    Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))." <br>
                                    Your sincerely, <p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
                                    <u>Lia Yuldinawati, S.T., MM.</u><br>
                                    Director of Strategic Partnership and International Office Telkom University<br>
                                    <br>
				  <br>
				  <br>
				  <br><br>
                  <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee</td>
                  <td><br>
                      Scholarship <br>
                       Scholarship</td>
                  </tr>
                  </table>
                  <br>
                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                 <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white'>Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
				<tr>
                  <td>1</td>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td>
				  <td></td>
                  <td> Scholarship</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
				  <td></td>
                  <td> Scholarship</td>
                  </tr>
				  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR 0</td>
                  <td></td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                                    <p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                                    <h2>Scholarship Information</h2>
                                    Below are the terms and condition of Telkom University Scholarship Scheme. <br>
                                    <h3 font color='blue'>".$scholarshiptype['SCHOLARSHIPTYPENAME']." (Admission Fee, Tuition Fee, Dormitory, and Living Cost Waived) </h3>
                                    The scholarship is dedicated to students who could show the best academic performance. A-Type
                                    scholarship holder will get: <br>
                                    1. Admission Fee (one time) <br>
                                    2. Tuition Fee, it will be reviewed every year. Student should maintain at least 3.0. If the GPA is less <br>
                                    than 3.00, the following scheme will be applied: <br>
                                    &nbsp; &nbsp; a. GPA ≥ 3.00. Tuition Fee waiver 100%. <br>
                                    &nbsp; &nbsp; b. 2.75 ≤ GPA &lt; 3.00. Tuition Fee waiver 75%. <br>
                                    &nbsp; &nbsp; c. 2.50 ≤ GPA &lt; 2.75. Tuition Fee waiver 50%. <br>
                                    &nbsp; &nbsp; d. 2.25 ≤ GPA &lt; 2.50. Tuition Fee waiver 25%. <br>
                                    &nbsp; &nbsp; e. GPA &lt; 2.25. Tuition Fee waiver 0%. <br>
                                    3. Dormitory will be reviewed per year. Student should maintain at least 3.0. If the GPA is less than
                                    3.00, the dormitory will be charged IDR 500.000,- (USD 35) monthly until the students could
                                    maintain to reach GPA ≥ 3.0 for the next year. <br>
                                    4. Living cost will be reviewed per year. Student should maintain at least 3.0. If the GPA is less than
                                    3.00, the living cost will be stopped until the students could maintain to reach GPA ≥ 3.0 for the
                                    next year. <br>
                                    <br>
                                    <h3 font color='blue'>Terms and Condition</h3>
                                    1. Please Note that all the Scholarship of Telkom University exclude flight ticket from and to your
                                    home country. <br>
                                    2. Indonesian Visa Application Fee in your home country is at your own cost. <br>
                                    3. The students cannot apply to another type of scholarship schemes once Telkom University
                                    decide the acceptance as mentioned in the Letter of Acceptance. <br>
                                    4. The students will receive the same scholarship condition if they could maintain the GPA to be at
                                    least 3.0. <br>
                                    5. The scholarship will not cover your health insurance and any other cost which is not mentioned
                                    in this terms and condition. <br>
                                    6. The recipient of the scholarship must obey all of the rules in Telkom University otherwise
                                    Telkom University has the right to discontinue the scholarship. <br>
                                    <br>
									<br>
									<br>
									<br>
                                    <h1>Telkom University Confirmation of<br>
                                    Admission</h1> <br>
                                    To: <br>
                                    Telkom University International Office, <br>
                                    Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
                                    <br>
                                    To whom it may concern, <br>
                                    Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective <br>
                                    student with the following identity: <br><br>
                                    <table border='1' align='center'>
                                    <tr bgcolor='#000000'>
                                      <th font color='white'>No</th>
                                      <th font color='white'>Student Information</th>
                                      <th font color='white' width='50%'>Details</th>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td>Full Name</td>
                                      <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td>Admission ID</td>
                                      <td>".$participant['ADMISSIONID']."</td>
                                    </tr>
                                    <tr>
                                      <td>3</td>
                                      <td>Birth Place and Birth Date</td>
                                      <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                                    </tr>
                                    <tr>
                                      <td>4</td>
                                      <td>Phone Number</td>
                                      <td>".$participant['PHONE']."</td>
                                    </tr>
                                    <tr>
                                      <td>5</td>
                                      <td>Email</td>
                                      <td>".$participant['EMAIL']."</td>
                                    </tr>
                                    </table>
                                    <br>
                                    <p align='justify'> I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                                    These confirmations and decisions I make without coercion and encouragement from any party. <br>
                                    <br>
                                    City of confirmation: ………………………………………………….. <br>
                                    Date Signed: ………………………………………………………………. <br>
                                    The Student, <br>
                                    <br>
                                    <br>
                                    <br>
									<br>
									<br>
                                    ………………………………………………………………………………….<br>
                                    ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."<br>
                                    *(Please sign above your name)<br>
									<br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  If you have any inquiries, please do not hesitate to contact us on: <br>
				  Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
				  Web: https://io.telkomuniversity.ac.id<br> 
				  Email: info@telkomuniversity.ac.id <br>
				  Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
					40257
				";
		}
               
        $tamp = explode(' Scholarship',$scholarshiptype['SCHOLARSHIPTYPENAME']); 
        $type =str_replace(" ", "", $tamp[0]);   
        //this the the PDF filename that user will get to download
        $pdfFilePath = "Letter_Acceptance_".$type.".pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D"); 
		$todayYear=date('Y');
		$this->insertDataLoa($loaNumberPdf,$todayYear,$participantId);
    }
	
	public function loaNumber($participantId){
		$this->load->model('participantmodel');		
		$participant= $this->participantmodel->GetParticipantDetail($participantId);
		//created QR Code
		$this->load->library('Ciqrcode');
		$params['data'] = $participant['ADMISSIONID'];
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'/uploads/media/participant/requirement/'.$participant['PASSPORTNO'].'/Qrcode.png';
		$this->ciqrcode->generate($params);
        
		$todayYear=date('Y');
		$dataLoa = $this->participantmodel->GetLoaNumber($participantId,$todayYear);
		if($dataLoa['LOANUMBER']==null || ($dataLoa['LOANUMBER']!=null && $dataLoa['ACTIVEYEAR']!=$todayYear)){
			$loaNumberPdf='001/AKD2/KUI/'.$todayYear;
		}else{
			$loaNumber=$dataLoa['LOANUMBER']+1;
			if((ceil(log10($dataLoa['LOANUMBER']))==1 && $dataLoa['LOANUMBER']!=9 && $dataLoa['LOANUMBER']!=10) || $dataLoa['LOANUMBER']==1 ){
				$loaNumberPdf='00'.$loaNumber.'/AKD2/KUI/'.$todayYear;
			}else if(((ceil(log10($dataLoa['LOANUMBER'])))==2 && $dataLoa['LOANUMBER']!=99 && $dataLoa['LOANUMBER']!=100) || $dataLoa['LOANUMBER']==9 || $dataLoa['LOANUMBER']==10){
				$loaNumberPdf='0'.$loaNumber.'/AKD2/KUI/'.$todayYear;
			}else if(ceil(log10($dataLoa['LOANUMBER']))==3 || $dataLoa['LOANUMBER']==99 || $dataLoa['LOANUMBER']==100){
				$loaNumberPdf=$loaNumber.'/AKD2/KUI/'.$todayYear;
			}	
		}
		return $loaNumberPdf;
	}

    public function templateTypeNone()//REGULAR
    {   
        $participantId=$this->uri->segment(3);
		$loaNumberPdf=$this->loaNumber($participantId);
		
		$this->load->model('participantmodel');
		$participant= $this->participantmodel->GetParticipantDetail($participantId);
		$academicYear = $this->participantmodel->GetAcademicYear($participantId);
    if($participant['STUDYPROGRAMNAME']=='S2 Master of Management'){
          $program = 'Master of Management (2 years)';
        }else if($participant['DEGREE']=='S2' && $participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
          $program = 'Master of Engineering (2 years)';
        }else{
          $program = '';
        }
    $fee = $this->getFee($participant['DEGREE'], $program);
		//$fee = $this->getFee($participant['DEGREE']);
		if($participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
			$html = "<p style='padding-left: 500px;'><img src='".base_url('/uploads/telkom.png')."' width='150px' height='50px' align='right'/></p>
                  <br>
                  No               : ".$loaNumberPdf." <br>
                  Subject          :  Letter of Acceptance <br>
                  Attachment     :  - <br>
                  <br>
                  To: <br><b>"
                   .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b><br>
                  Applicant Number <b>".$participant['ADMISSIONID']."</b> <br>
                  <br>
                  Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>,<br>
                  <p align='justify'>Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b> for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile confirms your commitment to personal and educational growth. This acceptance letter means that you have completed all necessary requirements to be our student.</p>
                 <p align='justify'> On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are pleased to inform you that you are accepted as Telkom University student which needs to cover the Admission and Tuition Fees by yourself or by your sponsors. Kindly find the attached for the School Fee details and how to pay the fees. Please do the payment before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE'])))).". </p>
                  <p align='justify'>Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." by using our online confirmation system or email at info@io.telkomuniversity.ac.id. </p>
                 <p align='justify'> Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site Registration, and Orientation Program. Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).". </p>
                  If you have any inquiries, please do not hesitate to contact us on: <br>
                        Phone    : +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
                        Web    : https://io.telkomuniversity.ac.id <br>
                        Email    : info@io.telkomuniversity.ac.id <br>
                      Address:: Telkom University, Learning Center Building R.118, Jl. Telekomunikasi No.1 Bandung 40257<br>
                  <br>
                  Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))." <br>
                  Your sincerely, <p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
				  <u>Lia Yuldinawati, S.T., MM.</u><br>
                  Director of Strategic Partnership and International Office Telkom University<br>
                  <br>
                  <br>
                  <br>
                  <br><br>
                  <h1>Telkom University Admission Details </h1><br>
                  Below are the details of the information about your admission and payment at Telkom University <br><br>
                  A. Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>
                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee <br>
					  c. Health Insurance <br>
					  d. Bank Fee</td>
                  <td><br>
                      Self Payment <br>
                      Self Payment<br>
					  Self Payment <br>
                      Self Payment</td>
                  </tr>
                  </table>
                  <br>
                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
                  <th font color='white'>Payment Amount</th>
                  <th font color='white' >Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment</td>
                  </tr>
				   <tr>
                  <td>3</td>
                  <td>Health Insurance</td>
                  <td>IDR ".number_format(150000,2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Bank Fee</td>
                  <td>IDR 2.500,00</td>
                  <td> ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR ".number_format(($fee['TUITIONFEE']+$fee['ADMISSIONFEE']+150000+2500),2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br> 
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  <h1>Telkom University Confirmation of <br>Admission</h1><br>
                  To: <br>
                  Telkom University International Office, <br>
                  Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
                  To whom it may concern, <br><br>
                  Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
                  <table align='center' border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>
                  <p align='justify'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                  These confirmations and decisions I make without coercion and encouragement from any party. <br>
                  <br>
                  City of confirmation: ………………………………………………….. <br>
                  Date Signed: ………………………………………………………………. <br>
                  The Student, <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  …………………………………………………………………………………. <br>
                  ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."<br>
                  *(Please sign above your name)
				  
                  ";
		}else{
			$html = "<p style='padding-left: 500px;'><img src='".base_url('/uploads/telkom.png')."' width='150px' height='50px' align='right'/></p>
                  <br>
                  No               : ".$loaNumberPdf." <br>
                  Subject          :  Letter of Acceptance <br>
                  Attachment     :  - <br>
                  <br>
                  To: <br><b>"
                   .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b><br>
                  Applicant Number <b>".$participant['ADMISSIONID']."</b> <br>
                  <br>
                  Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>,<br>
                  <p align='justify'>Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b> for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile confirms your commitment to personal and educational growth. This acceptance letter means that you have completed all necessary requirements to be our student.</p>
                 <p align='justify'> On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are pleased to inform you that you are accepted as Telkom University student which needs to cover the Admission and Tuition Fees by yourself or by your sponsors. Kindly find the attached for the School Fee details and how to pay the fees. Please do the payment before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE'])))).". </p>
                  <p align='justify'>Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." by using our online confirmation system or email at info@io.telkomuniversity.ac.id. </p>
                 <p align='justify'> Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site Registration, and Orientation Program. Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).". </p>
                  If you have any inquiries, please do not hesitate to contact us on: <br>
                        Phone    : +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
                        Web    : https://io.telkomuniversity.ac.id <br>
                        Email    : info@io.telkomuniversity.ac.id <br>
                      Address:: Telkom University, Learning Center Building R.118, Jl. Telekomunikasi No.1 Bandung 40257<br>
                  <br>
                  Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))." <br>
                  Your sincerely, <p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
				  <u>Lia Yuldinawati, S.T., MM.</u><br>
                  Director of Strategic Partnership and International Office Telkom University<br>
                  <br>
                  <br>
                  <br>
                  <br><br>
                  <h1>Telkom University Admission Details </h1><br>
                  Below are the details of the information about your admission and payment at Telkom University <br><br>
                  A. Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>
                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee <br>
					  c. Bank Fee </td>
                  <td><br>
                      Self Payment <br>
					  Self Payment <br>
                      Self Payment</td>
                  </tr>
                  </table>
                  <br>
                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
                  <th font color='white'>Payment Amount</th>
                  <th font color='white' >Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment</td>
                  </tr>
				  <tr>
                  <td>3</td>
                  <td>Bank Fee</td>
                  <td>IDR 2.500,00</td>
                  <td> ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR ".number_format(($fee['TUITIONFEE']+$fee['ADMISSIONFEE']+2500),2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br> 
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  <h1>Telkom University Confirmation of <br>Admission</h1><br>
                  To: <br>
                  Telkom University International Office, <br>
                  Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
                  To whom it may concern, <br><br>
                  Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
                  <table align='center' border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>
                  <p align='justify'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                  These confirmations and decisions I make without coercion and encouragement from any party. <br>
                  <br>
                  City of confirmation: ………………………………………………….. <br>
                  Date Signed: ………………………………………………………………. <br>
                  The Student, <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  …………………………………………………………………………………. <br>
                  ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."<br>
                  *(Please sign above your name)
                  ";
		}
        		 
        //this the the PDF filename that user will get to download
        $pdfFilePath = "Letter_Accept_Regular.pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
		$todayYear=date('Y');
		$this->insertDataLoa($loaNumberPdf,$todayYear,$participantId);
	}
	
	public function insertDataLoa($loaNumberPdf,$todayYear,$participantId){
		//insert new dataLOA
		$users=$this->session->userdata('users');        
        $this->session_user = $users[0];
		$newDataLoa=array(
			'LOANUMBER' => $loaNumberPdf,
			'ACTIVEYEAR' => $todayYear,
			'INPUTDATE' => date('Y-m-d H:i:s'),
			'INPUTBY' => $this->session_user['USERID'],
			'PARTICIPANTID' => $participantId
		);
		
		$this->participantmodel->insertLoa($newDataLoa); 
	}
	
    public function templateTypeB()
    {
        $this->load->model('participantmodel');
        $participantId=$this->uri->segment(3);
        $participant= $this->participantmodel->GetParticipantDetail($participantId);
		$academicYear = $this->participantmodel->GetAcademicYear($participantId);
     $scholarshiptype = $this->participantmodel->getScholarship('scholarshiptype',$participant['SCHOLARSHIPTYPEID']);
    if($participant['STUDYPROGRAMNAME']=='S2 Master of Management'){
          $program = 'Master of Management (2 years)';
        }else if($participant['DEGREE']=='S2' && $participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
          $program = 'Master of Engineering (2 years)';
        }else{
          $program = '';
        }
    $fee = $this->getFee($participant['DEGREE'], $program);
		//$fee = $this->getFee($participant['DEGREE']);
		$loaNumberPdf=$this->loaNumber($participantId);  
		if($participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
			$html = "<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                  <br>
                  No               :  ".$loaNumberPdf."<br>
                  Subject          :  Letter of Acceptance <br>
                  Attachment     :  - <br>
                  <br>
                  To: <br><b>"
                  .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b><br>
                  Applicant Number <b>".$participant['ADMISSIONID']."</b><br><br>
                  Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>,<br>
                  <p align='justify'> Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b>
				  for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile confirms your commitment to personal and educational growth. This acceptance letter means that you have completed all necessary requirements to be our student.</p>
                  <p align='justify'>On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are pleased to inform you that you will be granted <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b> covering your Admission Fee and Tuition Fees for your first year. 
				  We will also give you dormitory facility to support your living in our campus. The scholarship will be extended to further semesters as long as your academic performance meets the requirements of <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b>.</p>
                   <p align='justify'>Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." 
				   by using our online confirmation system or email at info@io.telkomuniversity.ac.id.</p>
                 <p align='justify'> Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site Registration, and Orientation Program. 
				 Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).".</p>
				<br>	
                  Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))."<br>
                  Your sincerely, <p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
                  <u>Lia Yuldinawati, S.T., MM.</u>  <br>
                  Director of Strategic Partnership and International Office Telkom University<br>
                  <br>
				  <br>
				  <br>
				  <br><br>
                  <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee<br>
					  c. Health Insurance<br>
					  d. Bank Fee</td>
                  <td><br>
                      Scholarship <br>
                       Scholarship<br>
					   Self Payment<br>
					   Self Payment</td>
                  </tr>
                  </table>
                  <br>

                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white' >Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
				<tr>
                  <td>1</td>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td> 
				  <td> </td>
                  <td> Scholarship</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
				  <td> </td>
                  <td> Scholarship</td>
                  </tr>
				  <tr>
                  <td>3</td>
                  <td>Health Insurance</td>
                  <td>IDR 150.000,00</td>
				  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))." </td>
                  <td> Self Payment</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Bank Fee</td>
                  <td>IDR 2.500,00</td>
                  <td> ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				   <td> Self Payment</td>
                  </tr>
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR 152.500,00</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  <h2> Scholarship Information </h2>
                  <p align = 'justify'>Below are the terms and condition of Telkom University Scholarship Scheme. <br>
                  <h3 font color='blue'>".$scholarshiptype['SCHOLARSHIPTYPENAME']." (Tuition fee and dormitory waived) </h3>
                  The scholarship is dedicated to students who could show the best academic performance. B-Type scholarship holder will get:<br>
                  1.    Admission Fee (one time) <br>
                  2.    Tuition Fee, it will be reviewed every year. Student should maintain at least 3.0. If the GPA is less than 3.00, the following scheme will be applied: <br>
                  &emsp; a.    GPA ≥ 3.00. Tuition Fee waiver 100%. <br>
                  &emsp; b.    2.75 ≤ GPA < 3.00. Tuition Fee waiver 75%. <br>
                  &emsp; c.    2.50 ≤ GPA < 2.75. Tuition Fee waiver 50%. <br>
                  &emsp; d.    2.25 ≤ GPA < 2.50. Tuition Fee waiver 25%. <br>
                  &emsp; e.    GPA < 2.25. Tuition Fee waiver 0%. <br>
                  3.    Dormitory will be reviewed per year. Student should maintain at least 3.0. If the GPA is less than 3.00, the dormitory will be charged IDR 500.000,- (USD 35) monthly until the students could maintain to reach GPA ≥ 3.0 for the next year. <br>
                  </p><br>
                  <h3 font color='blue'>Terms and Condition</h3> 
                  1.    Please note that all the Scholarship of Telkom University exclude flight ticket from and to your home country. <br>
                  2.    Indonesian Visa Application Fee in your home country is at your own cost. <br>
                  3.    The students cannot apply to another type of scholarship schemes once Telkom University decide the acceptance as mentioned in the Letter of Acceptance. <br>
                  4.    The students will receive the same scholarship condition if they could maintain the GPA to be at least 3.0. <br>
                  5.    The scholarship will not cover your health insurance and any other cost which is not mentioned in this terms and condition. <br>
                  6.    The recipient of the scholarship must obey all of the rules in Telkom University otherwise Telkom University has the right to discontinue the scholarship. <br>
                 <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
                   <h1>Telkom University Confirmation of <br>
				  Admission </h1> <br>
                  To: <br>
                  Telkom University International Office, <br>
                  Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
                  <br>
                  To whom it may concern, <br>
                  Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
                  <table border='1' align='center'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <p align = 'justify'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                  These confirmations and decisions I make without coercion and encouragement from any party.
                  <br>
                  City of confirmation: ………………………………………………….. <br>
                  Date Signed: ………………………………………………………………. <br>
                  The Student, <br>
                  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  …………………………………………………………………………………. <br>
                  ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."
                  *(Please sign above your name)
				  <br>
									<br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  If you have any inquiries, please do not hesitate to contact us on: <br>
				  Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
				  Web: https://io.telkomuniversity.ac.id<br> 
				  Email: info@telkomuniversity.ac.id <br>
				  Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
					40257
                  ";
		}else{
			$html = "<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                  <br>
                  No               :  ".$loaNumberPdf."<br>
                  Subject          :  Letter of Acceptance <br>
                  Attachment     :  - <br>
                  <br>
                  To: <br><b>"
                  .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b><br>
                  Applicant Number <b>".$participant['ADMISSIONID']."</b><br><br>
                  Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>,<br>
                  <p align='justify'> Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b>
				  for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile confirms your commitment to personal and educational growth. This acceptance letter means that you have completed all necessary requirements to be our student.</p>
                  <p align='justify'>On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are pleased to inform you that you will be granted <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b> covering your Admission Fee and Tuition Fees for your first year. 
				  We will also give you dormitory facility to support your living in our campus. The scholarship will be extended to further semesters as long as your academic performance meets the requirements of <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b>.</p>
                   <p align='justify'>Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." 
				   by using our online confirmation system or email at info@io.telkomuniversity.ac.id.</p>
                 <p align='justify'> Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site Registration, and Orientation Program. 
				 Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).".</p>
				<br>	
                  Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))."<br>
                  Your sincerely, <p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
                  <u>Lia Yuldinawati, S.T., MM.</u>  <br>
                  Director of Strategic Partnership and International Office Telkom University<br>
                  <br>
				  <br>
				  <br>
				  <br><br>
                  <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee</td>
                  <td><br>
                      Scholarship <br>
                       Scholarship</td>
                  </tr>
                  </table>
                  <br>

                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white' >Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
				<tr>
                  <td>1</td>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td> 
				  <td> </td>
                  <td> Scholarship</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
				  <td> </td>
                  <td> Scholarship</td>
                  </tr>
				  
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR 0</td>
                  <td></td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  <h2> Scholarship Information </h2>
                  <p align = 'justify'>Below are the terms and condition of Telkom University Scholarship Scheme. <br>
                  <h3 font color='blue'>".$scholarshiptype['SCHOLARSHIPTYPENAME']." (Tuition fee and dormitory waived) </h3>
                  The scholarship is dedicated to students who could show the best academic performance. B-Type scholarship holder will get:<br>
                  1.    Admission Fee (one time) <br>
                  2.    Tuition Fee, it will be reviewed every year. Student should maintain at least 3.0. If the GPA is less than 3.00, the following scheme will be applied: <br>
                  &emsp; a.    GPA ≥ 3.00. Tuition Fee waiver 100%. <br>
                  &emsp; b.    2.75 ≤ GPA < 3.00. Tuition Fee waiver 75%. <br>
                  &emsp; c.    2.50 ≤ GPA < 2.75. Tuition Fee waiver 50%. <br>
                  &emsp; d.    2.25 ≤ GPA < 2.50. Tuition Fee waiver 25%. <br>
                  &emsp; e.    GPA < 2.25. Tuition Fee waiver 0%. <br>
                  3.    Dormitory will be reviewed per year. Student should maintain at least 3.0. If the GPA is less than 3.00, the dormitory will be charged IDR 500.000,- (USD 35) monthly until the students could maintain to reach GPA ≥ 3.0 for the next year. <br>
                  </p><br>
                  <h3 font color='blue'>Terms and Condition</h3> 
                  1.    Please note that all the Scholarship of Telkom University exclude flight ticket from and to your home country. <br>
                  2.    Indonesian Visa Application Fee in your home country is at your own cost. <br>
                  3.    The students cannot apply to another type of scholarship schemes once Telkom University decide the acceptance as mentioned in the Letter of Acceptance. <br>
                  4.    The students will receive the same scholarship condition if they could maintain the GPA to be at least 3.0. <br>
                  5.    The scholarship will not cover your health insurance and any other cost which is not mentioned in this terms and condition. <br>
                  6.    The recipient of the scholarship must obey all of the rules in Telkom University otherwise Telkom University has the right to discontinue the scholarship. <br>
                 <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
                   <h1>Telkom University Confirmation of <br>
				  Admission </h1> <br>
                  To: <br>
                  Telkom University International Office, <br>
                  Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
                  <br>
                  To whom it may concern, <br>
                  Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
                  <table border='1' align='center'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <p align = 'justify'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                  These confirmations and decisions I make without coercion and encouragement from any party.
                  <br>
                  City of confirmation: ………………………………………………….. <br>
                  Date Signed: ………………………………………………………………. <br>
                  The Student, <br>
                  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  …………………………………………………………………………………. <br>
                  ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."
                  *(Please sign above your name)
				  <br>
									<br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  If you have any inquiries, please do not hesitate to contact us on: <br>
				  Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
				  Web: https://io.telkomuniversity.ac.id<br> 
				  Email: info@telkomuniversity.ac.id <br>
				  Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
					40257
                  ";
		}
        
        $tamp = explode(' Scholarship',$scholarshiptype['SCHOLARSHIPTYPENAME']); 
        $type =str_replace(" ", "", $tamp[0]);   
        //this the the PDF filename that user will get to download
        $pdfFilePath = "Letter_Acceptance_".$type.".pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
		$todayYear=date('Y');
		$this->insertDataLoa($loaNumberPdf,$todayYear,$participantId);
    }

    public function templateTypeC()
    {
        $this->load->model('participantmodel');
        $participantId=$this->uri->segment(3);
        $participant= $this->participantmodel->GetParticipantDetail($participantId);
		    $academicYear = $this->participantmodel->GetAcademicYear($participantId);
        $scholarshiptype = $this->participantmodel->getScholarship('scholarshiptype',$participant['SCHOLARSHIPTYPEID']);
       //$participant['STUDYPROGRAMNAME'] = 'S2 Master of Industrial Engineering';
        if($participant['STUDYPROGRAMNAME']=='S2 Master of Management'){
          $program = 'Master of Management (2 years)';
        }else if($participant['DEGREE']=='S2' && $participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
          $program = 'Master of Engineering (2 years)';
        }else{
          $program = '';
        }
		$fee = $this->getFee($participant['DEGREE'], $program);
   // echo "<pre>"; print_r($fee);die;
        $loaNumberPdf=$this->loaNumber($participantId);   
		if($participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
			$html = "<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                  <br>
                  No               : ".$loaNumberPdf." <br>
                  Subject          :  Letter of Acceptance <br>
                  Attachment     :  - <br>
                  <br>
                  To: <br><b>"
                  .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b><br>
                  Applicant Number <b>".$participant['ADMISSIONID']."</b><br><br>
                  Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>,<br>
                  <p align='justify'> Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b> for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile confirms your commitment to personal and educational growth. This acceptance letter means that you have completed all necessary requirements to be our student.</p>
                  <p align='justify'>On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are pleased to inform you that you will be granted <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b> covering your admission fee and tuition fees for your first year. The scholarship will be extended to further semesters as long as your academic performance meets the requirements of <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b>.</p>
                 <p align='justify'> Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." by using our online confirmation system or email at info@io.telkomuniversity.ac.id.</p>
                  <p align='justify'>Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site Registration, and Orientation Program. Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).".</p>
					<br>
                  Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))."<br>
                  Your sincerely,<p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
                  <u>Lia Yuldinawati, S.T., MM.</u> <br>
                  Director of Strategic Partnership and International Office Telkom University<br>
                 <br>
				  <br>
				  <br>
				  <br><br>
                  <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee<br>
					  c. Health Insurance <br>
					  d. Bank Fee</td>
                  <td><br>
                      Scholarship <br>
                       Scholarship <br>
					   Self Payment <br>
					   Self Payment </td>
                  </tr>
                  </table>
                  <br>

                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white' >Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
				<tr>
                  <td>1</td>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td>
				   <td> </td>
                  <td> Scholarship</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
				   <td> </td>
                  <td> Scholarship</td>
                  </tr>
				  <tr>
                  <td>3</td>
                  <td>Health Insurance</td>
                  <td>IDR 150.000,00</td>
                  <td> ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				   <td>Self Payment </td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Bank Fee</td>
                  <td>IDR 2.500,00</td>
                  <td> ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment </td>
                  </tr>
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR 152.500,00</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
					<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                  <h2> Scholarship Information </h2>
                 Below are the terms and condition of Telkom University Scholarship Scheme. <br>
                  <h3 font color='blue'>".$scholarshiptype['SCHOLARSHIPTYPENAME']." (Admission Fee and Tuition Fee Waived) </h3>
                  The scholarship is dedicated to students who could show the best academic performance. C-Type scholarship holder will get:<br>
                  1.    Admission Fee (one time) <br>
                  2.    Tuition Fee, it will be reviewed every year. Student should maintain at least 3.00. If the GPA is<br> &nbsp;&nbsp;&nbsp;less than 3.00, the following scheme will be applied: <br>
                  &nbsp; &nbsp; a.    GPA ≥ 3.00. Tuition Fee waiver 100%. <br>
                   &nbsp; &nbsp; b.    2.75 ≤ GPA < 3.00. Tuition Fee waiver 75%. <br>
                   &nbsp; &nbsp; c.    2.50 ≤ GPA < 2.75. Tuition Fee waiver 50%. <br>
                   &nbsp; &nbsp; d.    2.25 ≤ GPA < 2.50. Tuition Fee waiver 25%. <br>
                   &nbsp; &nbsp; e.    GPA < 2.25. Tuition Fee waiver 0%. <br>
                  3.     Since this type of scholarship doesn’t cover the dormitory cost, if you wish to stay at Telkom<br>
				  &nbsp; &nbsp;&nbsp;University’s dormitory you will be charged IDR 500.000,- (USD 35) monthly.
                  <br>
                   <h3 font color='blue'>Terms and Condition </h3>
                  1.    Please note that all the Scholarship of Telkom University exclude flight ticket from and to your home country. <br>
                  2.    Indonesian Visa Application Fee in your home country is at your own cost. <br>
                  3.    The students cannot apply to another type of scholarship schemes once Telkom University decide the acceptance as mentioned in the Letter of Acceptance. <br>
                  4.    The students will receive the same scholarship condition if they could maintain the GPA to be at least 3.00. <br>
                  5.    The scholarship will not cover your health insurance and any other cost which is not mentioned in this terms and condition. <br>
                  6.    The recipient of the scholarship must obey all of the rules in Telkom University otherwise Telkom University has the right to discontinue the scholarship. <br>
                   <br>
                  <br>
				  <br>
				<br>
				<br>
				  <br>
				  <br>
				  <br><br><br><br>
                 <h1> Telkom University Confirmation of <br>
				 Admission </h1>
                  <br>
                  To: <br>
                  Telkom University International Office, <br>
                  Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
                  <br>
                  To whom it may concern, <br>
                  Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
                  <table border='1' align='center'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <p align = 'justify'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                  These confirmations and decisions I make without coercion and encouragement from any party.
                  <br>
                  City of confirmation: ………………………………………………….. <br>
                  Date Signed: ………………………………………………………………. <br>
                  The Student, <br>
                  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  …………………………………………………………………………………. <br>
                  ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."
                  *(Please sign above your name)
				  <br>
									<br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  If you have any inquiries, please do not hesitate to contact us on: <br>
				  Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
				  Web: https://io.telkomuniversity.ac.id<br> 
				  Email: info@telkomuniversity.ac.id <br>
				  Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
					40257
                  ";
		}else{
			$html = "<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                  <br>
                  No               : ".$loaNumberPdf." <br>
                  Subject          :  Letter of Acceptance <br>
                  Attachment     :  - <br>
                  <br>
                  To: <br><b>"
                  .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b><br>
                  Applicant Number <b>".$participant['ADMISSIONID']."</b><br><br>
                  Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>,<br>
                  <p align='justify'> Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b> for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile confirms your commitment to personal and educational growth. This acceptance letter means that you have completed all necessary requirements to be our student.</p>
                  <p align='justify'>On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are pleased to inform you that you will be granted <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b> covering your admission fee and tuition fees for your first year. The scholarship will be extended to further semesters as long as your academic performance meets the requirements of <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b>.</p>
                 <p align='justify'> Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." by using our online confirmation system or email at info@io.telkomuniversity.ac.id.</p>
                  <p align='justify'>Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site Registration, and Orientation Program. Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).".</p>
					<br>
                   Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))."<br>
                  Your sincerely,<p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
                  <u>Lia Yuldinawati, S.T., MM.</u> <br>
                 Director of Strategic Partnership and International Office Telkom University<br>
                 <br>
				  <br>
				  <br>
				  <br><br>
                  <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee</td>
                  <td><br>
                      Scholarship <br>
                       Scholarship</td>
                  </tr>
                  </table>
                  <br>

                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white' >Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
				<tr>
                  <td>1</td>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td>
				   <td> </td>
                  <td> Scholarship</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
				   <td> </td>
                  <td> Scholarship</td>
                  </tr>
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR 0</td>
                  <td></td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
					<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                  <h2> Scholarship Information </h2>
                 Below are the terms and condition of Telkom University Scholarship Scheme. <br>
                  <h3 font color='blue'>".$scholarshiptype['SCHOLARSHIPTYPENAME']." (Admission Fee and Tuition Fee Waived) </h3>
                  The scholarship is dedicated to students who could show the best academic performance. C-Type scholarship holder will get:<br>
                  1.    Admission Fee (one time) <br>
                  2.    Tuition Fee, it will be reviewed every year. Student should maintain at least 3.00. If the GPA is <br>&nbsp; &nbsp;&nbsp;less than 3.00, the following scheme will be applied: <br>
                  &nbsp; &nbsp; a.    GPA ≥ 3.00. Tuition Fee waiver 100%. <br>
                   &nbsp; &nbsp; b.    2.75 ≤ GPA < 3.00. Tuition Fee waiver 75%. <br>
                   &nbsp; &nbsp; c.    2.50 ≤ GPA < 2.75. Tuition Fee waiver 50%. <br>
                   &nbsp; &nbsp; d.    2.25 ≤ GPA < 2.50. Tuition Fee waiver 25%. <br>
                   &nbsp; &nbsp; e.    GPA < 2.25. Tuition Fee waiver 0%. <br>
                   3.     Since this type of scholarship doesn’t cover the dormitory cost, if you wish to stay at Telkom<br>
				  &nbsp; &nbsp;University’s dormitory you will be charged IDR 500.000,- (USD 35) monthly.
                  <br>
                   <h3 font color='blue'>Terms and Condition </h3>
                  1.    Please note that all the Scholarship of Telkom University exclude flight ticket from and to your home country. <br>
                  2.    Indonesian Visa Application Fee in your home country is at your own cost. <br>
                  3.    The students cannot apply to another type of scholarship schemes once Telkom University decide the acceptance as mentioned in the Letter of Acceptance. <br>
                  4.    The students will receive the same scholarship condition if they could maintain the GPA to be at least 3.00. <br>
                  5.    The scholarship will not cover your health insurance and any other cost which is not mentioned in this terms and condition. <br>
                  6.    The recipient of the scholarship must obey all of the rules in Telkom University otherwise Telkom University has the right to discontinue the scholarship. <br>
                   <br>
                  <br>
				  <br>
				<br>
				<br>
				  <br>
				  <br>
				  <br><br>
                 <h1> Telkom University Confirmation of <br>
				 Admission </h1>
                  <br>
                  To: <br>
                  Telkom University International Office, <br>
                  Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
                  <br>
                  To whom it may concern, <br>
                  Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
                  <table border='1' align='center'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <p align = 'justify'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                  These confirmations and decisions I make without coercion and encouragement from any party.
                  <br>
                  City of confirmation: ………………………………………………….. <br>
                  Date Signed: ………………………………………………………………. <br>
                  The Student, <br>
                  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  …………………………………………………………………………………. <br>
                  ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."
                  *(Please sign above your name)
				  <br>
									<br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  If you have any inquiries, please do not hesitate to contact us on: <br>
				  Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
				  Web: https://io.telkomuniversity.ac.id<br> 
				  Email: info@telkomuniversity.ac.id <br>
				  Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
					40257
                  ";
		}
        
        $tamp = explode(' Scholarship',$scholarshiptype['SCHOLARSHIPTYPENAME']); 
        $type =str_replace(" ", "", $tamp[0]);   
        //this the the PDF filename that user will get to download
        $pdfFilePath = "Letter_Acceptance_".$type.".pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
		$todayYear=date('Y');
		$this->insertDataLoa($loaNumberPdf,$todayYear,$participantId);
    }

    public function templateTypeSECE()
    {
      $this->load->model('participantmodel');
        $participantId=$this->uri->segment(3);
        $participant= $this->participantmodel->GetParticipantDetail($participantId);
		$academicYear = $this->participantmodel->GetAcademicYear($participantId);
		
        $loaNumberPdf=$this->loaNumber($participantId); 
		if($participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
			$html = "
                <p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                <br>
              No                 : ".$loaNumberPdf." <br>
              Subject        : Letter of Acceptance <br>
              Attachment       : <br>
              <br>
              To: <br><b>
              ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')." </b><br>
              Applicant Number <b>".$participant['ADMISSIONID']." </b><br><br>
              Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, <br>
              <p align='justify'>Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted as an exchange student in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b> from <b>".date('F jS, Y',strtotime($participant['STARTINGDATE']))."</b> to <b>".date('F jS, Y',strtotime($participant['ENDINGDATE']))."</b>. Your academic profile confirms your commitment to personal and educational growth. As an exchange student you are exempted from paying the tuition fee at Telkom University. On behalf of the entire Telkom University academia, we extend a warm welcome and best wishes for your success. </p>
              <p align='justify'>Your confirmation on this Letter of Acceptance needs to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." by using our online confirmation system or email at info@io.telkomuniversity.ac.id.</p>
             <p align='justify'> Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document and 
			  On-site Registration. Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).". </p>
              <br>
              Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))."<br>
              Your sincerely,<p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
              <u>Lia Yuldinawati, S.T., MM.</u><br>
              Director of Strategic Partnership and International Office Telkom University <br>
              <br>
              <br>
              <br> 
              <br><br><br><br><br>
			   <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Health Insurance<br>
                      b. Bank Fee</td>
                  <td><br>
                      Self Payment <br>
                       Self Payment</td>
                  </tr>
                  </table>
                  <br>
                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                 <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white'>Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>				
				  <tr>
                  <td>1</td>
                  <td>Health Insurance</td>
                  <td>IDR 150.000,00</td>
				  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
                  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Bank Fee</td>
                  <td>IDR 2.500,00</td>
				  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
                  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR 152.500,00</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
              <h1> Telkom University Confirmation of <br>Admission </h1>
              To: <br>
              Telkom University International Office, <br>
              Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
              <br>
              To whom it may concern, <br>
              Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
              <table border='1' align='center'>
              <tr bgcolor='#000000'>
              <th font color='white'>No</th>
              <th font color='white'>Student Information</th>
              <th font color='white' width='50%'>Details</th>
              </tr>
              <tr>
              <td>1</td>
              <td>Full Name</td>
              <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
              </tr>
              <tr>
              <td>2</td>
              <td>Admission ID</td>
              <td>".$participant['ADMISSIONID']."</td>
              </tr>
              <tr>
              <td>3</td>
              <td>Birth Place and Birth Date</td>
              <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
              </tr>
              <tr>
              <td>4</td>
              <td>Phone Number</td>
              <td>".$participant['PHONE']."</td>
              </tr>
              <tr>
              <td>5</td>
              <td>Email</td>
              <td>".$participant['EMAIL']."</td>
              </tr>
              </table>
              <br>
              <p align='align'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
              These confirmations and decisions I make without coercion and encouragement from any party. <br>
              <br>
              City of confirmation: ………………………………………………….. <br>
              Date Signed: ………………………………………………………………. <br>
              The Student, <br>
              <br>
              <br>
              <br>
			  <br>
              <br>
              …………………………………………………………………………………. <br>
              ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."<br>
              *(Please sign above your name)<br>
        <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
          <br>
                  <br>
                  <br>
          <br>
          <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
          <br>
                  <br>
                  <br>
          <br>";
		}else{
			$html = "
                <p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                <br>
              No                 : ".$loaNumberPdf." <br>
              Subject        : Letter of Acceptance <br>
              Attachment       : <br>
              <br>
              To: <br><b>
              ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')." </b><br>
              Applicant Number <b>".$participant['ADMISSIONID']." </b><br><br>
              Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, <br>
              <p align='justify'>Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted as an exchange student in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b> from <b>".date('F jS, Y',strtotime($participant['STARTINGDATE']))."</b> to <b>".date('F jS, Y',strtotime($participant['ENDINGDATE']))."</b>. Your academic profile confirms your commitment to personal and educational growth. As an exchange student you are exempted from paying the tuition fee at Telkom University. On behalf of the entire Telkom University academia, we extend a warm welcome and best wishes for your success. </p>
              <p align='justify'>Your confirmation on this Letter of Acceptance needs to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." by using our online confirmation system or email at info@io.telkomuniversity.ac.id.</p>
             <p align='justify'> Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document and 
			  On-site Registration. Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).". </p>
              <br>
              Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))."<br>
              Your sincerely,<p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
              <u>Lia Yuldinawati, S.T., MM.</u><br>
              Director of Strategic Partnership and International Office Telkom University <br>
              <br>
              <br>
              <br> <br><br><br><br><br>
              <h1> Telkom University Confirmation of <br>Admission </h1>
              To: <br>
              Telkom University International Office, <br>
              Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
              <br>
              To whom it may concern, <br>
              Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
              <table border='1' align='center'>
              <tr bgcolor='#000000'>
              <th font color='white'>No</th>
              <th font color='white'>Student Information</th>
              <th font color='white' width='50%'>Details</th>
              </tr>
              <tr>
              <td>1</td>
              <td>Full Name</td>
              <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
              </tr>
              <tr>
              <td>2</td>
              <td>Admission ID</td>
              <td>".$participant['ADMISSIONID']."</td>
              </tr>
              <tr>
              <td>3</td>
              <td>Birth Place and Birth Date</td>
              <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
              </tr>
              <tr>
              <td>4</td>
              <td>Phone Number</td>
              <td>".$participant['PHONE']."</td>
              </tr>
              <tr>
              <td>5</td>
              <td>Email</td>
              <td>".$participant['EMAIL']."</td>
              </tr>
              </table>
              <br>
              <p align='align'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
              These confirmations and decisions I make without coercion and encouragement from any party. <br>
              <br>
              City of confirmation: ………………………………………………….. <br>
              Date Signed: ………………………………………………………………. <br>
              The Student, <br>
              <br>
              <br>
              <br>
			  <br>
              <br>
              …………………………………………………………………………………. <br>
              ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."<br>
              *(Please sign above your name)<br>
        <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
          <br>
                  <br>
                  <br>
          <br>
          <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
          <br>
                  <br>
                  <br>
          <br>";
		}

    if($participant['STATUSSUBJECTSETTING']=='Y'){
      $datax = $this->participantmodel->getStudySubjectBy($participant['PARTICIPANTID'], $participant['STUDYPROGRAMID']);
      //echo "<pre>"; print_r($datax);die;
      $html .="<h2 align='center'>The List of Selected Courses</h2>
      <h2>Telkom University Student Exchange Program ".$participant['SCHOOLYEAR']."</h2><br><br>
      Student's Name : ".$participant['FULLNAME']."<br>
      Participant Number : ".$participant['ADMISSIONID']."<br><br>
      <table border='1' align='center'>
        <tr bgcolor='#000000'>
          <th font color='white'>No</th>
          <th font color='white'>Course Code</th>
          <th font color='white'>Course</th>
          <th font color='white'>Credit</th>
          <th font color='white'>Semester</th>
          <th font color='white'>Study Program</th>
          <th font color='white'>Faculty</th>
        </tr>";
      // echo "<pre>"; print_r($participant);//die;
      // echo "<pre>"; print_r($datax);die;
      for($i=0; $i<count($datax); $i++){
        $html .= "<tr>
                  <td align='center'>".($i+1)."</td>
                  <td>".$datax[$i]['SUBJECTCODE']."</td>
                  <td>".$datax[$i]['SUBJECTNAME']."</td>
                  <td align='center'>".$datax[$i]['CREDIT']."</td>
                  <td align='center'>".$datax[$i]['SEMESTER']."</td>
                  <td>".$datax[$i]['STUDYPROGRAMNAME']."</td>
                  <td>".$datax[$i]['FACULTYNAMEENG']."</td></tr>";
        //$datax[$i]['NO'] = 
        if($i === count($datax)-1){
          $html .= "</table><br><br>
          *If you have any inquiries, please do not hesitate to contact us on: <br>
          Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
          Web: https://io.telkomuniversity.ac.id<br> 
          Email: info@telkomuniversity.ac.id <br>
          Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
          40257";
        }
      }
      
    }else{
      $html .= "
          If you have any inquiries, please do not hesitate to contact us on: <br>
          Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
          Web: https://io.telkomuniversity.ac.id<br> 
          Email: info@telkomuniversity.ac.id <br>
          Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
          40257";
    }
        
   // echo "<pre>"; print_r($html);die;
        //this the the PDF filename that user will get to download
        $pdfFilePath = "STUNDENT_EXCHANGE-CREDIT_EARNING.pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
		$todayYear=date('Y');
		$this->insertDataLoa($loaNumberPdf,$todayYear,$participantId);
    }
    public function templateTypeD(){

      $this->load->model('participantmodel');
      $participantId  = $this->uri->segment(3);
      $participant = $this->participantmodel->GetParticipantDetail($participantId);
	  $academicYear = $this->participantmodel->GetAcademicYear($participantId);
    $scholarshiptype = $this->participantmodel->getScholarship('scholarshiptype',$participant['SCHOLARSHIPTYPEID']);
    if($participant['STUDYPROGRAMNAME']=='S2 Master of Management'){
          $program = 'Master of Management (2 years)';
        }else if($participant['DEGREE']=='S2' && $participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
          $program = 'Master of Engineering (2 years)';
        }else{
          $program = '';
        }
    $fee = $this->getFee($participant['DEGREE'], $program);
		//$fee = $this->getFee($participant['DEGREE']);
	    $loaNumberPdf=$this->loaNumber($participantId);
if($participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
			$html = "<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                <br>
                  No               : ".$loaNumberPdf."<br>
                  Subject          :  Letter of Acceptance <br>
                  Attachment     :  - <br>
                  <br>
                  To: <br><b>"
                  .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b><br>
                  Applicant Number <b>".$participant['ADMISSIONID']."</b><br><br>
                  Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>,<br>
                  <p align='justify'> Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b> for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile confirms your commitment to personal and educational growth. This acceptance letter means that you have completed all necessary requirements to be our student. </p>
                  <p align='justify'> On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are pleased to inform you that you will be granted <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b> from Telkom University. The Scholarship will cover The Admission Fee only and you need to cover the Tuition Fees on each semester by yourself or by your sponsors. Kindly find the attachment for the Tuition Fee details and how to pay the fees.
                  Please do the payment of the Tuition Fee before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE'])))).". </p>
                  <p align='justify'> Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." by using our online confirmation system or email at info@io.telkomuniversity.ac.id.</p>
                   <p align='justify'>Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site Registration, and Orientation Program. Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).". </p>
                  If you have any inquiries, please do not hesitate to contact us on: <br>
                    &nbsp;&nbsp;    Phone    : +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
                   &nbsp;&nbsp;     Web    : https://io.telkomuniversity.ac.id <br>
                    &nbsp;&nbsp;    Email    : info@io.telkomuniversity.ac.id <br>
                    &nbsp;&nbsp;  Address:: Telkom University, Learning Center Building R.118, Jl. Telekomunikasi No.1 Bandung &nbsp;&nbsp;40257 <br>
                      <br>
                  Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))."<br>
                  Your sincerely, <p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
                  <u>Lia Yuldinawati, S.T., MM.</u> <br>
                  Director of Strategic Partnership and International Office Telkom University<br>
                  <br>
				  <br>
				  <br>
				  <br><br>
                  <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee <br>
					  c. Health Insurance <br>
					  d. Bank Fee </td>
                  <td><br>
                      Scholarship <br>
                      Self Payment <br>
					  Self Payment <br>
					  Self Payment </td>
                  </tr>
                  </table>
                  <br>

                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                 <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white' >Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
					<tr>
                  <td>1</td>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td>
				  <td></td>
                  <td>Scholarship</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment</td>
                  </tr>
				  <tr>
                  <td>3</td>
                  <td>Health Insurance</td>
                  <td>IDR 150.000,00</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Bank Fee</td>
                  <td>IDR 2.500,00</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))." </td>
				  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR ".number_format(($fee['TUITIONFEE']+150000+2500),2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				   <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  <h2>Scholarship Information </h2>
                  Below are the terms and condition of Telkom University Scholarship Scheme. <br>
                   <h3 font color='blue'>".$scholarshiptype['SCHOLARSHIPTYPENAME']." (Admission Fee Waived)</h3>
                  1. The scholarship will cover only The Admission Fee at Telkom University.<br>
                2.  Since this type of scholarship doesn’t cover the dormitory cost, if you wish to stay at Telkom University’s dormitory you will be charged IDR 500.000,- (USD 35) monthly.<br>
                 <h3 font color='blue'> Terms and Condition</h3>
                1. Please Note that all the Scholarship of Telkom University exclude flight ticket from and to your home country. <br>
                 2. Indonesian Visa Application Fee in your home country is at your own cost. <br>
                3. The students cannot apply to another type of scholarship schemes once Telkom University decide the acceptance as mentioned in the Letter of Acceptance. <br>
                4. The scholarship will not cover your health insurance and any other cost which is not mentioned in this terms and condition. <br>
                 <br><br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
                  <h1>Telkom University Confirmation of<br> Admission</h1> 
                  <br>
                  To: <br>
                  Telkom University International Office, <br>
                  Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br><br>
                  To whom it may concern, <br>
                  Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
                  <table border='1' align='center'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>
                  <p align = 'justify'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                  These confirmations and decisions I make without coercion and encouragement from any party.
                  <br>
                  City of confirmation: ………………………………………………….. <br>
                  Date Signed: ………………………………………………………………. <br>
                  The Student, <br>
                  <br>
                  <br>
                  <br><br>
				  <br>
									<br>
                  …………………………………………………………………………………. <br>
                  ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."
                  *(Please sign above your name)
				  <br>
									<br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  If you have any inquiries, please do not hesitate to contact us on: <br>
				  Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
				  Web: https://io.telkomuniversity.ac.id<br> 
				  Email: info@telkomuniversity.ac.id <br>
				  Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
					40257
                  ";
		}else{
			$html = "<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p>
                <br>
                  No               : ".$loaNumberPdf."<br>
                  Subject          :  Letter of Acceptance <br>
                  Attachment     :  - <br>
                  <br>
                  To: <br><b>"
                  .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b><br>
                  Applicant Number <b>".$participant['ADMISSIONID']."</b><br><br>
                  Dear <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>,<br>
                  <p align='justify'> Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b>, applicant number <b>".$participant['ADMISSIONID']."</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b> for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile confirms your commitment to personal and educational growth. This acceptance letter means that you have completed all necessary requirements to be our student. </p>
                  <p align='justify'> On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are pleased to inform you that you will be granted <b>".$scholarshiptype['SCHOLARSHIPTYPENAME']."</b> from Telkom University. The Scholarship will cover The Admission Fee only and you need to cover the Tuition Fees on each semester by yourself or by your sponsors. Kindly find the attachment for the Tuition Fee details and how to pay the fees.
                  Please do the payment of the Tuition Fee before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE'])))).". </p>
                  <p align='justify'> Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." by using our online confirmation system or email at info@io.telkomuniversity.ac.id.</p>
                   <p align='justify'>Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site Registration, and Orientation Program. Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).". </p>
                  If you have any inquiries, please do not hesitate to contact us on: <br>
                    &nbsp;&nbsp;    Phone    : +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
                   &nbsp;&nbsp;     Web    : https://io.telkomuniversity.ac.id <br>
                    &nbsp;&nbsp;    Email    : info@io.telkomuniversity.ac.id <br>
                    &nbsp;&nbsp;  Address:: Telkom University, Learning Center Building R.118, Jl. Telekomunikasi No.1 Bandung &nbsp;&nbsp;40257 <br>
                      <br>
                  Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))."<br>
                  Your sincerely, <p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
                  <u>Lia Yuldinawati, S.T., MM.</u> <br>
                  Director of Strategic Partnership and International Office Telkom University<br>
                  <br>
				  <br>
				  <br>
				  <br><br>
                  <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>					  
                      b. Tuition Fee<br>
					  c. Bank Fee</td>
                  <td><br>
                      Scholarship <br>
					  Self Payment <br>
                      Self Payment</td>
                  </tr>
                  </table>
                  <br>

                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                 <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white' >Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
					<tr>
                  <td>1</td>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td>
				  <td></td>
                  <td>Scholarship</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td>Self Payment</td>
                  </tr>
				  <tr>
                  <td>3</td>
                  <td>Bank Fee</td>
                  <td>IDR 2.500,00</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))." </td>
				  <td>Self Payment</td>
                  </tr>
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR ".number_format(($fee['TUITIONFEE']+2500),2,',','.')."</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				   <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  <h2>Scholarship Information </h2>
                  Below are the terms and condition of Telkom University Scholarship Scheme. <br>
                  <h3 font color='blue'>".$scholarshiptype['SCHOLARSHIPTYPENAME']." (Admission Fee Waived)</h3>
                  1. The scholarship will cover only The Admission Fee at Telkom University.<br>
                  2. Since this type of scholarship doesn’t cover the dormitory cost, if you wish to stay at Telkom University’s dormitory you will be charged IDR 500.000,- (USD 35) monthly.<br>
                 <h3 font color='blue'> Terms and Condition</h3>
                1. Please Note that all the Scholarship of Telkom University exclude flight ticket from and to your home country. <br>
                 2. Indonesian Visa Application Fee in your home country is at your own cost. <br>
                3. The students cannot apply to another type of scholarship schemes once Telkom University decide the acceptance as mentioned in the Letter of Acceptance. <br>
                 4. The scholarship will not cover your health insurance and any other cost which is not mentioned in this terms and condition. <br>
                 <br><br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
                  <h1>Telkom University Confirmation of<br> Admission</h1> 
                  <br>
                  To: <br>
                  Telkom University International Office, <br>
                  Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br><br>
                  To whom it may concern, <br>
                  Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
                  <table border='1' align='center'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>
                  <p align = 'justify'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                  These confirmations and decisions I make without coercion and encouragement from any party.
                  <br>
                  City of confirmation: ………………………………………………….. <br>
                  Date Signed: ………………………………………………………………. <br>
                  The Student, <br>
                  <br>
                  <br>
                  <br><br>
				  <br>
									<br>
                  …………………………………………………………………………………. <br>
                  ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."
                  *(Please sign above your name)
				  <br>
									<br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  If you have any inquiries, please do not hesitate to contact us on: <br>
				  Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
				  Web: https://io.telkomuniversity.ac.id<br> 
				  Email: info@telkomuniversity.ac.id <br>
				  Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
					40257
                  ";
                  
		}
        
    $tamp = explode(' Scholarship',$scholarshiptype['SCHOLARSHIPTYPENAME']); 
        $type =str_replace(" ", "", $tamp[0]);   
        //this the the PDF filename that user will get to download
        $pdfFilePath = "Letter_Acceptance_".$type.".pdf";
                  //load mPDF library
                  $this->load->library('m_pdf');

                  //generate the PDF from the given html
                  $this->m_pdf->pdf->WriteHTML($html);

                  //download it.
                  $this->m_pdf->pdf->Output($pdfFilePath, "D");
				  $todayYear=date('Y');
		$this->insertDataLoa($loaNumberPdf,$todayYear,$participantId);
                  //echo $html;
    }
	
	public function getFee($degree, $program){
		$this->load->model('feemodel');
		$fee = $this->feemodel->GetFeeByDegree($degree, $program);
		return $fee;
	}
	
	public function templateTypeX()
    {		
        $this->load->model('participantmodel');
        $participantId=$this->uri->segment(3);
        $participant= $this->participantmodel->GetParticipantDetail($participantId);
		$academicYear = $this->participantmodel->GetAcademicYear($participantId);
    if($participant['STUDYPROGRAMNAME']=='S2 Master of Management'){
          $program = 'Master of Management (2 years)';
        }else if($participant['DEGREE']=='S2' && $participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
          $program = 'Master of Engineering (2 years)';
        }else{
          $program = '';
        }
    $fee = $this->getFee($participant['DEGREE'], $program);
		//$fee = $this->getFee($participant['DEGREE']);
		$loaNumberPdf=$this->loaNumber($participantId);
		$scholarship = $this->participantmodel->getScholarship('scholarshiptype',$participant['SCHOLARSHIPTYPEID']);
		//echo "<pre>"; print_r($scholarship);die;
		if($participant['STUDYPROGRAMNAME']!='S2 Master of Management'){
			$html = "<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p><br>
                  <br>
                  No               :  ".$loaNumberPdf."<br>
                  Subject          :  Letter of Acceptance <br>
                  Attachment     :  - <br>
                  <br>
                  To: <br><b>"
                  .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b><br>
                  Applicant Number <b>".$participant['ADMISSIONID']."</b><br><br>
                  Dear Students,<br>
                  <p align='justify'> Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')." (".$participant['ADMISSIONID'].")</b> has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b>
				  for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile confirms your commitment to personal and educational growth. This acceptance letter means that you have completed all necessary requirements to be our student.</p>
                  <p align='justify'>On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are pleased to inform you that you will be granted <b>".$scholarship['SCHOLARSHIPTYPENAME']."</b> covering your Admission Fee and Tuition Fees for your first year. 
				  We will also give you dormitory facility to support your living in our campus. The scholarship will be extended to further semesters as long as your academic performance meets the requirements of <b>".$scholarship['SCHOLARSHIPTYPENAME']."</b>.</p>
                   <p align='justify'>Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." 
				   by using our online confirmation system or email at info@io.telkomuniversity.ac.id.</p>
                 <p align='justify'> Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site Registration, and Orientation Program. 
				 Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).".</p>
				<br>	
                  Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))."<br>
                  Your sincerely, <p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
                  <u>Lia Yuldinawati, S.T., MM.</u>  <br>
                  Director of Strategic Partnership and International Office Telkom University<br>
                  <br>
				  <br>
				  <br>
				  <br>	<br>			  
				  <h2> Scholarship Information </h2>
                  <p align = 'justify'>Below are the terms and condition of Telkom University Scholarship Scheme. <br>
                  <h3 font color='blue'> ".$scholarship['SCHOLARSHIPTYPENAME']." (Tuition fee and dormitory waived) </h3>
                  The scholarship is dedicated to students who could show the best academic performance. ".$scholarship['SCHOLARSHIPTYPENAME']." holder will get:<br>
                  1.    Admission Fee (one time) <br>
                  2.    Tuition Fee, it will be reviewed every year. Student should maintain at least 3.0. If the GPA is less than 3.00, the following scheme will be applied: <br>
                  &emsp; a.    GPA ≥ 3.00. Tuition Fee waiver 100%. <br>
                  &emsp; b.    2.75 ≤ GPA < 3.00. Tuition Fee waiver 75%. <br>
                  &emsp; c.    2.50 ≤ GPA < 2.75. Tuition Fee waiver 50%. <br>
                  &emsp; d.    2.25 ≤ GPA < 2.50. Tuition Fee waiver 25%. <br>
                  &emsp; e.    GPA < 2.25. Tuition Fee waiver 0%. <br>
                  3.    Dormitory will be reviewed per year. Student should maintain at least 3.0. If the GPA is less than 3.00, the dormitory will be charged IDR 500.000,- (USD 35) monthly until the students could maintain to reach GPA ≥ 3.0 for the next year. <br>
                  </p><br>
                  <h3 font color='blue'>Terms and Condition</h3> 
                  1.    Indonesian Visa Application Fee in your home country is at your own cost.<br>
                  2.    After decided by Telkom University by using the Letter of Acceptance, the students cannot apply to another type of scholarship schemes.<br>
                  3.    After decided by Telkom University by using the Letter of Acceptance, the students cannot apply to another study program.<br>
                  4.    The students will receive the same scholarship condition if they could maintain the GPA to be at least 3.0. <br>
                  5.    The scholarship will not cover your health insurance and any other cost which is not mentioned in this terms and condition. <br>
                  6.    The recipient of the scholarship must obey all of the rules in Telkom University otherwise Telkom University has the right to discontinue the scholarship. <br>
                 <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
                  <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee<br>
					  c. Health Insurance<br>
					  d. Bank Fee</td>
                  <td><br>
                      Scholarship <br>
                       Scholarship<br>
					   Self Payment<br>
					   Self Payment</td>
                  </tr>
                  </table>
                  <br>

                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white' >Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
				<tr>
                  <td>1</td>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td> 
				  <td> </td>
                  <td> Scholarship</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
				  <td> </td>
                  <td> Scholarship</td>
                  </tr>
				  <tr>
                  <td>3</td>
                  <td>Health Insurance</td>
                  <td>IDR 150.000,00</td>
				  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))." </td>
                  <td> Self Payment</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Bank Fee</td>
                  <td>IDR 2.500,00</td>
                  <td> ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				   <td> Self Payment</td>
                  </tr>
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR 152.500,00</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE']))))."</td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  
                   <h1>Telkom University Confirmation of <br>
				  Admission </h1> <br>
                  To: <br>
                  Telkom University International Office, <br>
                  Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
                  <br>
                  To whom it may concern, <br>
                  Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
                  <table border='1' align='center'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <p align = 'justify'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                  These confirmations and decisions I make without coercion and encouragement from any party.
                  <br>
                  City of confirmation: ………………………………………………….. <br>
                  Date Signed: ………………………………………………………………. <br>
                  The Student, <br>
                  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  …………………………………………………………………………………. <br>
                  ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."
                  *(Please sign above your name)
				  <br>
									<br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  If you have any inquiries, please do not hesitate to contact us on: <br>
				  Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
				  Web: https://io.telkomuniversity.ac.id<br> 
				  Email: info@telkomuniversity.ac.id <br>
				  Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
					40257
                  ";
		}else{
			$html = "<p style='padding-left: 540px;'><img src='".base_url('/uploads/telkom.png')."' width='129px' height='43px' align='right'/></p><br>
                  <br>
                  No               :  ".$loaNumberPdf."<br>
                  Subject          :  Letter of Acceptance <br>
                  Attachment     :  - <br>
                  <br>
                  To: <br><b>"
                  .htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b><br>
                  Applicant Number <b>".$participant['ADMISSIONID']."</b><br><br>
                  Dear Student,<br>
                  <p align='justify'> Hereby we confirm that <b>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</b> (".$participant['ADMISSIONID'].") has been accepted in <b>".$participant['STUDYPROGRAMNAME']."</b> study program of <b>".$participant['FACULTYNAMEENG']."</b>
				  for the academic year of <b>".$academicYear['SCHOOLYEAR']."</b>. Your academic profile confirms your commitment to personal and educational growth. This acceptance letter means that you have completed all necessary requirements to be our student.</p>
                  <p align='justify'>On behalf of Telkom University, we extend a warm welcome and best wishes for your success. We are pleased to inform you that you will be granted <b>".$scholarship['SCHOLARSHIPTYPENAME']."</b> covering your Admission Fee and Tuition Fees for your first year. 
				  We will also give you dormitory facility to support your living in our campus. The scholarship will be extended to further semesters as long as your academic performance meets the requirements of <b>".$scholarship['SCHOLARSHIPTYPENAME']."</b>.</p>
                   <p align='justify'>Your confirmation on this Letter of Acceptance and Our Terms and Conditions need to be informed to us before ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['DEADLINEDATE']))))." 
				   by using our online confirmation system or email at info@io.telkomuniversity.ac.id.</p>
                 <p align='justify'> Please be in Telkom University by ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE']))))." for Processing your Immigration Document, On-site Registration, and Orientation Program. 
				 Class will be started on ".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['STARTINGDATE'])))).".</p>
				<br>	
                  Bandung, ".date('F jS, Y',strtotime(str_replace('00:00:00','',$participant['ACCEPTANCEDATE'])))."<br>
                  Your sincerely, <p><img align='left' width='90px' height='90px' src='".base_url()."/images/ttdBuRina.png'/><img style='padding-left: 400px;' align='right' width='90px' height='90px' src='".base_url()."/uploads/media/participant/requirement/".$participant['PASSPORTNO']."/Qrcode.png'/></p>
                  <u>Lia Yuldinawati, S.T., MM.</u>  <br>
                  Director of Strategic Partnership and International Office Telkom University<br>
                  <br>
				  <br>
				  <br>
				  <br><br>
				  <h2> Scholarship Information </h2>
                  <p align = 'justify'>Below are the terms and condition of Telkom University Scholarship Scheme. <br>
                  <h3 font color='blue'> ".$scholarship['SCHOLARSHIPTYPENAME']." (Tuition fee and dormitory waived) </h3>
                  The scholarship is dedicated to students who could show the best academic performance. ".$scholarship['SCHOLARSHIPTYPENAME']." holder will get:<br>
                  1.    Admission Fee (one time) <br>
                  2.    Tuition Fee, it will be reviewed every year. Student should maintain at least 3.0. If the GPA is less than 3.00, the following scheme will be applied: <br>
                  &emsp; a.    GPA ≥ 3.00. Tuition Fee waiver 100%. <br>
                  &emsp; b.    2.75 ≤ GPA < 3.00. Tuition Fee waiver 75%. <br>
                  &emsp; c.    2.50 ≤ GPA < 2.75. Tuition Fee waiver 50%. <br>
                  &emsp; d.    2.25 ≤ GPA < 2.50. Tuition Fee waiver 25%. <br>
                  &emsp; e.    GPA < 2.25. Tuition Fee waiver 0%. <br>
                  3.    Dormitory will be reviewed per year. Student should maintain at least 3.0. If the GPA is less than 3.00, the dormitory will be charged IDR 500.000,- (USD 35) monthly until the students could maintain to reach GPA ≥ 3.0 for the next year. <br>
                  </p><br>
                  <h3 font color='blue'>Terms and Condition</h3> 
                  1.    Indonesian Visa Application Fee in your home country is at your own cost.<br>
                  2.    After decided by Telkom University by using the Letter of Acceptance, the students cannot apply to another type of scholarship schemes.<br>
                  3.    After decided by Telkom University by using the Letter of Acceptance, the students cannot apply to another study program.<br>
                  4.    The students will receive the same scholarship condition if they could maintain the GPA to be at least 3.0. <br>
                  5.    The scholarship will not cover your health insurance and any other cost which is not mentioned in this terms and condition. <br>
                  6.    The recipient of the scholarship must obey all of the rules in Telkom University otherwise Telkom University has the right to discontinue the scholarship. <br>
                 <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  
                  <h1> Telkom University Admission Details </h1>
                  Below are the details of the information about your admission and payment at Telkom University<br><br>
                  A.    Student Admission Information<br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <br>

                  B. Acceptance Detail <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th width='10' font color='white'>No</th>
                  <th width='50' font color='white'>Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Acceptance Date</td>
                  <td>".date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['ACCEPTANCEDATE']))))."</td>
                  </tr>

                  <tr>
                  <td>2</td>
                  <td>Accepted Study Program</td>
                  <td>".$participant['STUDYPROGRAMNAME']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Accepted Faculty</td>
                  <td>".$participant['FACULTYNAMEENG']."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Admission Year</td>
                  <td>".$academicYear['SCHOOLYEAR']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Payment Details <br>
                      a. Admission Fee <br>
                      b. Tuition Fee</td>
                  <td><br>
                      Scholarship <br>
                       Scholarship</td>
                  </tr>
                  </table>
                  <br>

                  C. Payment Information <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Term</th>
				  <th font color='white'>Payment Amount</th>
                  <th font color='white' >Due Date</th>
				  <th font color='white' >Payment Method</th>
                  </tr>
				<tr>
                  <td>1</td>
                  <td>Admission Fee</td>
                  <td>IDR ".number_format($fee['ADMISSIONFEE'],2,',','.')."</td> 
				  <td> </td>
                  <td> Scholarship</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Tuition Fee</td>
                  <td>IDR ".number_format($fee['TUITIONFEE'],2,',','.')."</td>
				  <td> </td>
                  <td> Scholarship</td>
                  </tr>
				  
                  <tr>
                  <td></td>
                  <td>Total Payment</td>
                  <td>IDR 0</td>
                  <td></td>
				  <td></td>
                  </tr>
                  </table> <br>
                  D. Payment Channel <br>
                  <table border='1'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Payment Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</th>
                  <td>Bank</td>
                  <td>BNI</td>
                  </tr>
                  <tr>
                  <td>2</th>
                  <td>Virtual Account Number</td>
                  <td>83219".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</th>
                  <td>SWIFT Code</td>
                  <td>BNINIDJA</td>
                  </tr>
                  </table>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
                  
                   <h1>Telkom University Confirmation of <br>
				  Admission </h1> <br>
                  To: <br>
                  Telkom University International Office, <br>
                  Jl. Telekomunikasi No.1 Bandung, Jabar, Indonesia 40257 <br>
                  <br>
                  To whom it may concern, <br>
                  Regarding the Letter of Acceptance ".$loaNumberPdf." from Telkom University. As a prospective student with the following identity: <br><br>
                  <table border='1' align='center'>
                  <tr bgcolor='#000000'>
                  <th font color='white'>No</th>
                  <th font color='white'>Student Information</th>
                  <th font color='white' width='50%'>Details</th>
                  </tr>
                  <tr>
                  <td>1</td>
                  <td>Full Name</td>
                  <td>".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."</td>
                  </tr>
                  <tr>
                  <td>2</td>
                  <td>Admission ID</td>
                  <td>".$participant['ADMISSIONID']."</td>
                  </tr>
                  <tr>
                  <td>3</td>
                  <td>Birth Place and Birth Date</td>
                  <td>".$participant['BIRTHPLACE'].', '.date('F jS, Y',strtotime(str_replace(' ','',str_replace('00:00:00','',$participant['BIRTHDATE']))))."</td>
                  </tr>
                  <tr>
                  <td>4</td>
                  <td>Phone Number</td>
                  <td>".$participant['PHONE']."</td>
                  </tr>
                  <tr>
                  <td>5</td>
                  <td>Email</td>
                  <td>".$participant['EMAIL']."</td>
                  </tr>
                  </table>
                  <p align = 'justify'>I hereby confirmed and accepted The Terms and Condition to be Telkom University Student for The Academic Year of ".$academicYear['SCHOOLYEAR']." with the whole responsibility to follow the rules at Telkom University and have all of the right as a student at Telkom University. </p>
                  These confirmations and decisions I make without coercion and encouragement from any party.
                  <br>
                  City of confirmation: ………………………………………………….. <br>
                  Date Signed: ………………………………………………………………. <br>
                  The Student, <br>
                  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  …………………………………………………………………………………. <br>
                  ".htmlentities($participant['FULLNAME'], ENT_NOQUOTES, 'UTF-8')."
                  *(Please sign above your name)
				  <br>
									<br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
				  <br>
                  <br>
                  <br>
				  <br>
				  If you have any inquiries, please do not hesitate to contact us on: <br>
				  Phone: +62 22 7564108 ext. 2400 / Mobile Phone (What’s App, Line): +62 813 2112 3400 <br>
				  Web: https://io.telkomuniversity.ac.id<br> 
				  Email: info@telkomuniversity.ac.id <br>
				  Address: Telkom University, Learning Center Building | R.118, Jl. Telekomunikasi No 1 Bandung<br>
					40257
                  ";
		}
        
        //this the the PDF filename that user will get to download
        $pdfFilePath = "Letter_Accept_Type_".str_replace(' ','_',$scholarship['SCHOLARSHIPTYPENAME']).".pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
		$todayYear=date('Y');
		$this->insertDataLoa($loaNumberPdf,$todayYear,$participantId);
    }
	

}