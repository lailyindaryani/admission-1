
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/studyProgramList">Study Program</a></li>
                <li class="active">Study Program Add</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Study Program Add</h2>
            <em>Study Program Data</em>
        </div>

        <div class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
                        </div>
                        <div class="widget-content">
                            <form action="<?php echo base_url()?>admin/studyProgramStore" class="form-horizontal" role="form" method="post" id="ioform">
                                <fieldset>
                                    <legend>Study Program Data</legend>
                                    <div class="form-group">
                                        <label for="ticket-subject" class="col-sm-3 control-label">Study Program Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="studyprogramname" id="studyprogramname" placeholder="Study Program Name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="ticket-subject" class="col-sm-3 control-label">iGracias Study Program ID</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="igraciasstudyprogramid" id="igraciasstudyprogramid" placeholder="iGracias Study Program ID">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="ticket-subject" class="col-sm-3 control-label">Faculty</label>
                                        <div class="col-sm-9">
                                            <select name="faculty" id="faculty">
                                                <option value="">Select Faculty</option>
                                                <?php foreach ($faculties as $faculty){?>
                                                    <option value="<?=$faculty['FACULTYID']?>"><?=$faculty['FACULTYNAMEENG']?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-2">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url()?>admin/studyProgramList'">Cancel</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    <!-- END SUPPORT TICKET FORM -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->




<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $("#faculty").select2({minimumResultsForSearch: -1});

    $( "#ioform" ).validate({
        rules: {
            studyprogramname: {
                required: true
            },
            faculty: {
                required: true
            }
        }
    });
</script>