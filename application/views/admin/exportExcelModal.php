<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Export.xls");
?>
	<table height="10" width="100%">
	<caption><?=ucwords(strtolower(str_replace('_',' ',$status)))?></caption>
        <thead>
            <tr>
				<td>Participant Number</td>
                <td>Fullname</td>
                <td>Passport</td>
                <td>Gender</td>
				<td>Email</td>
				<td>Phone</td>
                <td>Country</td>
				<td>Program Type</td>
                <td>Enrollment</td>
				<td>Faculty</td>
				<td>Study Program</td>
            </tr>
        </thead>
		<?php
		foreach($data_participant as $val){
			echo "<tr>";
            echo "<td>".$val['ADMISSIONID']."</td>";
			echo "<td>".$val['FULLNAME']."</td>";
			echo "<td>".$val['PASSPORTNO']."</td>";
			echo "<td>".$val['GENDER']."</td>";
			echo "<td>".$val['EMAIL']."</td>";
			echo "<td>".$val['PHONE']."</td>";
			echo "<td>".$val['NATIONALITY']."</td>";
			echo "<td>".$val['PROGRAMTYPE']."</td>";
			echo "<td>".$val['DESCRIPTION']."</td>";
			echo "<td>".$val['FACULTYNAMEENG']."</td>";
			echo "<td>".$val['STUDYPROGRAMNAME']."</td>";
            echo "</tr>";
		}?>	
	</table>
<?php die;?>