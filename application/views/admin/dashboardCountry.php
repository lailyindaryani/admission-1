
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
		<div class="col-lg-4 ">
				<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="<?php base_url().'admin/dashboardEnrollment'?>">Home</a></li>
						 <li class="active">Summary by Country</li>
				</ul>
		</div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
		<h2>Summary Report</h2>
		<em>Summary Report of Participants by Country</em>
</div>

<div class="main-content">
<div class="row">
		<div class="col-md-12">
				<!-- SUPPOR TICKET FORM -->
				<div class="widget">
						<div class="widget-header">
								<!--<h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
						</div>
					
						<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
						<div class="row" style="border: 1px solid #ccc; margin:5px;">
                <div class="col-md-12">
                    <div class="widget-content">
                        <div class="row form-horizontal">
                            <div class="col-md-4">
                                <p>
                                    <span>Acceptance Status</span>
                                    <select id="acceptance" name="acceptance" class="filter">
                                        <option value="">-All-</option>
                                        <option value="ACCEPTED">Accepted</option>
                                        <option value="UNACCEPTED">Not Accepted</option>
                                    </select>
                                </p>
                            </div>
						</div>
					</div>
					</div>
					</div>
					
						<div class="widget-content">
												<div class="table-basic">
														<table id="tableProgram" class="table table-sorting table-hover  table-striped datatable">
																<thead>
																<tr>
																		<th >No</th>
																		<th >Country</th>
																		<th >Number of Participants</th>
																</tr>
																</thead>
																<tbody>
																<tr>
																		<td colspan="10" class="dataTables_empty">Loading data from server</td>
																</tr>
																</tbody>
														</table>
												</div><!-- style="display:none" -->
					<?php echo '<table id="datatable" style="display:none" class="table table-sorting table-hover  table-striped datatable">';
                        echo '<thead>';
                        echo '<tr>';?>
							<?php echo '<th></th>';?>
							<?php for( $i=0; $i<sizeof($data_country) ; $i++ ){
                           echo "<th >".$data_country[$i]['NATIONALITY']."</th>";
							}?>
                        <?php echo '</tr>';
                        echo '</thead>';
                        echo '<tbody >';
                        echo '<tr>';?>
							<?php echo '<td font size="5">Participants</td>';?>
							<?php for( $i=0; $i<sizeof($data_country) ; $i++ ){
							echo "<td>".$data_country[$i]['JUMLAH']."</td>";
							}?>
                        <?php echo '</tr>';
                        echo '</tbody>';
                    echo '</table>';?>
						</div>
				</div>
			</div>
</div>
</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->




<script type="text/javascript">
		$(document).ready(function() {
			$("#acceptance").select2();

				$(".filter").on("change", function (e) {
					console.log($("#acceptance").val());
            $('#tableProgram').DataTable().ajax.reload();
        });
				var dt= $('#tableProgram').dataTable( {
						//"bJQueryUI": true,
						"order": [[ 2, "ASC" ]],
						/*  sDom: "T<'clearfix'>" +
						 "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
						 "t"+
						 "<'row'<'col-sm-6'i><'col-sm-6'p>>",
						 "tableTools": {

						 },*/
						 "columnDefs": [
                { "width": "8%", "targets": 0},
             //   { "width": "12%", "targets": 3},
            ],
						"sPaginationType": "full_numbers",
						"bProcessing": true,
						"bServerSide": true,
						"aLengthMenu": [
								[20, 30, 50, 100, -1],
								[20, 30, 50, 100, "All"]
						],
						"fnDrawCallback": function() {

								//initAction();

						},
						"sAjaxSource": "<?php echo base_url(); ?>admin/datatableCountryParticipant2",
						"fnRowCallback":
								function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                       var participantLink = "<button class='btn btn-xs btn-primary'><i class='fa fa-list'></i></button>";
										$(nRow).html(
												'<td>'+aData[0]+'</td>' +
														'<td>'+aData[1]+'</td>' +
														'<td>'+aData[2]+'</td>' 

										);
										return nRow;
								},
								
						"fnServerData": function ( sSource, aoData, fnCallback ) {
								/* Add some extra data to the sender */
								aoData.push(
									{ "name": "acceptance", "value": $("#acceptance").val() }
								);
								$.getJSON( sSource, aoData, function (json) {
										/* Do whatever additional processing you want on the callback, then tell DataTables */
										fnCallback(json)
								} );
						}
				} );
				
		Highcharts.chart('container', {
    data: {
        table: 'datatable'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Summary Report of Participants by Country'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Number of Participants'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

	$('#acceptance').on('change',function(){
		var acceptance= $(this).val();
		$.ajax(
            {
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getParticipantCountry",
                data:{ acceptanceStatus: acceptance},
                success:function(response)
                {
					$('#datatable thead').empty();
					$('#datatable tbody').empty();
					var tblHead = "<tr>"+"<th>"+"</th>";
					var tblRow = "<tr>"+ "<td font size=5>Participants</td>";
					var i;
					for (i = 0; i < response.length; i++) {
						tblHead += "<th>" + response[i]['NATIONALITY'] + "</th>";
						tblRow += "<td>" + response[i]['JUMLAH'] + "</td>";
					} 
					tblHead +=  "</tr>";
					tblRow +=  "</tr>";
					
					$(tblHead).appendTo("#datatable thead");
					$(tblRow).appendTo("#datatable tbody");
					
					Highcharts.chart('container', {
						data: {
							table: 'datatable'
						},
						chart: {
							type: 'column'
						},
						title: {
							text: 'Summary Report of '+acceptance+' Participants by Country'
						},
						yAxis: {
							allowDecimals: false,
						title: {
							text: 'Number of Participants'
						}
					},
					tooltip: {
						formatter: function () {
							return '<b>' + this.series.name + '</b><br/>' +
							this.point.y + ' ' + this.point.name.toLowerCase();
						}
					}
					});
                },
                error: function() 
                {
                    alert("Invalide!");
                }
            }
        );
		
	});
		});
</script>
