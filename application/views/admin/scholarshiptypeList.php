<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
            <li><a href="<?php echo base_url()?>admin/languageList">Scholarship</a></li>
            <li class="active">Scholarship List</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Scholarship List</h2>
    <em>Scholarship Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
<!--                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
            </div>
            <div class="widget-content">
                <div class="form-group">
                    <div class="btn-group">
                        <button onclick="location.href='<?php echo base_url()?>admin/scholarshiptypeAdd'" class="btn btn-danger"> Add New &nbsp;
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="table-basic">
                    <table id="tableScholarship" class="table table-sorting table-hover  table-striped datatable">
                        <thead>
                        <tr>
                            <th >No</th>
                            <th >Scholarshiptype Name</th>
                            <th >Active Status</th>
                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty">Loading data from server</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<!--BEGIN OF MODAL-->
<!--edit-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/scholarshiptypeUpdate" class="form-horizontal modal-form" role="form" method="post" id="ioform">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Scholarship</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="scholarshiptypeid" id="scholarshiptypeid" value="">
                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label">Scholarshiptype Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="scholarshiptypename" id="scholarshiptypename" placeholder="Scholarshiptype Name" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label">Active Status</label>
                        <div class="col-sm-9">
                            <select name="activestatus" class="" id="activestatus">
                                <option value="Y">Yes</option>
                                <option value="N">No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--delete-->
<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish to delete this scholarshiptype?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="delete-confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END OF MODAL-->

<script type="text/javascript" src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).on("click", ".edit-button", function () {
        var scholarshiptypeid = $(this).data('id');

        $.ajax({
            type    : "POST",
            url     : "<?php echo base_url()?>admin/scholarshiptypeAjaxGetDetail",
            data    : {scholarshiptypeid: scholarshiptypeid},
            success : function (data) {
                data = $.parseJSON( data );
                $('.modal-form #scholarshiptypeid').val(data[0].SCHOLARSHIPTYPEID);
                $('.modal-form #scholarshiptypename').val(data[0].SCHOLARSHIPTYPENAME);
                // $('.modal-form #activestatus').val(data[0].ACTIVESTATUS);
                $('.modal-form #activestatus').select2("val", data[0].ACTIVESTATUS);
            }
        });
    });

    $('#ioform').validate({
        rules: {
            scholarshiptypename: {
                required: true
            },
            activestatus: {
                required: true
            }
        },
        submitHandler: function (form) {
            var scholarshiptypeid = $(".modal-form #scholarshiptypeid").val();
            var scholarshiptypename = $(".modal-form #scholarshiptypename").val();
            var activestatus = $(".modal-form #activestatus").val();
            $.ajax({
                type    : "POST",
                data    : {scholarshiptypeid:scholarshiptypeid, scholarshiptypename:scholarshiptypename, activestatus: activestatus},
                url     : "<?php echo base_url()?>admin/scholarshiptypeUpdate",
                success : function(data) {
                    $('#tableScholarship').DataTable().ajax.reload();
                    $("#edit-modal").modal('hide');
                    return false;
                },
                error   : function (e) {
                    return false;
                }
            });
        }
    });

    $(document).on("click", ".delete-button", function () {
        var ID = $(this).data('id');
        $('#delete-confirm-button').data('id', ID)
    });

    $('#delete-confirm-button').click(function(){
        var scholarshiptypeid = $(this).data('id');

        $.ajax({
            method  : 'POST',
            data    : {scholarshiptypeid:scholarshiptypeid},
            url     : "<?php echo base_url()?>admin/scholarshiptypeDrop",
            success : function (data) {
                $('#tableScholarship').DataTable().ajax.reload();
                $("#confirm-delete-modal").modal('hide');
                return false;
            }
        });
    });

    $(document).ready(function() {
         $("#activestatus").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select Active Status",
        });
        var dt= $('#tableScholarship').dataTable( {
            //"bJQueryUI": true,
            "order": [[ 2, "ASC" ]],
            /*  sDom: "T<'clearfix'>" +
             "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
             "t"+
             "<'row'<'col-sm-6'i><'col-sm-6'p>>",
             "tableTools": {
             "sSwfPath": "plugins/datatable/exts/swf/copy_csv_xls_pdf.swf"
             },*/
            "columnDefs": [
                { "width": "15%", "targets": 2}
            ],
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "aLengthMenu": [
                [20, 30, 50, 100, -1],
                [20, 30, 50, 100, "All"]
            ],
            "fnDrawCallback": function() {

                //initAction();

            },
            "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateScholarship",
            "fnRowCallback":
                function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

                    $(nRow).html(
                        '<td>'+aData[0]+'</td>' +
                        '<td>'+aData[1]+'</td>' +
                        '<td>'+aData[2]+'</td>' +
                        '<td>' +
                        '   <button data-toggle="modal" data-id="'+aData[3]+'" title="edit" class="edit-button btn btn-xs btn-primary" href="#edit-modal"><span class=\"fa fa-edit\"></span></button> ' +
                        '   <button data-id="'+aData[3]+'" data-toggle="modal" data-target="#confirm-delete-modal" class="delete-button btn btn-xs btn-danger"><span class=\"fa fa-trash\"></span></button> ' +
                        '</td>'
                    );
                    return nRow;
                },
            // "fnServerData": function ( sSource, aoData, fnCallback ) {
            //     /* Add some extra data to the sender */
            //     aoData.push( { "name": "directorate", "value": $("#directorate").val() },
            //         { "name": "status", "value": $("#status").val() },
            //         { "name": "level", "value": $("#level").val() },
            //         { "name": "division", "value": $("#division").val() }
            //     );
            //     $.getJSON( sSource, aoData, function (json) {
            //         /* Do whatever additional processing you want on the callback, then tell DataTables */
            //         fnCallback(json)
            //     } );
            // }
        } );
    });
</script>



