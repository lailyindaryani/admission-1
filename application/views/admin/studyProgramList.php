<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
            <li><a href="<?php echo base_url()?>admin/studyProgramList">Study Program</a></li>
            <li class="active">Study Program List</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>StudyProgram List</h2>
    <em>StudyProgram Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
<!--                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
            </div>
            <div class="widget-content">
                <div class="form-group">
                    <div class="btn-group">
                        <button onclick="location.href='<?php echo base_url()?>admin/studyprogramAdd'" class="btn btn-danger"> Add New &nbsp;
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="table-basic">
                    <table id="tableStudyProgram" class="table table-sorting table-hover  table-striped datatable">
                        <thead>
                        <tr>
                            <th >No</th>
                            <th >Study Program Name</th>
                            <th >iGracias Study Program ID</th>
                            <th >Faculty</th>
                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty">Loading data from server</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<!--BEGIN OF MODAL-->
<!--edit-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/studyprogramUpdate" class="form-horizontal modal-form" role="form" method="post" id="ioform">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Study Program</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="studyprogramid" id="studyprogramid" value="">
                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label">Study Program Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="studyprogramname" id="studyprogramname" placeholder="Study Program Name" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label">iGracias Study Program ID</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="igraciasstudyprogramid" id="igraciasstudyprogramid" placeholder="iGracias Study Program ID" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label">Faculty</label>
                        <div class="col-sm-9">
                            <select name="faculty" id="faculty">
                                <option value="">Select Faculty</option>
                                <?php foreach ($faculties as $faculty){?>
                                    <option value="<?=$faculty['FACULTYID']?>"><?=$faculty['FACULTYNAMEENG']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--delete-->
<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish to delete this Study Program?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="delete-confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END OF MODAL-->

<script type="text/javascript" src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).on("click", ".edit-button", function () {
        var studyprogramid = $(this).data('id');

        $.ajax({
            type    : "POST",
            url     : "<?php echo base_url()?>admin/studyProgramAjaxGetDetail",
            data    : {studyprogramid: studyprogramid},
            success : function (data) {
                data = $.parseJSON( data );
                $('.modal-form #studyprogramid').val(data[0].STUDYPROGRAMID);
                $('.modal-form #studyprogramname').val(data[0].STUDYPROGRAMNAME);
                $('.modal-form #igraciasstudyprogramid').val(data[0].IGRACIASSTUDYPROGRAMID);
                $('.modal-form #faculty').select2("val",data[0].FACULTYID);
            }
        });
    });

    $('#ioform').validate({
        rules: {
            studyprogramname: {
                required: true
            },
            faculty: {
                required: true
            }
        },
        submitHandler: function (form) {
            var formData = $("#ioform").serialize();

            $.ajax({
                type    : "POST",
                data    : formData,
                url     : "<?php echo base_url()?>admin/studyProgramUpdate",
                success : function(data) {
                    $('#tableStudyProgram').DataTable().ajax.reload();
                    $("#edit-modal").modal('hide');
                    return false;
                },
                error   : function (e) {
                    return false;
                }
            });
        }
    });

    $(document).on("click", ".delete-button", function () {
        var ID = $(this).data('id');
        $('#delete-confirm-button').data('id', ID)
    });

    $('#delete-confirm-button').click(function(){
        var studyprogramid = $(this).data('id');

        $.ajax({
            method  : 'POST',
            data    : {studyprogramid:studyprogramid},
            url     : "<?php echo base_url()?>admin/studyProgramDrop",
            success : function (data) {
                $('#tableStudyProgram').DataTable().ajax.reload();
                $("#confirm-delete-modal").modal('hide');
                return false;
            }
        });
    });

    $(document).ready(function() {
        $("#faculty").select2({minimumResultsForSearch: -1});
        var dt= $('#tableStudyProgram').dataTable( {
            "order": [[ 2, "ASC" ]],
            "columnDefs": [
                { "width": "10%", "targets": 0},
                { "width": "25%", "targets": 2},
                { "width": "15%", "targets": 4},
            ],
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "aLengthMenu": [
                [20, 30, 50, 100, -1],
                [20, 30, 50, 100, "All"]
            ],
            "fnDrawCallback": function() {

                //initAction();

            },
            "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateStudyProgram",
            "fnRowCallback":
                function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

                    $(nRow).html(
                        '<td>'+aData[0]+'</td>' +
                        '<td>'+aData[1]+'</td>' +
                        '<td>'+aData[2]+'</td>' +
                        '<td>'+aData[3]+'</td>' +
                        '<td>' +
                        '   <button data-toggle="modal" data-id="'+aData[4]+'" title="edit" class="btn btn-xs btn-primary edit-button" href="#edit-modal"><span class="fa fa-edit"></span></button>  ' +
                        '   <button data-id="'+aData[4]+'" data-toggle="modal" data-target="#confirm-delete-modal" class="btn btn-xs btn-danger delete-button"><span class="fa fa-trash"></span></button>' +
                        '</td>'
                    );
                    return nRow;
                }
        } );
    });
</script>



