
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/questionaireList">Questionaire</a></li>
                <li class="active">Add Question</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Add Question</h2>
            <em>Questionaire Data</em>
        </div>

        <div class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
                        </div>
                        <div class="widget-content">
                            <form action="<?php echo base_url()?>admin/questionaireStore" class="form-horizontal" role="form" method="post" id="ioform">
                                <fieldset>
                                    <legend>Questionaire Data</legend>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Question</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="question" id="question" placeholder="Ask your question">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Question Type</label>
                                        <div class="col-sm-9">
                                            <label class="radio radio-inline">
                                                <input type="radio" class="question-type" id="question-multi-choice" name="question-type" value="" checked> Multiple Choice
                                            </label>
                                            <label class="radio radio-inline">
                                                <input type="radio" class="question-type" id="question-text" name="question-type" value="Y"> Text
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="option-multi-choice">
<!--                                    <div class="form-group" id="option-multi-choice">-->
                                        <label for="" class="col-sm-3 control-label">Option</label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="count" value="1">
                                            <div class="input-appendable-wrapper">
                                                <input type="hidden" id="count" value="1">
                                                <div class="input-group input-group-appendable" id="input-group-appendable1">
                                                    <input autocomplete="off" class="input form-control" id="field1" name="field[]" type="text">
                                                    <span class="input-group-btn">
										<button id="btn1" class="btn btn-primary add-more" type="button">+</button>
									</span>
                                                </div>
                                            </div>
                                            <br>
                                            <p>Press + to add another form field</p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-2">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url()?>admin/questionaireSetting'">Cancel</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <!-- END SUPPORT TICKET FORM -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->




<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#questionairetype").select2();

        $("#question-text").on('click',function () {
            $("#option-multi-choice").hide();
        });

        $("#question-multi-choice").on('click',function () {
            $("#option-multi-choice").show();
        });


        //*******************************************
        /*	INPUT APPEND
        /********************************************/

        $( "form.input-append" ).keypress(function(e) {
            if ( e.which == 13 ) {
                e.preventDefault();
            }
        });

        $('.input-group-appendable .add-more').click(function(){
            $wrapper = $(this).parents('.input-appendable-wrapper');
            $lastItem = $wrapper.find('.input-group-appendable').last();

            $newInput = $lastItem.clone(true);

            // change attribute for new item
            $count = $wrapper.find('#count').val();
            $count++;

            // change text input and the button
            $newInput.attr('id', 'input-group-appendable' + $count);
            $newInput.find('input[type="text"]').attr({
                id: "field" + $count,
                name: "field[]",
            });

            $newInput.find('.btn').attr('id', 'btn' + $count);
            $newInput.appendTo($wrapper);

            //change the previous button to remove
            $lastItem.find('.btn')
                .removeClass('add-more btn-primary')
                .addClass('btn-danger')
                .text('-')
                .off()
                .on('click', function(){
                    $(this).parents('.input-group-appendable').remove();
                });

            $wrapper.find('#count').val($count);

        });
    });

    $( "#ioform" ).validate({
        rules: {
            question: {
                required: true
            },
            questionairetype: {
                required: true
            }
        }
    });
</script>