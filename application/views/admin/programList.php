<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/programList">Program</a></li>
                <li class="active">Program List</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Program List</h2>
            <em>Program Data</em>
        </div>

        <div class="main-content">



            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">
                            <!--                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
                        </div>
                        <div class="widget-content">
                            <div class="form-group">
                                <div class="btn-group">
                                    <button onclick="location.href='<?php echo base_url()?>admin/programAdd'" class="btn btn-danger"> Add New &nbsp;
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="table-basic">
                                <table id="tableProgram" class="table table-sorting table-hover  table-striped datatable">
                                    <thead>
                                    <tr>
                                        <th >No</th>
                                        <th >Program Name</th>
                                        <th >Program Type</th>
                                        <th >Status Pengaturan Matakuliah</th>
                                        <th >Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="10" class="dataTables_empty">Loading data from server</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- END SUPPORT TICKET FORM -->
                </div>

            </div>


        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->

<!--MODAL-->
<div class="modal fade" id="edit-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/programUpdate" class="form-horizontal modal-form" role="form" method="post" id="ioform">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Program</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="ticket-subject" class="col-sm-3 control-label">Program Name</label>
                        <div class="col-sm-9">
                            <input type="hidden" name="programid" id="programid" value="">
                            <input type="text" class="form-control" name="programname" id="programname" placeholder="Program Name" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Program Type</label>
                        <div class="col-sm-9 ">
                            <select name="programtype" class="" id="programtype">
                                <option></option>
                                <option value="ACADEMIC">Academic</option>
                                <option value="NON_ACADEMIC">Non Academic</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Status Pengaturan Matakuliah</label>
                                        <div class="col-sm-9 ">
                                            <select name="statussubjectsetting" class="" id="statussubjectsetting">
                                                <option></option>
                                                <option value="Y">Ada</option>
                                                <option value="N">Tidak Ada</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group divmaxcredit">
                                        <label for="" class="col-sm-3 control-label">Maksimal SKS</label>
                                        <div class="col-sm-9 ">
                                            <input type="text" placeholder="Maksimal SKS" oninput="validateNumber(this)" name="maxcredit" class="form-control" id="maxcredit">
                                        </div>
                                    </div>
                                    <div class="form-group divmaxstudy">
                                        <label for="" class="col-sm-3 control-label">Maksimal Program Studi</label>
                                        <div class="col-sm-9 ">
                                            <input type="text" placeholder="Maksimal Program Studi" oninput="validateNumber(this)" name="maxstudy" class="form-control" id="maxstudy">
                                        </div>
                                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-danger">Update</button>
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--END OF MODAL-->
<script type="text/javascript" src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).on("click", ".edit-button", function () {
        var programid = $(this).data('id');

        $.ajax({
            type    : "POST",
            url     : "<?php echo base_url()?>admin/programAjaxGetDetail",
            data    : {programid: programid},
            success : function (data) {
                data = $.parseJSON( data );
                $('.modal-form #programid').val(data.PROGRAMID);
                $('.modal-form #programname').val(data.PROGRAMNAME);
                $('.modal-form #programtype').select2("val",(data.PROGRAMTYPE));
                if(data.STATUSSUBJECTSETTING=='Y'){
                    $(".divmaxcredit").show();
                    $(".divmaxstudy").show();
                    $("#maxcredit").val(data.MAXCREDIT);
                    $("#maxstudy").val(data.MAXSTUDYPROGRAM);
                }else{
                    $(".divmaxstudy").hide();
                    $(".divmaxcredit").hide();
                }
                $('.modal-form #statussubjectsetting').select2("val",(data.STATUSSUBJECTSETTING));
            }
        });
    });

    $('#ioform').validate({
        rules: {
            programname: {
                required: true
            },
            programtype: {
                required: true
            }
        },
        submitHandler: function (form) {
            var programid   = $(".modal-form #programid").val();
            var programname = $(".modal-form #programname").val();
            var programtype = $(".modal-form #programtype").val();
            var statussubjectsetting = $(".modal-form #statussubjectsetting").val();
            var maxcredit = $(".modal-form #maxcredit").val();
            var maxstudy = $(".modal-form #maxstudy").val();
            $.ajax({
                type    : "POST",
                data    : {programid:programid, programname:programname,programtype:programtype, statussubjectsetting:statussubjectsetting, maxcredit:maxcredit, maxstudy:maxstudy},
                url     : "<?php echo base_url()?>admin/programUpdate",
                success : function(data) {
                    $('#tableProgram').DataTable().ajax.reload();
                    $("#edit-modal").modal('hide');
                    return false;
                },
                error   : function (e) {
                    return false;
                }
            });
        }
    });

    $(document).ready(function() {
        $(".divmaxcredit").hide();
        $(".divmaxstudy").hide();
        $("#programtype").select2({
            minimumResultsForSearch: -1,
            placeholder : 'Select Program Type'
        });
        $("#statussubjectsetting").select2({
            minimumResultsForSearch: -1,
            placeholder : 'Pilih Status Pengaturan Matakuliah'
        });

        $("#statussubjectsetting").change(function(){
            if($(this).val()=='Y'){
                $(".divmaxcredit").show();
                $(".divmaxstudy").show();
            }else{
                $(".divmaxcredit").hide();
                $(".divmaxstudy").hide();
            }
        })

        var dt= $('#tableProgram').dataTable( {
            //"bJQueryUI": true,
            "order": [],
            "columnDefs": [
                { "width": "5%", "targets": 0},
                { "width": "65%", "targets": 1},
                { "width": "20%", "targets": 2},
                { "width": "10%", "targets": 4}
            ],
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "aLengthMenu": [
                    [20, 30, 50, 100, -1],
                [20, 30, 50, 100, "All"]
				 // [-1,30, 40, 50, 100],
     //            ["All",30, 40, 50, 100, ]
            ],
            "fnDrawCallback": function() {

                //initAction();

            },
            "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateProgram",
            "fnRowCallback":
                function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $(nRow).html(
                        '<td>'+aData[0]+'</td>' +
                        '<td>'+aData[1]+'</td>' +
                        '<td>'+aData[2]+'</td>' +
                        '<td>'+aData[4]+'</td>' +
                        '<td>' +
                        '   <button class="btn btn-xs btn-default" onclick=\"location.href=\'<?php echo base_url()?>admin/manageProgram/'+aData[3]+'/selectDegree\'"\' title="Manage"><span class=\"fa fa-list\"></span></button>' +
                        '   <button data-toggle="modal" data-id="'+aData[3]+'" title="Edit" class="btn btn-xs btn-primary edit-button" href="#edit-modal"><span class=\"fa fa-edit\"></span></button> ' +
                        '</td>'
                    );
                    return nRow;
                },
        } );
		
		 $("input[type='search']").on('keyup',function(){
            $('#tableCoupon').DataTable().destroy();
            $('#tableCoupon').DataTable().search(this.value,true).draw();
        });
    });
</script>