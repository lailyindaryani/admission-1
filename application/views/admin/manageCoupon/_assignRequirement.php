
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
            <li><a href="<?php echo base_url()?>admin/programList">Program</a></li>
            <li class="active">Manage</li>
        </ul>
    </div>
</div>

<!-- main -->
<div class="content">
    <div class="main-header">
        <h2>Program</h2>
        <em>Manage Program</em>
    </div>

<div class="main-content">
    <div class="widget">
        <div class="widget-header">
            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
        </div>
        <div class="widget-content">
            <div class="wizard-wrapper">
                <div id="manageWizard" class="wizard">
                    <ul class="steps">
                        <li data-target="#step1"><span class="badge badge-info">1</span> Select Degree<span class="chevron"></span></li>
                        <li data-target="#step2" class=""><span class="badge badge-info">2</span>Select Study Program<span class="chevron"></span></li>
                        <li data-target="#step3" class="active"><span class="last"><span class="badge">3</span>Assign Requirements</li>
                    </ul>
                </div>
                <div class="step-content">
                    <div class="step-pane active" id="step2">
                        <?php foreach ($programdegree as $degree) { ?>
                            <div class="well">
                                <div class="row">
                                    <label for="degree" class="col-sm-3 control-label">
                                        <h2><?php echo $degree['DEGREE']?></h2>
                                        <em><button data-toggle="modal" data-id="<?php echo $degree['PROGRAMDEGREEID']?>" title="Add Requirement" href="#add-modal" class="btn btn-primary add-button">
                                                <i class="fa fa-plus"></i> Add New
                                            </button>
                                        </em>
                                    </label>
                                    <div class="col-sm-9">
                                        <table id="table-<?php echo $degree['PROGRAMDEGREEID']?>" class="table table-sorting table-hover table-condensed datatable">
                                            <thead>
                                            <tr>
                                                <th >No</th>
                                                <th >Requirements Name</th>
                                                <th >Type</th>
                                                <th >Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="10" class="dataTables_empty">Loading data from server</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="actions">
                    <button onclick="location.href='<?php echo base_url()?>admin/manageProgram/<?php echo $programid?>/selectStudyProgram'" type="button" id="btPrevWizard" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</button>
<!--                    <button type="button" id="btNextWizard" class="btn btn-danger btn-next" data-last="Finish">Save <i class="fa fa-arrow-right"></i></button>-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<!--BEGIN OF MODAL-->
<div class="modal fade" id="add-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/setRequirement" class="form-horizontal modal-form" role="form" method="post" id="ioform">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Requirement</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="programdegreeid" id="programdegreeid" value="">
                    <div class="form-group">
                        <label for="studyprogramid" class="col-sm-3 control-label">Requirement</label>
                        <div class="col-sm-9 ">
                            <select name="requirementid" id="requirementid">
                                <option></option>
                                <?php foreach ((array)$requirements as $row){ ?>
                                    <option value="<?php echo $row['REQUIREMENTID']?>"><?php echo $row['REQUIREMENTNAME']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Add</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--delete-->
<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish to drop this requirement?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="delete-confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END OF MODAL-->

<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>themes/_assets/js/plugins/wizard/wizard.min.js"></script>
<script src="<?php echo base_url();?>themes/_assets/js/plugins/parsley-validation/parsley.min.js"></script>

<script type="text/javascript">
    $(document).on("click", ".add-button", function () {
        $('#requirementid').select2('val','');
        $('.modal-form #programdegreeid').val($(this).data('id'));
    });

    $("#requirementid").select2({
        minimumResultsForSearch: -1,
        placeholder: "Select a requirement",
        allowClear: true
    });

    $('#ioform').validate({
        rules: {
            programdegreeid: {
                required: true
            },
            studyprogramid: {
                required: true
            },
            language: {
                required: true
            }
        },
        submitHandler: function (form) {
            var programdegreeid = $(".modal-form #programdegreeid").val();
            var requirementid = $(".modal-form #requirementid").val();

            $.ajax({
                type    : "POST",
                data    : {programdegreeid:programdegreeid, requirementid:requirementid},
                url     : "<?php echo base_url()?>admin/setRequirement",
                success : function(data) {
                    $('#table-'+programdegreeid).DataTable().ajax.reload();
                    $("#add-modal").modal('hide');
                    return false;
                },
                error   : function (e) {
                    return false;
                }
            });
        }
    });

    $(document).on("click", ".delete-button", function () {
        var ID = $(this).data('id');
        $('#delete-confirm-button').data('id', ID)
    });

    $('#delete-confirm-button').click(function(){
        var requirementid = $(this).data('id')[0];
        var programdegreeid = $(this).data('id')[1];

        $.ajax({
            method  : 'POST',
            data    : {requirementid:requirementid,programdegreeid:programdegreeid},
            url     : "<?php echo base_url()?>admin/unsetRequirement",
            success : function (data) {
                $('.datatable').DataTable().ajax.reload();
                $("#confirm-delete-modal").modal('hide');
                return false;
            },
            error: function (e) {
                console.log(e);
            }
        });
    });

    <?php foreach ($programdegree as $degree) { ?>
    $("#table-<?php echo $degree['PROGRAMDEGREEID']?>").dataTable({
        "paging": false,
        "order": [2, "ASC"],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateAssignedRequirements/<?php echo $degree['PROGRAMDEGREEID']?>",
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            $(nRow).html(
                '<td>'+aData[0]+'</td>' +
                '<td>'+aData[1]+'</td>' +
                '<td>'+aData[2]+'</td>' +
                '<td>' +
                '   <button data-id="['+aData[3]+','+aData[4]+']" data-toggle="modal" title="Delete" data-target="#confirm-delete-modal" class="delete-button"><span class="fa fa-trash"></span></button>' +
                '</td>'
            );
            return nRow;
        },
    });
    <?php } ?>

</script>