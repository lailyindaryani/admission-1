
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
            <li><a href="<?php echo base_url()?>admin/couponList">Coupon</a></li>
            <li class="active">Manage</li>
        </ul>
    </div>
</div>

<!-- main -->
<div class="content">
    <div class="main-header">
        <h2>Coupon</h2>
        <em>Manage Coupon</em>
    </div>

<div class="main-content">
    <div class="widget">
        <div class="widget-header">
            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
        </div>
        <div class="widget-content">
            <div class="wizard-wrapper">
                <div id="manageWizard" class="wizard">
                    <ul class="steps">
                        <li data-target="#step1"><span class="badge badge-info">1</span> Select Enrollment<span class="chevron"></span></li>
                        <li data-target="#step2" class="active"><span class="last"><span class="badge">2</span>Select Country</li>
                    </ul>
                </div>
                <div class="step-content">
                    <div class="step-pane active" id="step2">
                            <div class="well">
                                <div class="row">
                                    <label for="degree" class="col-sm-3 control-label">
                                        <h2><?php echo $enrollments['DESCRIPTION']?></h2>
                                        <em><button data-toggle="modal" data-id="<?php echo $enrollmentid?>" title="Add Country" href="#add-modal" class="btn btn-primary add-button">
                                                <i class="fa fa-plus"></i> Add New
                                            </button>
                                        </em>
                                    </label>
                                    <div class="col-sm-9">
                                        <table id="table-country" class="table table-sorting table-hover table-condensed datatable">
                                            <thead>
                                            <tr>
                                                <th >No</th>
                                                <th >Country Name ENG</th>
                                                <th >Country Name INA</th>
                                                <th >Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="10" class="dataTables_empty">Loading data from server</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <button onclick="location.href='<?php echo base_url()?>admin/manageCoupon/selectEnrollment/<?php echo $couponid?>'" type="button" id="btPrevWizard" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<!--BEGIN OF MODAL-->
<div class="modal fade" id="add-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url()?>admin/setCountry" class="form-horizontal modal-form" role="form" method="post" id="ioform">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Country</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="couponid" id="couponid" value="<?php echo $couponmapping['couponid']?>">
                    <input type="hidden" name="enrollmentid" id="enrollmentid" value="<?php echo $couponmapping['enrollmentid']?>">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Country</label>
                        <div class="col-sm-9 ">
                            <select name="countryid" id="countryid">
                                <option></option>
                                <?php foreach ((array)$countrys as $row){ ?>
                                    <option value="<?php echo $row['COUNTRYID']?>"><?php echo $row['COUNTRYNAMEENG']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="update" class="btn btn-primary">Add</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--delete-->
<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish to delete this country?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="delete-confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END OF MODAL-->

<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>themes/_assets/js/plugins/wizard/wizard.min.js"></script>
<script src="<?php echo base_url();?>themes/_assets/js/plugins/parsley-validation/parsley.min.js"></script>

<script type="text/javascript">
    var options = $("#language");

    $("#countryid").select2({
        allowClear: true,
        placeholder: 'Select Country'
    });

    $('#ioform').validate({
        rules: {
            countryid: {
                required: true
            },
        },
        submitHandler: function (form) {
            var couponid        = $(".modal-form #couponid").val();
            var countryid       = $(".modal-form #countryid").val();
            var enrollmentid    = $(".modal-form #enrollmentid").val();

            $.ajax({
                type    : "POST",
                data    : {countryid:countryid,enrollmentid:enrollmentid,couponid:couponid},
                url     : "<?php echo base_url()?>admin/setCountry",
                success : function(data) {
                    $('#table-country').DataTable().ajax.reload();
                    $("#add-modal").modal('hide');
                    return false;
                },
                error   : function (e) {
                    return false;
                }
            });
        }
    });

    $(document).on("click", ".delete-button", function () {
        var ID = $(this).data('id');
        $('#delete-confirm-button').data('id', ID)
    });

    $('#delete-confirm-button').click(function(){
        var couponmappingid = $(this).data('id');

        $.ajax({
            method  : 'POST',
            data    : {couponmappingid:couponmappingid},
            url     : "<?php echo base_url()?>admin/unsetCountry",
            success : function (data) {
                console.log(data);
                $('.datatable').DataTable().ajax.reload();
                $("#confirm-delete-modal").modal('hide');
                return false;
            },
            error: function (e) {
                console.log(e);
            }
        });
    });

    $("#table-country").dataTable({
        "paging": false,
        "order": [2, "ASC"],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "<?php echo base_url(); ?>admin/dataTablePopulateAssignedCountry/<?php echo $couponid?>/<?php echo $enrollmentid?>",
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            $(nRow).html(
                '<td>'+aData[0]+'</td>' +
                '<td>'+aData[1]+'</td>' +
                '<td>'+aData[2]+'</td>' +
                '<td>' +
                '   <button data-id='+aData[3]+' data-toggle="modal" title="Delete" data-target="#confirm-delete-modal" class="delete-button"><span class="fa fa-trash"></span></button>' +
                '</td>'
            );
            return nRow;
        },
    });
</script>