<?php
//echo "<pre>"; print_r($data_country);die;
?>
<!--<div id="loading">
  <p><img style="display:block; margin: 0 auto; overflow: hidden;" src="<?php echo base_url('images/loading.gif')?>" class="lds-dual-ring" autofocus/> Please Wait</p>
</div>-->
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
		<div class="col-lg-4 ">
				<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="<?php echo base_url().'admin/dashboardEnrollment'?>">Home</a></li>
				</ul>
		</div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
		<h2>Summary Report</h2>
		<em>All of Summary Reports</em>
</div>

<div class="main-content">
<div class="row">
<div class="col-sm-12">
<div class="widget">
				<div class="widget-header">
				</div>
				<div class="widget-content">
				<div class="row" style="border: 1px solid #ccc; margin:5px;">
                <div class="col-md-12">
                    <div class="widget-content">					
                        <div class="row form-horizontal">
                            <div class="col-md-5">
                                <p>
                                    <span>Academic Year</span>
                                    <select id="academicyear1" name="academicyear" class="filter">
										<option value="">-All-</option>
                                        <?php foreach ($academicyear as $pr){
											
											if($pr['ACADEMICYEAR']==$academicyear_filter[0]['FILTERNAME'] && $academicyear_filter!=0){ ?>
												<option selected value="<?=$pr['ACADEMICYEAR']?>"><?=$pr['ACADEMICYEAR']?></option>
										<?php }else{ ?>
												<option value="<?=$pr['ACADEMICYEAR']?>"><?=$pr['ACADEMICYEAR']?></option>
										<?php }
										?>
                                            
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
                            <div class="col-md-5">
                                <p>
                                    <span>Period</span>
                                    <select id="period" name="period" multiple="multiple" class="period filter js-example-basic-multiple">
										<option value="">-All-</option>
										<?php foreach ($period as $pr){ ?>
                                            <option id="<?=$pr['PERIOD']?>" value="<?=$pr['PERIOD']?>"><?=$pr['PERIOD']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
							<div class="col-md-2">
							<br>
								<a type="button" id="set_default" class="btn btn-primary" >Set Default</a>
							</div>
						</div>
					</div>
					</div>
				</div>
				</div>
				</div>
			<div class="widget">
				<div class="widget-header">
				</div>
				<div class="widget-content">
				<!--<div class="row" style="border: 1px solid #ccc; margin:5px;">
                <div class="col-md-12">
                    <div class="widget-content">					
                        <div class="row form-horizontal">
                            <div class="col-md-4">
                                <p>
                                    <span>Academic Year</span>
                                    <select id="academicyear1" name="academicyear" class="filter">
										<option value="">-All-</option>
                                        <?php foreach ($academicyear as $pr){ ?>
                                            <option value="<?=$pr['ACADEMICYEAR']?>"><?=$pr['ACADEMICYEAR']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
						</div>
					</div>
					</div>
				</div>-->

				<div class="row form-horizontal">
                    <div class="col-md-12">
			<div class="col-md-6">
			<div class="widget">
						<div class="widget-header">
								<h3><i class="fa fa-user"></i> Number of Acounts (Registration Step)</h3>
						</div>
						 <div class="widget-content">
						 <p id="number_of_acounts" align="center"><a id="number_acounts" href="<?php echo base_url()?>admin/modalNumberParticipants/acount/Number_of_Acounts/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $number_acounts?></b></font></a></p>
						 </div>
						 </div>
			</div>
                        <div class="col-md-6">
                            <div class="widget">
                                <div class="widget-header">
                                    <h3><i class="fa fa-user"></i> Number of Participants</h3>
                                </div>
                                <div class="widget-content">
                                    <p id="number_of_participants" align="center"><a id="number_participants" href="<?php echo base_url()?>admin/modalNumberParticipants/all/Number_of_Participants/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $number_participants?></b></font></a></p>
                                </div>
                            </div>
                        </div>
                        </div>
                    <div class="col-md-12">
			<div class="col-md-6">
			<div class="widget">
						<div class="widget-header">
								<h3><i class="fa fa-user"></i> Number of Who Register for a Scholarship</h3>
						</div>
						 <div class="widget-content">
						 <p id="number_of_scholarship" align="center"><a id="number_scholarship" href="<?php echo base_url()?>admin/modalNumberParticipants/scholarship/Number_of_Who_Register_for_a_Scholarship/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color: #17202a;" size="100000%"><b><?php echo $number_scholarship?></b></font></a></p>
						 </div>
						 </div>
			</div>
			<div class="col-md-6">
			<div class="widget">
						<div class="widget-header">
								<h3><i class="fa fa-user"></i> Number of Who Register for not a Scholarship</h3>
						</div>
						 <div class="widget-content">
						 <p id="number_of_notscholarship" align="center"><a id="number_notsholarship" href="<?php echo base_url()?>admin/modalNumberParticipants/notscholarship/Number_of_Who_Register_for_not_a_Scholarship/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color: #17202a;" size="100000%"><b><?php echo $number_not_scholarship?></b></font></a></p>
						 </div>
						 </div>
			</div>
			</div>
                    </div>
						 </div>
			</div>
			</div>
		<div class="col-md-12">
				<!-- SUPPOR TICKET FORM -->
				
				<div class="widget">
						<div class="widget-header">
								<h3><i class="fa fa-flag"></i> Number of Foreign Students Based on Education Program</h3>
						</div>

						<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
						
						<div class="widget-content">
						<!--<div class="row" style="border: 1px solid #ccc; margin:5px;">
                <div class="col-md-12">
                    <div class="widget-content">					
                        <div class="row form-horizontal">
                            <div class="col-md-4">
                                <p>
                                    <span>Academic Year</span>
                                    <select id="academicyear2" name="academicyear" class="filter">
										<option value="">-All-</option>
                                        <?php foreach ($academicyear as $pr){ ?>
                                            <option value="<?=$pr['ACADEMICYEAR']?>"><?=$pr['ACADEMICYEAR']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
						</div>
					</div>
					</div>
					</div>-->
						<div>
						<a type="button" id="hide" class="btn btn-success" >Hide Chart</a>
						<a type="button" id="show" class="btn btn-primary" >Show Chart</a><p></p>
						</div>
												<div class="table-basic">
														<table id="tableProgram" class="table table-sorting table-hover  table-striped datatable">
																<thead>
																<tr>
																		<th >No</th>
																		<th >Program Name</th>
																		<th >Number of Students</th>																		
																		<th >Action</th>
																</tr>
																</thead>
																<tbody>
																<tr>
																		<td colspan="10" class="dataTables_empty">Loading data from server</td>
																</tr>
																</tbody>
														</table>
												</div><!-- style="display:none" -->
					<?php echo '<table id="datatable2" style="display:none" class="table table-sorting table-hover  table-striped datatable">';
                        echo '<thead>';
                        echo '<tr>';?>
							<?php echo '<th></th>';?>
							<?php for( $i=0; $i<sizeof($data_enrollment) ; $i++ ){
                           echo "<th >".$data_enrollment[$i]['PROGRAMNAME']."</th>";
							}?>
                        <?php echo '</tr>';
                        echo '</thead>';
                        echo '<tbody >';
                        echo '<tr>';?>
							<?php echo '<td font size="5">Participants</td>';?>
							<?php for( $i=0; $i<sizeof($data_enrollment) ; $i++ ){
							echo "<td>".$data_enrollment[$i]['JUMLAH']."</td>";
							}?>
                        <?php echo '</tr>';
                        echo '</tbody>';
                    echo '</table>';?>
					
						</div>
				</div>
			</div>
			
			<div class="col-sm-12">
			<div class="widget">
				<div class="widget-header">
					<h3><i class="fa fa-flag"></i> Number of Who Register Based on Nationality</h3>
				</div>
				<div class="widget-content">
				
				<div class="row form-horizontal">
				<div class="col-md-3">
			<div class="widget">
						<div class="widget-header">
								<h3><i class="fa fa-flag"></i> Number of Nationalities</h3>
						</div>
						 <div class="widget-content">
						 <p align="center" id="number_of_country"><font id="number_country" style="color: #17202a;" size="100000%"><b><?php echo $number_country?></b></font></p>
						 
						 </div>
						 </div>
						 </div>
			<div class="col-md-9">
				<!-- SUPPOR TICKET FORM -->
				<div class="widget">
						<div class="widget-header">
								<h3><i class="fa fa-flag"></i> Number of Who Register Based on Nationality</h3>
						</div>
					
						<div id="containerCountry" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
						<div class="row" style="border: 1px solid #ccc; margin:5px;">
                <div class="col-md-12">
                    <div class="widget-content">
					
                        <div class="row form-horizontal">
                            <div class="col-md-6">
                                <p>
                                    <span>Acceptance Status</span>
                                    <select id="acceptance" name="acceptance" class="filter">
                                        <option value="">-All-</option>
                                        <option value="ACCEPTED">Accepted</option>
                                        <option value="UNACCEPTED">Not Accepted</option>
                                    </select>
                                </p>
                            </div>
							<!--<div class="col-md-4">
                                <p>
                                    <span>Academic Year</span>
                                    <select id="academicyear3" name="academicyear" class="filter">
										<option value="">-All-</option>
                                        <?php foreach ($academicyear as $pr){ ?>
                                            <option value="<?=$pr['ACADEMICYEAR']?>"><?=$pr['ACADEMICYEAR']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>-->
						</div>
					</div>
					</div>
					</div>
					
						<div class="widget-content">
						<div>
						<a type="button" id="hideCountry" class="btn btn-success" >Hide Chart</a>
						<a type="button" id="showCountry" class="btn btn-primary" >Show Chart</a><p></p>
						</div>
												<div class="table-basic">
														<table id="tableCountry" class="table table-sorting table-hover  table-striped datatable">
																<thead>
																<tr>
																		<th >No</th>
																		<th >Country</th>
																		<th >Number of Participants</th>
																</tr>
																</thead>
																<tbody>
																<tr>
																		<td colspan="10" class="dataTables_empty">Loading data from server</td>
																</tr>
																</tbody>
														</table>
												</div><!-- style="display:none" -->
					<?php echo '<table id="datatableCountry" style="display:none" class="table table-sorting table-hover  table-striped datatable">';
                        echo '<thead>';
                        echo '<tr>';?>
							<?php echo '<th></th>';?>
							<?php for( $i=0; $i<sizeof($data_country) ; $i++ ){
                           echo "<th >".$data_country[$i]['NATIONALITY']."</th>";
							}?>
                        <?php echo '</tr>';
                        echo '</thead>';
                        echo '<tbody >';
                        echo '<tr>';?>
							<?php if($data_country!=null){
								echo '<td font size="5">Participants</td>';
							}else{
								echo '<td colspan="10" class="dataTables_empty">No data</td>';
							}?>
							<?php for( $i=0; $i<sizeof($data_country) ; $i++ ){
							echo "<td>".$data_country[$i]['JUMLAH']."</td>";
							}?>
                        <?php echo '</tr>';
                        echo '</tbody>';
                    echo '</table>';?>
						</div>
				</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			<div class="col-sm-12">
			<div class="widget">
				<div class="widget-header">
					<h3><i class="fa fa-globe"></i> Number of Foreign Students Based on Study Program</h3>
				</div>
				<div class="widget-content">
				<div class="row form-horizontal">
				<div class="col-md-3">
			
			<div class="widget">
						<div class="widget-header">
								<h3><i class="fa fa-user"></i> Number of Students</h3>
						</div>
						 <div class="widget-content">
						 <p align="center" id="number_students"><font id="number_of_students" style="color: #17202a;" size="100000%"><b><?php echo $number_students?></b></font></p>
						 </div>
						 </div>
			</div>
			<div class="col-md-9">
				<!-- SUPPOR TICKET FORM -->
				<div class="widget">
						<div class="widget-header">
								<h3><i class="fa fa-globe"></i>  Number of Foreign Students Based on Study Program </h3>
						</div>

						<div id="containerStudyProgram" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
						<div class="widget-content">
						<!--<div class="row" style="border: 1px solid #ccc; margin:5px;">
                <div class="col-md-12">
                    <div class="widget-content">
					
                        <div class="row form-horizontal">
							<div class="col-md-4">
                                <p>
                                    <span>Academic Year</span>
                                    <select id="academicyear4" name="academicyear" class="filter">
										<option value="">-All-</option>
                                        <?php foreach ($academicyear as $pr){ ?>
                                            <option value="<?=$pr['ACADEMICYEAR']?>"><?=$pr['ACADEMICYEAR']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
						</div>
					</div>
					</div>
					</div>-->
						<div>
						<a type="button" id="hideStudyProgram" class="btn btn-success" >Hide Chart</a>
						<a type="button" id="showStudyProgram" class="btn btn-primary" >Show Chart</a><p></p>
						</div>
												<div class="table-basic">
														<table id="tableStudyProgram" class="table table-sorting table-hover  table-striped datatable">
																<thead>
																<tr>
																		<th >No</th>
																		<th >Study Program</th>
																		<th >Number of Students</th>
																</tr>
																</thead>
																<tbody>
																<tr>
																		<td colspan="10" class="dataTables_empty">Loading data from server</td>
																</tr>
																</tbody>
														</table>
												</div><!-- style="display:none" -->
					<?php echo '<table id="datatableStudyProgram" style="display:none" class="table table-sorting table-hover  table-striped datatable">';
                        echo '<thead>';
                        echo '<tr>';?>
							<?php echo '<th></th>';?>
							<?php for( $i=0; $i<sizeof($data_studyprogram) ; $i++ ){
                           echo "<th >".$data_studyprogram[$i]['STUDYPROGRAMNAME']."</th>";
							}?>
                        <?php echo '</tr>';
                        echo '</thead>';
                        echo '<tbody >';
                        echo '<tr>';?>
							<?php echo '<td font size="5">Participants</td>';?>
							<?php for( $i=0; $i<sizeof($data_studyprogram) ; $i++ ){
							echo "<td>".$data_studyprogram[$i]['JUMLAH']."</td>";
							}?>
                        <?php echo '</tr>';
                        echo '</tbody>';
                    echo '</table>';?>
						</div>
				</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			<div class="col-sm-12">
			<div class="widget">
				<div class="widget-header">
					<h3><i class="fa fa-globe"></i> Number of Foreign Students Based on Current Step and Acceptance Status</h3>
				</div>
				<div class="widget-content">
				<!--<div class="row" style="border: 1px solid #ccc; margin:5px;">
                <div class="col-md-12">
                    <div class="widget-content">					
                        <div class="row form-horizontal">
							<div class="col-md-4">
                                <p>
                                    <span>Academic Year</span>
                                    <select id="academicyear5" name="academicyear" class="filter">
										<option value="">-All-</option>
                                        <?php foreach ($academicyear as $pr){ ?>
                                            <option value="<?=$pr['ACADEMICYEAR']?>"><?=$pr['ACADEMICYEAR']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
						</div>
					</div>
					</div>
					</div>-->
				<div class="row form-horizontal">
				<div class="col-sm-12">
					<div class="widget">
						<div class="widget-header">
							<h3><i class="fa fa-user"></i>Personal Data</h3>
						</div>
						<div class="widget-content">
						<div class="row form-horizontal">
					<div class="col-md-4">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-user"></i> Personal Data Step</h3>
							</div>
							<div class="widget-content">
								<p align="center" id="step_of_2"><a id="number_step2" href="<?php echo base_url()?>admin/modalNumberParticipants/2/Personal_Data_Step/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $step2?></b></font></a></p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-users"></i> Family Data Step</h3>
							</div>
							<div class="widget-content">
								<p align="center" id="step_of_3"><a id="number_step3" href="<?php echo base_url()?>admin/modalNumberParticipants/3/Family_Data_Step/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $step3?></b></font></a></p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-graduation-cap"></i>Educational Data Step</h3>
							</div>
							<div class="widget-content">
								<p align="center" id="step_of_4"><a id="number_step4" href="<?php echo base_url()?>admin/modalNumberParticipants/4/Educational_Data_Step/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $step4?></b></font></a></p>
							</div>
						</div>
					</div>
					</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="widget">
						<div class="widget-header">
							<h3><i class="fa fa-tasks"></i>Interview</h3>
						</div>
						<div class="widget-content">
						<div class="row form-horizontal">
						<div class="col-md-12">
					<div class="col-md-6">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-hand-o-up"></i> Program SelectionStep</h3>
							</div>
							<div class="widget-content">
								 <p align="center" id="step_of_1"><a id="number_step1" href="<?php echo base_url()?>admin/modalNumberParticipants/1/Program_Selection_Step/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $step1?></b></font></a></p>
							</div>
						</div>
					</div>					
					<div class="col-md-6">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-file-text"></i> Requirement Docs Step</h3>
							</div>
							<div class="widget-content">
								<p align="center" id="step_of_5"><a id="number_step5" href="<?php echo base_url()?>admin/modalNumberParticipants/5/Requirement_Docs_Step/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $step5?></b></font></a></p>
							</div>
						</div>
					</div>

					</div>
					<div class="col-md-12">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-tasks"></i> Interview Step</h3>
							</div>
							<div class="widget-content">
							<div class="row form-horizontal">
								<div class="col-md-4">
								<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-file-text"></i>Unscheduled and Participant Card Step</h3>
							</div>
							<div class="widget-content">
								<p align="center"  id="unscheduled_interview"><a id="number_unscheduled_interview" href="<?php echo base_url()?>admin/modalNumberParticipants/unscheduled_interview/Unscheduled_Interview/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $number_unscheduled_interview?></b></font></a></p>
								</div>
						</div>
								</div>
								<div class="col-md-4">
								<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-file-text"></i> Not Interviewed Yet</h3>
							</div>
							<div class="widget-content">
								<p align="center"  id="not_interview"><a id="number_not_interview" href="<?php echo base_url()?>admin/modalNumberParticipants/not_interview/Not_Interview_Yet/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $number_not_interview?></b></font></a></p>
								</div>
								</div>
								</div>
								<div class="col-md-4">
								<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-file-text"></i> Have Been Interviewed</h3>
							</div>
							<div class="widget-content">
								<p align="center"  id="interview"><a id="number_interview" href="<?php echo base_url()?>admin/modalNumberParticipants/have_interview/Have_Been_Interviewed/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $number_interview?></b></font></a></p>
								</div>
								</div>
								</div>
							</div>
								 
							</div>
						</div>
					</div>
					</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="widget">
						<div class="widget-header">
							<h3><i class="fa fa-check-square"></i>Acceptance Status</h3>
						</div>
						<div class="widget-content">
						<div class="row form-horizontal">
					<div class="col-md-4">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-check-square"></i> Acceptance Status Step</h3>
							</div>
							<div class="widget-content">
								<p align="center" id="step_of_7"><a id="number_step7" href="<?php echo base_url()?>admin/modalNumberParticipants/7/Acceptance_Status_Step/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $step7?></b></font></a></p>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-tasks"></i> Accepted</h3>
							</div>
							<div class="widget-content">
								 <p align="center" id="accept"><a id="number_accept" href="<?php echo base_url()?>admin/modalNumberParticipants/ACCEPTED/Accepted/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $number_accept?></b></font></a></p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-tasks"></i> Unaccepted</h3>
							</div>
							<div class="widget-content">
								 <p align="center" id="unaccept"><a id="number_unaccept" href="<?php echo base_url()?>admin/modalNumberParticipants/UNACCEPTED/Unaccepted/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $number_unaccept?></b></font></a></p>
							</div>
						</div>
					</div>
					<!--
					<div class="col-md-6">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-tasks"></i> Absent</h3>
							</div>
							<div class="widget-content">
								 <p align="center" id="absent"><a id="number_absent" href="<?php echo base_url()?>admin/modalNumberParticipants/absent/Absent/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $number_absent[0]['JUMLAH']?></b></font></a></p>
							</div>
						</div>
					</div>
					-->
					<div class="col-md-12">
						<div class="widget">
							<div class="widget-header">
								<h3><i class="fa fa-tasks"></i>  Confirmation of Attendance</h3>
							</div>
							<div class="widget-content">
								 <p align="center" id="confirm"><a id="number_confirm" href="<?php echo base_url()?>admin/modalNumberParticipants/attend/Confirmation_of_Attendance/x/x" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b><?php echo $number_confirm[0]['JUMLAH']?></b></font></a></p>
							</div>
						</div>
					</div>
					
					</div>
						</div>
					</div>
				</div>
				</div>
				</div>
				</div>
			</div>
</div>
</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->


<div id="edit-modal" class="modal fade" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <!-- Content will be loaded here from "remote.php" file -->
        </div>
    </div>
</div> 



<script type="text/javascript">

		$(document).ready(function() {
			//$('#loading').hide();
			$("#academicyear1").select2();
			$("#period").select2();
			
			$.ajax({
				type:"post",
				//dataType: 'json',
				url: "<?php echo base_url(); ?>ajax/getFilter",
				data:{status: 'PERIOD'},
				success:function(response){
					//$('#loading').show();
					data = JSON.parse(response);
					arr=[];
					for(var x=0; x<data.length; x++){
						arr[x] = data[x].FILTERNAME;
					}
					$(".period").select2("val", arr);
					var academicyear = $("#academicyear1").val();
					var period = $("#period").val();
					//alert(period);die;
					
					
					//datatab(academicyear, period);
					$('#tableStudyProgram').DataTable().ajax.reload();
					$('#tableCountry').DataTable().ajax.reload();
					$('#tableProgram').DataTable().ajax.reload();
					dashboard(period,academicyear);
					//$('#loading').hide();
				}
			});
			
			$("#set_default").click(function(){
				var academicyear = $("#academicyear1").val();
				var period = $("#period").val();
				//alert(period);
				$.ajax({
					type:"post",
					dataType: 'json',
					url: "<?php echo base_url(); ?>ajax/setDefaultFilter",
					data:{ academicyear: academicyear, period: period},
					success:function(response){
						alert(response);
						location.reload();
					}
				});
			});
			
			$("#academicyear1, #period").on('change',function(){
				//$('#loading').show();
				var academicyear = $("#academicyear1").val();
				var period = $("#period").val();
				
				//var periods = cek_period(period);
				//alert(periods);die;
				dashboard(period,academicyear);
			});
			
			
			$('#container').hide();
			$('#containerCountry').hide();
			$('#containerStudyProgram').hide();
			$(".filter").on("change", function (e) {
					console.log($("#academicyear2").val());
            $('#tableProgram').DataTable().ajax.reload();
        });
				var dt= $('#tableProgram').dataTable( {
						//"bJQueryUI": true,
						"order": [[ 1, "ASC" ]],
						/*  sDom: "T<'clearfix'>" +
						 "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
						 "t"+
						 "<'row'<'col-sm-6'i><'col-sm-6'p>>",
						 "tableTools": {

						 },*/
						 "columnDefs": [
                { "width": "8%", "targets": 0},
             //   { "width": "12%", "targets": 3},
            ],
						"sPaginationType": "full_numbers",
						"bProcessing": true,
						"bServerSide": true,
						"aLengthMenu": [
								[20, 30, 50, 100, -1],
								[20, 30, 50, 100, "All"]
						],
						"fnDrawCallback": function() {

								//initAction();

						},
						"sAjaxSource": "<?php echo base_url(); ?>admin/datatableEnrollment",
						"fnRowCallback":
								function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
										if($("#academicyear2").val()==''){
											aData[4]='x';
										}else{
											aData[4]=$("#academicyear2").val();
										}
									    var manageLink = "<?php echo base_url()?>admin/dashboardStudyProgramByEnrollment/"+aData[3]+"/"+aData[1]+"/"+aData[4];
										$(nRow).html(
												'<td>'+aData[0]+'</td>' +
												'<td>'+aData[1]+'</td>' +
												'<td>'+aData[2]+'</td>' +
												'<td>'+'<button onclick="location.href=\'' + manageLink + '\'" title="Manage"><span class=\"fa fa-list\"></span></button>'+'</td>'		
										);
										return nRow;
								},
								
						"fnServerData": function ( sSource, aoData, fnCallback ) {
								/* Add some extra data to the sender */
								aoData.push(
									{ "name": "academicyear", "value": $("#academicyear1").val() },
									{ "name": "period", "value": $("#period").val() }
								);
								$.getJSON( sSource, aoData, function (json) {
										/* Do whatever additional processing you want on the callback, then tell DataTables */
										fnCallback(json)
								} );
						}
				} );
				
	$('#container').highcharts({
        data: {
            table: document.getElementById('datatable2'),
            switchRowsAndColumns: true,
            endRow: 1
        },
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Summary Report of Students by Enrollment'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Number of Participants'
            }
        },
        tooltip: {
            formatter: function() {
                return '<b>'+ this.series.name +'</b><br/>'+
                    this.point.y +' '+ this.point.name.toLowerCase();
            }
        }
    });
	
	
	$('#hide').on('click', function(){
		$('#container').hide();
	});
	$('#show').on('click', function(){
		$('#container').show();
	});
	
	//country
	$("#acceptance").select2();

				$(".filter").on("change", function (e) {
					console.log($("#acceptance").val());
            $('#tableCountry').DataTable().ajax.reload();
        });
				var dt= $('#tableCountry').dataTable( {
						//"bJQueryUI": true,
						"order": [[ 2, "ASC" ]],
						/*  sDom: "T<'clearfix'>" +
						 "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
						 "t"+
						 "<'row'<'col-sm-6'i><'col-sm-6'p>>",
						 "tableTools": {

						 },*/
						 "columnDefs": [
                { "width": "8%", "targets": 0},
             //   { "width": "12%", "targets": 3},
            ],
						"sPaginationType": "full_numbers",
						"bProcessing": true,
						"bServerSide": true,
						"aLengthMenu": [
								[20, 30, 50, 100, -1],
								[20, 30, 50, 100, "All"]
						],
						"fnDrawCallback": function() {

								//initAction();

						},
						"sAjaxSource": "<?php echo base_url(); ?>admin/datatableCountryParticipant",
						"fnRowCallback":
								function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                       var participantLink = "<button class='btn btn-xs btn-primary'><i class='fa fa-list'></i></button>";
										$(nRow).html(
												'<td>'+aData[0]+'</td>' +
														'<td>'+aData[1]+'</td>' +
														'<td>'+aData[2]+'</td>' 

										);
										return nRow;
								},
								
						"fnServerData": function ( sSource, aoData, fnCallback ) {
								/* Add some extra data to the sender */
								aoData.push(
									{ "name": "acceptance", "value": $("#acceptance").val() },
									{ "name": "academicyear", "value": $("#academicyear1").val() },
									{ "name": "period", "value": $("#period").val() }
								);
								$.getJSON( sSource, aoData, function (json) {
										/* Do whatever additional processing you want on the callback, then tell DataTables */
										fnCallback(json)
								} );
						}
				} );
				
		Highcharts.chart('containerCountry', {
    data: {
        table: 'datatableCountry'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Summary Report of Participants by Country'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Number of Participants'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});
	$('#acceptance').on('change', function(){		
		var acceptance= $('#acceptance').val();
		var academicyear= $('#academicyear1').val();
		var period= $('#period').val();
		$.ajax(
            {
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getParticipantCountry",
                data:{ acceptanceStatus: acceptance, academicyear: academicyear, period: period},
                success:function(response)
                {
					$('#datatableCountry thead').empty();
					$('#datatableCountry tbody').empty();
					var tblHead = "<tr>"+"<th>"+"</th>";
					var tblRow = "<tr>"+ "<td font size=5>Participants</td>";
					var i;
					for (i = 0; i < response.length; i++) {
						tblHead += "<th>" + response[i]['NATIONALITY'] + "</th>";
						tblRow += "<td>" + response[i]['JUMLAH'] + "</td>";
					} 
					tblHead +=  "</tr>";
					tblRow +=  "</tr>";
					
					$(tblHead).appendTo("#datatableCountry thead");
					$(tblRow).appendTo("#datatableCountry tbody");
					
					Highcharts.chart('containerCountry', {
						data: {
							table: 'datatableCountry'
						},
						chart: {
							type: 'column'
						},
						title: {
							text: 'Summary Report of '+acceptance+' Participants by Country '+academicyear
						},
						yAxis: {
							allowDecimals: false,
						title: {
							text: 'Number of Participants'
						}
					},
					tooltip: {
						formatter: function () {
							return '<b>' + this.series.name + '</b><br/>' +
							this.point.y + ' ' + this.point.name.toLowerCase();
						}
					}
					});
					
					$.ajax({
						type:"post",
						dataType: 'json',
						url: "<?php echo base_url(); ?>ajax/getCountryByAcademicYear",
						data:{ academicyear: academicyear, acceptanceStatus: acceptance, period: period},
						success:function(responseCountry){
							$('number_country').remove();
							document.getElementById('number_of_country').innerHTML = '<font id="number_country" style="color: #17202a;" size="100000%"><b>'+responseCountry+'</b></font>';
						},
					});
                },
                error: function() 
                {
                    alert("Invalide!");
                }
            }
        );	
	});
	
	$('#hideCountry').on('click', function(){
		$('#containerCountry').hide();
	});
	$('#showCountry').on('click', function(){
		$('#containerCountry').show();
	});
	
	//study program
	$(".filter").on("change", function (e) {
					console.log($("#academicyear4").val());
            $('#tableStudyProgram').DataTable().ajax.reload();
        });
	var dt= $('#tableStudyProgram').dataTable( {
						//"bJQueryUI": true,
						"order": [[ 2, "DESC" ]],
						/*  sDom: "T<'clearfix'>" +
						 "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
						 "t"+
						 "<'row'<'col-sm-6'i><'col-sm-6'p>>",
						 "tableTools": {

						 },*/
						 "columnDefs": [
                { "width": "8%", "targets": 0},
             //   { "width": "12%", "targets": 3},
            ],
						"sPaginationType": "full_numbers",
						"bProcessing": true,
						"bServerSide": true,
						"aLengthMenu": [
								[20, 30, 50, 100, -1],
								[20, 30, 50, 100, "All"]
						],
						"fnDrawCallback": function() {

								//initAction();

						},
						"sAjaxSource": "<?php echo base_url(); ?>admin/datatableStudyProgram",
						"fnRowCallback":
								function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                       var participantLink = "<button class='btn btn-xs btn-primary'><i class='fa fa-list'></i></button>";
										$(nRow).html(
												'<td>'+aData[0]+'</td>' +
														'<td>'+aData[1]+'</td>' +
														'<td>'+aData[2]+'</td>' 

										);
										return nRow;
								},
								
								
						"fnServerData": function ( sSource, aoData, fnCallback ) {
								/* Add some extra data to the sender */
								aoData.push(
									{ "name": "academicyear", "value": $("#academicyear1").val() },
									{ "name": "period", "value": $("#period").val() }
								);
								$.getJSON( sSource, aoData, function (json) {
										/* Do whatever additional processing you want on the callback, then tell DataTables */
										fnCallback(json)
								} );
						}
				} );
	Highcharts.chart('containerStudyProgram', {
    data: {
        table: 'datatableStudyProgram'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Summary Report of Students by Study Program'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Number of Participants'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});
	$('#academicyear4').on('change', function(){	
		var academicyear= $('#academicyear4').val();
		var period= $('#period').val();
		$.ajax(
            {
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getParticipantStudyProgramByAcademicYear",
                data:{academicyear: academicyear, period: period},
                success:function(response)
                {
					$('#datatableStudyProgram thead').empty();
					$('#datatableStudyProgram tbody').empty();
					var tblHead = "<tr>"+"<th>"+"</th>";
					var tblRow = "<tr>"+ "<td font size=5>Participants</td>";
					var i;
					for (i = 0; i < response.length; i++) {
						tblHead += "<th>" + response[i]['STUDYPROGRAMNAME'] + "</th>";
						tblRow += "<td>" + response[i]['JUMLAH'] + "</td>";
					} 
					tblHead +=  "</tr>";
					tblRow +=  "</tr>";
					
					$(tblHead).appendTo("#datatableStudyProgram thead");
					$(tblRow).appendTo("#datatableStudyProgram tbody");
					
					Highcharts.chart('containerStudyProgram', {
						data: {
							table: 'datatableStudyProgram'
						},
						chart: {
							type: 'column'
						},
						title: {
							text: 'Summary Report of Students by Study Program '+academicyear
						},
						yAxis: {
							allowDecimals: false,
						title: {
							text: 'Number of Participants'
						}
					},
					tooltip: {
						formatter: function () {
							return '<b>' + this.series.name + '</b><br/>' +
							this.point.y + ' ' + this.point.name.toLowerCase();
						}
					}
					});
					
					$.ajax({
						type:"post",
						dataType: 'json',
						url: "<?php echo base_url(); ?>ajax/getProdByAcademicYear",
						data:{ academicyear: academicyear, period: period},
						success:function(responseProd){
							$('number_of_students').remove();
							document.getElementById('number_students').innerHTML = '<font id="number_of_students" style="color: #17202a;" size="100000%"><b>'+responseProd+'</b></font>';
						},
					});
                },
                error: function() 
                {
                    alert("Invalide!");
                }
            }
        );	
	});
$('#hideStudyProgram').on('click', function(){
		$('#containerStudyProgram').hide();
	});
	$('#showStudyProgram').on('click', function(){
		$('#containerStudyProgram').show();
	});
	
	 $(".modal").on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
        });
		
		});
function datatab(academicyear, period){
	//alert(academicyear);die;
	var acceptance= $('#acceptance').val();
	$('#tableStudyProgram').DataTable().ajax.reload();
	$('#tableCountry').DataTable().ajax.reload();
	$('#tableProgram').DataTable().ajax.reload();
																										$.ajax({
																											type:"post",
																											dataType: 'json',
																											url: "<?php echo base_url(); ?>ajax/getParticipantStudyProgramByAcademicYear",
																											data:{academicyear: academicyear, period: period},
																											success:function(response){
																												$.ajax({
																													type:"post",
																													dataType: 'json',
																													url: "<?php echo base_url(); ?>ajax/getProdByAcademicYear",
																													data:{ academicyear: academicyear, period: period},
																													success:function(responseProd){
																														$('number_of_students').remove();
																														document.getElementById('number_students').innerHTML = '<font id="number_of_students" style="color: #17202a;" size="100000%"><b>'+responseProd+'</b></font>';
																														$('#datatableStudyProgram thead').empty();
																												$('#datatableStudyProgram tbody').empty();
																												var tblHead = "<tr>"+"<th>"+"</th>";
																												var tblRow = "<tr>"+ "<td font size=5>Participants</td>";
																												var i;
																												for (i = 0; i < response.length; i++) {
																													tblHead += "<th>" + response[i]['STUDYPROGRAMNAME'] + "</th>";
																													tblRow += "<td>" + response[i]['JUMLAH'] + "</td>";
																												} 
																												tblHead +=  "</tr>";
																												tblRow +=  "</tr>";
					
																												$(tblHead).appendTo("#datatableStudyProgram thead");
																												$(tblRow).appendTo("#datatableStudyProgram tbody");
																												
																												Highcharts.chart('containerStudyProgram', {
																													data: {
																														table: 'datatableStudyProgram'
																													},
																													chart: {
																														type: 'column'
																													},
																													title: {
																														text: 'Summary Report of Students by Study Program '+academicyear22
																													},
																													yAxis: {
																														allowDecimals: false,
																													title: {
																														text: 'Number of Participants'
																													}
																												},
																												tooltip: {
																													formatter: function () {
																														return '<b>' + this.series.name + '</b><br/>' +
																														this.point.y + ' ' + this.point.name.toLowerCase();
																													}
																												}
																												});
																													},
																												});
																												//alert();
																								
					
																												
																											},
																											error: function() 
																											{
																												alert("Invalide!");
																											}
																										});
																										
																										$.ajax({
																												type:"post",
																												dataType: 'json',
																												url: "<?php echo base_url(); ?>ajax/getParticipantCountry",
																												data:{ acceptanceStatus: acceptance, academicyear: academicyear, period: period},
																												success:function(response)
																												{
																													$('#datatableCountry thead').empty();
																													$('#datatableCountry tbody').empty();
																													var tblHead = "<tr>"+"<th>"+"</th>";
																													var tblRow = "<tr>"+ "<td font size=5>Participants</td>";
																													var i;
																													for (i = 0; i < response.length; i++) {
																														tblHead += "<th>" + response[i]['NATIONALITY'] + "</th>";
																														tblRow += "<td>" + response[i]['JUMLAH'] + "</td>";
																													} 
																													tblHead +=  "</tr>";
																													tblRow +=  "</tr>";
																													
																													$(tblHead).appendTo("#datatableCountry thead");
																													$(tblRow).appendTo("#datatableCountry tbody");
																													
																													Highcharts.chart('containerCountry', {
																														data: {
																															table: 'datatableCountry'
																														},
																														chart: {
																															type: 'column'
																														},
																														title: {
																															text: 'Summary Report of '+acceptance+' Participants by Country '+academicyear22
																														},
																														yAxis: {
																															allowDecimals: false,
																														title: {
																															text: 'Number of Participants'
																														}
																													},
																													tooltip: {
																														formatter: function () {
																															return '<b>' + this.series.name + '</b><br/>' +
																															this.point.y + ' ' + this.point.name.toLowerCase();
																														}
																													}
																													});
																													
																													
																												},
																												error: function() 
																												{
																													alert("Invalide!");
																												}
																											});
																											
																											$.ajax({
																														type:"post",
																														dataType: 'json',
																														url: "<?php echo base_url(); ?>ajax/getCountryByAcademicYear",
																														data:{ academicyear: academicyear, acceptanceStatus: acceptance, period: period},
																														success:function(responseCountry){
																															$('number_country').remove();
																															document.getElementById('number_of_country').innerHTML = '<font id="number_country" style="color: #17202a;" size="100000%"><b>'+responseCountry+'</b></font>';
																														},
																													});
		
																											$.ajax({
																													type:"post",
																													dataType: 'json',
																													url: "<?php echo base_url(); ?>ajax/getParticipantProgramByAcademicYear",
																													data:{academicyear: academicyear, period: period},
																													success:function(response)
																													{					
																														$('#datatable2 thead').empty();
																														$('#datatable2 tbody').empty();
																														var tblHead = "<tr>"+"<th>"+"</th>";
																														var tblRow = "<tr>"+ "<td font size=5>Participants</td>";
																														var i;
																														for (i = 0; i < response.length; i++) {
																															tblHead += "<th>" + response[i]['PROGRAMNAME'] + "</th>";
																															tblRow += "<td>" + response[i]['JUMLAH'] + "</td>";
																														} 
																														tblHead +=  "</tr>";
																														tblRow +=  "</tr>";
																														
																														$(tblHead).appendTo("#datatable2 thead");
																														$(tblRow).appendTo("#datatable2 tbody");
																														
																														$('#container').highcharts({
																															data: {
																																table: document.getElementById('datatable2'),
																																switchRowsAndColumns: true,
																																endRow: 1
																															},
																															chart: {
																																type: 'pie'
																															},
																															title: {
																																text: 'Summary Report of Students by Enrollment '+academicyear22
																															},
																															yAxis: {
																																allowDecimals: false,
																																title: {
																																	text: 'Number of Participants'
																																}
																															},
																															tooltip: {
																																formatter: function() {
																																	return '<b>'+ this.series.name +'</b><br/>'+
																																	this.point.y +' '+ this.point.name.toLowerCase();
																																}
																															}
																														});
																													},
																													error: function() 
																													{
																														alert("Invalide!");
																													}
																												});
		
}		
	

	function dashboard(period, academicyear){
		var academicyear = $("#academicyear1").val();
		var period = $("#period").val();
		var periods = 'x';
		
		//alert(periods);die;
		$.ajax(
				{
                type:"post",
				dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getNumberParticipantsByAcademicYear",
                data:{ academicyear: academicyear, period: period},
                success:function(response1)
                {
					$.ajax({
					type:"post",
					dataType: 'json',
					url: "<?php echo base_url(); ?>ajax/getNumberScholarshipsByAcademicYear",
					data:{ academicyear: academicyear, period: period},
					success:function(response2){
						$.ajax({
							type:"post",
							dataType: 'json',
							url: "<?php echo base_url(); ?>ajax/getNumberNotScholarshipByAcademicYear",
							data:{ academicyear: academicyear, period: period},
							success:function(response3){
                                $.ajax({
                                        type:"post",
                                        dataType: 'json',
                                        url: "<?php echo base_url(); ?>ajax/getNumberAcountsByAcademicYear",
                                        data:{ academicyear: academicyear, period: period},
                                        success:function(response4)
                                        {
											$.ajax(
				{
                type:"post",
				dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getStepByAcademicYear",
                data:{ academicyear: academicyear, no: 1, period: period},
                success:function(response11)
                {
					$.ajax({
					type:"post",
					dataType: 'json',
					url: "<?php echo base_url(); ?>ajax/getStepByAcademicYear",
					data:{ academicyear: academicyear, no: 2, period: period},
					success:function(response21){
						$.ajax({
							type:"post",
							dataType: 'json',
							url: "<?php echo base_url(); ?>ajax/getStepByAcademicYear",
							data:{ academicyear: academicyear, no: 3, period: period},
							success:function(response31){
								$.ajax({
									type:"post",
									dataType: 'json',
									url: "<?php echo base_url(); ?>ajax/getStepByAcademicYear",
									data:{ academicyear: academicyear, no: 4, period: period},
									success:function(response41){
										$.ajax({
											type:"post",
											dataType: 'json',
											url: "<?php echo base_url(); ?>ajax/getStepByAcademicYear",
											data:{ academicyear: academicyear, no: 5, period: period},
											success:function(response51){
														$.ajax({
															type:"post",
															dataType: 'json',
															url: "<?php echo base_url(); ?>ajax/getStepByAcademicYear",
															data:{ academicyear: academicyear, no: 7, period: period},
															success:function(response71){	
																/*$.ajax({
																	type:"post",
																	dataType: 'json',
																	url: "<?php echo base_url(); ?>ajax/getConfirmByAcademicYear",
																	data:{ academicyear: academicyear, status: 'unaccept', period: period},
																	success:function(response121){*/
																		$.ajax({
																			type:"post",
																			dataType: 'json',
																			url: "<?php echo base_url(); ?>ajax/getStatusByAcademicYear",
																			data:{ academicyear: academicyear, status: 'ACCEPTED', period: period},
																			success:function(response91){
																				$.ajax({
																					type:"post",
																					dataType: 'json',
																					url: "<?php echo base_url(); ?>ajax/getStatusByAcademicYear",
																					data:{ academicyear: academicyear, status: 'UNACCEPTED', period: period},
																					success:function(response101){
																						$.ajax({
																							type:"post",
																							dataType: 'json',
																							url: "<?php echo base_url(); ?>ajax/getConfirmByAcademicYear",
																							data:{ academicyear: academicyear, status: 'accept', period: period},
																							success:function(response111){
																								$.ajax({
																									type:"post",
																									dataType: 'json',
																									url: "<?php echo base_url(); ?>ajax/getInterviewByAcademicYear",
																									data:{ academicyear: academicyear, period: period},
																									success:function(response81){
																										
																										datatab(academicyear,period);
																										$('number_participants').remove();
																										$('number_scholarship').remove();
																										$('number_notsholarship').remove();
																										$('number_acounts').remove();
																										var academicyear22;
																										if(academicyear==''){
																											academicyear22='x';
																										}else if(academicyear=='UNDEFINED YEAR'){
																											academicyear22='UNDEFINED_YEAR';
																										}else{
																											academicyear22=academicyear;
																										}
																										//var periodss;//var period = ;
																										//periods=cek_period(period);
																										//alert(periods);
																										document.getElementById('number_of_acounts').innerHTML = '<a id="number_acounts" href="<?php echo base_url()?>admin/modalNumberParticipants/acount/Number_of_Acounts/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response4+'</b></font></a>';
																										document.getElementById('number_of_participants').innerHTML = '<a id="number_participants" href="<?php echo base_url()?>admin/modalNumberParticipants/all/Number_of_Participants/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response1+'</b></font></a>';
																										document.getElementById('number_of_scholarship').innerHTML = '<a id="number_scholarship" href="<?php echo base_url()?>admin/modalNumberParticipants/scholarship/Number_of_Who_Register_for_a_Scholarship/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color: #17202a;" size="100000%"><b>'+response2+'</b></font></a>';
																										document.getElementById('number_of_notscholarship').innerHTML = '<a id="number_notsholarship" href="<?php echo base_url()?>admin/modalNumberParticipants/notscholarship/Number_of_Who_Register_for_not_a_Scholarship/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color: #17202a;" size="100000%"><b>'+response3+'</b></font></a>';
																										
																										$('number_step1').remove();
																										$('number_step2').remove();
																										$('number_step3').remove();
																										$('number_step4').remove();
																										$('number_step5').remove();
																										$('number_step6').remove();
																										$('number_step7').remove();
																										$('number_interview').remove();
																										$('number_unscheduled_interview').remove();
																										$('number_not_interview').remove();
																										$('number_accept').remove();
																										$('number_unaccept').remove();
																										$('number_confirm').remove();
																										//$('number_absent').remove();
																										document.getElementById('step_of_1').innerHTML = '<a id="number_step1" href="<?php echo base_url()?>admin/modalNumberParticipants/1/Program_Selection_Step/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response11+'</b></font></a>';
																										document.getElementById('step_of_2').innerHTML = '<a id="number_step2" href="<?php echo base_url()?>admin/modalNumberParticipants/2/Personal_Data_Step/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response21+'</b></font></a>';
																										document.getElementById('step_of_3').innerHTML = '<a id="number_step3" href="<?php echo base_url()?>admin/modalNumberParticipants/3/Family_Data_Step/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response31+'</b></font></a>';
																										document.getElementById('step_of_4').innerHTML = '<a id="number_step4" href="<?php echo base_url()?>admin/modalNumberParticipants/4/Educational_Data_Step/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response41+'</b></font></a>';
																										document.getElementById('step_of_5').innerHTML = '<a id="number_step5" href="<?php echo base_url()?>admin/modalNumberParticipants/5/Requirement_Docs_Step/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response51+'</b></font></a>';

																										document.getElementById('step_of_7').innerHTML = '<a id="number_step7" href="<?php echo base_url()?>admin/modalNumberParticipants/7/Acceptance_Status_Step/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response71+'</b></font></a>';
																										
																										document.getElementById('accept').innerHTML = '<a id="number_accept" href="<?php echo base_url()?>admin/modalNumberParticipants/ACCEPTED/Accepted/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response91+'</b></font></a>';
																										document.getElementById('unaccept').innerHTML = '<a id="number_unaccept" href="<?php echo base_url()?>admin/modalNumberParticipants/UNACCEPTED/Unaccepted/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response101+'</b></font></a>';
																										document.getElementById('confirm').innerHTML = '<a id="number_confirm" href="<?php echo base_url()?>admin/modalNumberParticipants/attend/Confirmation_of_Attendance/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response111+'</b></font></a>';
																										//document.getElementById('absent').innerHTML = '<a id="number_absent" href="<?php echo base_url()?>admin/modalNumberParticipants/absent/Absent/'+academicyear22+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response121+'</b></font></a>';
																										for (i = 0; i < response81.length; i++) {
																											document.getElementById('unscheduled_interview').innerHTML = '<a id="number_unscheduled_interview" href="<?php echo base_url()?>admin/modalNumberParticipants/unscheduled_interview/Unscheduled_Interview/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response81[0]['blmdijadwalkan']+'</b></font></a>';
																											document.getElementById('not_interview').innerHTML = '<a id="number_not_interview" href="<?php echo base_url()?>admin/modalNumberParticipants/not_interview/Not_Interview_Yet/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response81[1]['blminterview']+'</b></font></a>';
																											document.getElementById('interview').innerHTML = '<a id="number_interview" href="<?php echo base_url()?>admin/modalNumberParticipants/have_interview/Have_Been_Interviewed/'+academicyear22+'/'+periods+'" style="text-decoration: none" data-toggle="modal" data-target="#edit-modal"><font style="color:#3498db;" size="100000%"><b>'+response81[2]['sudah']+'</b></font></a>';
																										} 
																										
																										//$('#loading').hide();
																										
																										
																									},
																								});
																							},
																						});
																					},
																				});
																			},																	
																		});																
																	//},
																//});
															},
														});
											},
										});	
									},
								});
							},
						});
						},
					});
				},
                });
							},
						});
						},
					});
				},
				});
				},
                error: function() 
                {
                    alert("Invalide!");
                }
            });

	}
//function cek_period(period){
		/*var periodss;
		var periods;
		if(period!=''){
			periodss = period.toString(); //period.toString().replace(",","_")
			periods = periodss.replace(",","_");
		}else if(period==''){
			periods='x';
		}else if(period=='UNDEFINED YEAR'){
			periods='UNDEFINED_YEAR';
		}
		return periods;*/
	//}	
</script>
