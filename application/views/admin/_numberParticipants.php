            <?php
			if($academicyear==''){
				$academicyear22 ='x';
			}else{
                $academicyear22 =$academicyear;
            }
			if($period==''){
				$periods ='x';
			}else{
                $periods =$period;
            }
			?>			
						   <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><?=$judul?></h4>
                </div>
                <div class="modal-body">
				<div id="export">
				<?php 
					echo '<a type="button" href="'.base_url().'admin/exportExcelModal/'.$status.'/'.$academicyear22.'/'.$period.'" id="export" class="btn btn-success" >Export Excel</a><br><br>';

				?>
				</div>
					 <div class="table table-responsive">
                    <table id="tableProgram2" class="table table-sorting table-hover table-striped datatable">
                        <thead>
                        <tr>
                            <th >No</th>
                            <th >Fullname</th>
                            <th >Gender</th>
                            <th >Country</th>
							<th >Program Type</th>
                            <th >Enrollment</th>
                            <th >Faculty</th>
                            <th >Study Program</th>
							<!--<th >Action</th>-->
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty">Loading data from server</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
					

                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

<script>
	$(document).ready(function(){
		
		 $(".filter").on("change", function (e) {
            $('#tableProgram2').DataTable().ajax.reload();
        });

        var dt = $('#tableProgram2').DataTable( {
            /*dom: 'Bfrtip',
           buttons: [
             'colvis',
                'excel',
                'pdf',
                'csv',
        ],*/
            "order": [],
            "columnDefs": [
              { "width": "5%", "targets": 0},
            ],
			
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "aLengthMenu": [
                [-1,30, 40, 50, 100, -1],
                ["All",30, 40, 50, 100, ]
            ],
            "fnDrawCallback": function() {

                //initAction();

            },
            "sAjaxSource": "<?php echo base_url()?>admin/dataTableModal/<?php echo $status.'/'.$academicyear22.'/'.$periods;?>",
			"fnRowCallback":
				function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					  var participantLink = "<button onclick=\"window.open('<?php echo base_url()?>admin/participantDetail/" +aData[7]+ "')\" title='Participant Detail' target='_blank' class='btn btn-xs btn-primary btn-outline'><i class='fa fa-user'></i></button>";
					$(nRow).html(
						'<td>'+aData[0]+'</td>' +
						'<td>'+aData[1]+'</td>' +
						'<td>'+aData[2]+'</td>' +
						'<td>'+aData[3]+'</td>' +
						'<td>'+aData[8]+'</td>' +
						'<td>'+aData[4]+'</td>' +
						'<td>'+aData[5]+'</td>' +
						'<td>'+aData[6]+'</td>' //+
						//'<td>'+participantLink+'</td>'		
					);
					return nRow;
				},
        } );

        $("input[type='search']").on('keyup',function(){
            $('#tableProgram').DataTable().destroy();
            $('#tableProgram2').DataTable().search(this.value,true).draw();
        });
		
	})
</script>			