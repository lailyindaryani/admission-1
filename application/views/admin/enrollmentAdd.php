<?php
date_default_timezone_set('Asia/Jakarta');
?>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/enrollmentList">Enrollment</a></li>
                <li class="active">Enrollment Add</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Enrollment</h2>
            <em>Enrollment Data</em>
        </div>

        <div class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <!-- SUPPOR TICKET FORM -->
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
                        </div>
                        <div class="widget-content">
                            <form action="<?php echo base_url()?>admin/enrollmentStore" class="form-horizontal" role="form" method="post" id="ioform" enctype="multipart/form-data">
                                <fieldset>
                                    <legend>Enrollment Data</legend>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Enrollment Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="enrollmentname" id="enrollmentname" placeholder="Enter Enrollment Name" class="form-control" required/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Program</label>
                                        <div class="col-sm-9 ">
                                            <select name="program" class="" id="program" required>
                                                <option></option>
                                                <?php foreach ((array)$programs as $row){ ?>
                                                    <option value="<?php echo $row['PROGRAMID']?>"><?php echo $row['PROGRAMNAME']?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Degree</label>
                                        <div class="col-sm-9 ">
                                            <select name="degree" id="degree" required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Academic Year</label>
                                        <div class="col-sm-9">
                                            <select name="year" class="" id="year" required>
                                                <option></option>
                                                <?php
                                                    $yearnow = date('Y');
                                                    for($i = $yearnow-1;$i<=$yearnow+1;$i++){
                                                        $selected = $i==$yearnow?"selected":"";
                                                        echo "<option value=$i $selected>$i</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Period</label>
										<div class="col-sm-5">
                                           <select name="period_year" class="" id="period_year" required>
                                                <option></option>
                                                <?php
                                                    $yearnow = date('Y');
                                                    for($i = $yearnow-1;$i<=$yearnow+1;$i++){
                                                        $selected = $i==$yearnow?"selected":"";
                                                        echo "<option value=$i $selected>$i</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
										
                                        <div class="col-sm-4">
										<select name="period_bulan" class="" id="period_bulan" required>
                                                <option></option>
                                                <?php
                                                    //$month = date('n');
                                                    for($i = 1;$i<=12;$i++){
                                                        $selected = $i==$month?"selected":"";
                                                        echo "<option value=$i $selected>$i</option>";
                                                    }
                                                ?>
                                            </select>
                                           <!-- <input type="text" name="period" id="period" placeholder="Enter period" class="form-control" required/>
                                            <p class="help-block"><em>Example: <?=date('Y')?>-1; or <?=date('Y')?>-2; or <?=date('M Y')?></em></p>-->
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Start Date</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input id="startdate" name="startdate" class="form-control" placeholder="Enter start date" type="text" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">End Date</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input id="enddate" name="enddate" class="form-control" placeholder="Enter end date" type="text" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Description</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" name="programdescription" id="programdescription" placeholder="Enrollment Description"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Initial Status</label>
                                        <div class="col-sm-9">
                                            <label class="radio radio-inline">
                                                <input type="radio" id="statusT" name="status" value="OPEN"> OPEN
                                            </label>
                                            <label class="radio radio-inline">
                                                <input type="radio" id="statusF" name="status" value="CLOSED" checked> CLOSED
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-9 col-xs-offset-3">
                                            <div class="error-message text text-danger"></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                            <button type="button" class="btn btn-default" onclick="location.href='<?php echo base_url()?>admin/enrollmentList'">Cancel</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    <!-- END SUPPORT TICKET FORM -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->




<script src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#program").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select program",
        });
        $("#program").select2('val','');
		
		$("#period_bulan").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select month period",
        });
        $("#period_bulan").select2('val','');
		
		$("#period_year").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select year period",
        });
        $("#period_year").select2('val','');

        $("#degree").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select degree",
            allowClear: true
        });

        $("#year").select2({
            minimumResultsForSearch: -1,
            placeholder: "Select academic year",
        });
        $('#year').select2('val','');

        $('#program').on("change", function(e) {
            var programid = $("#program").val();

            $.ajax({
                type    : "GET",
                url     : "<?php echo base_url()?>admin/ajaxGetDegreeByProgram/"+programid,
                success : function (data) {
                    data = $.parseJSON( data );
                    var options = $("#degree");
                    options.empty();
                    $.each(data, function() {
                        options.append($("<option />").val(this.DEGREEID).text(this.DEGREE));
                    });
                },
                error : function (e) {
                    console.log(e.responseText);
                }
            });
        });

        $("#startdate").datepicker({
            defaultDate: new Date(),
            autoclose: true
        });

        $("#enddate").datepicker({
            defaultDate: new Date(),
            autoclose: true
        });
    });

    $("#ioform" ).validate({
        rules: {
            program: {
                required: true,
            },
            enrollmentname: {
                required: true,
            },
            degree: {
                required: true,
            },
            year: {
                required: true,
            },
            period:{
                required: true,
            },
            startdate: {
                required: true,
            },
            enddate: {
                required: true,
            }
        },
        highlight: function (label) {
//            $(label).closest('.form-group').addClass('has-error');
        },
        messages: {
            program:    "Program is required",
            degree:     "Degree is required",
            year:       "Academic year is required",
            period:     "Period is required",
            startdate:  "Start date is required",
            enddate:    "End date is required",
        },
//        wrapper:'p',
//        errorLabelContainer: '.error-message',
    });
</script>