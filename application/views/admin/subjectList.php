<!-- <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
<!-- content-wrapper -->
<style type="text/css">
    table.dataTable tbody td {
        word-break: break-word;
        vertical-align: top;
    }
</style>
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url()?>admin">Home</a></li>
                <li><a href="<?php echo base_url()?>admin/subjectList">Matakuliah</a></li>
                <li class="active">Pengaturan Matakuliah</li>
            </ul>
        </div>
    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Daftar Matakuliah</h2>
            <em>Data Matakuliah</em>
        </div>

        <div class="main-content">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 15px;">
                    <ul class="nav nav-tabs nav-tabs-custom-colored tabs-iconized">
                    <li class="active">
                        <a href="#all-tab" data-toggle="tab"><i class="fa fa-cog"></i> Pengaturan Matakuliah</a>
                    </li>
                    <li >
                        <a href="#mapping-tab" data-toggle="tab"><i class="fa fa-check-square-o"></i> Matakuliah Terpilih</a>
                    </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    
                        <form action="<?php echo base_url()?>admin/subjectStore" class="form-horizontal" role="form" method="post" id="ioform">
                        
                        

                        <div class="tab-content">
                            <div class="tab-pane all active" id="all-tab">
                                <div class="widget">
                                <div class="widget-header">
                             <h3>Tambah Pengaturan Matakuliah</h3>
                        </div>
                                <div class="widget-content">
                                    <div class="actions" style="margin-bottom: 50px;margin-right:5px;">
                                        <button type="button" style="float:right;" value="sinkron" class="btn btn-success btn-next sinkron">Sinkronisasi Data</button>
                                    </div>
                                <div class="row" style="border: 1px solid #ccc; margin:5px;">
                                    <div class="col-md-12" >
                                        
                                             <div class="row form-horizontal ">
                                            <div class="col-md-12">
                                                <span><h4><b>Simpan pada</b></h4></span><hr>
                                            </div>
                                            <div class="col-md-6">
                                                <p data-toggle="tooltip" data-placement="top" title="Pilih enrollment terlebih dahulu untuk memulai pengaturan mata kuliah">
                                                <span>Enrollment List</span>
                                                <select  id="enrollment_save" name="enrollment_save" class="filter">
                                                    <option value="">-All-</option>
                                                    <?php foreach ($enrollment as $rq){ ?>
                                                    <option data-degree="<?php echo $rq['DEGREE']; ?>" value="<?php echo $rq['ENROLLMENTID']?>"><?php echo $rq['DESCRIPTION']?></option>
                                                    <?php } ?>
                                                </select>
                                                </p>
                                            </div>
                                        <div class="col-md-6">
                                            <p>
                                            <span>Degree</span>
                                            <input type="text" id="degree" name="degree" class="form-control" disabled>
                                            <!-- <select id="degree" name="degree" class="filter" disabled>
                                                <option value=""></option>
                                                <?php foreach ($degree as $row){
                                                   
                                                ?>
                                                <option value="<?php echo $row['DEGREE']?>" ><?php echo $row['DEGREE']?></option>
                                                    <?php } ?>
                                            </select> -->
                                            </p>
                                        </div>    
                                     <!--    </div>      -->                               
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="border: 1px solid #ccc; margin:5px;">
                            <div class="col-md-12" >
                               <!--  <div class="widget-content"> -->
                                    <div class="row form-horizontal ">
                                        <div class="col-md-12">
                                            <span ><h4><b>Filter Pencarian</b></h4></span><hr>
                                        </div>
                                        <!-- <div class="col-md-6">
                                            <p>
                                            <span>Enrollment List</span>
                                            <select id="enrollment" name="enrollment" class="filter">
                                                <option value="">-All-</option>
                                                <?php foreach ($enrollment as $rq){ ?>
                                                <option value="<?php echo $rq['ENROLLMENTID']?>"><?php echo $rq['DESCRIPTION']?></option>
                                                <?php } ?>
                                            </select>
                                             </p>
                                        </div> -->
                                        <div class="col-md-4">
                                            <p>
                                            <span>Study Program Name</span>
                                            <select id="studyprogramname" name="studyprogramname" class="filter" disabled>
                                                <option value="">-All-</option>
                                              
                                            </select>
                                            </p>
                                        </div>
                                        
                                   <!--  </div> -->
                                   <!--  <div class="row form-horizontal"> -->
                                        <div class="col-md-4">
                                            <p>
                                            <span>Curriculum Year</span>
                                            <select id="curriculum" name="curriculum" class="filter" disabled>
                                                <option value="">-All-</option>
                                                <?php foreach ($allcurriculum as $rq){ 
                                                    if($curriculum[0]['CURICULUMYEAR']== $rq['CURICULUMYEAR']){
                                                        echo '<option value="'.$rq['CURICULUMYEAR'].'" selected>'.$rq['CURICULUMYEAR'].'</option>';
                                                    }else{
                                                        echo '<option value="'.$rq['CURICULUMYEAR'].'">'.$rq['CURICULUMYEAR'].'</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>
                                            <span>Semester</span>
                                            <select id="semester" name="semester" class="filter" disabled>
                                                <option value="">-All-</option>
                                                <option value="ganjil">Ganjil</option>
                                                <option value="genap">Genap</option>
                                            </select>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                                <div class="widget-content" >                                
                                        <div class="table-basic" id="table-subject-container">                                        
                                            <table id="tableSubject" class="table table-sorting table-hover  table-striped datatable" style="width:100%;">
                                                <thead>
                                                    <tr>
                                                        <th >No</th>
                                                        <th >Nama Matakuliah</th>
                                                        <th >SKS</th>
                                                        <th >Deskripsi Matakuliah</th>
                                                        <th >Study Program Name</th>
                                                        <th >Curriculum Year</th>
                                                        <th >Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="10" class="dataTables_empty">Loading data from server</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- <div class="row" style="border: 1px solid #ccc; margin:5px;"> -->
                                    
                                <!-- </div> -->
                                        <div class="actions" style="margin-bottom: 10px;">
                                            <button disabled type="button" style="margin-right: 45%; margin-left: 45%;" value="proses" class="btn btn-primary btn-next proses">Proses</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            </div>

                            <div class="tab-pane mapping" id="mapping-tab">
                                <div class="widget">
                                <div class="widget-header">
                             <h3> Daftar Matakuliah Terpilih</h3>
                        </div>
                                <div class="row" style="border: 1px solid #ccc; margin:5px;">
                            <div class="col-md-12">
                                <div class="widget-content">
                                    <div class="row form-horizontal ">
                                        <div class="col-md-4">
                                            <p>
                                            <span>Enrollment List</span>
                                            <select id="enrollment_b" name="enrollment_b" class="filter">
                                                <option value="">-All-</option>
                                                <?php foreach ($enrollment as $rq){ ?>
                                                <option value="<?php echo $rq['ENROLLMENTID']?>"><?php echo $rq['DESCRIPTION']?></option>
                                                <?php } ?>
                                            </select>
                                             </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>
                                            <span>Study Program Name</span>
                                            <select id="studyprogramname_b" name="studyprogramname_b" class="filter">
                                                <option value="">-All-</option>
                                                <?php foreach ($studyprogramname as $row){
                                                   // $selected='';
                                                    //if($row['STUDYPROGRAMID']==$idprodi){
                                                        //$selected="selected";
                                                    //     echo '<option value="'.$row['STUDYPROGRAMID'].'" selected>'.$row['STUDYPROGRAMNAME'].'</option>';
                                                    // }else{
                                                        echo '<option value="'.$row['STUDYPROGRAMID'].'" >'.$row['STUDYPROGRAMNAME'].'</option>';
                                                   // }
                                                ?>
                                               <!--  <option value="<?php echo $row['STUDYPROGRAMID']?>" <?php echo $selected;?>><?php echo $row['STUDYPROGRAMNAME']?></option> -->
                                                <?php } ?>
                                            </select>
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>
                                            <span>Curriculum Year</span>
                                            <select id="curriculum_b" name="curriculum_b" class="filter">
                                               
                                                <option value="">-All-</option>
                                                <?php foreach ($allcurriculum as $rq){ 
                                                    if($curriculum[0]['CURICULUMYEAR']== $rq['CURICULUMYEAR']){
                                                        echo '<option value="'.$rq['CURICULUMYEAR'].'" selected>'.$rq['CURICULUMYEAR'].'</option>';
                                                    }else{
                                                        echo '<option value="'.$rq['CURICULUMYEAR'].'">'.$rq['CURICULUMYEAR'].'</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row form-horizontal">
                                        <div class="col-md-4">
                                            <p>
                                            <span>Semester</span>
                                            <select id="semester_b" name="semester_b" class="filter">
                                                <option value="">-All-</option>
                                                <option value="ganjil">Ganjil</option>
                                                <option value="genap">Genap</option>
                                            </select>
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p>
                                            <span>Degree</span>
                                            <select id="degree_b" name="degree_b" class="filter">
                                                <option value="">-All-</option>
                                                <?php foreach ($degree as $row){
                                                   
                                                ?>
                                                <option value="<?php echo $row['DEGREE']?>" ><?php echo $row['DEGREE']?></option>
                                                    <?php } ?>
                                            </select>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="widget-content">
                                    <div class="table-basic">
                                        <table id="tableMapping" class="table table-sorting table-hover  table-striped datatable" width="100%">
                                            <thead>
                                                <tr>
                                                    <th >No</th>
                                                    <th >Nama Matakuliah</th>
                                                    <th >SKS</th>
                                                    <th >Deskripsi Matakuliah</th>
                                                    <th >Study Program Name</th>
                                                    <th >Curriculum Year</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="10" class="dataTables_empty">Loading data from server</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->

<div class="modal fade" id="result_sinkron" tabindex="1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 700px;">
        <div class="modal-content">
            <div class="modal-header">
               <!--  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                <center><h4 class="modal-title">Loading...</h4></center>
            </div>
            <div class="modal-body bar_sinkron">
                <div class="progress">
                    <div id="barr" class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        <span id="current-progress"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" src="<?php echo base_url()?>themes/_assets/js/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        ///$('.loader').hide();
        $('[data-toggle="tooltip"]').tooltip();
        $("#table-subject-container").hide();
        var arr = [];      
        $("#enrollment_save").select2();
        $("#studyprogramname").select2();
        //$("#degree").select2();
        $("#curriculum").select2();
        $("#enrollment_b").select2();
        $("#studyprogramname_b").select2();
        $("#degree_b").select2();
        $("#curriculum_b").select2();
        $("#semester").select2();
        $("#semester_b").select2();
        $.ajax({
            type:"post",
            dataType: 'json',
            url: "<?php echo base_url(); ?>ajax/getPopulateSubject",
            data:{ 
                'filter1' : $('#studyprogramname').val(),
                'filter2' : $('#enrollment_save').val(),
                'filter3' : $('#degree').val(),
                'filter4' : $('#curriculum').val(),
                'filter5' : $('#semester').val()
            },
            success:function(data){
                //console.log(data);
                for(var i in data){
                    var tamp = {
                        'SUBJECTCODE' : data[i].SUBJECTCODE,
                        'CURICULUMYEAR' : data[i].CURICULUMYEAR,
                        'STUDYPROGRAMID' : data[i].STUDYPROGRAMID
                    }; 
                    arr.push(tamp);
                    //console.log(data[i].SUBJECTCODE);
                }
                console.log(arr);
            }
        });

        var table1 = $("#tableSubject").dataTable({
            // "processing": true,
            // "serverSide": true,
            "ajax": {
                "url": "<?php echo base_url()?>admin/dataTablePopulateSubject",
                "type": "POST",
                "data" :{
                    'filter1' : $('#studyprogramname').val(),
                    'filter2' : $('#enrollment_save').val(),
                    'filter3' : $('#degree').val(),
                    'filter4' : $('#curriculum').val(),
                    'filter5' : $('#semester').val()
                }
            },
            "destroy": true,
            "columns": [
                { "data": "NO", width: "5%"},
                { "data": "SUBJECTNAME", width: "30%"},
                { "data": "CREDIT", width: "10%"},
                { "data": "SUBJECTDESCRIPTION", width: "35%"},
                { "data": "STUDYPROGRAMNAME", width: "10%" },
                { "data": "CURICULUMYEAR", width: "5%"},
                { "data": "ACTION", width: "5%"}
            ]
        });
        var table2 = $("#tableMapping").dataTable({
            // "processing": true,
            // "serverSide": true,
            "ajax": {
                "url": "<?php echo base_url()?>admin/dataTableSubjectMapping",
                "type": "POST",
                "data" :{
                    'filter1' : $('#studyprogramname_b').val(),
                    'filter2' : $('#enrollment_b').val(),
                    'filter3' : $('#degree_b').val(),
                    'filter4' : $('#curriculum_b').val(),
                    'filter5' : $('#semester_b').val()
                }
            },
            "destroy": true,
            "columns": [
                { "data": "NO", width: "5%"},
                { "data": "SUBJECTNAME", width: "30%"},
                { "data": "CREDIT", width: "10%"},
                { "data": "SUBJECTDESCRIPTION", width: "40%"},
                { "data": "STUDYPROGRAMNAME", width: "10%"},
                { "data": "CURICULUMYEAR", width: "5%"}
            ]
        });

        $("#enrollment_save").change(function(){
            var degree = $(this).select2().find(":selected").data("degree");
            //var study = $('#studyprogramname').val()
            arr = [];
            $("#degree").val(degree);
            if($(this).val()!=''){
                $(".proses").removeAttr('disabled');
                $("#semester").removeAttr('disabled'); 
                $("#curriculum").removeAttr('disabled');        
                $.ajax({
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getStudyExchange",
                    data:{ 
                        'filter' : degree
                    },
                    success:function(data){                        
                        $("#studyprogramname").empty();
                        var html = '<option value="">-All-</option>';
                        for(var i in data){
                            html += '<option value="'+data[i].STUDYPROGRAMID+'">'+data[i].STUDYPROGRAMNAME+'</option>';
                        }
                        $("#studyprogramname").append(html);
                        $("#studyprogramname").select2("val", '');
                        $("#studyprogramname").removeAttr('disabled');                        
                    }
                }).done(function(){
                    table1.fnDestroy();//.clear().draw();//.destroy(true);
                   // alert($('#studyprogramname').val());
                    table1 = $("#tableSubject").dataTable({
                    // "processing": true,
                    // "serverSide": true,
                        "ajax": {
                            "url": "<?php echo base_url()?>admin/dataTablePopulateSubject",
                            "type": "POST",
                            "data" :{
                                'filter1' : $('#studyprogramname').val(),
                                'filter2' : $('#enrollment_save').val(),
                                'filter3' : $('#degree').val(),
                                'filter4' : $('#curriculum').val(),
                                'filter5' : $('#semester').val()
                            }
                        },
                        "columns": [
                            { "data": "NO", width: "5%"},
                            { "data": "SUBJECTNAME", width: "30%"},
                            { "data": "CREDIT", width: "10%"},
                            { "data": "SUBJECTDESCRIPTION", width: "35%"},
                            { "data": "STUDYPROGRAMNAME", width: "10%" },
                            { "data": "CURICULUMYEAR", width: "5%"},
                            { "data": "ACTION", width: "5%"}
                        ]
                    });
                    $("#table-subject-container").show();
                    
                    $.ajax({
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getPopulateSubject",
                        data:{ 
                            'filter1' : $('#studyprogramname').val(),
                            'filter2' : $('#enrollment_save').val(),
                            'filter3' : $('#degree').val(),
                            'filter4' : $('#curriculum').val(),
                            'filter5' : $('#semester').val()
                        },
                        success:function(data){
                            //console.log(data);
                            for(var i in data){
                                var tamp = {
                                    'SUBJECTCODE' : data[i].SUBJECTCODE,
                                    'CURICULUMYEAR' : data[i].CURICULUMYEAR,
                                    'STUDYPROGRAMID' : data[i].STUDYPROGRAMID
                                }; 
                                arr.push(tamp);
                                //console.log(data[i].SUBJECTCODE);
                            }
                            console.log(arr);
                        }
                    });

                })
            }else{
                $(".proses").attr('disabled',true);
                $("#studyprogramname").attr('disabled',true);
                $("#semester").attr('disabled',true);
                $("#curriculum").attr('disabled',true);
                $("#table-subject-container").hide();
            }
            
        });

        $("#studyprogramname, #degree, #curriculum, #semester").change(function(){
            //alert();
            // $('.loader').show();
            table1.fnDestroy();//.clear().draw();//.destroy(true);
            table1 = $("#tableSubject").dataTable({
            // "processing": true,
            // "serverSide": true,
                "ajax": {
                    "url": "<?php echo base_url()?>admin/dataTablePopulateSubject",
                    "type": "POST",
                    "data" :{
                        'filter1' : $('#studyprogramname').val(),
                        'filter2' : $('#enrollment_save').val(),
                        'filter3' : $('#degree').val(),
                        'filter4' : $('#curriculum').val(),
                        'filter5' : $('#semester').val()
                    }
                },
                "columns": [
                    { "data": "NO", width: "5%"},
                    { "data": "SUBJECTNAME", width: "30%"},
                    { "data": "CREDIT", width: "10%"},
                    { "data": "SUBJECTDESCRIPTION", width: "35%"},
                    { "data": "STUDYPROGRAMNAME", width: "10%" },
                    { "data": "CURICULUMYEAR", width: "5%"},
                    { "data": "ACTION", width: "5%"}
                ]
            });
            $.ajax({
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getPopulateSubject",
                data:{ 
                    'filter1' : $('#studyprogramname').val(),
                    'filter2' : $('#enrollment_save').val(),
                    'filter3' : $('#degree').val(),
                    'filter4' : $('#curriculum').val(),
                    'filter5' : $('#semester').val()
                },
                success:function(data){
                    //console.log(data);
                    arr = [];
                    for(var i in data){
                        var tamp = {
                            'SUBJECTCODE' : data[i].SUBJECTCODE,
                            'CURICULUMYEAR' : data[i].CURICULUMYEAR,
                            'STUDYPROGRAMID' : data[i].STUDYPROGRAMID
                        }; 
                        arr.push(tamp);
                        //console.log(data[i].SUBJECTCODE);
                    }
                    console.log(arr);
                }
            });
        });

        $("#enrollment_b, #studyprogramname_b, #degree_b, #curriculum_b, #semester_b").change(function(){
            //alert();
            table2.fnDestroy();//.clear().draw();//.destroy(true);
            table2 = $("#tableMapping").dataTable({
            // "processing": true,
            // "serverSide": true,
                "ajax": {
                    "url": "<?php echo base_url()?>admin/dataTableSubjectMapping",
                    "type": "POST",
                    "data" :{
                        'filter1' : $('#studyprogramname_b').val(),
                        'filter2' : $('#enrollment_b').val(),
                        'filter3' : $('#degree_b').val(),
                        'filter4' : $('#curriculum_b').val(),
                        'filter5' : $('#semester_b').val()
                    }
                },
                "columns": [
                    { "data": "NO", width: "5%"},
                    { "data": "SUBJECTNAME", width: "30%"},
                    { "data": "CREDIT", width: "10%"},
                    { "data": "SUBJECTDESCRIPTION", width: "40%"},
                    { "data": "STUDYPROGRAMNAME", width: "10%"},
                    { "data": "CURICULUMYEAR", width: "5%"}
                ]
            });
        });

        $("#tableSubject").on('change',"input[type='checkbox']",function(e){
            var subjectcode = $(this).attr('id');
            var studyprogramid = $(this).attr('studyprogramid');
            if($(this).is(':checked')){               
               var curiculum = $(this).attr('curiculum'); 
               var tamp = {
                    'SUBJECTCODE' : subjectcode,
                    'CURICULUMYEAR' : curiculum,
                    'STUDYPROGRAMID' : studyprogramid                
               };            
               arr.push(tamp);  
            }else{
                for (var i = 0; i < arr.length; i++){
                    if (arr[i].SUBJECTCODE === subjectcode && studyprogramid === arr[i].STUDYPROGRAMID) { 
                        arr.splice(i, 1);
                        break;
                    }
                }
            }
            console.log(arr);
        });

        $(document).on('click','.sinkron',function(e){
            e.preventDefault();
            //alert('yuhuu');
            $("#result_sinkron").modal('show');
            $.ajax({
                xhr: function(){
                    var xhr = new window.XMLHttpRequest();
                    //Upload progress, request sending to server
                    xhr.upload.addEventListener("progress", function(evt){
                        console.log("in Upload progress");
                        console.log("Upload Done");
                    }, false);
                    //Download progress, waiting for response from server
                    xhr.addEventListener("progress", function(e){
                        console.log("in Download progress");
                        e.preventDefault();
                        if (e.lengthComputable) {
                            //percentComplete = (e.loaded / e.total) * 100;
                            var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                            console.log(percentComplete);
                            $("#barr")
                                .css("width", percentComplete + "%")
                                .attr("aria-valuenow", percentComplete)
                                .text(percentComplete + "% Complete");
                            progress("#barr",percentComplete,'start');
                        }
                        else{
                            console.log("Length not computable.");
                            $("#barr")
                                .css("width", 100 + "%")
                                .attr("aria-valuenow", 100)
                                .text(100 + "% Complete");
                            progress("#barr",100,'start');
                        }
                    }, false);
                    return xhr;
                },
                beforeSend:function(){
                    progress("#barr",1,'start');
                },
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>admin/sinkronSubject",
                data:{ 
                    filter: 'sinkron'
                },
                success:function(data){                    
                    if(data=='SUCCESS' && $("#barr").attr('aria-valuenow')==100){
                        console.log(data);
                        alert(data);
                        $("#result_sinkron").modal('hide');
                    }
                }
            });
            // $.ajax({
            //     type:"post",
            //     dataType: 'json',
            //     url: "<?php echo base_url(); ?>admin/sinkronSubject",
            //     data:{ 
            //         filter: 'sinkron'
            //     },
            //     success:function(data){
            //         alert(data);
            //     }
            // });
        })

        $(document).on('click','.proses',function(e){// $(".proses").unbind('click', function(){//on("click",function(e){
            e.preventDefault();
            var idx=1;
            for(var i in arr){
               // console.log(arr[i].SUBJECTCODE);
                $.ajax({
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>admin/subjectStore",
                    data:{ 
                        'subjectcode' : arr[i].SUBJECTCODE,
                        'curiculum' : arr[i].CURICULUMYEAR,
                        'studyprogramid' : arr[i].STUDYPROGRAMID,
                        'enrollmentid' : $("#enrollment_save").val(),
                        'index' : i,
                        'selectstudyprogram' :$("#studyprogramname").val()
                    },
                    success:function(data){
                    }
                    ,complete : function() {
                            // console.log(parseInt(i));
                        console.log(idx);
                        if(parseInt(idx) === arr.length){                            
                            console.log("yeahh");
                            setTimeout(function() { 
                                alert('SUCCESS');
                                table2.fnDestroy();//.clear().draw();//.destroy(true);
                                table2 = $("#tableMapping").dataTable({
                                // "processing": true,
                                // "serverSide": true,
                                    "ajax": {
                                        "url": "<?php echo base_url()?>admin/dataTableSubjectMapping",
                                        "type": "POST",
                                        "data" :{
                                            'filter1' : $('#studyprogramname_b').val(),
                                            'filter2' : $('#enrollment_b').val(),
                                            'filter3' : $('#degree_b').val(),
                                            'filter4' : $('#curriculum_b').val(),
                                            'filter5' : $('#semester_b').val()
                                        }
                                    },
                                    "columns": [
                                        { "data": "NO", width: "5%"},
                                        { "data": "SUBJECTNAME", width: "30%"},
                                        { "data": "CREDIT", width: "10%"},
                                        { "data": "SUBJECTDESCRIPTION", width: "35%"},
                                        { "data": "STUDYPROGRAMNAME", width: "10%"},
                                        { "data": "CURICULUMYEAR", width: "10%"}
                                    ]
                                });
                            }, 1000);
                        }
                        idx=idx+1;
                    }
                });
            }
            //if(i==arr.length){
                
            //}
        });
    });

function progress(go,current_progress, status){
    if(status=='stop'){
        return false;
    }else{
        var interval = setInterval(function() {
        if ($(go).attr('aria-valuenow') == 100){
            clearInterval(interval);
            return;
        }else{
            $(go)
                .css("width", current_progress + "%")
                .attr("aria-valuenow", current_progress)
                .text(current_progress + "% Complete");
            if(current_progress<100){
                current_progress += 1;
            }

        }
    }, 1000);
    }    
}
</script>