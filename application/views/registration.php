
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<title>Registration | KingAdmin - Admin Dashboard</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="KingAdmin Dashboard">
	<meta name="author" content="The Develovers">

	<!-- CSS -->
	<link href="<?php echo base_url();?>themes/_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>themes/_assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>themes/_assets/css/main.css" rel="stylesheet" type="text/css">

	<!--[if lte IE 9]>
		<link href="<?php echo base_url();?>themes/_assets/css/main-ie.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>themes/_assets/css/main-ie-part2.css" rel="stylesheet" type="text/css" />
	<![endif]-->

	<!-- Fav and touch icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon72x72.png">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon57x57.png">
	<link rel="shortcut icon" href="<?php echo base_url();?>themes/_assets/ico/favicon.png">

</head>

<body>
	<div class="wrapper full-page-wrapper page-auth page-login text-center">
		<div class="inner-page">

			<div class="login-box center-block" style="background-color: #FFFFFF">
                <div style="text-align: center">
                    <img src="<?php echo base_url()?>images/io.png" style="width: 250px;margin:auto;">
                </div><br>
                <?php $this->load->view('includes/messages'); ?>
				<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>registration/registrationProcess">
				<input type="hidden" name="coupon_id" value="<?php echo $coupon_id;?>">
				<input type="hidden" name="program_type" value="<?php echo $program_type;?>">
					<p class="title">Register your Account</p>
					<div class="form-group">
						<label for="username" class="control-label sr-only">Nationality</label>
						<div class="col-sm-12">
							<div class="input-group" style="width:100%">
								<select name="nationality" id="nationality" class="select2">
										<?php 
											if(!empty($listCountry))
											{
												foreach ($listCountry as $country) {
													echo "<option data-ext='".$country['PHONECODE']."' value='".$country['COUNTRYNAMEENG']."'>".$country['COUNTRYNAMEENG']."</option>";
												}
											}
										?>
								</select>
							</div>
						</div>
					</div>
					<label for="password" class="control-label sr-only">Fullname</label>
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group"><!--onkeypress="return blockSpecialChar(event)"-->
								<input type="text" placeholder="Fullname" id="fullname" name="fullname" required class="form-control" onpaste="return blockSpecialChar(event)" onkeypress="return blockSpecialChar(event)">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
							</div>
						</div>
						<div class="col-sm-12" align="left" style="text-size: 1px;">
							<p style="font-size: 11px;" class="help-block"><em>Special character not allowed (ex: @'"óè etc)</em></p>
						</div>
					</div>
					<div class="form-group">
						<label for="username" class="control-label sr-only">Phone</label>
						<div class="col-sm-3" style="padding-right:0px !important">
							<div class="input-group">
								<input type="text" placeholder="Phone" id="phone-ext" name="phone_ext" class="form-control" readonly>
							</div>
						</div>
						<div class="col-sm-9">
							<div class="input-group">
								<input type="text" placeholder="Phone" id="phone" name="phone" class="form-control" onkeypress="return _only_number_(event,this)" required>
								<span class="input-group-addon"><i class="fa fa-phone"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="username" class="control-label sr-only">Email</label>
						<div class="col-sm-12">
							<div class="input-group">
								<input type="email" placeholder="Email" id="email" name="email" class="form-control" required onchange="return validateEmail()">
								<span class="input-group-addon"><i class="fa fa-at"></i></span>
							</div>
						</div>
						<div class="col-sm-12" align="left" style="text-size: 1px;">
							<!--<p style="font-size: 11px;" class="help-block"><em>To register, kindly use email address Google Mail</em></p>-->
						</div>
					</div>
					<div class="form-group">
						<label for="username" class="control-label sr-only">Password</label>
						<div class="col-sm-12">
							<div class="input-group">
								<input type="password" placeholder="Password" id="password" name="password" class="form-control" required>
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="username" class="control-label sr-only">Password Confirmation</label>
						<div class="col-sm-12">
							<div class="input-group">
								<input type="password" placeholder="Password Confirmation" id="confirm-password" name="confirm_password" class="form-control" onchange="return validatePass()" required>
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							</div>
						</div>
						<div id="password_confirm" class="col-sm-12" align="left" style="text-size: 1px;">
							
						</div>
					</div>
					<button class="btn  btn-lg btn-block btn-auth btn-danger" type="submit" value="register" name="register"><i class="fa fa-arrow-circle-o-right"></i> Register Now</button>
				</form>

				<div class="links">
					<p><a href="#">Already have Account ?</a></p><br>
					<p><a href="<?php echo base_url()?>login" class="btn btn-success">Log in</a></p>
				</div>
			</div>
		</div>
	</div>

	<footer class="footer">&copy; <?php echo Date('Y'); ?> SISFO Telkom University</footer>

	<!-- Javascript -->
	<script type="text/javascript" src="<?php echo base_url();?>themes/_assets/js/general.function.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/bootstrap/bootstrap.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/plugins/modernizr/modernizr.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/plugins/select2/select2.min.js"></script>
</body>

<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();		
		var phone_ext = $('#nationality option:selected').attr('data-ext');
		    $('#phone-ext').val("+" + phone_ext);
		$("#nationality").on("change", function(e) {
		   	var phone_ext = $('#nationality option:selected').attr('data-ext');
		    $('#phone-ext').val("+" + phone_ext);
		});
		/*
		$('#confirm-password').on('keypress', function () {
			//alert($("#password_confirm").children().length);
			if ($('#password').val() != $('#confirm-password').val()) {
				//if($("#password_confirm").children().length==0){
					$('#password_confirm').append('<p style="font-size: 11px;" class="help-block"><em>Special character not allowed (ex: @óè etc)</em></p>');
				//}				
			}else{
				$("#password_confirm").remove();
			}
		});
		*/
	});
	function validatePass(){
		if ($('#password').val() != $('#confirm-password').val()) {
				//if($("#password_confirm").children().length==0){	
$('#password_confirm').empty();				
					$('#confirm-password').val('');	
					$('#password_confirm').append('<p style="font-size: 11px;" class="help-block"><em>Password harus sama</em></p>');	
					return false;
				//}				
		}else{				
				$("#password_confirm").children().remove();
				return true;
			}
	}
	function blockSpecialChar(e){
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
    }
	function validateEmail(){
		var emailField = $("#email").val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(emailField) == false){
			var emailField = $("#email").val('');
            alert('Please type a valid email address');
            return false;
        }
		var type = 'gmail.com';
			/*var file = $('#email').val();
			var valid;
			if (file.substr(file.length - type.length, type.length).toLowerCase() == type.toLowerCase()) {
				valid = true;
			}
			if (valid!=true) {
				$('#email').val('');
				alert("To register, kindly use email address other than Yahoo Mail");
			}*/
        return true;
	}
</script>
</html>

