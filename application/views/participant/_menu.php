<?php //echo "<pre>"; print_r($participant);die;?>
<div class="col-md-2 left-sidebar">

    <!-- main-nav -->
    <nav class="main-nav">

        <ul class="main-menu">
			<input type="hidden" id="currentstep" value="<?php echo $participant['CURRENTSTEP']?>">
            <li><a href="<?php echo base_url()?>participant/selectProgram" value="1" id="satu" onclick="return validasi('#satu')"><i class="fa fa-hand-o-up  fa-fw"></i><span class="text">Program Selection</span></a></li>

            <li><a href="<?php echo base_url()?>participant/personalData" value=2 id="dua" onclick="return validasi('#dua')"><i class="fa fa-user  fa-fw"></i><span class="text">Personal Data</span></a></li>

            <li><a href="<?php echo base_url()?>participant/familyData" value=3 id="tiga" onclick="return validasi('#tiga')"><i class="fa fa-users fa-fw"></i><span class="text">Family Data</span></a></li>

            <li><a href="<?php echo base_url()?>participant/educationData" value=4 id="empat" onclick="return validasi('#empat')"><i class="fa fa-graduation-cap fa-fw"></i><span class="text">Educational Data </span></a></li>

            <li><a href="<?php echo base_url()?>participant/supportingData" value=5 id="lima" onclick="return validasi('#lima')"><i class="fa fa-file-text fa-fw"></i><span class="text">Requirement Docs</span></a></li>

            <?php //if($participant['CURRENTSTEP'] == 6 || $participant['CURRENTSTEP'] == 7) { ?>
            <li><a href="<?php echo base_url()?>participant/summaryData" value=6 id="enam" onclick="return validasi('#enam')"><i class="fa fa-file-text fa-fw"></i><span class="text">Participant Card</span></a></li>
            
            <?php //} ?>
            <li><a href="<?php echo base_url()?>participant/acceptanceStatus" value=7 id="tujuh" onclick="return validasi('#tujuh')"><i class="fa fa-check-square fa-fw"></i><span class="text">Acceptance Status</span></a></li>
        </ul>
    </nav>
    <!-- /main-nav -->

    <div class="sidebar-minified js-toggle-minified">
        <i class="fa fa-angle-left"></i>
    </div>


</div>
<script>
	function validasi(id){
		var aktif = $(id).attr('value');
		var currentstep = $('#currentstep').attr('value');
		if(aktif<=currentstep){
			
		}else{
			alert("Sorry, you can not acces this menu, please finish the current step first");
			return false;
		}
	}
</script>