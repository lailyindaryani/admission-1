<?php //echo "<pre>"; print_r($requirements);die;?>
<script type="text/javascript">
    function fileValidation(idFile){
        var fileInput = document.getElementById(idFile);
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.pdf)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Please upload file having extensions .jpeg/.jpg/.pdf only.');
            fileInput.value = '';
            return false;
        }else{
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
			//Image cek Size
			if (fileInput.files && fileInput.files[0].size/ 1024 / 1024 > 1) {
				alert("File too large. File size max: 1 MB"); // Do your thing to handle the error.
				fileInput.value = null; // Clear the field.
			}
        }
	}
</script>

<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="#">Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layouts</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Requirement Docs</h2>
    <em>Form Supporting Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
            </div>
            <div class="widget-content">
                <?php $this->load->view('includes/messages'); ?>
                <div class="wizard-wrapper">
                    <?php $this->load->view('participant/_headerStep', $active); ?>
                    <div class="step-content">
                        <div class="step-pane active" id="step1">
                            <form class="form-horizontal" role="form" id="inputForm" method="post" action="<?php echo base_url()?>participant/supportingDataProcess" enctype="multipart/form-data">
                            <fieldset>
                                <legend>Supporting Data</legend>
                                <?php 
                                    if(count($requirements)==0){
                                        echo "Mohon maaf supporting data belum disediakan.";
                                    }
                                    foreach ($requirements as $req) {							
				                                ?>   
                                    <div class="form-group">
                                        <label for="ticket-attachment" class="col-sm-3 control-label"><?php echo $req['REQUIREMENTNAME']; ?></label>
                                        <div class="col-md-8">
                                            <?php
												$required='required';
                                                if(strtoupper($req['TYPE']) == 'ATTACHMENT'){
                                                    if(!empty($requirementvalue[$req['REQUIREMENTID']])){ 
														$required='';
                                                        echo '<a href="'.base_url().$requirementvalue[$req['REQUIREMENTID']]['FILEURL'].$requirementvalue[$req['REQUIREMENTID']]['FILENAME'].'" target="_blank">'.$requirementvalue[$req['REQUIREMENTID']]['FILENAME'].'</a>';     
													}else if($req['TEMPLATEFILEURL']!='' && $req['TEMPLATEFILE']!=''){
														 echo '<a href="'.base_url().$req['TEMPLATEFILEURL'].'" target="_blank" class="btn btn-primary">Download</a>';
													}else{
														if($req['ISREQUIRED']=='NO'){
															$required='';
														}														
													}
												//if($req['TEMPLATEFILEURL']=='' && $req['TEMPLATEFILE']==''){
                                            ?>
											<br><br>
                                                <input type="file" onchange="return fileValidation('<?php echo 'requirement_'.$req['REQUIREMENTID']; ?>')" class="<?php echo $required;?>" id="<?php echo 'requirement_'.$req['REQUIREMENTID']; ?>" name="<?php echo 'requirement_'.$req['REQUIREMENTID']; ?>">

                                                <p class="help-block"><em>Valid file type: .jpg, .jpeg, .pdf. File size max: 1 MB</em></p>
												<!-- } -->
											<?php } else { 
														if(!empty($requirementvalue[$req['REQUIREMENTID']])){?>
														<textarea name="<?php echo 'requirement_'.$req['REQUIREMENTID']; ?>" class="form-control"><?php echo $requirementvalue[$req['REQUIREMENTID']]['VALUE']?></textarea>
											<?php	}else{
														if($req['ISREQUIRED']=='NO'){
															$required='';
														}else{
															$required='required';
														}
														?>
														<textarea name="<?php echo 'requirement_'.$req['REQUIREMENTID']; ?>" class="form-control <?php echo $required;?>"></textarea>
											<?php 	}?>
                                                
                                            <?php 
                                                } ?>
                                        </div>
                                    </div>
                                <?php
                                    }
                                ?>
                            </fieldset>
                        </div>
                    </div>

                    <div class="actions">
                        <a  href="<?php echo base_url()?>participant/educationData/" type="button" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</a>
                        <button type="submit" class="btn btn-primary btn-next">Next <i class="fa fa-arrow-right"></i></button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->
<script type="text/javascript">
    $(document).ready(function(){


        $("#inputForm").validate();


    });

</script>
<!-- <script src="<?php echo base_url();?>themes/_assets/js/plugins/wizard/wizard.min.js"></script> -->


