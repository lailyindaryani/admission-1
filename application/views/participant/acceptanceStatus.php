<?php
//echo "<pre>"; print_r($participantDetail);die;
 date_default_timezone_set('Asia/Jakarta');?>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Form Layouts</li>
            </ul>
        </div>

    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Acceptance Status</h2>
            <em>Form Acceptance Status</em>
        </div>

        <div class="main-content">

            <!--<div class="alert alert-warning alert-dismissable">
                <a href="#" class="close">�</a>
                <i>your submitting still being process</i>
            </div>-->

            <div class="col-md-12">
                <!-- SUPPOR TICKET FORM -->
                <div class="widget">
                    <div class="widget-header">
                        <h3><i class="fa fa-edit"></i> Information</h3>
                    </div>
                    <div class="widget-content">
                        <?php $this->load->view('includes/messages'); ?>
                        <div class="wizard-wrapper">
                            <?php $this->load->view('participant/_headerStep', $active); ?>
                            <div class="step-content">
                                <div class="step-pane active" id="step7">
                       

                            <!-- What date are you willing to attend on Telkom University? -->
                            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post"
                                  id="inputForm" action="<?php echo base_url() ?>participant/ConfirmStatusProcess">
                                <fieldset>
                                     <?php /*<div class="form-group">
                                        <div class="col-sm-3">
                                            <input type="text" id="datepicker" class="form-control datetime-format"
                                                   name="date" placeholder="Date" value="<?php echo  $participant['CONFIRMDATE']?>">
                                        </div>
                                    </div>
                                    */?>
                                    <div>
                                            <?php 
											$acceptanceStatus = $participant['ACCEPTANCESTATUS'];
											if($participantDetail['PROGRAMTYPE']=='ACADEMIC'){
											
                                            if(($acceptanceStatus == 'ACCEPTED' && $participantDetail['PUBLISHDATE']==$todayDate && $participantDetail['PUBLISHTIME']<$todayTime) || ($acceptanceStatus == 'ACCEPTED' && $participantDetail['PUBLISHDATE']<$todayDate)){ ?>
												<h3>
                                                Congratulations!<br>
You are accepted as new student of Telkom University.<br>
Kindly download the LoA for further details.<br><br></h3>
                                            <?php $programid = $participant['PROGRAMID'];
                                                if(strpos($participantDetail['PROGRAMNAME'], 'REGULAR') !== false){//$programid == '5'){?><!--REGULAR-->
                                                     <a href="<?php echo base_url()?>Pdf/templateTypeNone/<?php echo  $participant['PARTICIPANTID']?>" class="btn btn-primary" role="button">Download LOA</a>
                                                <?php }
                                                if(strpos($participantDetail['PROGRAMNAME'], 'STUDENT EXCHANGE') !== false || strpos($participantDetail['PROGRAMNAME'], 'CREDIT EARNING') !== false){//$programid == '1'){?><!--STUDENT EXCHANGE-->
                                                     <a href="<?php echo base_url()?>Pdf/templateTypeSECE/<?php echo  $participant['PARTICIPANTID']?>" class="btn btn-primary" role="button">Download LOA</a>
                                                <?php }
                                                if(strpos($participantDetail['PROGRAMNAME'], 'SCHOLARSHIP') !== false){//$programid == '3'){//SCHOLARSHIP
													if($participant['SCHOLARSHIPTYPEID']=='1'){
														echo '<a href="'.base_url().'Pdf/templateTypeA/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
													}else if($participant['SCHOLARSHIPTYPEID']=='2'){
														echo '<a href="'.base_url().'Pdf/templateTypeB/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
													}else if($participant['SCHOLARSHIPTYPEID']=='3'){
														echo '<a href="'.base_url().'Pdf/templateTypeC/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
													}else if($participant['SCHOLARSHIPTYPEID']=='4'){
														echo '<a href="'.base_url().'Pdf/templateTypeD/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
													}else if($participant['SCHOLARSHIPTYPEID']=='5'){
														echo '<a href="'.base_url().'Pdf/templateTypeX/'.$participant['PARTICIPANTID'].'" class="btn btn-primary" role="button">Download LOA</a>';
													}?><!--STUDENT EXCHANGE-->
                                                <?php }
                                                //if($programid == '2'){?>
                                                  <!--   <a href="<?php echo base_url()?>Pdf/templateTypeNone/<?php echo  $participant['PARTICIPANTID']?>" class="btn btn-primary" role="button">Download LOA</a> -->
                                                <?php //}
                                                ?>
                                                 <br>
                                                 <br>
                                                <input type="hidden" name="passportno" value="<?php echo  $participant['PASSPORTNO']?>">
                                                <input type="hidden" name="participantid" value="<?php echo  $participant['PARTICIPANTID']?>">
												<div class="form-group">
                                                    <label for="ticket-message" class="col-sm-2 control-label">Upload Confirmation of Admission</label>
													<?php if($participant['LOAFILE']!=null){
														echo '<a href="'.base_url().$participant['PHOTOURL'].$participant['LOAFILE'].'" target="_blank">'.$participant['LOAFILE'].'</a>';
															}?>
                                                        <div class="col-sm-4">
                                                            <input type="file" class="form-control" onchange="fileValidation('loa')" id="loa" name="loa" size="20" />
															<p class="help-block"><em>Valid file type: .pdf. File size max: 2 MB</em></p>
                                                        </div>
                                                    </div>
												<?php if($participantDetail['PROGRAMNAME']=='REGULAR'){
													echo '<div class="form-group">
                                                        <label for="ticket-type" class="col-sm-2 control-label">Payment Date</label>
                                                        <div class="col-sm-4">
                                                            <input type="date" readOnly class="form-control" name="paymentdate" value="'.str_replace(' ','',str_replace('00:00:00','',$participant['PAYMENTDATE'])).'">
                                                        </div>
                                                    </div>';
												}?>
												 <div class="form-group">
                                                        <label for="ticket-type" class="col-sm-2 control-label">Arrival Date</label>
                                                        <div class="col-sm-4">
                                                            <input type="date" readOnly class="form-control" name="arrivaldate2" value="<?php echo  str_replace(' ','',str_replace('00:00:00','',$participant['ARRIVALDATE'])) ?>">
                                                        </div>
                                                    </div>
                                                <div class="form-group">
                                                        <label for="ticket-type" class="col-sm-2 control-label">Confirm Arrival Date</label>
                                                        <div class="col-sm-4">
                                                            <input type="date" class="form-control" name="arrivaldate" value="<?php echo  str_replace(' ','',str_replace('00:00:00','',$participant['CONFIRMDATE'])) ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ticket-message" class="col-sm-2 control-label">Upload Arrival Ticket</label>
														<?php if($participant['ARRIVALTICKET'] != null){
														echo '<a href="'.base_url().$participant['PHOTOURL'].$participant['ARRIVALTICKET'].'" target="_blank">'.$participant['ARRIVALTICKET'].'</a>';
														}?>
                                                        <div class="col-sm-4">
														
                                                             <input type="file" onchange="fileValidation2('imgInp')" class="form-control"  name="userfile" size="20" id="imgInp" />
															 <p class="help-block"><em>Valid file type: .gif, .jpg, .png. File size max: 2 MB</em></p>
                                                             <?php if ($participant['ARRIVALTICKET'] != null){
															echo '<img style="max-height: 200px;max-width: 150px; image-orientation: from-image;" id="displayImage" alt="Photo" src="'.base_url().$participant['PHOTOURL'].$participant['ARRIVALTICKET'].'" />';                                                            
															}else{echo "You Have not Uploaded Ticket";}?>
                                                        </div>
                                                    </div>
                                                <?php

                                                 }else 
												if(($acceptanceStatus == 'UNACCEPTED' && $participantDetail['PUBLISHDATE']==$todayDate && $participantDetail['PUBLISHTIME']<$todayTime) || ($acceptanceStatus == 'UNACCEPTED' && $participantDetail['PUBLISHDATE']<$todayDate)){?>
													<h3>
													Thank you for your application to Telkom University.  Your application and test result have been carefully reviewed by the Admission Committee. We had a large number of exceptional applicants and the process of narrowing down the applicants is very challenging.<br>
However, your selection result have not yet meet Telkom University passing grade's standard, and we regret to inform you that YOU ARE NOT SELECTED as Telkom University student. <br>
Kindly re-apply again for the next admission batch here:  <a href="https://io.telkomuniversity.ac.id/admission">https://io.telkomuniversity.ac.id/admission.</a><br>
We would like to thank you for your interest in our campus and we wish you the utmost success in your endeavor.<br><br>
Yours Sincerely,<br></h3>
                                              <?php  }else{?>
													<h3>
													Your administration requirements still processed.<br>
                                                    <?php 
                                                    if($participantDetail['PUBLISHDATE']!='0000-00-00' && $participantDetail['PUBLISHTIME']!='00:00:00'){
                                                        echo 'The acceptance results will be announced on '.date('F jS Y',strtotime($participantDetail['PUBLISHDATE'])).' at '. substr($participantDetail['PUBLISHTIME'],0,5).' (GMT +7).<br></h3>';
                                                    }
														
												}
											}else{
												//NON ACADEMIC
												if(($acceptanceStatus == 'ACCEPTED' && $participantDetail['PUBLISHDATE']==$todayDate && $participantDetail['PUBLISHTIME']<$todayTime) || ($acceptanceStatus == 'ACCEPTED' && $participantDetail['PUBLISHDATE']<$todayDate)){
													echo '<h3>';
													echo 'Congratulations!<br>';
													echo 'You have been registered as a participant for a short course/joint course <b>'.$participantDetail['DESCRIPTION'].'</b> program that will be held by Telkom University.<br>';
													echo 'Thank you for your registration. We look forward to meeting you at the event on '.date('F jS Y',strtotime($participantDetail['ARRIVALDATE'])).'<br><br></h3>';
												}else if(($acceptanceStatus == 'UNACCEPTED' && $participantDetail['PUBLISHDATE']==$todayDate && $participantDetail['PUBLISHTIME']<$todayTime) || ($acceptanceStatus == 'UNACCEPTED' && $participantDetail['PUBLISHDATE']<$todayDate)){
													echo '<h3>Dear Applicant,<br>';
													echo 'We appreciate that you took time to apply in our Short Course/Joint Course Program. We had a large number of applicants and process of narrowing down the applicants is very challenging.<br>';
													echo 'However, due to the limited space that we have for the Short Course/Joint Course, we regret to inform you that YOU ARE NOT SELECTED as  <b>'.$participantDetail['DESCRIPTION'].'</b> participant.<br>';
													echo 'We would like to thank you for your interest in our program, and we wish you the utmost success in your endeavor.<br><br>';
													echo 'Regards,<br>';
													echo 'Telkom University<br><br></h3>';
													}else{
													echo '<h3>';
													echo 'Thank you for your registration. Your application is still in process now. <br>';
													echo 'You will get the confirmation of your application soon. Kindly check the confirmation in your acceptance status.<br><br>';
													echo '</h3>';
											}}

                                            ?>

                                        </div>
                                </fieldset>                        
                                </div>
                                   
                            </div>
							<div class="actions">
								<a href="<?php echo base_url()?>participant/summaryData/" type="button" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</a>
							<?php $acceptanceStatus = $participant['ACCEPTANCESTATUS'];
                                            if($acceptanceStatus == 'ACCEPTED' && $participant['PROGRAMTYPE']=='ACADEMIC'){ 
							
								echo '<button type="submit" class="btn btn-primary btn-next"> Save </i></button>';
							
											 }?>
									</div>
									
							  </form>
                        </div>

                    </div>
                </div>
            </div>


        </div>
        <!-- /main-content -->
    </div>
    <!-- /main -->
</div>
<!-- /content-wrapper -->

<!-- <script src="<?php echo base_url(); ?>themes/_assets/js/plugins/wizard/wizard.min.js"></script> -->

<script type="text/javascript">
    $(document).ready(function () {
        $('.datetime-format').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        function UrlExists(url)
        {
            var http = new XMLHttpRequest();
            http.open('HEAD', url, false);
            http.send();
            return http.status!=404;
        }
        
		 function readURL(input) {
            if (input.files && input.files[0]) {
                $("#displayImage").css("display", "block");
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#displayImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
       
        $("#imgInp").change(function(){
          //  readURL(this);
        });		

        $('.datetime-format').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
    });
	
	function fileValidation(idFile){
        var fileInput = document.getElementById(idFile);
        var filePath = fileInput.value;
        var allowedExtensions = /(\.pdf)/;
        if(!allowedExtensions.exec(filePath)){
            alert('Please upload file having extensions .pdf only.');
            fileInput.value = '';
            return false;
        }else{
			if (fileInput.files && fileInput.files[0].size/ 1024 / 1024 > 2) {
				alert("File too large. File size max: 2 MB"); // Do your thing to handle the error.
				fileInput.value = null; // Clear the field.
			}
		}
    }
	function fileValidation2(idFile){
        var fileInput = document.getElementById(idFile);
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.png|\.gif)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Please upload file having extensions .jpg, .png, .gif only.');
            fileInput.value = '';
            return false;
        }
		validateSize(fileInput);
    }
	function validateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 2) {
            alert('File size exceeds 2 MB');
			$(file).val('');
			return false;
        }else{
			readURL(this);
		} 
    }
</script>



