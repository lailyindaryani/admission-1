<div id="demo-wizard" class="wizard">
    <ul class="steps">
        <li class="<?php echo $active == 1 ? 'active' : ''; ?> "><span class="badge <?php echo $active == 1 ? 'badge-info' : ''; ?>">1</span> Program<span class="chevron"></span></li>

        <li class="<?php echo $active == 2 ? 'active' : ''; ?>"><span class="badge <?php echo $active == 2 ? 'badge-info' : ''; ?>">2</span>Personal<span class="chevron"></span></li>

        <li class="<?php echo $active == 3 ? 'active' : ''; ?>"><span class="badge <?php echo $active == 3 ? 'badge-info' : ''; ?>">3</span>Family<span class="chevron"></span></li>

        <li class="<?php echo $active == 4 ? 'active' : ''; ?>"><span class="badge <?php echo $active == 4 ? 'badge-info' : ''; ?>">4</span>Education<span class="chevron"></span></li>

        <li class="<?php echo $active == 5 ? 'active' : ''; ?>"><span class="badge <?php echo $active == 5 ? 'badge-info' : ''; ?>">5</span>Requirement Docs<span class="chevron"></span></li>

        <li class="<?php echo $active == 6 ? 'active' : ''; ?>" ><span class="badge <?php echo $active == 6 ? 'badge-info' : ''; ?>">6</span>Participant Card</li><span class="chevron"></span></li>

        <li class="<?php echo $active == 7 ? 'active' : ''; ?>" ><span class="badge <?php echo $active == 7 ? 'badge-info' : ''; ?>">7</span>Acceptance Status<span class="chevron"></span></</li>

    </ul>
</div>