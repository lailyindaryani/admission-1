<?php //echo "<pre>"; print_r($participant);die;?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
<style type="text/css">
    div.clear
{
    clear: both;
}

div.product-chooser{
    
}

    div.product-chooser.disabled div.product-chooser-item
    {
        zoom: 1;
        filter: alpha(opacity=60);
        opacity: 0.6;
        cursor: default;
    }

    div.product-chooser div.product-chooser-item{
        padding: 11px;
        border-radius: 6px;
        cursor: pointer;
        position: relative;
        border: 1px solid #efefef;
        margin-bottom: 10px;
        margin-left: 10px;
        margin-right: 10x;
    }
    
    div.product-chooser div.product-chooser-item.selected{
        border: 4px solid #428bca;
        background: #efefef;
        padding: 8px;
        filter: alpha(opacity=100);
        opacity: 1;
    }
    
        div.product-chooser div.product-chooser-item img{
            padding: 0;
        }
        
        div.product-chooser div.product-chooser-item span.title{
            display: block;
            margin: 10px 0 5px 0;
            font-weight: bold;
            font-size: 12px;
        }
        
        div.product-chooser div.product-chooser-item span.description{
            font-size: 12px;
        }
        
        div.product-chooser div.product-chooser-item input{
            position: absolute;
            left: 0;
            top: 0;
            visibility:hidden;
        }

</style>

<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="#">Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layouts</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Program Selection</h2>
    <em>Form Program Selection</em>
</div>

<div class="main-content">

<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
            </div>
            <div class="widget-content">
                <?php $this->load->view('includes/messages'); ?>
                <div class="wizard-wrapper">
                    <?php $this->load->view('participant/_headerStep', $active); ?>
                    <div class="step-content">
                        <div class="step-pane active" id="step1">
                            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>participant/selectionProgramProcess" id="inputForm">
                                <fieldset style="margin-bottom: 15px;">
                                    <input type="hidden" name="programdegreeid" id="programdegree-id">
                                    <input type="hidden" name="language_delivery_mapping_id" id="languagedeliverymapping-id">
                                    <input type="hidden" name="acceptstatus" id="acceptstatus" value="<?php echo $participant['ACCEPTANCESTATUS']?>">
                                    <input type="hidden" name="after_programid"  value="<?php echo $participant['PROGRAMID']?>">
                                    <input type="hidden" name="after_enrollmentid" value="<?php echo $participant['ENROLLMENTID']?>">
                                    <input type="hidden" name="after_studyprogramid" value="<?php echo $participant['STUDYPROGRAMID']?>">
                                    <input type="hidden" name="after_courseid" value="<?php echo $participant['COURSEID']?>">
                                    <input type="hidden" name="participantid" id="participantid" value="<?php echo $participant['USERID']?>">
                                     <input type="hidden" name="participantidd" id="participantidd" value="<?php echo $participant['PARTICIPANTID']?>">
                                    <input type="hidden" name="programtypee" id="programtypee" value="<?php echo $participant['PROGRAMTYPE']?>">
                                    <input type="hidden" name="statussubjectsetting" id="statussubjectsetting" value="">
                                    <legend>Program Selection</legend>
                                    <div class="form-group">
                                        <label for="ticket-type" class="col-sm-3 control-label">Select Program</label>
                                       <div class="col-sm-9">
                                            <select id="select-program" name="program_id" class="select2 required" required>
                                                <option value="">-- Select Program --</option>
                                                <?php
                                                    if(!empty($listProgram))
                                                    {

                                                        foreach ($listProgram as $program) {
                                                        $selected = $program['PROGRAMID'] == $participant['PROGRAMID'] ? 'selected' : '';
                                                ?>
                                                        <option data-maxcredit="<?php echo $program['MAXCREDIT']; ?>" data-maxstudy="<?php echo $program['MAXSTUDYPROGRAM']; ?>"  data-type="<?php echo $program['PROGRAMTYPE']; ?>" data-subjectstatus="<?php echo $program['STATUSSUBJECTSETTING']; ?>" value="<?php echo $program['PROGRAMID'];?>" <?php echo $selected; ?>><?php echo $program['PROGRAMNAME']; ?></option>
                                                <?php
                                                        }
                                                    }
                                                ?> 
                                            </select>
                                        </div>
                                    </div>
                                    <div id="enrollment-academic">

                                        <div class="form-group">
                                            <label for="ticket-type" class="col-sm-3 control-label">Enrollment</label>
                                            <div class="col-sm-9">
                                                <select id="select-enrollment" name="enrollment_id" class="select2 required">
                                                    <option value="">-- Select Enrollment --</option>
                                                    <?php /*

                                                    <?php
                                                        if(!empty($listEnrollment))
                                                        {

                                                            foreach ($listEnrollment as $enrollment) {
                                                            $selected = $enrollment['ENROLLMENTID'] == $participant['ENROLLMENTID'] ? 'selected' : '';
                                                    ?>
                                                            <option value="<?php echo $enrollment['ENROLLMENTID'];?>" <?php echo $selected; ?>><?php echo $enrollment['DESCRIPTION']; ?></option>
                                                    <?php
                                                            }
                                                        }
                                                    ?>

                                                    */ ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="ticket-type" class="col-sm-3 control-label">Degree</label>
                                        <div class="col-sm-9">
                                              <input type="text" name="degreename" id="degree-id" readonly class="form-control">
                                            <input type="hidden" name="degreeid" id="degree-id-hidden">
                                        </div>
                                    </div>
                                    <div class="form-group" id="language-delivery-container">
                                        <label for="ticket-type" class="col-sm-3 control-label">Language Delivery</label>
                                        <div class="col-sm-9">
                                            <select id="select-language" name="language_delivery_id" class="select2 required">
                                                <option value=""> -- Select Language Delivery -- </option>
                                                
                                                 <?php
                                                    if(!empty($listLanguageDelivery))
                                                    {

                                                        foreach ($listLanguageDelivery as $data) {
                                                        if($data['LANGUAGEDELIVERYID'] == $languageDeliveryId['LANGUAGEDELIVERYID']){
                                                ?>
                                                        <option value="<?php echo $data['LANGUAGEDELIVERYID'];?>" selected="selected"><?php echo $data['LANGUAGE']; ?></option>
                                                <?php
                                                            }else{ ?>
                                                        <option value="<?php echo $data['LANGUAGEDELIVERYID'];?>"><?php echo $data['LANGUAGE']; ?></option> 
                                                <?php       }
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php  
                                      if($participant['PROGRAMTYPE'] == 'ACADEMIC')
                                        {?>
                                            <div class="form-group" id="study-program-container">
                                        <label for="ticket-type" class="col-sm-3 control-label">Study Program</label>
                                        <div class="col-sm-9">
                                            <select id="select-studyprogram" name="study_program_id" class="select2 required">
                                            <option value=""> -- Select Study Program -- </option>
                                          
                                                  <?php
                                                    if(!empty($listStudyProgram))
                                                    {

                                                        foreach ($listStudyProgram as $data) {
                                                       if($data['STUDYPROGRAMID'] == $participant['STUDYPROGRAMID']){
                                                ?>
                                                        <option value="<?php echo $data['STUDYPROGRAMID'];?>" selected="selected"><?php echo $data['STUDYPROGRAMNAME']; ?></option>
                                                <?php
                                                       }
                                                        }
                                                    }
                                                ?> 

                                           </select>
                                        </div>
                                    </div>
                                    <?php 
                                    }else{?>
                                    <div class="form-group" id="course-container">
                                        <label for="ticket-type" class="col-sm-3 control-label">Course</label>
                                        <div class="col-sm-9">
                                            <select id="select-course" name="study_course_id" class="select2 required">
                                            <option value=""> -- Select Course-- </option>
                                          
                                                  <?php
                                                    if(!empty($listCourse))
                                                    {

                                                        foreach ($listCourse as $data) {
                                                       if($data['COURSEID'] == $participant['COURSEID']){
                                                ?>
                                                        <option value="<?php echo $data['COURSEID'];?>" selected="selected"><?php echo $data['COURSENAME']; ?></option>
                                                <?php
                                                       }
                                                        }
                                                    }
                                                ?> 

                                           </select>
                                        </div>
                                    </div>
                                    <?php 
                                    }?>
                                         
                                </fieldset>
                        
                            <div id="subject-container">
                                        <div class="table-basic" id="div_tab_sub" style="margin-bottom: 10px;">
                                            <h4><b>Daftar Mata Kuliah yang dapat diambil</b></h4>
                                            <div class="alert alert-info alert-dismissible">
                                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                                <p id="info_subject"></p>
                                            </div>
                                            <table id="tableSubject" class="table table-sorting table-hover  table-striped datatable" style="width:100%;">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Nama Matakuliah</th>
                                                        <th>SKS</th>    
                                                        <th>Study Program</th>
                                                        <th>Language Delivery</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="table-primary" id="div_tab_map" style="margin-bottom: 10px;">
                                            <h4><b>Daftar Mata Kuliah yang telah diplih</b></h4>
                                            <table id="tableMapping" class="table table-sorting table-hover  table-striped datatable" style="width:100%;">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Nama Matakuliah</th>                                     
                                                        <th>Study Program</th>
                                                        <th>Language Delivery</th>
                                                        <th>SKS</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th></th>
                                                        <th colspan="3">Total SKS</th>
                                                        <th id="credit_total">0</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <div class="table-primary" id="div_tab_accept" style="display: none;">
                                            <h4><b>Daftar Mata Kuliah yang disetujui</b></h4>
                                            <table id="tableAccepted" class="table table-sorting table-hover  table-striped datatable" style="width:100%;">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Matakuliah</th>                                     
                                                        <th>Study Program</th>
                                                        <th>Language Delivery</th>
                                                        <th>SKS</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="3">Total SKS</th>
                                                        <th id="credit_total_accepted">0</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>   

                    <div class="actions">
                        <!-- <button type="button" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</button> -->
                        <button type="submit" value="program_selection" class="btn btn-primary btn-next">Save & Next <i class="fa fa-arrow-right"></i></button>
                    </div>

                    </form>
                </div>

            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>
    </div>

</div>

</div>


</div>
<!-- /main-content -->
</div>

</div>
<!-- /content-wrapper -->

<script type="text/javascript">

    var baseurl = '<?php echo base_url(); ?>';
    $(document).ready(function(){
        var arr = [];
        $("#enrollment-academic").hide();
        $("#course-container").hide();
        $("#subject-container").hide();
        $("#study-program-container").hide();
        $("#language-delivery-container").hide();
        $('div.product-chooser').on('click', '.product-chooser-item', function(){
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);
        });
        $.ajax({
            type:"post",
            dataType: 'json',
            url: "<?php echo base_url(); ?>ajax/getCountSubMapping",
            data:{ 
                filter:$("#participantidd").val()
            },
            success:function(data){
                if(data!=null){
                    $("#credit_total").html(data);
                }               
            },
            error: function(){
                alert("Invalide!");
            }
        }).done(function(){
           if(parseInt($("#credit_total").html())>0){
                $.ajax({
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getSubjectMapping",
                    data:{ 
                        filter:$("#participantidd").val()
                    },
                    success:function(data){
                        for(var i in data){
                            var tamp = {
                                'SUBJECTCODE' : data[i].SUBJECTCODE,
                                'CURICULUMYEAR' : data[i].CURICULUMYEAR,
                                'STUDYPROGRAMID' : data[i].STUDYPROGRAMID,
                                'ENROLLMENTID' : data[i].ENROLLMENTID                
                            };            
                            arr.push(tamp);
                        }
                        console.log(arr);   
                    },
                    error: function(){
                        alert("Invalide!");
                    }
                });
            }
        });  
        var table1 = $("#tableMapping").dataTable({
            "ajax": {
                "url": "<?php echo base_url()?>participant/dataTableSubjectMappingExchange",
                "type": "POST",
                "data" :{
                    'filter' : $("#select-enrollment").select2('val')
                }
            },
            "destroy": true,
            "columns": [
                { "data" : "SUBJECTCODE"},
                // { "data": "ACTION" },
                { "data": "SUBJECTNAME" , width: "55%"},
                { "data": "STUDYPROGRAMNAME", width: "30%" },
                { "data": "LANGUAGE" },
                { "data": "CREDIT" }
            ],
            "columnDefs": [
                { visible: false, targets: 0}
            ]
        });
        var table2 = $("#tableSubject").dataTable({
            "ajax": {
                "url": "<?php echo base_url()?>participant/dataTableSubjectExchange",
                "type": "POST",
                "data" :{
                    'filter' : ''
                }
            },
            "destroy": true,
            "columns": [
                { "data": "ACTION" },
                { "data": "SUBJECTNAME" , width: "55%"},
                { "data": "CREDIT" },                                        
                { "data": "STUDYPROGRAMNAME", width: "30%" },
                { "data": "LANGUAGE" }
            ]
        });
        <?php 
            if(!empty($participant['PROGRAMID']))
            { ?>
                loadEnrollment(<?php echo $participant['ENROLLMENTID']; ?>);
                <?php /*loadDegree(<?php echo $participant['PROGRAMID']?>, <?php echo $participant['DEGREEID'] ?>); */?>
            <?php 
            }
        ?>

        $('#select-enrollment').on("change", function(e) { 
            var value = $('#select-enrollment').select2('val');
            var program = $('#select-program').select2('val');
            //console.log('yeahh');
            var subjectstatus = $("#select-program").select2().find(":selected").data("subjectstatus");
            var maxcredit = $("#select-program").select2().find(":selected").data("maxcredit");
            var maxstudy = $("#select-program").select2().find(":selected").data("maxstudy");
            table1.fnClearTable();
            $("#credit_total").html(0);
            arr = [];
            if(value != ""){
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getProgramDegree",
                        data:{ enrollmentid:value},
                        success:function(response)
                        {
                            $('#degree-id').val(response.DEGREE);
                            $('#degree-id-hidden').val(response.DEGREEID);
                            $('#programdegree-id').val(response.PROGRAMDEGREEID);
                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );
            } else {
                //alert();
                $('#degree-id').val("");
                $('#degree-id-hidden').val("");
                $('#programdegree-id').val("");
                // $('.study-program-container').show();
                // $('#language-delivery-container').show();
                // $("#course-container").show();
            }
            $.ajax({
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>participant/deleteEnrollMapping",
                data:{ 
                },
                success:function(response){
                },
                error: function(){
                    alert("Invalide!");
                }
            }).done(function(){
                if(subjectstatus=='Y'){
                    table2.fnDestroy();
                    table2 = $("#tableSubject").dataTable({
                        "ajax": {
                            "url": "<?php echo base_url()?>participant/dataTableSubjectExchange",
                            "type": "POST",
                            "data" :{
                                'filter' : value
                            }
                        },
                        "destroy": true,
                        "columns": [
                            { "data": "ACTION" },
                            { "data": "SUBJECTNAME" , width: "55%"},
                            { "data": "CREDIT" },
                            { "data": "STUDYPROGRAMNAME", width: "30%" },
                            { "data": "LANGUAGE" }
                        ]
                    });
                    console.log(maxcredit);//deleteEnrollMapping
                    console.log(maxstudy);
                    $("#info_subject").empty();
                    $("#info_subject").append('Hanya diperkenankan mengambil maksimal '+maxcredit+' SKS dan '+maxstudy+' Program Studi.');
                    $("#subject-container").show();
                    $("#study-program-container").hide();
                    $('#language-delivery-container').hide();
                }else if(subjectstatus!='Y' && $("#programtypee").val()=='ACADEMIC'){
                    $("#subject-container").hide();
                    $('#select-studyprogram').empty();
                    $("#select-studyprogram").append($('<option>', {value: '', text: '-- Select Study Program --'}));
                    $("#select-studyprogram").select2("val", '');
                    $("#study-program-container").show('slow');
                    $('#language-delivery-container').show();
                }else if(subjectstatus!='Y' && $("#programtypee").val()!='ACADEMIC'){
                    $("#subject-container").hide();
                    $('#course-container').show();
                    $('#language-delivery-container').show();
                }  
            })
            
        });
        // $("#inputForm").validate({
        //     errorPlacement: function(error, element) {
        //         if (element.attr("name") == "email" )
        //             error.insertAfter(".some-class");
        //         else if  (element.attr("name") == "phone" )
        //             error.insertAfter(".some-other-class");
        //         else
        //             error.insertAfter(element);
        //     }

        // });
       // Getstudy();
    function Getstudy(){
            var type = $("#select-program").select2().find(":selected").data("type");
            var subjectstatus = $("#select-program").select2().find(":selected").data("subjectstatus");
            var programdegreeid = $('#programdegree-id').val();
            var languagedeliveryid = $('#select-language').val();
            // alert (programdegreeid);
            if(programdegreeid != "" && languagedeliveryid != "")
            {               
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getStudyProgram",
                        data:{ programdegreeid:programdegreeid, languagedeliveryid:languagedeliveryid, type: type},
                        success:function(response)
                        {
                            var program = $('#select-program').select2('val');
                            if(subjectstatus!='Y' && type=='ACADEMIC'){
                                $("#study-program-container").show('slow');
                                $("#subject-container").hide();
                                $('#select-studyprogram').empty();
                                $("#select-studyprogram").append($('<option>', {value: '', text: '-- Select Study Program --'}));
                                $.each(response, function(index, value) {
                                    $("#select-studyprogram").append($('<option>', {value: value['STUDYPROGRAMID'], text: value['STUDYPROGRAMNAME']}));
                                });
                                $("#select-studyprogram").select2("val", '');
                            }else if(subjectstatus=='Y' && type=='ACADEMIC'){
                                $("#subject-container").show();
                                $("#study-program-container").hide();
                            }else{
                                $("#course-container").show('slow');
                                $('#select-course').empty();
                                $("#select-course").append($('<option>', {value: '', text: '-- Select Course --'}));
                                $.each(response, function(index, value) {
                                    $("#select-course").append($('<option>', {value: value['COURSEID'], text: value['COURSENAME']}));
                                });
                                $("#select-course").select2("val", '');
                                
                            }
                            
                            
                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );  
                
                }else {
              //  alert('Please select Enrollment data');
                $("#select-language").select2('val', "");
            }
    }  
    
        $('#select-language').on("change", function(e) { 
            Getstudy(); 
        });

         $('#select-studyprogram').on("change", function(e) { 
            var programdegreeid = $('#programdegree-id').select2('val');
            var languagedeliveryid = $('#select-language').select2('val');
            // alert(programdegreeid);
            if(programdegreeid != "" && languagedeliveryid != "")
            {
                $.ajax(
                    {
                        type:"post",
                        dataType: 'json',
                        url: "<?php echo base_url(); ?>ajax/getStudyProgram",
                        data:{ programdegreeid:programdegreeid, languagedeliveryid:languagedeliveryid},
                        success:function(response)
                        {
                            
                           for (i = 0; i < response.length; i++) {
                                    //alert(response[i].STUDYPROGRAMNAME);
                                } 
                            // $('#languagedeliverymapping-id').val(response.LANGUAGEDELIVERYMAPPINGID);
                            // $('#studyprogram-name').val(response.STUDYPROGRAMNAME);
                            // $('#studyprogram-id').val(response.STUDYPROGRAMID);

                        },
                        error: function() 
                        {
                            alert("Invalide!");
                        }
                    }
                );   
            } else {
                alert('Please select Study Program');
                $("#select-studyprogram").select2('val', "");
            }
        });

        $('#select-program').on("change", function(e) { 
            var value = $('#select-program').select2('val');
            var participantid = $("#participantid").val();
            //alert(participantid);die;
            var type = $("#select-program").select2().find(":selected").data("type");
            var subjectstatus = $("#select-program").select2().find(":selected").data("subjectstatus");
            //alert(subjectstatus);
            $("#statussubjectsetting").val(subjectstatus);
            $.ajax(
                {
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getListProgramEnrollment",
                    data:{ programId:value, participantid: participantid},
                    success:function(response)
                    {
                            $("#enrollment-academic").show('slow');
                            $('#select-enrollment').empty();
                            $("#select-enrollment").append($('<option>', {value: '', text: '-- Select Enrollment Program --'}));
                            $.each(response, function(index, value) {
                                $("#select-enrollment").append($('<option>', {value: value['ENROLLMENTID'], text: value['DESCRIPTION']}));
                            });
                            $("#select-enrollment").select2("val", '');
                            
                            $('#degree-id').val('');
                            
                            $('#select-course').empty();
                            $("#select-course").append($('<option>', {value: '', selected: 'selected', text: '-- Select Course --'}));
                            $("#select-course").select2("val", '');                          
                            $("#subject-container").hide();
                            arr = [];
                            if(subjectstatus=='Y'){
                               // $("#subject-container").show();
                                $("#study-program-container").hide();
                                $("#language-delivery-container").hide();
                            }else if(subjectstatus!='Y' && $("#programtypee").val()=='ACADEMIC'){
                                $('#select-language').empty();
                                $("#select-language").append($('<option>', {value: '', selected: 'selected', text: '-- Select Language --'}));
                                $("#select-language").append($('<option>', {value: '1', text: 'Indonesian'}));
                                $("#select-language").append($('<option>', {value: '2', text: 'English'}));
                                $("#select-language").select2("val", '');

                                $('#select-studyprogram').empty();
                                $("#select-studyprogram").append($('<option>', {value: '', selected: 'selected', text: '-- Select Study Program --'}));
                                $("#select-studyprogram").select2("val", '');
                                $("#study-program-container").show('slow');
                                $("#language-delivery-container").show('slow');
                            }else if(subjectstatus!='Y' && $("#programtypee").val()!='ACADEMIC'){
                                $("#course-container").show('slow');
                                $("#language-delivery-container").show('slow');
                            }                           
                        
                    },
                    error: function() 
                    {
                        alert("Invalide!");
                    }
                }
            );
        });

        $("#tableSubject").on('change',".right", function(e){
            var subjectcode = $(this).attr('id');
            var credit = $(this).attr('credit'); 
            var total = parseInt($("#credit_total").html());
            var maxstudy = parseInt($("#select-program").select2().find(":selected").data("maxstudy"));
            var maxcredit = parseInt($("#select-program").select2().find(":selected").data("maxcredit"));
            var studyprogramname = $(this).attr('studyprogramname'); 
            var studyprogramid = $(this).attr('studyprogramid');
            console.log(arr);
            if($(this).is(':checked')){
                var cek=true;
                var credit_total = total+parseInt(credit);
                if(credit_total>maxcredit){
                    cek=false;                    
                    alert('Maksimal SKS yang diambil adalah '+maxcredit);
                }

                if(cek){                    
                    var curiculum = $(this).attr('curiculum'); 
                    var enrollmentid = $(this).attr('enrollmentid');    
                    var subjectname = $(this).attr('subjectname'); 
                    var language = $(this).attr('language');
                    
                    $('#tableMapping').DataTable().row.add({
                        'SUBJECTCODE' : subjectcode,
                        // 'ACTION': '<input type="checkbox" class="left" name="" credit="'+credit+'" subjectname="'+subjectname+'" studyprogramname="'+studyprogramname+'" studyprogramid="'+studyprogramid+'" curiculum="'+curiculum+'" enrollmentid="'+enrollmentid+'">',
                        'SUBJECTNAME' :subjectname,                    
                        'STUDYPROGRAMNAME' : studyprogramname,
                        'LANGUAGE' : language,
                        'CREDIT' : credit
                    }).draw();
                    var count = $('#tableMapping').DataTable()
                        .column(2)
                        .data()
                        .unique();
                    if(count.length>maxstudy){
                        var active = $('#tableMapping').DataTable().rows( function ( idx, data, node ) {
                          //console.log(data['INDEX']);
                            return data['SUBJECTCODE'] === subjectcode;
                        }).remove().draw();
                        $(this).prop( "checked", false );
                        // credit_total = total-parseInt(credit);
                        // $("#credit_total").html(credit_total);
                        alert('Maksimal Program Studi yang diambil adalah '+maxstudy);
                    }else{
                        $("#credit_total").html(credit_total);
                        var tamp = {
                            'SUBJECTCODE' : subjectcode,
                            'CURICULUMYEAR' : curiculum,
                            'STUDYPROGRAMID' : studyprogramid,
                            'ENROLLMENTID' : enrollmentid                
                        };            
                        arr.push(tamp); 
                    }
                    
                }else{
                    $(this).prop( "checked", false );
                }
            }else{
                var active = $('#tableMapping').DataTable().rows( function ( idx, data, node ) {
                  //console.log(data['INDEX']);
                    return data['SUBJECTCODE'] === subjectcode &&  data['STUDYPROGRAMNAME'] === studyprogramname;
                }).remove().draw();
                for (var i = 0; i < arr.length; i++){
                    if (arr[i].SUBJECTCODE === subjectcode && arr[i].STUDYPROGRAMID === studyprogramid) { 
                        arr.splice(i, 1);
                        break;
                    }
                }
                var credit_total = total-parseInt(credit);
                $("#credit_total").html(credit_total);
            }
            console.log(arr);
        });

        // $('#inputForm').submit(function() {
        //     alert();
        //     $("#inputForm").submit();
        // });
        $('#inputForm').submit(function(e) {//$(document).submit('#inputForm',function(e){//$('#inputForm').submit(function(e) { 
            e.preventDefault();
            console.log(arr);
            if(arr.length>0 && $("#acceptstatus").val()==''){
                var idx=1;
                for(var i in arr){
                    $.ajax({ 
                        type: 'post',
                        dataType: 'json',
                       // async: true,
                        url: "<?php echo base_url(); ?>participant/enrollSubMapping",
                        data:{
                            'subjectcode' : arr[i].SUBJECTCODE,
                            'curiculum' : arr[i].CURICULUMYEAR,
                            'studyprogramid' : arr[i].STUDYPROGRAMID,
                            'enrollmentid' : arr[i].ENROLLMENTID,
                            'index' : i
                        },
                        success: function(data) { // your success handler
                            // console.log(parseInt(data)+1);
                            // console.log(arr.length);
                            // if((parseInt(data)+1)==arr.length){
                            //     $('#inputForm').unbind('submit');
                            //     $('#inputForm').submit();
                            // }
                            
                        },
                        complete : function() {
                            // console.log(parseInt(i));
                            console.log(idx);
                        if(parseInt(idx) === arr.length){                            
                            console.log("yeahh");
                            setTimeout(function() { 
                                $('#inputForm').unbind('submit');
                                $('#inputForm').submit();
                            }, 1000);
                        }
                        idx=idx+1;
                        }
                    })//.done(function(){
                        // console.log(arr.length);
                        
                    //})

                }                
            }else{
                $('#inputForm').unbind('submit');
                $('#inputForm').submit();
            }                
        });
            
    });
    //ini untuk edit; beda dengan yang diatas
    function loadEnrollment(enrollment_id) {
        var value = $('#select-program').select2('val');
        var participantid = $("#participantid").val();
        var type = $("#select-program").select2().find(":selected").data("type");
        var subjectstatus = $("#select-program").select2().find(":selected").data("subjectstatus");
        var maxcredit = $("#select-program").select2().find(":selected").data("maxcredit");
        var maxstudy = $("#select-program").select2().find(":selected").data("maxstudy");
        arr = [];
        $("#statussubjectsetting").val(subjectstatus);
            $.ajax(
                {
                    type:"post",
                    dataType: 'json',
                    url: "<?php echo base_url(); ?>ajax/getListProgramEnrollment",
                    data:{ programId:value, participantid: participantid},
                    success:function(response)
                    {
                            $("#enrollment-academic").show('slow');
                            $('#select-enrollment').empty();
                            $("#select-enrollment").append($('<option>', {value: '', text: '-- Select Enrollment Program --'}));
                            $.each(response, function(index, value) {
                                $("#select-enrollment").append($('<option>', {value: value['ENROLLMENTID'], text: value['DESCRIPTION']}));
                                
                            });
                            //if()
                            $("#select-enrollment").select2("val", enrollment_id);

                            if(subjectstatus=='Y'){
                                $("#tableSubject").dataTable().fnDestroy();
                                table2 = $("#tableSubject").dataTable({
                                    "ajax": {
                                        "url": "<?php echo base_url()?>participant/dataTableSubjectExchange",
                                        "type": "POST",
                                        "data" :{
                                            'filter' : enrollment_id
                                        }
                                    },
                                    "destroy": true,
                                    "columns": [
                                        { "data": "ACTION" },
                                        { "data": "SUBJECTNAME" , width: "55%"},
                                        { "data": "CREDIT" },
                                        { "data": "STUDYPROGRAMNAME", width: "30%" },
                                        { "data": "LANGUAGE" }
                                    ]
                                });                                
                                console.log(maxcredit);
                                console.log(maxstudy);    
                                $("#info_subject").append('Hanya diperkenankan mengambil maksimal '+maxcredit+' SKS dan '+maxstudy+' Program Studi.');
                                $("#subject-container").show();
                                $("#study-program-container").hide();
                                $('#language-delivery-container').hide();                                
                            }else if(subjectstatus!='Y' && $("#programtypee").val()=='ACADEMIC'){
                                $("#subject-container").hide();
                                // $('#select-studyprogram').empty();
                                // $("#select-studyprogram").append($('<option>', {value: '', text: '-- Select Study Program --'}));
                                // $("#select-studyprogram").select2("val", '');
                                $("#study-program-container").show('slow');
                                $('#language-delivery-container').show();
                            }else if(subjectstatus!='Y' && $("#programtypee").val()!='ACADEMIC'){
                                $("#subject-container").hide();
                                $('#course-container').show();
                                $('#language-delivery-container').show();
                            } 


                            loadDegree(enrollment_id);
                        
                    },
                    error: function() 
                    {
                        alert("Invalide!");
                    }
                }
            ); 
    }

    function sortcourseHtml(id, title, description, img_url, selected, checked)
    {
        return '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">' +
            '<div class="product-chooser-item '+selected+' ">' +
                '<a href="'+img_url+'" target="_blank"><img src="'+img_url+'" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="'+title+'"></a>' +
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">' +
                    '<span class="title">'+title+'</span>' +
                    '<span class="description">'+description+'</span>' +
                    '<input type="radio" name="enrollmentn_id" value="'+id+'" '+checked+'>' +
                '</div>' +
                '<div class="clear"></div>' +
            '</div>' +
        '</div>';

        /*var tes1 = '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">' +
            '<div class="product-chooser-item">' +
                '<img src="'+img_url+'" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="'+title+'">' +
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">' +
                    '<span class="title">'+title+'</span>' +
                    '<span class="description">'+description+'</span>' +
                    '<input type="radio" name="enrollment_id" value="'+id+'">' +
                '</div>' +
                '<div class="clear"></div>' +
            '</div>' +
        '</div>';

        var tesxx = '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">'+
            '<div class="product-chooser-item selected">'+
                '<img src="http://renswijnmalen.nl/bootstrap/desktop_mobile.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile and Desktop">'+
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">'+
                    '<span class="title">Mobile and Desktop</span>'+
                    '<span class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</span>' +
                    '<input type="radio" name="product" value="mobile_desktop" checked="checked">'+
                '</div>'+
                '<div class="clear"></div>'+
            '</div>'+
        '</div>';

        var tesxx1 = '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">'+
            '<div class="product-chooser-item">'+
                '<img src="http://renswijnmalen.nl/bootstrap/desktop_mobile.png" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile and Desktop">'+
                '<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">'+
                    '<span class="title">Mobile and Desktop</span>'+
                    '<span class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</span>' +
                    '<input type="radio" name="product" value="mobile_desktop">'+
                '</div>'+
                '<div class="clear"></div>'+
            '</div>'+
        '</div>'; */

        //return tesxx + ' ' + tesxx1;
    }

    function loadDegree(enrollment_id)
    {
        var type = $("#select-program").select2().find(":selected").data("type");
        var subjectstatus = $("#select-program").select2().find(":selected").data("subjectstatus");
        var acceptstatus = $("#acceptstatus").val();

        $.ajax(
            {
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getProgramDegree",
                data:{ enrollmentid:enrollment_id},
                success:function(response)
                {
                    $('#degree-id').val(response.DEGREE);
                    $('#degree-id-hidden').val(response.DEGREEID);
                    $('#programdegree-id').val(response.PROGRAMDEGREEID);
                    if(type=='ACADEMIC' && $("#subjectstatussetting").val()!='Y'){
                        $('.study-program-container').show();
                    }else if(type!='ACADEMIC' && $("#subjectstatussetting").val()!='Y'){
                        $('#course-container').show();
                    }                        
                       // $('#language-delivery-container').show();
                    if(acceptstatus!=''){
                        $("#select-program").attr("disabled", "disabled");
                        $("#select-enrollment").attr("disabled", "disabled");
                        $("#degree-id").attr("disabled", "disabled");
                        $("#select-language").attr("disabled", "disabled");
                        $("#select-studyprogram").attr("disabled", "disabled");
                        $("#select-course").attr("disabled", "disabled");
                        //
                        if(subjectstatus=='Y'){
                            //alert(subjectstatus);
                            
                            var table3 = $("#tableAccepted").dataTable({
                                "ajax": {
                                    "url": "<?php echo base_url()?>participant/dataTableSubjectExAccepted",
                                    "type": "POST",
                                    "data" :{
                                        // 'filter1' : $("#participantidd").val(),
                                        // 'filter2' : $("input[name='after_studyprogramid']").val()
                                    }
                                },
                                "destroy": true,
                                "columns": [
                                    { "data": "SUBJECTNAME" , width: "55%"},                  
                                    { "data": "STUDYPROGRAMNAME", width: "30%" },
                                    { "data": "LANGUAGE" },
                                    { "data": "CREDIT" }
                                ]
                            });
                            $("#div_tab_accept").show();
                            $("#div_tab_sub").hide();
                            $("#div_tab_map").hide();
                            $("#study-program-container").show();
                            $.ajax({
                                type:"post",
                                dataType: 'json',
                                url: "<?php echo base_url(); ?>ajax/getCountSubMapping2",
                                data:{ 
                                    filter1:$("#participantidd").val()//,
                                    //filter2:$("input[name='after_studyprogramid']").val()
                                },
                                success:function(data){
                                    if(data!=null){
                                        $("#credit_total_accepted").html(data);
                                    }               
                                },
                                error: function(){
                                    alert("Invalide!");
                                }
                            })
                        }
                    }
                    
                },
                error: function() 
                {
                    alert("Invalide!");
                }
            }
        );
    }
    

    <?php /*
    function loadDegree(program_id, degree_id) {
        $.ajax(
            {
                type:"post",
                dataType: 'json',
                url: "<?php echo base_url(); ?>ajax/getListProgramDegree",
                data:{ programId:program_id},
                success:function(response)
                {
                    $('#select-degree').empty();
                    $("#select-degree").append($('<option>', {value: 0, text: 'Select Degree Program'}));
                    $.each(response, function(index, value) {
                        $("#select-degree").append($('<option>', {value: value['DEGREEID'], text: value['DEGREE']}));
                    });
                    $("#select-degree").select2("val", degree_id);
                },
                error: function() 
                {
                    alert("Invalide!");
                }
            }
        );
    } */ ?>
</script>
<!-- <script src="<?php echo base_url();?>themes/_assets/js/plugins/wizard/wizard.min.js"></script> -->


