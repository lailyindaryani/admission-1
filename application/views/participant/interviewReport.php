
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="#">Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layouts</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Interview Report</h2>
    <em>Report Interview Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
                <h3><i class="fa fa-edit"></i> This is Your Interview Report </h3>
            </div>
            <div class="widget-content">
            <?php $this->load->view('includes/messages'); ?>
                <div class="wizard-wrapper">
                    <?php $this->load->view('participant/_headerStep', $active); ?>
                    <div class="step-content">
                        <div class="step-pane active" id="step1">
                            <form class="form-horizontal" role="form" id="inputForm" method="post" action="<?php echo base_url()?>participant/educationDataProcess">
                                <fieldset>
                                    <legend>Educational Data</legend>
                                    <!-- INTERVIEW TAB CONTENT -->
                                        <div class="tab-pane activity" id="interview-tab">
                                            <div class="project-section activity">
                                                <form action="<?php echo base_url()?>staff/updateInterview" method="post" class="form-horizontal" role="form">
                                                    <fieldset>
                                                <!--    <div class="form-group">
                                                        <label for="ticket-type" class="col-sm-2 control-label">Interview Status</label>
                                                        <div class="col-sm-4">
                                                           <select name="interviewStatus" class="form-control"  disabled>
                                                               <option value="">-select status-</option>
                                                        <option value="Accept" <?php if($participant['INTERVIEWSTATUS'] == 'Accept') echo 'selected';  ?>>Accept</option>
                                                        <option value="Reject" <?php if($participant['INTERVIEWSTATUS'] == 'Reject') echo 'selected';  ?>>Reject</option>
                                                           </select>
                                                        </div>
                                                    </div>-->
													

                                                    <div class="form-group">
                                                        <label for="ticket-type" class="col-sm-2 control-label">Interview Date</label>
                                                        <div class="col-sm-4">
                                                            <input type="date" class="form-control" name="interviewDate" value="<?=str_replace(' ','',str_replace('00:00:00','',$participant['INTERVIEWDATE']));?>" disabled>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="ticket-type" class="col-sm-2 control-label">&nbsp;</label>
                                                    </div>
                                            </div>
                                        </div>
                                    <!-- END DATA VERIFICATION TAB CONTENT -->
                                </fieldset>
                    <div class="actions">
                        <a href="<?php echo base_url()?>participant/summaryData/" type="button" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</a>
                        <a href="<?php echo base_url()?>participant/AcceptanceStatus/" type="button" class="btn btn-primary btn-next">Next&nbsp; &nbsp;<i class="fa fa-arrow-right"></i> </a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>
</div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<script type="text/javascript">
    $(document).ready(function(){


        $("#inputForm").validate();
    });

</script>
    
<!-- <script src="<?php echo base_url();?>themes/_assets/js/plugins/wizard/wizard.min.js"></script> -->


