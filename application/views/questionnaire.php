<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<title>Questionnaire | International Office</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="KingAdmin Dashboard">
	<meta name="author" content="The Develovers">

	<!-- CSS -->
	<link href="<?php echo base_url();?>themes/_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>themes/_assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>themes/_assets/css/main.css" rel="stylesheet" type="text/css">

	<!--[if lte IE 9]>
		<link href="<?php echo base_url();?>themes/_assets/css/main-ie.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>themes/_assets/css/main-ie-part2.css" rel="stylesheet" type="text/css" />
	<![endif]-->

	<!-- Fav and touch icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon72x72.png">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon57x57.png">
	<link rel="shortcut icon" href="<?php echo base_url();?>themes/_assets/ico/favicon.png">

</head>

<body>
	<div class="wrapper full-page-wrapper page-auth page-login text-center" style="background-color: #E2F3EB">
		<div class="inner-page">

			<div class="login-box center-block" style="background-color: #FFFFFF; width:50% !important">
                <div class="row">

	                <div class="col-sm-12" style="text-align: center">
	                    <img src="<?php echo base_url()?>images/io.png" style="width: 250px;margin:auto;">
	                <hr>
	                </div><br>
                    <div class="col-sm-12">

                        <div class="widget">
                            <div class="widget-header" style="background-color: rgb(15, 157, 88);">
                                <h3 style="color: #ffffff"><i class="fa fa-edit"></i> QUESTIONNAIRE</h3>
                            </div>
							<?php $this->load->view('includes/messages'); ?>
                            <div class="widget-content">
                                <form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>participant/savequestionnaire">
                                    <fieldset>
                                        <?php echo $views; ?>
                                    </fieldset>
                                    <div class="actions">
                                        <button type="submit" value="program_selection" class="btn btn-primary btn-next">Submit <i class="fa fa-arrow-right"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>

	<footer class="footer">&copy; <?php echo Date('Y'); ?> SISFO Telkom University</footer>

	<!-- Javascript -->
	<script src="<?php echo base_url();?>themes/_assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/bootstrap/bootstrap.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/plugins/modernizr/modernizr.js"></script>
</body>

</html>

