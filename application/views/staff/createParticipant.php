<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
    <div class="row">
        <div class="col-lg-4 ">
            <ul class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url().'staff/participants'?>">Home</a></li>
                <li class="active">Create Participants</li>
            </ul>
        </div>
    </div>

    <!-- main -->
    <div class="content">
        <div class="main-header">
            <h2>Create Participants</h2>
            <em>Participant Data</em>
        </div>

        <div class="main-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget">
                        <div class="widget-header">
                            <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
                        </div>

                        <div class="widget-content">
                            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>staff/createParticipantProcess">
                                <div class="form-group">
                                    <label for="ticket-subject" class="col-sm-2 control-label">Nationality</label>
                                    <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                            <select name="nationality" id="nationality" class="select2">
                                                <?php
                                                if(!empty($listCountry))
                                                {
                                                    foreach ($listCountry as $country) {
                                                        echo "<option data-ext='".$country['PHONECODE']."' value='".$country['COUNTRYNAMEENG']."'>".$country['COUNTRYNAMEENG']."</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-subject" class="col-sm-2 control-label">Full Name</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" placeholder="Fullname" id="fullname" name="fullname" class="form-control">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-subject" class="col-sm-2 control-label">Phone</label>
                                    <div class="col-sm-3" style="padding-right:0px !important">
                                        <div class="input-group">
                                            <input type="text" placeholder="Phone" id="phone-ext" name="phone_ext" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="text" placeholder="Phone" id="phone" name="phone" class="form-control" onkeypress="return _only_number_(event,this)">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-subject" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="email" placeholder="Email" id="email" name="email" class="form-control" required>
                                            <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-subject" class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ticket-subject" class="col-sm-2 control-label">Password Confirmation</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="password" placeholder="Password Confirmation" id="confirm-password" name="confirm_password" class="form-control">
                                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-sm-2">
                                <button class="btn btn-block btn-auth btn-primary" type="submit" value="register" name="register"><i class="fa fa-arrow-circle-o-right"></i>Save</button>
                                </div>
                        </div>
                                </form>
                        </div>
                    </div>
                    <!-- END SUPPORT TICKET FORM -->
                </div>
            </div>
    </div><!-- /main-content -->
    </div> <!-- /main -->
</div>
<!-- /content-wrapper -->

<script type="text/javascript">
    $(document).ready(function(){
        $('.select2').select2();
        $("select[name='nationality'").on('change', function(){
            alert();
            var phone_ext = $('#nationality option:selected').attr('data-ext');

            $('#phone-ext').val("+" + phone_ext);
        });
    });
</script>
