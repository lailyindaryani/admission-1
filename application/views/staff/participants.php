<!-- content-wrapper -->
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/plug-ins/1.10.19/filtering/type-based/accent-neutralise.js"></script>

<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="#">Home</a></li>
            <li class="active">Participants</li>
        </ul>
    </div>
</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Participants</h2>
    <em>Participant Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
                <!--<h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
            </div>

            <div class="row" style="border: 1px solid #ccc; margin:5px;">
                <div class="col-md-12">
                    <div class="widget-content">
                        <div class="row form-horizontal">
                            <div class="col-md-4">
                                <p>
                                    <span>Completeness of Document</span>
                                    <select id="requirement" name="requirement" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($requirements as $rq){ ?>
                                            <option value="<?=$rq['REQUIREMENTID']?>"><?=$rq['REQUIREMENTNAME']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>

                            <div class="col-md-4">
                                <p>
                                    <span>Enrollment</span>
                                    <select id="enrollment" name="enrollment" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($enrollments as $row){
                                            $selectedenrollment = $row['ENROLLMENTID'] == $enrollmentId ? "selected" : "";
                                            ?>
                                            <option value="<?=$row['ENROLLMENTID']?>" <?=$selectedenrollment?>><?=$row['DESCRIPTION']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>

                            <div class="col-md-4">
                                <p>
                                    <span>Nationality</span>
                                    <select id="country" name="country" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($countries as $row){
                                            ?>
                                            <option value="<?=$row['COUNTRYID']?>"><?=$row['COUNTRYNAMEENG']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
                        </div>

                        <div class="row form-horizontal">
                            <div class="col-md-4">
                                <p>
                                    <span>Acceptance Status</span>
                                    <select id="acceptance" name="acceptance" class="filter">
                                        <option value="">-All-</option>
                                        <option value="ACCEPTED">Accepted</option>
                                        <option value="UNACCEPTED">Not Accepted</option>
                                    </select>
                                </p>
                            </div>

                            <div class="col-md-4">
                                <p>
                                    <span>Study Program</span>
                                    <select id="studyprogram" name="studyprogram" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($studyprograms as $sp){ ?>
                                            <option value="<?=$sp['STUDYPROGRAMID']?>"><?=$sp['STUDYPROGRAMNAME']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>

                            <div class="col-md-4">
                                <p>
                                    <span>Period</span>
                                    <select id="period" name="period" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($periods as $pr){ ?>
                                            <option value="<?=$pr['PERIOD']?>"><?=$pr['PERIOD']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
                        </div>
                        <div class="row form-horizontal">
                            <div class="col-md-4">
                                <p>
                                    <span>Faculty</span>
                                    <select name="faculty" id="faculty" class="filter">
                                        <option value="">-All-</option>
                                        <?php foreach ($faculties as $faculty){?>
                                            <option value="<?=$faculty['FACULTYID']?>"><?=$faculty['FACULTYNAMEENG']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>

                            <div class="col-md-4">
                                <p>
                                    <span>Program Type</span>
                                    <select name="program-type" id="program-type" class="filter">
                                        <option value="">-All-</option>
                                        <option value="ACADEMIC">Academic</option>
                                        <option value="NON_ACADEMIC">Non Academic</option>
                                    </select>
                                </p>
                            </div>
							<div class="col-md-4">
                                <p>
                                    <span>School Year</span>
                                    <select name="school-year" id="school-year" class="filter">
                                        <option value="">-All-</option>
										<?php foreach ($schoolyears as $schoolyear){?>
                                            <option value="<?=$schoolyear['SCHOOLYEAR']?>"><?=$schoolyear['SCHOOLYEAR']?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
            <div class="widget-content">
                <?php $this->load->view('includes/messages'); ?>

                <div class="col-lg-2" id="export-container">
                </div>
                <div class="col-lg-12">
                    <div class="top-content">
                        <ul class="list-inline quick-access">
                            <li>
                                <h4>Number of participants: <em id="participant-number">4</em></h4>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table-basic">
                    <table id="tableProgram" class="table table-sorting table-hover table-striped datatable">
                        <thead>
                        <tr>
                            <th >No</th>
                            <th >Fullname</th>
                            <th >Passport</th>
                            <th >Gender</th>
                            <th >Nationality</th>
                            <th >Period</th>
                            <th >Status</th>
                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty">Loading data from server</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->




<script type="text/javascript">
    $(document).ready(function() {
		var click=0;
		$('#acceptance, #school-year').on('change', function(){
			var schoolyears= $('#school-year').val().replace(/[#_&?@%]/g,'');
			/*var schoolyear='';
			if(!schoolyears.match('/')){
				schoolyear = $('#school-year').val().replace('/','_');
			}		*/	
			
			if($('#acceptance').val()=='ACCEPTED' && $('#school-year').val()!=''){
				if(click==0){
					$("#export-container").append('<a type="button" href="<?php echo base_url()?>staff/exportExcel/'+schoolyears+'" id="export" class="btn btn-success" >Export Excel</a>');
				}else{
					$('#export').remove();
					$("#export-container").append('<a type="button" href="<?php echo base_url()?>staff/exportExcel/'+schoolyears+'" id="export" class="btn btn-success" >Export Excel</a>');
				}
				click++;
			}else if($('#acceptance').val()=='' || $('#school-year').val()==''){
				$('#export').remove();
			}	
		});	
		$('#requirement, #studyprogram, #enrollment, #faculty, #program-type, #country, #period').on('change', function(){
			$('#export').remove();
		});
		
        $("#requirement").select2();
        $("#acceptance").select2();
        $("#studyprogram").select2();
        $("#enrollment").select2();
        $("#faculty").select2();
        $("#program-type").select2();
        $("#country").select2();
        $("#period").select2();
		 $("#school-year").select2();

        var enrollmentid    = "";
        var studyprogramid  = "";
        var documentid      = "";
        var acceptance      = "";
        var programtype     = "";
        var country         = "";
        var period          = "";

        $(".filter").on("change", function () {
            $('#tableProgram').DataTable().ajax.reload();
        });

        var dt = $('#tableProgram').DataTable( {

           "order": [],
            "columnDefs": [
			
                { "width": "25%", "targets": 1},
                { "width": "10%", "targets": 6},
                { "width": "15%", "targets": 7},
				 {
                    "targets"   : 7,
                    "data"      : 7
                }
            ],
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
			"bSearchable" : true,
            "aLengthMenu": [
                [-1,30, 40, 50, 100],
                ["All",30, 40, 50, 100, ]
            ],
            "fnDrawCallback": function() {

                //initAction();

            },
            "sAjaxSource": "<?php echo base_url()?>staff/dataTablePopulateParticipants",
            "fnRowCallback":
                function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    var participantLink = "<button onclick=\"location.href='<?php echo base_url()?>staff/participantDetail/" +aData[7]+ "'\" title='Participant Detail' class='btn btn-xs btn-primary btn-outline'><i class='fa fa-user'></i></button>";
					var loginAsParticipant = "<button onclick=\"location.href='<?php echo base_url()?>login/validateLogin/"+aData[8]+"'\" title='Login as this user' class='btn btn-xs btn-warning btn-outline'>Login as this user</button>";
                    var status = "<span class='label label-warning'>NOT SET</span>";

                    if(aData[6]=='ACCEPTED')
                        status = "<span class='label label-success'>ACCEPTED</span>";
                    else if(aData[6]=='UNACCEPTED')
                        status = "<span class='label label-default'>NOT ACCEPTED</span>";
					/*if(isNaN(aData[4])){
						aData[4] = aData[9];
					}*/
                  //  alert(aData[1]);
                    //var fullname = removeAccents(aData[1]);
                    $(nRow).html(
                        '<td>'+aData[0]+'</td>' +
                            "<td>"+aData[1]+"</td>" +
                            '<td>'+aData[2]+'</td>' +
                            '<td>'+aData[3]+'</td>' +
                            '<td>'+aData[4]+'</td>' +
                            '<td>'+aData[5]+'</td>' +
                            '<td>'+status+'</td>' +
                            '<td>'+participantLink+' '+loginAsParticipant+'</td>'
							
                    );
                    return nRow;
                },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                    { "name": "enrollmentid", "value": $("#enrollment").val() },
                    { "name": "studyprogramid", "value": $("#studyprogram").val() },
                    { "name": "acceptance", "value": $("#acceptance").val() },
                    { "name": "faculty", "value": $("#faculty").val() },
                    { "name": "documentid", "value": $("#requirement").val() },
                    { "name": "programtype", "value": $("#program-type").val() },
                    { "name": "country", "value": $("#country").val() },
                    { "name": "period", "value": $("#period").val() },
					{ "name": "schoolyear", "value": $("#school-year").val() }
                );
                $.getJSON( sSource, aoData, function (json) {
                    /* Do whatever additional processing you want on the callback, then tell DataTables */
                    fnCallback(json);
                    $("#participant-number").text(json.iTotalRecords)
                    // console.log(json.iTotalRecords);
                } );
            }
        } );

        $("input[type='search']").on('keyup',function(){
            $('#tableProgram').DataTable().destroy();
            $('#tableProgram').DataTable().search(this.value,true).draw();
        });
/*
        function removeAccents(data){
            return data
                .replace(/[áÁàÀâÂäÄãÃåÅæÆ]/g, 'a')
                .replace(/[çÇ]/g, 'c')
                .replace(/[éÉèÈêÊëË]/g, 'e')
                .replace(/[íÍìÌîÎïÏîĩĨĬĭ]/g, 'i')
                .replace(/[ñÑ]/g, 'n')
                .replace(/[óÓòÒôÔöÖœŒ]/g, 'o')
                .replace(/[ß]/g, 's')
                .replace(/[úÚùÙûÛüÜ]/g, 'u')
                .replace(/[ýÝŷŶŸÿ]/g, 'n')
                .replace(/[áÁàÀâÂäÄãÃåÅæÆ]/g, 'a')
                .replace(/[çÇ]/g, 'c')
                .replace(/[éÉèÈêÊëË]/g, 'e')
                .replace(/[íÍìÌîÎïÏîĩĨĬĭ]/g, 'i')
                .replace(/[ñÑ]/g, 'n')
                .replace(/[óÓòÒôÔöÖœŒ]/g, 'o')
                .replace(/[ß]/g, 's')
                .replace(/[úÚùÙûÛüÜ]/g, 'u')
                .replace(/[ýÝŷŶŸÿ]/g, 'n');
        }*/

    });
</script>
