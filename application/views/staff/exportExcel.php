<?php

ob_end_clean();
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=AcceptedReport.xls");
date_default_timezone_set('Asia/Jakarta');
?>
	<table height="10" width="100%">
        <thead>
            <tr>
                <td>iduser</td>
                <td>nomorpeserta</td>
                <td>nama</td>
                <td>hp</td>
                <td>email</td>
                <td>tempatlahir</td>
				<td>tanggallahir</td>
				<td>gender</td>
				<td>rumah</td>
				<td>kotarumah</td>
				<td>propinsirumah</td>
				<td>kodeposrumah</td>
				<td>sma</td>
				<td>kotasma</td>
				<td>propinsisma</td>
				<td>idjalur</td>
				<td>jalur</td>
				<td>idprodi</td>
				<td>namaprodi</td>
				<td>skorsmbb</td>
				<td>up3</td>
				<td>bpp</td>
				<td>sdp2</td>
				<td>asrama</td>
				<td>diskonup3</td>
				<td>diskonbpp</td>
				<td>diskonsdp2</td>
				<td>diskonasrama</td>
				<td>asuransi</td>
				<td>keterangan</td>
				<td>type</td>
				<td>enddate</td>
				<td>oldstudentid</td>
				<td>pilihanke</td>
				<td>password</td>
				<td>tahunajaran</td>
            </tr>
        </thead>
		<tbody>
		<?php foreach($participant as $val){
			echo "<tr>";
            echo "<td>".$val['ADMISSIONID']."</td>";
            echo "<td></td>";
			echo "<td>".$val['FULLNAME']."</td>";
			echo "<td>'".$val['PHONE']."'</td>";
			echo "<td>".$val['EMAIL']."</td>";
			echo "<td>".$val['BIRTHPLACE']."</td>";
			echo "<td>'".str_replace('00:00:00','',$val['BIRTHDATE'])."'</td>";
			echo "<td>".$val['GENDER']."</td>";
			echo "<td>".$val['ADDRESS']."</td>";
			echo "<td>".$val['CITY']."</td>";
			echo "<td></td>";
			echo "<td>".$val['ZIPCODE']."</td>";
			echo "<td>".$val['SCHOOLNAME']."</td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td>32</td>";
			echo "<td>INTERNATIONAL</td>";
			echo "<td>".$val['STUDYPROGRAMID']."</td>";
			echo "<td>".$val['STUDYPROGRAMNAME']."</td>";
			echo "<td>".$val['INTERVIEWSCORE']."</td>";
			echo "<td>".$val['ADMISSIONFEE']."</td>";
			echo "<td>".$val['TUITIONFEE']."</td>";
			echo "<td>0</td>";
			echo "<td>0</td>";
            if($val['SCHOLARSHIPTYPEID']!=4 && $val['SCHOLARSHIPTYPEID']!=0){
                echo "<td>".$val['ADMISSIONFEE']."</td>";
                echo "<td>".$val['TUITIONFEE']."</td>";
            }else if($val['SCHOLARSHIPTYPEID']==4){
                echo "<td>".$val['ADMISSIONFEE']."</td>";
                echo "<td>0</td>";
            }else{
			    echo "<td>0</td>";
                echo "<td>0</td>";
            }
			echo "<td>0</td>";
			echo "<td>0</td>";
			echo "<td>0</td>";
			echo "<td>".$val['PROGRAMNAME']."</td>";
			echo "<td></td>";
            if($val['PAYMENTDATE']=='0000-00-00 00:00:00'){
                echo "<td>'0000-00-00'</td>";
            }else{
             echo "<td>'".date('Y-m-d',strtotime($val['PAYMENTDATE']))."'</td>";
             }
			echo "<td></td>";
			echo "<td>1</td>";
			echo "<td>".$val['PASSWORD']."</td>";
			echo "<td>".$val['SCHOOLYEAR']."</td>";
            echo "</tr>";
		}?>	
		</tbody>
	</table>
	<?php die;?>