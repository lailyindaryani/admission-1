            <?php date_default_timezone_set('Asia/Jakarta'); ?>
			<form action="<?php echo base_url()?>staff/updateProfileInfo" class="form-horizontal modal-form" role="form" method="post" id="edit-profile-info-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Profile Info</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="participantid" id="participantid" value="<?php echo $participant['PARTICIPANTID']?>">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Full Name</label>
                                <div class="col-sm-9 ">
                                    <input type="text" onpaste="return blockSpecialChar(event)" onkeypress="return blockSpecialChar(event)" class="form-control" name="fullname" id="fullname" placeholder="Full Name" value="<?php echo $participant['FULLNAME']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Passport Number</label>
                                <div class="col-sm-9 ">
                                    <input type="text" class="form-control" name="passportnumber" id="passportnumber" placeholder="Passport Number" value="<?php echo $participant['PASSPORTNO']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Valid Until</label>
                                <div class="col-sm-9 ">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" name="validuntil" id="validuntil" placeholder="Valid Until" value="<?php echo date('m/d/Y',strtotime($participant['PASSPORTVALID']))?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Issued By</label>
                                <div class="col-sm-9 ">
                                    <input type="text" class="form-control" name="issuedby" id="issuedby" placeholder="Issued By" value="<?php echo $participant['PASSPORTISSUED']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Birth Place</label>
                                <div class="col-sm-9 ">
                                    <input type="text" class="form-control" name="birthplace" id="birthplace" placeholder="Birth Place" value="<?php echo $participant['BIRTHPLACE']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Birth Date</label>
                                <div class="col-sm-9 ">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input id="birthdate" name="birthdate" class="form-control" placeholder="Enter birth date" type="text" value="<?php echo date('m/d/Y',strtotime($participant['BIRTHDATE']))?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Gender</label>
                                <div class="col-sm-9 ">
                                    <label class="control-inline fancy-radio col-sm-4">
                                        <input type="radio" name="gender" value="Male" <?php echo $participant['GENDER']=='Male'?'checked':''?>>
                                        <span><i></i>Male</span>
                                    </label>
                                    <label class="control-inline fancy-radio col-sm-4">
                                        <input type="radio" name="gender" value="Female" <?php echo $participant['GENDER']=='Female'?'checked':''?>>
                                        <span><i></i>Female</span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Marital Status</label>
                                <div class="col-sm-9 ">
                                    <label class="control-inline fancy-radio col-sm-4">
                                        <input type="radio" name="maritalstatus" value="married" <?php if($participant['MARITALSTATUS'] == 'married') echo 'checked';  ?>>
                                        <span><i></i>Married</span>
                                    </label>
                                    <label class="control-inline fancy-radio col-sm-4">
                                        <input type="radio" name="maritalstatus" value="single" <?php if($participant['MARITALSTATUS'] == 'single') echo 'checked';  ?>>
                                        <span><i></i>Single</span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Religion</label>
                                <div class="col-sm-9 ">
                                    <select name="religion" id="religion" class="">
                                        <option value="ISLAM" <?php if($participant['RELIGION'] == 'ISLAM') echo 'selected';  ?>>ISLAM</option>
                                        <option value="CRISTIAN" <?php if($participant['RELIGION'] == 'CRISTIAN') echo 'selected';  ?>>CRISTIAN</option>
                                        <option value="BUDDHA" <?php if($participant['RELIGION'] == 'BUDDHA') echo 'selected';  ?>>BUDDHA</option>
                                        <option value="HINDU" <?php if($participant['RELIGION'] == 'HINDU') echo 'selected';  ?>>HINDU</option>
                                        <option value="OTHER" <?php if($participant['RELIGION'] == '') echo 'selected';  ?>>OTHER</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Nationality</label>
                                <div class="col-sm-9 ">
                                    <select name="nationality" id="nationality" class="">
                                        <?php
                                        if(!empty($listCountry))
                                        {
                                            foreach ($listCountry as $country) {
                                                if($country['COUNTRYNAMEENG']==$participant['NATIONALITY'] || $country['COUNTRYID']==$participant['NATIONALITY'])
                                                    echo "<option value='".$country['COUNTRYNAMEENG']."' selected>".$country['COUNTRYNAMEENG']."</option>";
                                                else
                                                    echo "<option value='".$country['COUNTRYNAMEENG']."'>".$country['COUNTRYNAMEENG']."</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">First Language</label>
                                <div class="col-sm-9 ">
                                    <input type="text" class="form-control" name="firstlanguage" id="firstlanguage" placeholder="First Language" value="<?php echo $participant['FIRSTLANGUAGE']?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Proficiency In Bahasa</label>
                                <div class="col-sm-9 ">
                                    <label class="control-inline fancy-radio">
                                        <input type="radio" name="profiency_in_indonesia" value="Fluent" <?php if($participant['PROFICIENCYINA'] == 'Fluent') echo 'checked';  ?>>
                                        <span><i></i>Fluent</span>
                                    </label>
                                    <label class="control-inline fancy-radio">
                                        <input type="radio" name="profiency_in_indonesia" value="Good" <?php if($participant['PROFICIENCYINA'] == 'Good') echo 'checked';  ?>>
                                        <span><i></i>Good</span>
                                    </label>
                                    <label class="control-inline fancy-radio">
                                        <input type="radio" name="profiency_in_indonesia" value="Fair" <?php if($participant['PROFICIENCYINA'] == 'Fair') echo 'checked';  ?>>
                                        <span><i></i>Fair</span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Proficiency In English</label>
                                <div class="col-sm-9 ">
                                    <label class="control-inline fancy-radio">
                                        <input type="radio" name="profiency_in_english" value="Fluent" <?php if($participant['PROFICIENCYEN'] == 'Fluent') echo 'checked';  ?>>
                                        <span><i></i>Fluent</span>
                                    </label>
                                    <label class="control-inline fancy-radio">
                                        <input type="radio" name="profiency_in_english" value="Good" <?php if($participant['PROFICIENCYEN'] == 'Good') echo 'checked';  ?>>
                                        <span><i></i>Good</span>
                                    </label>
                                    <label class="control-inline fancy-radio">
                                        <input type="radio" name="profiency_in_english" value="Fair" <?php if($participant['PROFICIENCYEN'] == 'Fair') echo 'checked';  ?>>
                                        <span><i></i>Fair</span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9 ">
                                    <textarea class="form-control" rows="4" name="address" id="address" placeholder="Address"><?php echo $participant['ADDRESS']?></textarea>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">City</label>
                                <div class="col-sm-9 ">
                                    <input type="text" class="form-control" name="city" id="city" placeholder="City" value="<?php echo $participant['CITY']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Country</label>
                                <div class="col-sm-9 ">
                                    <select name="country" id="country" class="">
                                        <?php
                                        if(!empty($listCountry))
                                        {
                                            foreach ($listCountry as $country) {
                                                if($country['COUNTRYID']==$participant['COUNTRYID'])
                                                    echo "<option value='".$country['COUNTRYID']."' selected>".$country['COUNTRYNAMEENG']."</option>";
                                                else
                                                    echo "<option value='".$country['COUNTRYID']."'>".$country['COUNTRYNAMEENG']."</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Zip Code</label>
                                <div class="col-sm-9 ">
                                    <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zip Code" value="<?php echo $participant['ZIPCODE']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9 ">
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $participant['PHONE']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Fax</label>
                                <div class="col-sm-9 ">
                                    <input type="text" class="form-control" name="fax" id="fax" placeholder="Fax" value="<?php echo $participant['FAX']?>">
                                </div>
                            </div>

                        <!--    <div class="form-group">
                                <label for="" class="col-sm-3 control-label">About Me</label>
                                <div class="col-sm-9 ">
                                    <textarea class="form-control" name="note" rows="4"></textarea>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <button type="submit" id="update" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </form>

            <script type="text/javascript">
                $("#birthdate").datepicker({
                    defaultDate: new Date(),
                    autoclose: true
                });

                $("#validuntil").datepicker({
                    defaultDate: new Date(),
                    autoclose: true
                });

                $("#edit-profile-info-form #nationality").select2({
                    placeholder: "Select Country",
                    allowClear: true,
                });

                $("#edit-profile-info-form #religion").select2({
                    minimumResultsForSearch: -1,
                    placeholder: "Select Religion",
                    allowClear: true,
                });

                $("#edit-profile-info-form #country").select2({
                    placeholder: "Select Country",
                    allowClear: true,
                });
				function blockSpecialChar(e){
				var k;
				document.all ? k = e.keyCode : k = e.which;
				return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
				}
            </script>