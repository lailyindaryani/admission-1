<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
		<div class="col-lg-4 ">
				<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Home</a></li>
                        <li class="active">User Activation</li>
				</ul>
		</div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
		<h2>User Activation</h2>
		<em>User Data</em>
</div>

<div class="main-content">
<div class="row">
		<div class="col-md-12">
				<!-- SUPPOR TICKET FORM -->
				<div class="widget">
						<div class="widget-header">
								<!--<h3><i class="fa fa-edit"></i> Please complete the form data below</h3>-->
						</div>

						<div class="widget-content">
												<div class="table-basic">
														<table id="tableProgram" class="table table-sorting table-hover  table-striped datatable">
																<thead>
																<tr>
																		<th >No</th>
																		<th >Email</th>
																		<th >Fullname</th>																		
																		<th >Activation Status</th>
																		<th>Action</th>
																</tr>
																</thead>
																<tbody>
																<tr>
																		<td colspan="10" class="dataTables_empty">Loading data from server</td>
																</tr>
																</tbody>
														</table>
												</div>
						</div>
				</div>
			</div>
</div>
</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure you wish to delete this country?
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button id="delete-confirm-button" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
		$(document).ready(function() {
				$("#academicYear").select2();
				$("#program").select2();

				$(".filter").on("change", function (e) {
						console.log($("#program").val());
            			$('#tableProgram').DataTable().ajax.reload();
            	});

				var dt= $('#tableProgram').dataTable( {
						//"bJQueryUI": true,
						"order": [],
						/*  sDom: "T<'clearfix'>" +
						 "<'row'<'col-sm-6'l><'col-sm-6'f>r>"+
						 "t"+
						 "<'row'<'col-sm-6'i><'col-sm-6'p>>",
						 "tableTools": {

						 },*/
						"sPaginationType": "full_numbers",
						"bProcessing": true,
						"bServerSide": true,
						"aLengthMenu": [
							/*	[20, 30, 50, 100, -1],
								[20, 30, 50, 100, "All"]*/
								 [-1,30, 40, 50, 100],
                ["All",30, 40, 50, 100, ]
						],
						"fnDrawCallback": function() {

								//initAction();

						},
						"sAjaxSource": "<?php echo base_url(); ?>staff/dataTableUserActivation",
						"fnRowCallback":
								function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
										var modal3 = "<button type='button' class='aktivasi btn btn-primary' id='myBtn'>Open Modal</button>";
										var modal4 = "<button  class='aktivasi btn btn-success'>Activated<span class='badge badge-success'></span></button>";
                                        var act = "<button onclick=\"location.href='<?php echo base_url()?>staff/participantDetail/" +aData[7]+ "'\" title='Participant Detail' class='aktivasi btn btn-xs btn-primary btn-outline'><span class='badge badge-secondary'>Secondary</span></button>";
                                        //var status = "<span class='label label-warning'>Activated</span>";

                                      if(aData[3]=='Y')
                        				status = "<button onclick=\"location.href='<?php echo base_url()?>staff/updateUserActivation/" +aData[4]+ "'\"  title='Change Activation' class='aktivasi btn btn-danger'>Non Activated<span class=' badge badge-success'></span></button>";
                    				  else if(aData[3]=='N')
                        				status = "<button onclick=\"location.href='<?php echo base_url()?>staff/userNonactivated/" +aData[4]+ "'\" title='Change Activation' class='aktivasi btn btn-success'>Activated<span class=' badge badge-success'></span></button>";

										$(nRow).html(
														'<td>'+aData[0]+'</td>' +
														'<td>'+aData[1]+'</td>' +
														'<td>'+aData[2]+'</td>' +
														'<td>'+aData[3]+'</td>' +
														'<td>'+status+'</td>'

										);
										return nRow;
								},
						"fnServerData": function ( sSource, aoData, fnCallback ) {
								/* Add some extra data to the sender */
								aoData.push(
									{ "name": "academicyear", "value": $("#academicYear").val() },
									{ "name": "programid", "value": $("#program").val() },
								);
								$.getJSON( sSource, aoData, function (json) {
										/* Do whatever additional processing you want on the callback, then tell DataTables */
										fnCallback(json)
								} );
						}
				} );
			
			$("input[type='search']").on('keyup',function(){
				$('#tableProgram').DataTable().destroy();
				$('#tableProgram').DataTable().search(this.value,true).draw();
			});

				
		});
</script>
