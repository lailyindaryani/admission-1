<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<title>Login | IO Telkomuniversity</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="IO Telkom University Admission Application">
	<meta name="author" content="Sisfo Telkom University">

	<!-- CSS -->
	<link href="<?php echo base_url();?>themes/_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>themes/_assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>themes/_assets/css/main.css" rel="stylesheet" type="text/css">

	<!--[if lte IE 9]>
		<link href="<?php echo base_url();?>themes/_assets/css/main-ie.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>themes/_assets/css/main-ie-part2.css" rel="stylesheet" type="text/css" />
	<![endif]-->

	<!-- Fav and touch icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon72x72.png">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url();?>themes/_assets/ico/kingadmin-favicon57x57.png">


</head>

<body>
	<div class="wrapper full-page-wrapper page-auth page-login text-center">
		<div class="inner-page">

			<div class="login-box center-block" style="background-color: #FFFFFF">
                <div style="text-align: center">
                    <img src="<?php echo base_url()?>images/io.png" style="width: 250px;margin:auto;">
                </div><br>
				<?php $this->load->view('includes/messages'); ?>
				<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>login/validateLogin">
					<input type="hidden" name="type" value="<?php echo $type; ?>">
					<p class="title">Use your username</p>
					<div class="form-group">
						<label for="username" class="control-label sr-only">Username</label>
						<div class="col-sm-12">
							<div class="input-group">
								<input type="text" placeholder="Email" id="username" name="username" class="form-control">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
							</div>
						</div>
					</div>
					<label for="password" class="control-label sr-only">Password</label>
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								<input type="password" placeholder="password" id="password" name="password" class="form-control">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							</div>
						</div>
					</div>
					<label class="fancy-checkbox">
						<input type="checkbox">
						<span>Remember me next time</span>
					</label>
					<button class="btn  btn-lg btn-block btn-auth btn-danger" type="submit" value="login" name="login"><i class="fa fa-arrow-circle-o-right"></i> Login</button>
				</form>

				<div class="links">
					<p><a href="<?=base_url()?>login/forgotPassword">Forgot your Password?</a></p><br>
					<p><a href="<?php echo base_url()?>registration/<?php echo strtolower($type); ?>" class="btn btn-success">Create New Account</a></p>
				</div>
			</div>
		</div>
	</div>

	<footer class="footer">&copy; <?php echo Date('Y'); ?> SISFO Telkom University</footer>

	<!-- Javascript -->
	<script src="<?php echo base_url();?>themes/_assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/bootstrap/bootstrap.js"></script>
	<script src="<?php echo base_url();?>themes/_assets/js/plugins/modernizr/modernizr.js"></script>
</body>

</html>

