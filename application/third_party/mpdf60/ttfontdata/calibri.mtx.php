<?php
$name='Calibri';
$type='TTF';
$desc=array (
  'CapHeight' => 632,
  'XHeight' => 464,
  'FontBBox' => '[-476 -194 1214 952]',
  'Flags' => 4,
  'Ascent' => 952,
  'Descent' => -194,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 507,
);
$unitsPerEm=2048;
$up=-113;
$ut=65;
$strp=250;
$strs=65;
$ttffile='F:/XAMPP/htdocs/pdf _app/mpdf-6.1/ttfonts/Calibri.ttf';
$TTCfontID='0';
$originalsize=266111;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='calibri';
$panose=' 8 0 2 f 5 2 2 2 4 3 2 4';
$haskerninfo=false;
$haskernGPOS=true;
$hassmallcapsGSUB=true;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 750, -250, 221
// usWinAscent/usWinDescent = 952, -269
// hhea Ascent/Descent/LineGap = 750, -250, 221
$useOTL=255;
$rtlPUAstr='';
$GSUBScriptLang=array (
  'cyrl' => 'DFLT SRB  ',
  'grek' => 'DFLT ',
  'latn' => 'DFLT ROM  TRK  ',
);
$GSUBFeatures=array (
  'cyrl' => 
  array (
    'DFLT' => 
    array (
      'locl' => 
      array (
        0 => 2,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
    ),
    'SRB ' => 
    array (
      'locl' => 
      array (
        0 => 2,
        1 => 3,
      ),
      'salt' => 
      array (
        0 => 3,
        1 => 17,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
    ),
  ),
  'grek' => 
  array (
    'DFLT' => 
    array (
      'locl' => 
      array (
        0 => 1,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
    ),
  ),
  'latn' => 
  array (
    'DFLT' => 
    array (
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
      'ordn' => 
      array (
        0 => 26,
      ),
      'liga' => 
      array (
        0 => 27,
      ),
      'dlig' => 
      array (
        0 => 28,
      ),
    ),
    'ROM ' => 
    array (
      'salt' => 
      array (
        0 => 0,
        1 => 17,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
      'ordn' => 
      array (
        0 => 26,
      ),
      'liga' => 
      array (
        0 => 27,
      ),
      'dlig' => 
      array (
        0 => 28,
      ),
    ),
    'TRK ' => 
    array (
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 22,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
      'ordn' => 
      array (
        0 => 26,
      ),
      'liga' => 
      array (
        0 => 27,
      ),
      'dlig' => 
      array (
        0 => 28,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233300,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233318,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233332,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233362,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233376,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233612,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233732,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233804,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233886,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233922,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233956,
    ),
    'MarkFilteringSet' => '',
  ),
  11 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234006,
    ),
    'MarkFilteringSet' => '',
  ),
  12 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234042,
    ),
    'MarkFilteringSet' => '',
  ),
  13 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234106,
    ),
    'MarkFilteringSet' => '',
  ),
  14 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234200,
    ),
    'MarkFilteringSet' => '',
  ),
  15 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234294,
    ),
    'MarkFilteringSet' => '',
  ),
  16 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234350,
    ),
    'MarkFilteringSet' => '',
  ),
  17 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234406,
    ),
    'MarkFilteringSet' => '',
  ),
  18 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234504,
    ),
    'MarkFilteringSet' => '',
  ),
  19 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 235136,
    ),
    'MarkFilteringSet' => '',
  ),
  20 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 235712,
    ),
    'MarkFilteringSet' => '',
  ),
  21 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 235810,
    ),
    'MarkFilteringSet' => '',
  ),
  22 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 235828,
    ),
    'MarkFilteringSet' => '',
  ),
  23 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 235846,
    ),
    'MarkFilteringSet' => '',
  ),
  24 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 235918,
    ),
    'MarkFilteringSet' => '',
  ),
  25 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 236040,
    ),
    'MarkFilteringSet' => '',
  ),
  26 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 236348,
    ),
    'MarkFilteringSet' => '',
  ),
  27 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 236536,
    ),
    'MarkFilteringSet' => '',
  ),
  28 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 237098,
    ),
    'MarkFilteringSet' => '',
  ),
  29 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 237204,
    ),
    'MarkFilteringSet' => '',
  ),
  30 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 237310,
    ),
    'MarkFilteringSet' => '',
  ),
  31 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 237328,
    ),
    'MarkFilteringSet' => '',
  ),
  32 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 237342,
    ),
    'MarkFilteringSet' => '',
  ),
  33 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 237378,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'cyrl' => 'DFLT SRB  ',
  'grek' => 'DFLT ',
  'latn' => 'DFLT ROM  TRK  ',
);
$GPOSFeatures=array (
  'cyrl' => 
  array (
    'DFLT' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
    'SRB ' => 
    array (
      'kern' => 
      array (
        0 => 1,
      ),
    ),
  ),
  'grek' => 
  array (
    'DFLT' => 
    array (
      'kern' => 
      array (
        0 => 1,
      ),
    ),
  ),
  'latn' => 
  array (
    'DFLT' => 
    array (
      'kern' => 
      array (
        0 => 1,
      ),
    ),
    'ROM ' => 
    array (
      'kern' => 
      array (
        0 => 1,
      ),
    ),
    'TRK ' => 
    array (
      'kern' => 
      array (
        0 => 1,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 186330,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 2,
    'Flag' => 8,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 187218,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>