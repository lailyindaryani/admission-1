<?php

/*  -------- SETTING IMAGE UPLOAD  --------------  */

$config['images']['upload_path'] 		= "./media/images";
$config['images']['access_path'] 		= "/media/images/";
$config['images']['allowed_types'] 		= "jpg|png|jpeg";
$config['images']['overwrite'] 			= TRUE;
$config['images']['max_size'] 			= "1024000"; // Can be set to particular file size ; here it is 1 MB

/*  -------- END SETTING IMAGE UPLOAD  ----------  */

/*  -------- SETTING REQUIREMENT UPLOAD  --------------

$config['requirement']['upload_path'] 		= "./uploads/media/participant/requirement";
$config['requirement']['access_path'] 		= "/uploads/media/participant/requirement/";
$config['requirement']['allowed_types'] 	= "jpg|jpeg|pdf";
$config['requirement']['overwrite'] 		= TRUE;
$config['requirement']['max_size'] 			= "1024000"; // Can be set to particular file size ; here it is 1 MB

/*  -------- END SETTING REQUIREMENT UPLOAD  ----------  */

/*  -------- SETTING APP DOCUMENT UPLOAD  --------------  */

$config['documents']['upload_path'] 	= "./media/documents";
$config['documents']['access_path']     = "/media/documents/";
$config['documents']['allowed_types'] 	= "jpg|png|jpeg|pdf|doc|docx";
$config['documents']['overwrite'] 		= TRUE;
$config['documents']['max_size'] 		= "1024000"; // Can be set to particular file size ; here it is 1 MB

/*  -------- END SETTING APP DOCUMENT UPLOAD  ----------  */