function _only_number_ (evt,obj)
{
    var temp = obj.value;
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if(charCode == 46) temp += '.';
    tempArr = temp.split('.');
    if(tempArr.length > 2) return false;

    return ((charCode == 8) || (charCode >= 48) && (charCode <= 57));
}

function datepicker_model(element)
{
    $(element).datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });
}

function numeric_model(element)
{
    $(element).autoNumeric("init",{
        aSep: '.',
        aDec: ',', 
        aSign: 'Rp. '
    });
}