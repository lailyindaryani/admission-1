<div class="del-style-switcher">
	<div class="del-switcher-toggle toggle-hide"></div>
	<form>
		<section class="del-section del-section-skin">
			<h5 class="del-switcher-header">Choose Skins:</h5>

			<ul>
				<li><a href="#" title="Slate Gray" class="switch-skin slategray" data-skin="<?php echo STYLE_URL?>skins/slategray.css">Slate Gray</a></li>
				<li><a href="#" title="Dark Blue" class="switch-skin darkblue" data-skin="<?php echo STYLE_URL?>skins/darkblue.css">Dark Blue</a></li>
				<li><a href="#" title="Dark Brown" class="switch-skin darkbrown" data-skin="<?php echo STYLE_URL?>skins/darkbrown.css">Dark Brown</a></li>
				<li><a href="#" title="Light Green" class="switch-skin lightgreen" data-skin="<?php echo STYLE_URL?>skins/lightgreen.css">Light Green</a></li>
				<li><a href="#" title="Orange" class="switch-skin orange" data-skin="<?php echo STYLE_URL?>skins/orange.css">Orange</a></li>
				<li><a href="#" title="Red" class="switch-skin red" data-skin="<?php echo STYLE_URL?>skins/red.css">Red</a></li>
				<li><a href="#" title="Teal" class="switch-skin teal" data-skin="<?php echo STYLE_URL?>skins/teal.css">Teal</a></li>
				<li><a href="#" title="Yellow" class="switch-skin yellow" data-skin="<?php echo STYLE_URL?>skins/yellow.css">Yellow</a></li>
			</ul>

			<button type="button" class="switch-skin-full fulldark" data-skin="<?php echo STYLE_URL?>skins/fulldark.css">Full Dark</button>
			<button type="button" class="switch-skin-full fullbright" data-skin="<?php echo STYLE_URL?>skins/fullbright.css">Full Bright</button>
		</section>
		<label class="fancy-checkbox"><input type="checkbox" id="fixed-top-nav"><span>Fixed Top Navigation</span></label>
		<p><a href="#" title="Reset Style" class="del-reset-style">Reset Style</a></p>
	</form>
</div>
