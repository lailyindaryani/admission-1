<!-- content-wrapper -->
<div class="col-md-10 content-wrapper">
<div class="row">
    <div class="col-lg-4 ">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="#">Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layouts</li>
        </ul>
    </div>

</div>

<!-- main -->
<div class="content">
<div class="main-header">
    <h2>Education Data</h2>
    <em>Form Education Data</em>
</div>

<div class="main-content">



<div class="row">
    <div class="col-md-12">
        <!-- SUPPOR TICKET FORM -->
        <div class="widget">
            <div class="widget-header">
                <h3><i class="fa fa-edit"></i> Please complete the form data below</h3>
            </div>
            <div class="widget-content">
            <?php $this->load->view('includes/messages'); ?>
                <div class="wizard-wrapper">
                    <?php $this->load->view('participant/_headerStep', $active); ?>
                    <div class="step-content">
                        <div class="step-pane active" id="step1">
                            <form class="form-horizontal" role="form" id="inputForm" method="post" action="<?php echo base_url()?>participant/educationDataProcess">
                                <fieldset>
								<input type="hidden" name="degree" value="<?php echo $degree['DEGREE']; ?>">
					<?php if($degree['DEGREE']=='S1'){?>
		
                                    <legend>Educational Data</legend>
                                    <div class="form-group">
                                        <label for="ticket-message" class="col-sm-3 control-label">High School</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="h_educationlevel" value="<?php echo $highschool['SCHOOLNAME']; ?>" placeholder="High School">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ticket-message" class="col-sm-3 control-label">From (Year)</label>
                                        <div class="col-sm-3">
                                            <input  type="text" style="" class="date-picker-year form-control" name="h_start" value="<?php echo $highschool['START']; ?>" placeholder="From (Year)">
                                        </div>
                                        <label for="ticket-message" class="col-sm-1 control-label">To</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="date-picker-year form-control" name="h_end" value="<?php echo $highschool['END']; ?>" placeholder="To">
                                        </div>
                                    </div>
			<?php } ?>			
                                    <br><br>
							<?php if($degree['DEGREE']=='S2'){?>

                                    <div class="form-group">
                                        <label for="ticket-message" class="col-sm-3 control-label">University / College (If Any)</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="u_educationlevel" value="<?php echo $university['SCHOOLNAME']; ?>" placeholder="University / College">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ticket-message" class="col-sm-3 control-label">From (Year)</label>
                                        <div class="col-sm-3">
										<?php if($university['START']==0){
											echo '<input type="text" class="date-picker-year form-control" name="u_start" value="" placeholder="From (Year)">';
										}else{
											echo '<input type="text" class="date-picker-year form-control" name="u_start" value="'.$university['START'].'" placeholder="From (Year)">';
										}?>
                                        </div>
                                        <label for="ticket-message" class="col-sm-1 control-label">To</label>
                                        <div class="col-sm-3">
										<?php if($university['END']==0){
											echo '<input type="text" class="date-picker-year form-control" name="u_end" value="" placeholder="To">';
										}else{
											echo '<input type="text" class="date-picker-year form-control" name="u_end" value="'.$university['END'].'" placeholder="To">';
										}?>
                                        </div>   
                                    </div>


                                    <div class="form-group">
                                        <label for="ticket-message" class="col-sm-3 control-label">GPA (Out of 4.0)</label>
                                        <div class="col-sm-3">
                                            <input type="text" style="width:200px;" class="form-control" name="u_degrees" value="<?php echo $university['DEGREES']; ?>" placeholder="GPA">
										 </div>
										 <p></p>
										 <p align="left"><em>/4.0</em></p>				
                                    </div>
									 <?php } ?>
                            </fieldset>
                        </div>
                    </div>

                    <div class="actions">
                        <a href="<?php echo base_url()?>participant/familyData/" type="button" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Prev</a>
                        <button type="submit" class="btn btn-primary btn-next"> Save & Next <i class="fa fa-arrow-right"></i></button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END SUPPORT TICKET FORM -->
    </div>

</div>


</div>
<!-- /main-content -->
</div>
<!-- /main -->
</div>
<!-- /content-wrapper -->

<script type="text/javascript">
    $(document).ready(function(){
		$('.date-picker-year').datepicker({
            format: "yyyy",
			weekStart: 1,
			orientation: "bottom",
			language: "{{ app.request.locale }}",
			keyboardNavigation: false,
			viewMode: "years",
			minViewMode: "years"
        });

        $("#inputForm").validate();
    });
    

</script>
    
<!-- <script src="<?php echo base_url();?>themes/_assets/js/plugins/wizard/wizard.min.js"></script> -->


